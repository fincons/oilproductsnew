#!/bin/bash

ENV=$1
CRON=$2

if  [ $ENV == "PROD" ]
then
#echo 'produzione'
php app/console eop:synchWithEpic --env=prod --download_from_remote=true --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=oilproducts.support@reply.it --elaborate_current=false --max_num_of_archives_to_elaborate=1 >/dev/null 2>&1;
elif [ $CRON == "CRON" ]
then
echo "Run synchWithEpic $(date)"
php /home/oilproducts/www/html/eni-oilproducts/current/app/console eop:synchWithEpic --env=prod --download_from_remote=true --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=a.schifano@reply.it --elaborate_current=false --max_num_of_archives_to_elaborate=1 >/dev/null 2>&1;
else
#echo 'NO produzione'
php app/console eop:synchWithEpic --download_from_remote=true --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=d.tresoldi@reply.it --elaborate_current=false --max_num_of_archives_to_elaborate=1

fi