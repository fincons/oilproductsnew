#!/bin/bash

function permissions() {
  echo "Give permissions ..."
  sudo chmod 777 -R app/cache/
  sudo chmod 777 -R app/logs/
  sudo chmod 777 -R web/uploads/media
}

function clean(){
  echo "Clean ..."
  sudo rm -fr app/cache/dev
  sudo rm -fr app/cache/prod
  sudo rm -fr app/cache/stage
  sudo rm -fr web/css
  sudo rm -fr web/js
  sudo rm -fr web/bundles
  composer install
}

function assetic() {
    echo "Run assetic ..."
    php app/console assetic:dump
}

function loadData() {
    echo "Load data..."
    php bin/load_data.php
}

function updateFromStaticProject() {
    echo "Update from static project..."
    rm -fr src/AppBundle/Resources/public/img
    rm -fr src/AppBundle/Resources/assets/less/carousel.less
    rm -fr src/AppBundle/Resources/assets/less/variables.less
    rm -fr src/AppBundle/Resources/assets/less/main.less
    rm -fr src/AppBundle/Resources/assets/less/responsive.less
    rm -fr src/AppBundle/Resources/assets/less/custom.less
    rm -fr src/AppBundle/Resources/assets/js/custom.js
    cp -r ../eni-oilproducts-static/img/  src/AppBundle/Resources/public/
    cp -r ../eni-oilproducts-static/less/carousel.less src/AppBundle/Resources/assets/less/carousel.less
    cp -r ../eni-oilproducts-static/less/variables.less src/AppBundle/Resources/assets/less/variables.less
    cp -r ../eni-oilproducts-static/less/main.less src/AppBundle/Resources/assets/less/main.less
    cp -r ../eni-oilproducts-static/less/responsive.less src/AppBundle/Resources/assets/less/responsive.less
    cp -r ../eni-oilproducts-static/less/custom.less src/AppBundle/Resources/assets/less/custom.less
    cp -r ../eni-oilproducts-static/js/custom.js  src/AppBundle/Resources/assets/js/custom.js
}

function importTranslations() {
#  ./app/console lexik:translations:import AppBundle
echo ""
}

function exportTranslations() {
#  ./app/console lexik:translations:export
echo ""
}

function release() {
  permissions
  clean
  assetic
  ./app/console doctrine:migrations:migrate
  permissions
}



if [ $# -eq 1 ] && [ $1 = '-clean' ]; then
    clean
elif [ $# -eq 1 ] && [ $1 = '-assetic' ]; then
    clean
    assetic
elif [ $# -eq 1 ] && [ $1 = '-data' ]; then
    loadData
elif [ $# -eq 1 ] && [ $1 = '-sudo' ]; then
    permissions
elif [ $# -eq 1 ] && [ $1 = '-update-from-static' ]; then
    updateFromStaticProject
elif [ $# -eq 1 ] && [ $1 = '-import-translations' ]; then
    importTranslations
elif [ $# -eq 1 ] && [ $1 = '-export-translations' ]; then
    exportTranslations
elif [ $# -eq 1 ] && [ $1 = '-reinstall' ]; then
    permissions
    exportTranslations
    clean
    loadData
    assetic
    #php app/console doctrine:migrations:version 20160508204605 --add --no-interaction
    #php app/console doctrine:migrations:version 20160509180928 --add --no-interaction
    #php app/console doctrine:migrations:version 20160510152806 --add --no-interaction
    #php app/console doctrine:migrations:version 20160511102323 --add --no-interaction
    #php app/console doctrine:migrations:version 20160511103629 --add --no-interaction
    #php app/console doctrine:migrations:version 20160511151216 --add --no-interaction
    #php app/console doctrine:migrations:version 20160526093509 --add --no-interaction
    #php app/console doctrine:migrations:version 20160530144325 --add --no-interaction
    php app/console doctrine:migrations:migrate --no-interaction
    importTranslations
    permissions
elif [ $# -eq 1 ] && [ $1 = '-release' ]; then
    release
else
    echo "Do nothing... See inside script to see options..."
fi
