<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),

        	new Sonata\CoreBundle\SonataCoreBundle(),
        	new Sonata\BlockBundle\SonataBlockBundle(),
        	new Knp\Bundle\MenuBundle\KnpMenuBundle(),
        	new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
        	new Sonata\AdminBundle\SonataAdminBundle(),

        	new FOS\UserBundle\FOSUserBundle(),
        	new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
        	new Application\Sonata\UserBundle\ApplicationSonataUserBundle(),
        	new Sonata\CacheBundle\SonataCacheBundle(),
        	new Sonata\SeoBundle\SonataSeoBundle(),

         	new Sonata\TranslationBundle\SonataTranslationBundle(),

        	new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
        	new Symfony\Bundle\AsseticBundle\AsseticBundle(),

        	new BeSimple\I18nRoutingBundle\BeSimpleI18nRoutingBundle(),

            new Sonata\MediaBundle\SonataMediaBundle(),
            new Sonata\IntlBundle\SonataIntlBundle(),

            // You need to add this dependency to make media functional
            new JMS\SerializerBundle\JMSSerializerBundle(),

            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new Sonata\ClassificationBundle\SonataClassificationBundle(),
            new Application\Sonata\ClassificationBundle\ApplicationSonataClassificationBundle(),

            new Lexik\Bundle\TranslationBundle\LexikTranslationBundle(),
            new Ibrows\SonataTranslationBundle\IbrowsSonataTranslationBundle(),

            new Ddeboer\DataImportBundle\DdeboerDataImportBundle(),

            new EightPoints\Bundle\GuzzleBundle\GuzzleBundle(),

            // Polymorphic collections
            new Infinite\FormBundle\InfiniteFormBundle(),
            
            new FOS\RestBundle\FOSRestBundle(),

            new AppBundle\AppBundle(),

            //Handle Cache headers by route
            new FOS\HttpCacheBundle\FOSHttpCacheBundle(),

            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
