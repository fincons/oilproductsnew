<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161020100534 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    	$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    	$this->addSql('CREATE TABLE product_old LIKE product;');
    	$this->addSql('INSERT product_old SELECT * FROM product;');
    	$this->addSql('CREATE TABLE product_translation_old LIKE product_translation;');
    	$this->addSql('INSERT product_translation_old SELECT * FROM product_translation;');
    	$this->addSql('CREATE TABLE product_localized_old LIKE product_localized;');
    	$this->addSql('INSERT product_localized_old SELECT * FROM product_localized;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    	$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    	$this->addSql('DROP TABLE product_old');
    	$this->addSql('DROP TABLE product_translation_old');
    	$this->addSql('DROP TABLE product_localized_old');
    }
}
