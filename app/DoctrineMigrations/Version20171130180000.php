<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 * creata per aggiungere la colonna LANGUAGE nella tabella dei product_classification_row,area_classification_row
 * per permettere diverse visualizzazioni nelle varie country
 */
class Version20171130180000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE product_classification_row ADD language VARCHAR(45) NOT NULL DEFAULT \'it_IT|en_GB\' ;");
        
        $this->addSql("ALTER TABLE area_classification_row ADD language VARCHAR(45) NOT NULL DEFAULT \'it_IT|en_GB\' ;");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE product_classification_row DROP language;");
        
        $this->addSql("ALTER TABLE area_classification_row DROP language;");
    }
}
