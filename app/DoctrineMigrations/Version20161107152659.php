<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161107152659 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    	$this->addSql('CREATE TABLE product_category_old LIKE product_category;');
    	$this->addSql('INSERT product_category_old SELECT * FROM product_category;');
    	$this->addSql('CREATE TABLE product_category_translation_old LIKE product_category_translation;');
    	$this->addSql('INSERT product_category_translation_old SELECT * FROM product_category_translation;');
    	$this->addSql('CREATE TABLE product_category_localized_old LIKE product_category_localized;');
    	$this->addSql('INSERT product_category_localized_old SELECT * FROM product_category_localized;');    	 
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    	$this->addSql('DROP TABLE product_category_old');
    	$this->addSql('DROP TABLE product_category_translation_old');
    	$this->addSql('DROP TABLE product_category_localized_old');
    }
}
