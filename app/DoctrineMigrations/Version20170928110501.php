<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170928110501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("
        insert INTO product_category (id, brochure_id, description, shortDescription, title, slug, metaKeywords, metaDescription, createdAt, updatedAt, published, contactGroup_id, bannerImage_id, previewImage_id, thumbImage_id, detailImage_id, brochureIcon_id, epicKey, fromEpic, duplicatedOf_id) 
select 268, brochure_id, description, shortDescription, title, slug, metaKeywords, metaDescription, createdAt, updatedAt, published, contactGroup_id, bannerImage_id, previewImage_id, thumbImage_id, detailImage_id, brochureIcon_id, epicKey, fromEpic, duplicatedOf_id from product_category where id = 100;


insert INTO product_category_translation (object_id, locale, field, content) SELECT 268, locale, field, content from product_category_translation WHERE object_id = 100;
");

        $this->addSql("UPDATE enisymfony.product_category_translation
		SET content = '' 
        where field in ( 'shortDescription', 'description' ) 
        AND object_id = 268 ");

        $this->addSql("UPDATE enisymfony.product_category
		SET shortDescription = '', description =''
        where id = 268 ");

        $this->addSql("
        UPDATE product SET code=LPAD('5177',6,0) WHERE code=LPAD('5143',6,0);
UPDATE product SET code=LPAD('5211',6,0) WHERE code=LPAD('0',6,0);
UPDATE product SET code=LPAD('0C1191',6,0) WHERE code=LPAD('fu1191',6,0);
UPDATE product SET code=LPAD('00C151',6,0) WHERE code=LPAD('151',6,0);
UPDATE product SET code=LPAD('0C1311',6,0) WHERE code=LPAD('1311',6,0);
UPDATE product SET code=LPAD('00C391',6,0) WHERE code=LPAD('391bis',6,0);
UPDATE product SET code=LPAD('00C371',6,0) WHERE code=LPAD('371',6,0);
UPDATE product SET code=LPAD('0C1331',6,0) WHERE code=LPAD('1331',6,0);
UPDATE product SET code=LPAD('C391bis',6,0) WHERE code=LPAD('391',6,0);
UPDATE product SET code=LPAD('00C457',6,0) WHERE code=LPAD('457',6,0);
UPDATE product SET code=LPAD('0C1355',6,0) WHERE code=LPAD('1355',6,0);
UPDATE product SET code=LPAD('0C1401',6,0) WHERE code=LPAD('fu1401',6,0);
UPDATE product SET code=LPAD('0C1411',6,0) WHERE code=LPAD('1411',6,0);
UPDATE product SET code=LPAD('00C401',6,0) WHERE code=LPAD('401',6,0);
UPDATE product SET code=LPAD('00C481',6,0) WHERE code=LPAD('481',6,0);
UPDATE product SET code=LPAD('C4211',6,0) WHERE code=LPAD('4211',6,0);
UPDATE product SET code=LPAD('0C4351',6,0) WHERE code=LPAD('4351',6,0);
UPDATE product SET code=LPAD('0C4371',6,0) WHERE code=LPAD('4371',6,0);
UPDATE product SET code=LPAD('0C4431',6,0) WHERE code=LPAD('4431',6,0);
UPDATE product SET code=LPAD('0C4381',6,0) WHERE code=LPAD('4381',6,0);
UPDATE product SET code=LPAD('0C4611',6,0) WHERE code=LPAD('4611',6,0);
UPDATE product SET code=LPAD(' C3391',6,0) WHERE code=LPAD('3391',6,0);
UPDATE product SET code=LPAD('C9040',6,0) WHERE code=LPAD('0N9040',6,0);
UPDATE product SET code=LPAD('0C4111',6,0) WHERE code=LPAD('4111',6,0);
UPDATE product SET code=LPAD('0C6033',6,0) WHERE code=LPAD('6033',6,0);
UPDATE product SET code=LPAD('0C6031',6,0) WHERE code=LPAD('6031',6,0);
UPDATE product SET code=LPAD('00C925',6,0) WHERE code=LPAD('925',6,0);
UPDATE product SET code=LPAD('C221',6,0) WHERE code=LPAD('221',6,0);
UPDATE product SET code=LPAD('C9041',6,0) WHERE code=LPAD('0N9041',6,0);
UPDATE product SET code=LPAD('9011',6,0) WHERE code=LPAD('502003',6,0);
UPDATE product SET code=LPAD('9016',6,0) WHERE code=LPAD('502404',6,0);
UPDATE product SET code=LPAD('9022',6,0) WHERE code=LPAD('502601',6,0);
UPDATE product SET code=LPAD('9025',6,0) WHERE code=LPAD('502701',6,0);
UPDATE product SET code=LPAD('5214',6,0) WHERE code=LPAD('521401',6,0);
UPDATE product SET code=LPAD('5215',6,0) WHERE code=LPAD('521501',6,0);
UPDATE product SET code=LPAD('9017',6,0) WHERE code=LPAD('502301',6,0);
UPDATE product SET code=LPAD('5297',6,0) WHERE code=LPAD('503001',6,0);
UPDATE product SET code=LPAD('9014',6,0) WHERE code=LPAD('427303',6,0);
UPDATE product SET code=LPAD('5016',6,0) WHERE code=LPAD('501601',6,0);
UPDATE product SET code=LPAD('4052',6,0) WHERE code=LPAD('0N9020',6,0);
UPDATE product SET code=LPAD('5114',6,0) WHERE code=LPAD('423007',6,0);
UPDATE product SET code=LPAD('5113',6,0) WHERE code=LPAD('511404',6,0);
UPDATE product SET code=LPAD('5118',6,0) WHERE code=LPAD('511805',6,0);
UPDATE product SET code=LPAD('5147',6,0) WHERE code=LPAD('0N9025',6,0);
UPDATE product SET code=LPAD('9013',6,0) WHERE code=LPAD('0N9026',6,0);
UPDATE product SET code=LPAD('5116',6,0) WHERE code=LPAD('511604',6,0);
UPDATE product SET code=LPAD('4973',6,0) WHERE code=LPAD('497301',6,0);
UPDATE product SET code=LPAD('5157',6,0) WHERE code=LPAD('515702',6,0);
UPDATE product SET code=LPAD('4955',6,0) WHERE code=LPAD('515701',6,0);
UPDATE product SET code=LPAD('4943',6,0) WHERE code=LPAD('465405',6,0);
UPDATE product SET code=LPAD('4925',6,0) WHERE code=LPAD('492501',6,0);
UPDATE product SET code=LPAD('5109',6,0) WHERE code=LPAD('510902',6,0);
UPDATE product SET code=LPAD('5153',6,0) WHERE code=LPAD('415905',6,0);
UPDATE product SET code=LPAD('5302',6,0) WHERE code=LPAD('431304',6,0);
UPDATE product SET code=LPAD('4182',6,0) WHERE code=LPAD('41820',6,0);
UPDATE product SET code=LPAD('1273',6,0) WHERE code=LPAD('1201',6,0);
UPDATE product SET code=LPAD('1290',6,0) WHERE code=LPAD('1271',6,0);
UPDATE product SET code=LPAD('1299',6,0) WHERE code=LPAD('2520',6,0);
UPDATE product SET code=LPAD('7445',6,0) WHERE code=LPAD('1449',6,0);
UPDATE product SET code=LPAD('1030',6,0) WHERE code=LPAD('6183',6,0);
UPDATE product SET code=LPAD('C20900',6,0) WHERE code=LPAD('20900',6,0);
UPDATE product SET code=LPAD('C20520',6,0) WHERE code=LPAD('20520',6,0);
UPDATE product SET code=LPAD('6700',6,0) WHERE code=LPAD('XX6700',6,0);
UPDATE product SET code=LPAD('7275',6,0) WHERE code=LPAD('XX7275',6,0);
UPDATE product SET code=LPAD('7172',6,0) WHERE code=LPAD('XX7172',6,0);
UPDATE product SET code=LPAD('6802',6,0) WHERE code='00-6802';
UPDATE product SET code=LPAD('C20700',6,0) WHERE code=LPAD('20340',6,0);
UPDATE product SET code=LPAD('C20380',6,0) WHERE code=LPAD('20380',6,0);
UPDATE product SET code=LPAD('5654',6,0) WHERE code=LPAD('565403',6,0);
UPDATE product SET code=LPAD('C4141',6,0) WHERE code=LPAD('4141',6,0);
UPDATE product SET code=LPAD('C4131',6,0) WHERE code=LPAD('4121',6,0);
UPDATE product SET code=LPAD('C4151',6,0) WHERE code=LPAD('4151',6,0);
UPDATE product SET code=LPAD('5162',6,0) WHERE code=LPAD('516205',6,0);
UPDATE product SET code=LPAD('5116',6,0) WHERE code=LPAD('51160',6,0);
UPDATE product SET code=LPAD('9013',6,0) WHERE code=LPAD('90130',6,0);
UPDATE product SET code=LPAD('C1913',6,0) WHERE code=LPAD('1913',6,0);
UPDATE product SET code=LPAD('241',6,0) WHERE code=LPAD('241',6,0);
UPDATE product SET code=LPAD('C463',6,0) WHERE code=LPAD('463',6,0);
UPDATE product SET code=LPAD('641',6,0) WHERE code=LPAD('641',6,0);
UPDATE product SET code=LPAD('C1191',6,0) WHERE code=LPAD('3411',6,0);
UPDATE product SET code=LPAD('C101',6,0) WHERE code=LPAD('101',6,0);

UPDATE product SET code=LPAD('5105',6,0) WHERE code=LPAD('510502',6,0);
UPDATE product SET code=LPAD('5006',6,0) WHERE code=LPAD('500601',6,0);
UPDATE product SET code=LPAD('4953',6,0) WHERE code=LPAD('0N9039',6,0);

UPDATE product SET code=LPAD('5136',6,0) WHERE code=LPAD('0N9012',6,0);
UPDATE product SET code=LPAD('5275',6,0) WHERE code=LPAD('52750',6,0);
UPDATE product SET code=LPAD('5276',6,0) WHERE code=LPAD('52760',6,0);
UPDATE product SET code=LPAD('9003',6,0) WHERE code=LPAD('523305',6,0);
        ");

        $this->addSql("
        INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('87', 'en_GB', 'url', '/en_GB/areas/bitumen/paving-grade-bitumen');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('87', 'en_GB', 'label', 'Paving grade bitumen');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('88', 'en_GB', 'url', '/en_GB/areas/bitumen/industrial-bitumen');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('88', 'en_GB', 'label', 'Industrial bitumen');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('89', 'en_GB', 'url', '/en_GB/areas/bitumen');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('89', 'en_GB', 'label', 'Bitumen');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('95', 'en_GB', 'url', '/en_GB/areas/lubricants/automotive-lubricants/lubricants-for-classic-cars');
INSERT INTO link_translation (`object_id`, `locale`, `field`, `content`) VALUES ('95', 'en_GB', 'label', 'Lubricants for classic cars');
        ");

        $this->addSql("

        INSERT INTO area_category_tab_translation (`object_id`, `locale`, `field`, `content`) VALUES ('241', 'en_GB', 'title', 'Automotive Lubricants');
        INSERT INTO area_category_tab_translation (`object_id`, `locale`, `field`, `content`) VALUES ('241', 'en_GB', 'slug', '/en_GB/areas/lubricants/automotive-lubricants');
        INSERT INTO area_category_tab_translation (`object_id`, `locale`, `field`, `content`) VALUES ('243', 'en_GB', 'title', 'Lubricants for classic cars');
        INSERT INTO area_category_tab_translation (`object_id`, `locale`, `field`, `content`) VALUES ('243', 'en_GB', 'slug', 'lubricants-for-classic-cars');
        INSERT INTO area_category_tab_translation (`object_id`, `locale`, `field`, `content`) VALUES ('242', 'en_GB', 'title', 'Lubricants for classic cars');

        
        ");


        $this->addSql("
        UPDATE area_category_block_translation SET content='{\"text\":\"For more detailed information, please contact:\",\"email\":\"marinelubricantssales@eni . com\"}' WHERE id=104;
        UPDATE area_category_block_translation SET content='{\"text\":\"For more detailed information, please contact:\",\"email\":\"marinelubricantssales@eni . com\"}' WHERE id=146;

        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("UPDATE enisymfony.product_category_translation
		SET content = '<p>Some machineries and electrical components require lubricants with insulating characteristics in order to prevent electrical discharges between surfaces at different electrical potentials that are present, for example, inside trasformers. These systems generate a huge amount of heat that lubricants can help to remove thanks to their specific heat and thermal conductivity values.</p>' 
        where field in ( 'shortDescription', 'description' ) 
        AND object_id = 268");

        $this->addSql("UPDATE enisymfony.product_category
		SET shortDescription = '<p>All&rsquo;interno di alcune macchine o componenti elettrici &egrave; necessario l&rsquo;impiego di un prodotto che abbia principalmente caratteristiche isolanti. All&rsquo;interno dei trasformatori, ad esempio, l&rsquo;olio consente di prevenire le scariche elettriche tra superfici a differente potenziale elettrico e tale caratteristica &egrave; definita come rigidit&agrave; dielettrica dell&rsquo;olio. A seguito delle ingenti quantit&agrave; di calore che si sviluppano in tali sistemi, &egrave; altres&igrave; importante che l&rsquo;olio ne favorisca il raffreddamento attraverso adeguati valori di calore specifico e conducibilit&agrave; termica.</p>',
 		description ='<p>All&rsquo;interno di alcune macchine o componenti elettrici &egrave; necessario l&rsquo;impiego di un prodotto che abbia principalmente caratteristiche isolanti. All&rsquo;interno dei trasformatori, ad esempio, l&rsquo;olio consente di prevenire le scariche elettriche tra superfici a differente potenziale elettrico e tale caratteristica &egrave; definita come rigidit&agrave; dielettrica dell&rsquo;olio. A seguito delle ingenti quantit&agrave; di calore che si sviluppano in tali sistemi, &egrave; altres&igrave; importante che l&rsquo;olio ne favorisca il raffreddamento attraverso adeguati valori di calore specifico e conducibilit&agrave; termica.</p>'
        where id = 268 ");

    }
}
