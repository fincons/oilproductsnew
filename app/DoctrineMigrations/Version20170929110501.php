<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170929110501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
      
        
        $this->addSql("
		INSERT INTO `enisymfony`.`product_translation` (`object_id`, `locale`, `field`, `content`)
		VALUES ('525', 'en_GB', 'shortDescription', '(Dispersant tipically used in crankcase engine lubricants)');
		
		UPDATE enisymfony.product_translation set content = '(PPD High performance)'
		where object_id=529 and field = 'shortDescription';
		
		UPDATE enisymfony.product set shortDescription = '(PPD High performance)'
		where id=529 and code='004943';

		");

        $this->addSql(
        "INSERT INTO enisymfony.area_category_block_translation (object_id, locale, field, content)
		VALUES ('263', 'en_GB', 'content', '{\"text\":\"Eni operates in the marine lubricants field, with owned blending plants and/or
		in co-ownership, and/or with processing agreements entrusted to licensors. Eni Marine, for each case and for each specific need,
		produces high performance lubricants, formulated to meet the specifications issued from the makers.<br />\n		
		The refinery in Leghorn is the production plant of base oils. Instead, in Robassomero Plant, Eni makes additives and
		keeps commercial relationships with other international suppliers of additives. The laboratories for the research and the development of the products&rsquo; formulation are set in Milan.<br />\n
		Eni collaborates and holds relationships with the OEM; besides provides technical assistance to the customers through
		the <strong>FAST </strong>(Fast Analysis System) with an analysis service of the oils used on board.<br />\"}');
        
        INSERT INTO enisymfony.product_category_translation (`object_id`, `locale`, `field`, `content`) 
		VALUES ('266', 'en_GB', 'shortDescription', 'Agip Novecento is born, a new Eni lubricant line dedicated to classic cars.<br />\n
		This new line ensures top road performances to all owners and collectors of classic cars, thanks to a lubricants range 
		with specific formulas designed to provide outstanding protection and maximum performances for this special category of cars.');

        INSERT INTO enisymfony.product_category_translation (`object_id`, `locale`, `field`, `content`) 
		VALUES ('266', 'en_GB', 'description', 'Agip Novecento is born, a new Eni lubricant line dedicated to classic cars.<br />\n
		This new line ensures top road performances to all owners and collectors of classic cars, thanks to a lubricants range 
		with specific formulas designed to provide outstanding protection and maximum performances for this special category of cars.');
        		
        UPDATE enisymfony.area_category_tab SET published=0 WHERE id=228;
		
        		");
        
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


    }
}
