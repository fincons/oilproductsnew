<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160508210747 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // some link and page changes
        
        $this->addSql("UPDATE link SET published = 1");
        $this->addSql("INSERT INTO page(routeId, routeParams, title, slug, metaKeywords, metaDescription, createdAt, updatedAt, published, bannerImage_id)
        VALUES('businesssolution_detail', NULL, 'Dettaglio business solution', '#', NULL, NULL, '2016-05-08 21:26:33.0', '2016-05-08 21:26:33.0', true, NULL);");
        $this->addSql("INSERT INTO page(routeId, routeParams, title, slug, metaKeywords, metaDescription, createdAt, updatedAt, published, bannerImage_id)
        VALUES('area_detail', NULL, 'Dettaglio settori', '#', NULL, NULL, '2016-05-08 21:26:33.0', '2016-05-08 21:26:33.0', true, NULL);");
        $this->addSql("INSERT INTO page(routeId, routeParams, title, slug, metaKeywords, metaDescription, createdAt, updatedAt, published, bannerImage_id)
        VALUES('search', NULL, 'Risultati ricerca', '#', NULL, NULL, '2016-05-08 21:26:33.0', '2016-05-08 21:26:33.0', true, NULL);");
        $this->addSql("INSERT INTO link_group (label, type, createdAt, updatedAt, published) VALUES ('Link for Block sliders', 'block_slider_link', '2016-05-09 16:16:19', '2016-05-09 16:16:19', 1)");
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
