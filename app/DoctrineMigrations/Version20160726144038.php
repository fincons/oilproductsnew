<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160726144038 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
      $this->addSql("update area_category_block set content = CONCAT('{\"docs\":[', content, ']}')  where type = 'brochure' and content NOT LIKE '{\"docs\":[%';");
      $this->addSql("update business_solution_block set content = CONCAT('{\"docs\":[', content, ']}')  where type = 'brochure' and content NOT LIKE '{\"docs\":[%';");
      $this->addSql("update block set content = CONCAT('{\"docs\":[', content, ']}')  where type = 'brochure' and content NOT LIKE '{\"docs\":[%';");
    }
      /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    	$this->addSql("update area_category_block set content =  TRIM(LEADING '{\"docs\":[' FROM (TRIM(TRAILING ']}' FROM content))) where type = 'brochure' and content LIKE '{\"docs\":[%';");
    	$this->addSql("update business_solution_block set content =  TRIM(LEADING '{\"docs\":[' FROM (TRIM(TRAILING ']}' FROM content))) where type = 'brochure' and content LIKE '{\"docs\":[%';");
    	$this->addSql("update block set content =  TRIM(LEADING '{\"docs\":[' FROM (TRIM(TRAILING ']}' FROM content))) where type = 'brochure' and content LIKE '{\"docs\":[%';");
    }
}



