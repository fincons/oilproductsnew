<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170803124101 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE epicKey_area_category (id INT AUTO_INCREMENT NOT NULL, epicKey VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, areaCategory_id INT NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE epicKey_product_category (id INT AUTO_INCREMENT NOT NULL, epicKey VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, productCategory_id INT NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE epicKey_area_category ADD CONSTRAINT FK_8A53D5C3E394E7CD FOREIGN KEY (areaCategory_id) REFERENCES area_category (id)');
        $this->addSql('ALTER TABLE epicKey_product_category ADD CONSTRAINT FK_8A53D5C3E394E8CD FOREIGN KEY (productCategory_id) REFERENCES product_category (id)');

        $this->addSql('UPDATE enisymfony.product 
SET code = LPAD(code,6,00)
WHERE id IN (472,
476,
509,
510,
511,
512,
515,
518,
519,
523,
524,
533,
537,
541,
544,
546,
548,
549,
550,
633,
634,
635,
638,
639,
640,
641,
650,
664,
667,
668,
669,
673,
679,
680,
681,
683,
684,
685,
686,
687,
688,
689,
691,
692)');


        $this->addSql("UPDATE `enisymfony`.`product` SET `code`='XX6700' WHERE `id`='638'");
        $this->addSql("UPDATE `enisymfony`.`product` SET `code`='XX7172' WHERE `id`='650'");
        $this->addSql("UPDATE `enisymfony`.`product` SET `code`='XX7275' WHERE `id`='640'");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE epicKey_area_category');
        $this->addSql('DROP TABLE epicKey_product_category');

    }
}
