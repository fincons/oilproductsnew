<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160511102323 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, cylinder TINYINT(1) NOT NULL, littleBarrel TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE office (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, city_id INT NOT NULL, province_id INT NOT NULL, address VARCHAR(255) NOT NULL, cap VARCHAR(5) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, fax VARCHAR(255) NOT NULL, lat VARCHAR(255) NOT NULL, lng VARCHAR(255) NOT NULL, INDEX IDX_74516B02979B1AD6 (company_id), INDEX IDX_74516B028BAC62AF (city_id), INDEX IDX_74516B02E946114A (province_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offices_provinces (office_id INT NOT NULL, province_id INT NOT NULL, INDEX IDX_129F0C3BFFA0C224 (office_id), INDEX IDX_129F0C3BE946114A (province_id), PRIMARY KEY(office_id, province_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE static_province (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE static_city (id INT AUTO_INCREMENT NOT NULL, province_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_B6F55B81E946114A (province_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE office ADD CONSTRAINT FK_74516B02979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE office ADD CONSTRAINT FK_74516B028BAC62AF FOREIGN KEY (city_id) REFERENCES static_city (id)');
        $this->addSql('ALTER TABLE office ADD CONSTRAINT FK_74516B02E946114A FOREIGN KEY (province_id) REFERENCES static_province (id)');
        $this->addSql('ALTER TABLE offices_provinces ADD CONSTRAINT FK_129F0C3BFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
        $this->addSql('ALTER TABLE offices_provinces ADD CONSTRAINT FK_129F0C3BE946114A FOREIGN KEY (province_id) REFERENCES static_province (id)');
        $this->addSql('ALTER TABLE static_city ADD CONSTRAINT FK_B6F55B81E946114A FOREIGN KEY (province_id) REFERENCES static_province (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE office DROP FOREIGN KEY FK_74516B02979B1AD6');
        $this->addSql('ALTER TABLE offices_provinces DROP FOREIGN KEY FK_129F0C3BFFA0C224');
        $this->addSql('ALTER TABLE office DROP FOREIGN KEY FK_74516B02E946114A');
        $this->addSql('ALTER TABLE offices_provinces DROP FOREIGN KEY FK_129F0C3BE946114A');
        $this->addSql('ALTER TABLE static_city DROP FOREIGN KEY FK_B6F55B81E946114A');
        $this->addSql('ALTER TABLE office DROP FOREIGN KEY FK_74516B028BAC62AF');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE office');
        $this->addSql('DROP TABLE offices_provinces');
        $this->addSql('DROP TABLE static_province');
        $this->addSql('DROP TABLE static_city');
    }
}
