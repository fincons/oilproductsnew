<?php

namespace Application\Migrations;

use AppBundle\Entity\AreaCategory;
use AppBundle\Entity\AreaCategoryTab;
use AppBundle\Entity\AreaCategoryTextBlock;
use AppBundle\Entity\AreaClassificationRow;
use AppBundle\Entity\Product;
use AppBundle\Utils\Constants;
use AppBundle\Utils\TextUtils;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use AppBundle\Entity\BusinessSolution;
use AppBundle\Entity\BusinessSolutionTab;
use AppBundle\Entity\BusinessSolutionTextBlock;
use Symfony\Component\Yaml\Parser;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160508214448 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer() {
        return $this->container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {

        // importing area classification rows

        $logger = $this->getContainer()->get('monolog.logger.enioil');
        $fileLocator = $this->getContainer()->get('file_locator');
        $path = $fileLocator->locate('@AppBundle/DataFixtures/data/official_area_classification_rows.csv');

        $file = new \SplFileObject($path);
        $reader = new CsvReader($file, '|');
        $reader->setHeaderRowNumber(0);

        $manager = $this->getContainer()->get('doctrine')->getManager();
        $isMissingCreated = true;
        $this->createAreaPageIfMissing(Constants::EMPTY_CATEGORY_TITLE, null, null, null, null, null, null, $isMissingCreated, $manager);

        $sampleBannerImgCategory = $this->createImage(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/img', 'CATEGORY-BANNER.jpg', "category banner sample", 'product_category');
        $samplePreviewImgCategory = $this->createImage(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/img', 'CATEGORY-PREVIEW.jpg', "category preview sample", 'product_category');
        $sampleThumbImgCategory = $this->createImage(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/img', 'CATEGORY-THUMB.jpg', "category thumb sample", 'product_category');
        $sampleDetailImgCategory = $this->createImage(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/img', 'CATEGORY-DETAIL.jpg', "category detail sample", 'product_category');
        $sampleBrochureCategoryIT = $this->createFile(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/file', 'SAMPLE-BROCHURE-IT.pdf', "sample brochure it", 'product_category');
        $sampleBrochureCategoryEN = $this->createFile(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/file', 'SAMPLE-BROCHURE-EN.pdf', "sample brochure en", 'product_category');
        $sampleBrochureProductIT = $this->createFile(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/file', 'SAMPLE-BROCHURE-IT.pdf', "sample brochure it", 'product');
        $sampleBrochureProductEN = $this->createFile(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/file', 'SAMPLE-BROCHURE-EN.pdf', "sample brochure en", 'product');

        $isFirstLev2Created = false;
        $isFirstLev3Created = false;
        $isFirstLev4Created = false;
        $isFirstLev5Created = false;
        $isFirstLev6Created = false;
        $isFirstProductCreated = false;

        $i=1;
        foreach ($reader as $row) {
            $logger->info('Importing row ' . $i);
            $areaLev2 = $this->createAreaPageIfMissing($row['lev2'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev2Created, $manager);
            $areaLev3 = $this->createAreaPageIfMissing($row['lev3'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev3Created, $manager);
            $areaLev4 = $this->createAreaPageIfMissing($row['lev4'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev4Created, $manager);
            $areaLev5 = $this->createAreaPageIfMissing($row['lev5'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev5Created, $manager);
            $areaLev6 = $this->createAreaPageIfMissing($row['lev6'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev6Created, $manager);
            $product = $this->createProductIfMissing($row['prodcode'], $row['prodname'], $sampleBrochureProductIT, $sampleBrochureProductEN, $isFirstProductCreated, $manager);
            $this->createAreaClassificationRowIfMissing($areaLev2, $areaLev3, $areaLev4, $areaLev5, $areaLev6, $product, $row['position'], $manager);
            $i++;
        }
        }

        private function createAreaPageIfMissing($title, $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureIT, $sampleBrochureEN, &$isFirstCreated, $manager) {
            if (!$title) {
                $title = Constants::EMPTY_CATEGORY_TITLE;
            }
            $page = $manager->getRepository('AppBundle\Entity\AreaCategory')->findOneByTitle($title);

            if (!$page) {
                $page = new AreaCategory();
                $page->setLocale('it_IT');
                $page->setTitle($title);
                $page->setSlug(TextUtils::slugify($title));
                //$page->setRouteId('area_detail');
                if (!$isFirstCreated) {
                    $page->setBannerImage($sampleBannerImgCategory);
                    $page->setPreviewImage($samplePreviewImgCategory);
                    $page->setThumbImage($sampleThumbImgCategory);
                    $page->setDetailImage($sampleDetailImgCategory);
                    $page->setDescription("<p>Descrizione della categoria <strong>" . $title . "</strong>...</p><p>Eni &egrave; da sempre impegnata....</p>");
                    $page->setShortDescription("<p>Descrizione breve della categoria <strong>" . $title . "</strong>...</p>");
                }

                $manager->persist($page);
                $manager->flush();

                if (!$isFirstCreated) {
                    $page = $manager->find('AppBundle\Entity\AreaCategory', $page->getId());

                    $this->loadTabPresentazione($manager, $page, true);
                    $this->loadTabAssistenzaTecnica($manager, $page);

                    $page->setLocale('en_GB');
                    $page->setTitle($title . " EN");
                    $page->setSlug(TextUtils::slugify($page->getTitle()));
                    $page->setDescription("<p>Description of category <strong>" . $title . "</strong>...</p><p>Eni &egrave; da sempre impegnata....</p>");
                    $page->setShortDescription("<p>Short description of category <strong>" . $title . "</strong>...</p>");
                    $manager->persist($page);
                    $manager->flush();

                } else {
                    $this->loadTabPresentazione($manager, $page, true);
                }

                $isFirstCreated = true;

            }

            return $page;
        }

        private function loadTabPresentazione($manager, $page, $rich) {
            // tab
            $tab = new AreaCategoryTab();
            $tab->setPosition(1);
            $tab->setTitle("Presentazione");
            $tab->setSlug(TextUtils::slugify($tab->getTitle()));
            $tab->setLocale('it_IT');
            $tab->setPage($page);
            $manager->persist($tab);
            $manager->flush();
            // text block
            $block = new AreaCategoryTextBlock();
            $block->setText("<p> Contenuto blocco testo 1 presentazione " . $page->getTitle() . " </p>");
            $block->setLocale('it_IT');
            $tab->addBlock($block);
            $manager->persist($block);
            $manager->flush();
        }

        private function loadTabAssistenzaTecnica($manager, $page) {
            // tab
            $tab = new AreaCategoryTab();
            $tab->setPosition(1);
            $tab->setTitle("Assistenza tecnica");
            $tab->setSlug(TextUtils::slugify($tab->getTitle()));
            $tab->setLocale('it_IT');
            $tab->setPage($page);
            $manager->persist($tab);
            $manager->flush();
            // block
            $block = new AreaCategoryTextBlock();
            $block->setText("<p> Contenuto blocco testo 1 assistenza " . $page->getTitle() . " </p>");
            $block->setLocale('it_IT');
            $tab->addBlock($block);
            $manager->persist($block);
            $manager->flush();
        }


        private function createProductIfMissing($code, $name, $sampleBrochureIT, $sampleBrochureEN, &$isFirstCreated, $manager) {
            if (!$code) {
                return null;
            }
            $product = $manager->getRepository('AppBundle:Product')->findOneByCode($code);
            if (!$product) {
                $product = new Product();
                $product->setLocale('it_IT');
                $product->setTitle($name);
                $product->setSlug(TextUtils::slugify($product->getTitle()));
                $product->setCode($code);
                $product->setCountry('IT');
                if (!$isFirstCreated) {
                    $product->setBrochure($sampleBrochureIT);
                }
                $product->setFromAlis(false);
                $manager->persist($product);
                $manager->flush();

                if (!$isFirstCreated) {
                    $product = $manager->find('AppBundle\Entity\Product', $product->getId());
                    $product->setLocale('en_GB');
                    $product->setTitle($name . " EN");
                    $product->setSlug(TextUtils::slugify($product->getTitle()));
                    $product->setBrochure($sampleBrochureEN);
                    $manager->persist($product);
                    $manager->flush();
                }

                $isFirstCreated = true;
            }

            return $product;
        }

        private function createAreaClassificationRowIfMissing($areaLev2, $areaLev3, $areaLev4, $areaLev5, $areaLev6, $product, $position, $manager) {
            $areaClassificationRow = $manager->getRepository('AppBundle\Entity\AreaClassificationRow')->findOneBy(
                array(	'published'=> true,
                    'areaLev2' => $areaLev2,
                    'areaLev3' => $areaLev3,
                    'areaLev4' => $areaLev4,
                    'areaLev5' => $areaLev5,
                    'areaLev6' => $areaLev6,
                    'product' => $product,
                )
                );

            if (!$areaClassificationRow) {
                $areaClassificationRow = new AreaClassificationRow();
                $areaClassificationRow->setAreaLev2($areaLev2);
                $areaClassificationRow->setAreaLev3($areaLev3);
                $areaClassificationRow->setAreaLev4($areaLev4);
                $areaClassificationRow->setAreaLev5($areaLev5);
                $areaClassificationRow->setAreaLev6($areaLev6);
                $areaClassificationRow->setProduct($product);
                $areaClassificationRow->setPosition($position);
                $manager->persist($areaClassificationRow);
                $manager->flush();
            } else {
                //$logger = $this->container->get('monolog.logger.enioil');
                //$logger->info('Row exists...');
            }
        }

        public function createImage($dir, $fileName, $imgTitle, $categoryName) {
            $img = Finder::create()->name($fileName)->in($dir);
            foreach ($img as $file) {
                $mediaManager = $this->getMediaManager();
                $media = $mediaManager->create();
                $media->setBinaryContent($file);
                $media->setEnabled(true);
                $media->setName($imgTitle);
                $media->setDescription("");
                $media->setAuthorName("");
                $media->setCopyright("");
                $categoryManager = $this->getContainer()->get('sonata.classification.manager.category');
                $category = $categoryManager->findOneBy(array(
                    'name' => $categoryName,
                ));
                $media->setCategory($category);
                $mediaManager->save($media, 'default', 'sonata.media.provider.image');
            }
            return $media;
        }

        public function createFile($dir, $fileName, $imgTitle, $categoryName) {
            $img = Finder::create()->name($fileName)->in($dir);
            foreach ($img as $file) {
                $mediaManager = $this->getMediaManager();
                $media = $mediaManager->create();
                $media->setBinaryContent($file);
                $media->setEnabled(true);
                $media->setName($imgTitle);
                $media->setDescription("");
                $media->setAuthorName("");
                $media->setCopyright("");
                $categoryManager = $this->getContainer()->get('sonata.classification.manager.category');
                $category = $categoryManager->findOneBy(array(
                    'name' => $categoryName,
                ));
                $media->setCategory($category);
                $mediaManager->save($media, 'default', 'sonata.media.provider.file');
            }
            return $media;
        }

        public function getMediaManager()
        {
            return $this->getContainer()->get('sonata.media.manager.media');
        }


    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
