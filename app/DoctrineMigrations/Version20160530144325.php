<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160530144325 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE area_category_tab ADD link_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE area_category_tab ADD CONSTRAINT FK_60FA4C0ADA40271 FOREIGN KEY (link_id) REFERENCES link (id)');
        $this->addSql('CREATE INDEX IDX_60FA4C0ADA40271 ON area_category_tab (link_id)');
        $this->addSql('ALTER TABLE tab ADD link_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tab ADD CONSTRAINT FK_73E3430CADA40271 FOREIGN KEY (link_id) REFERENCES link (id)');
        $this->addSql('CREATE INDEX IDX_73E3430CADA40271 ON tab (link_id)');
        $this->addSql('ALTER TABLE business_solution_tab ADD link_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE business_solution_tab ADD CONSTRAINT FK_2FB2DF57ADA40271 FOREIGN KEY (link_id) REFERENCES link (id)');
        $this->addSql('CREATE INDEX IDX_2FB2DF57ADA40271 ON business_solution_tab (link_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE area_category_tab DROP FOREIGN KEY FK_60FA4C0ADA40271');
        $this->addSql('DROP INDEX IDX_60FA4C0ADA40271 ON area_category_tab');
        $this->addSql('ALTER TABLE area_category_tab DROP link_id');
        $this->addSql('ALTER TABLE business_solution_tab DROP FOREIGN KEY FK_2FB2DF57ADA40271');
        $this->addSql('DROP INDEX IDX_2FB2DF57ADA40271 ON business_solution_tab');
        $this->addSql('ALTER TABLE business_solution_tab DROP link_id');
        $this->addSql('ALTER TABLE tab DROP FOREIGN KEY FK_73E3430CADA40271');
        $this->addSql('DROP INDEX IDX_73E3430CADA40271 ON tab');
        $this->addSql('ALTER TABLE tab DROP link_id');
    }
}
