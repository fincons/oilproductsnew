<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160508204605 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // updating schema
        
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE business_solution (id INT AUTO_INCREMENT NOT NULL, description TEXT, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, metaKeywords VARCHAR(255) DEFAULT NULL, metaDescription VARCHAR(255) DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, bannerImage_id INT DEFAULT NULL, previewImage_id INT DEFAULT NULL, INDEX IDX_9703274DE4C68F85 (bannerImage_id), INDEX IDX_9703274D2B41B23D (previewImage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_category_block (id INT AUTO_INCREMENT NOT NULL, tab_id INT NOT NULL, content TINYTEXT NOT NULL COMMENT \'(DC2Type:json)\', template VARCHAR(255) DEFAULT NULL, position INT NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_DA3A0D578D0C9323 (tab_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_solution_block (id INT AUTO_INCREMENT NOT NULL, tab_id INT NOT NULL, content TINYTEXT NOT NULL COMMENT \'(DC2Type:json)\', template VARCHAR(255) DEFAULT NULL, position INT NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_23ED90838D0C9323 (tab_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_category_tab_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_79C5C44C232D562B (object_id), UNIQUE INDEX lookup_unique_block_translation_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_solution_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_AE7C4F2F232D562B (object_id), UNIQUE INDEX lookup_unique_block_translation_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_category_tab (id INT AUTO_INCREMENT NOT NULL, page_id INT NOT NULL, position INT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, metaKeywords VARCHAR(255) DEFAULT NULL, metaDescription VARCHAR(255) DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, INDEX IDX_60FA4C0C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_classification_row (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, position DOUBLE PRECISION NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, areaLev2_id INT NOT NULL, areaLev3_id INT NOT NULL, areaLev4_id INT NOT NULL, areaLev5_id INT NOT NULL, INDEX IDX_7A53D5C36CF671FC (areaLev2_id), INDEX IDX_7A53D5C3D44A1699 (areaLev3_id), INDEX IDX_7A53D5C3499D2E20 (areaLev4_id), INDEX IDX_7A53D5C3F1214945 (areaLev5_id), INDEX IDX_7A53D5C34584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_category_block_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_975BD043232D562B (object_id), UNIQUE INDEX lookup_unique_block_translation_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_solution_block_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_5DFA8372232D562B (object_id), UNIQUE INDEX lookup_unique_block_translation_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_category (id INT AUTO_INCREMENT NOT NULL, brochure_id INT DEFAULT NULL, description TEXT, shortDescription TEXT, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, metaKeywords VARCHAR(255) DEFAULT NULL, metaDescription VARCHAR(255) DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, contactGroup_id INT DEFAULT NULL, bannerImage_id INT DEFAULT NULL, previewImage_id INT DEFAULT NULL, thumbImage_id INT DEFAULT NULL, detailImage_id INT DEFAULT NULL, INDEX IDX_C5434709E9457BED (contactGroup_id), INDEX IDX_C5434709E4C68F85 (bannerImage_id), INDEX IDX_C54347092B41B23D (previewImage_id), INDEX IDX_C5434709236F0A67 (thumbImage_id), INDEX IDX_C5434709D7542693 (detailImage_id), INDEX IDX_C5434709B96114D1 (brochure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_category_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_FD33BC8232D562B (object_id), UNIQUE INDEX lookup_unique_block_translation_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_solution_tab_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_E9DB4E19232D562B (object_id), UNIQUE INDEX lookup_unique_block_translation_idx (locale, object_id, field), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_solution_tab (id INT AUTO_INCREMENT NOT NULL, page_id INT NOT NULL, position INT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, metaKeywords VARCHAR(255) DEFAULT NULL, metaDescription VARCHAR(255) DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, published TINYINT(1) NOT NULL, INDEX IDX_2FB2DF57C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE business_solution ADD CONSTRAINT FK_9703274DE4C68F85 FOREIGN KEY (bannerImage_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE business_solution ADD CONSTRAINT FK_9703274D2B41B23D FOREIGN KEY (previewImage_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE area_category_block ADD CONSTRAINT FK_DA3A0D578D0C9323 FOREIGN KEY (tab_id) REFERENCES area_category_tab (id)');
        $this->addSql('ALTER TABLE business_solution_block ADD CONSTRAINT FK_23ED90838D0C9323 FOREIGN KEY (tab_id) REFERENCES business_solution_tab (id)');
        $this->addSql('ALTER TABLE area_category_tab_translation ADD CONSTRAINT FK_79C5C44C232D562B FOREIGN KEY (object_id) REFERENCES area_category_tab (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE business_solution_translation ADD CONSTRAINT FK_AE7C4F2F232D562B FOREIGN KEY (object_id) REFERENCES business_solution (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE area_category_tab ADD CONSTRAINT FK_60FA4C0C4663E4 FOREIGN KEY (page_id) REFERENCES area_category (id)');
        $this->addSql('ALTER TABLE area_classification_row ADD CONSTRAINT FK_7A53D5C36CF671FC FOREIGN KEY (areaLev2_id) REFERENCES area_category (id)');
        $this->addSql('ALTER TABLE area_classification_row ADD CONSTRAINT FK_7A53D5C3D44A1699 FOREIGN KEY (areaLev3_id) REFERENCES area_category (id)');
        $this->addSql('ALTER TABLE area_classification_row ADD CONSTRAINT FK_7A53D5C3499D2E20 FOREIGN KEY (areaLev4_id) REFERENCES area_category (id)');
        $this->addSql('ALTER TABLE area_classification_row ADD CONSTRAINT FK_7A53D5C3F1214945 FOREIGN KEY (areaLev5_id) REFERENCES area_category (id)');
        $this->addSql('ALTER TABLE area_classification_row ADD CONSTRAINT FK_7A53D5C34584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE area_category_block_translation ADD CONSTRAINT FK_975BD043232D562B FOREIGN KEY (object_id) REFERENCES area_category_block (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE business_solution_block_translation ADD CONSTRAINT FK_5DFA8372232D562B FOREIGN KEY (object_id) REFERENCES business_solution_block (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE area_category ADD CONSTRAINT FK_C5434709E9457BED FOREIGN KEY (contactGroup_id) REFERENCES contact_group (id)');
        $this->addSql('ALTER TABLE area_category ADD CONSTRAINT FK_C5434709E4C68F85 FOREIGN KEY (bannerImage_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE area_category ADD CONSTRAINT FK_C54347092B41B23D FOREIGN KEY (previewImage_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE area_category ADD CONSTRAINT FK_C5434709236F0A67 FOREIGN KEY (thumbImage_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE area_category ADD CONSTRAINT FK_C5434709D7542693 FOREIGN KEY (detailImage_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE area_category ADD CONSTRAINT FK_C5434709B96114D1 FOREIGN KEY (brochure_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE area_category_translation ADD CONSTRAINT FK_FD33BC8232D562B FOREIGN KEY (object_id) REFERENCES area_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE business_solution_tab_translation ADD CONSTRAINT FK_E9DB4E19232D562B FOREIGN KEY (object_id) REFERENCES business_solution_tab (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE business_solution_tab ADD CONSTRAINT FK_2FB2DF57C4663E4 FOREIGN KEY (page_id) REFERENCES business_solution (id)');
        $this->addSql('ALTER TABLE block ADD position INT NOT NULL');
        $this->addSql('ALTER TABLE post CHANGE highlight highlight TINYINT(1) NOT NULL, CHANGE showInHomePage showInHomePage TINYINT(1) NOT NULL');
        
        $this->addSql('ALTER TABLE area_classification_row ADD areaLev6_id INT NOT NULL');
        $this->addSql('ALTER TABLE area_classification_row ADD CONSTRAINT FK_7A53D5C3E394E6AB FOREIGN KEY (areaLev6_id) REFERENCES area_category (id)');
        $this->addSql('CREATE INDEX IDX_7A53D5C3E394E6AB ON area_classification_row (areaLev6_id)');
        
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE business_solution_translation DROP FOREIGN KEY FK_AE7C4F2F232D562B');
        $this->addSql('ALTER TABLE business_solution_tab DROP FOREIGN KEY FK_2FB2DF57C4663E4');
        $this->addSql('ALTER TABLE area_category_block_translation DROP FOREIGN KEY FK_975BD043232D562B');
        $this->addSql('ALTER TABLE business_solution_block_translation DROP FOREIGN KEY FK_5DFA8372232D562B');
        $this->addSql('ALTER TABLE area_category_block DROP FOREIGN KEY FK_DA3A0D578D0C9323');
        $this->addSql('ALTER TABLE area_category_tab_translation DROP FOREIGN KEY FK_79C5C44C232D562B');
        $this->addSql('ALTER TABLE area_category_tab DROP FOREIGN KEY FK_60FA4C0C4663E4');
        $this->addSql('ALTER TABLE area_classification_row DROP FOREIGN KEY FK_7A53D5C36CF671FC');
        $this->addSql('ALTER TABLE area_classification_row DROP FOREIGN KEY FK_7A53D5C3D44A1699');
        $this->addSql('ALTER TABLE area_classification_row DROP FOREIGN KEY FK_7A53D5C3499D2E20');
        $this->addSql('ALTER TABLE area_classification_row DROP FOREIGN KEY FK_7A53D5C3F1214945');
        $this->addSql('ALTER TABLE area_category_translation DROP FOREIGN KEY FK_FD33BC8232D562B');
        $this->addSql('ALTER TABLE business_solution_block DROP FOREIGN KEY FK_23ED90838D0C9323');
        $this->addSql('ALTER TABLE business_solution_tab_translation DROP FOREIGN KEY FK_E9DB4E19232D562B');
        $this->addSql('DROP TABLE business_solution');
        $this->addSql('DROP TABLE area_category_block');
        $this->addSql('DROP TABLE business_solution_block');
        $this->addSql('DROP TABLE area_category_tab_translation');
        $this->addSql('DROP TABLE business_solution_translation');
        $this->addSql('DROP TABLE area_category_tab');
        $this->addSql('DROP TABLE area_classification_row');
        $this->addSql('DROP TABLE area_category_block_translation');
        $this->addSql('DROP TABLE business_solution_block_translation');
        $this->addSql('DROP TABLE area_category');
        $this->addSql('DROP TABLE area_category_translation');
        $this->addSql('DROP TABLE business_solution_tab_translation');
        $this->addSql('DROP TABLE business_solution_tab');
        $this->addSql('ALTER TABLE block DROP position');
        $this->addSql('ALTER TABLE post CHANGE highlight highlight TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE showInHomePage showInHomePage TINYINT(1) DEFAULT \'0\' NOT NULL');
        
        $this->addSql('ALTER TABLE area_classification_row DROP FOREIGN KEY FK_7A53D5C3E394E6AB');
        $this->addSql('DROP INDEX IDX_7A53D5C3E394E6AB ON area_classification_row');
        $this->addSql('ALTER TABLE area_classification_row DROP areaLev6_id');
    }
}
