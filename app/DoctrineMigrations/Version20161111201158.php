<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161111201158 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE product_category ADD duplicatedOf_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC735683BFEB6A FOREIGN KEY (duplicatedOf_id) REFERENCES product_category (id)');
        $this->addSql('CREATE INDEX IDX_CDFC735683BFEB6A ON product_category (duplicatedOf_id)');
        //$this->addSql('ALTER TABLE product_category_translation ADD CONSTRAINT FK_1DAAB487232D562B FOREIGN KEY (object_id) REFERENCES product_category (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC735683BFEB6A');
        $this->addSql('DROP INDEX IDX_CDFC735683BFEB6A ON product_category');
        $this->addSql('ALTER TABLE product_category DROP duplicatedOf_id');
        $this->addSql('ALTER TABLE product_category_translation DROP FOREIGN KEY FK_1DAAB487232D562B');
    }
}
