<?php

namespace Application\Migrations;

use AppBundle\Entity\BusinessSolution;
use AppBundle\Entity\BusinessSolutionTab;
use AppBundle\Entity\BusinessSolutionTextBlock;
use AppBundle\Utils\TextUtils;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160508212925 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getContainer() {
        return $this->container;
    }
    
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // importing business solutions sample
        
        $manager = $this->getContainer()->get('doctrine')->getManager();
        $yamlParser = new Parser();
        $fileLocator = $this->getContainer()->get('file_locator');
        
        $path = $fileLocator->locate('@AppBundle/DataFixtures/data/businesssolutions.yml');
        $datas = $yamlParser->parse(file_get_contents($path));
        $i = 0;
        foreach ($datas as $data) {
             
            $i++;
             
            $businessSolution = new BusinessSolution();
            $businessSolution->setPublished($data['it']['published']);
            $businessSolution->setTitle($data['it']['title']);
            $businessSolution->setSlug(TextUtils::slugify($businessSolution->getTitle()));
            $businessSolution->setDescription($data['it']['description']);
            $businessSolution->setLocale('it_IT');
        
            if (array_key_exists('previewImage', $data['it'])) {
                $img = $this->createImage(__DIR__.'/../../src/AppBundle/DataFixtures/data/media/img', $data['it']['previewImage'], "post preview " . $i, 'post');
                $businessSolution->setPreviewImage($img);
            }
        
            $manager->persist($businessSolution);
            $manager->flush();
        
            $this->loadTab($manager, $businessSolution, "Presentazione");
            $this->loadTab($manager, $businessSolution, "Assistenza Tecnica");
        
            $businessSolution = $manager->find('AppBundle\Entity\BusinessSolution', $businessSolution->getId());
            $businessSolution->setTitle($data['en']['title']);
            $businessSolution->setSlug(TextUtils::slugify($businessSolution->getTitle()));
            $businessSolution->setDescription($data['en']['description']);
            $businessSolution->setLocale('en_GB');
            $manager->persist($businessSolution);
            $manager->flush();
        
        }
    }
    
    
    private function loadTab($manager, $businessSolution, $tabTitle) {
        // tab
        $tab = new BusinessSolutionTab();
        $tab->setPosition(1);
        $tab->setTitle($tabTitle);
        $tab->setSlug(TextUtils::slugify($tab->getTitle()));
        $tab->setLocale('it_IT');
        $tab->setPage($businessSolution);
        $manager->persist($tab);
        $manager->flush();
        // text block
        $block = new BusinessSolutionTextBlock();
        $block->setText("<p> Contenuto blocco testo " . $tabTitle . "1 </p>");
        $block->setLocale('it_IT');
        $tab->addBlock($block);
        $manager->persist($block);
        $manager->flush();
    }
    
    public function createImage($dir, $fileName, $imgTitle, $categoryName) {
        $img = Finder::create()->name($fileName)->in($dir);
        foreach ($img as $file) {
            $mediaManager = $this->getMediaManager();
            $media = $mediaManager->create();
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            $media->setName($imgTitle);
            $media->setDescription("");
            $media->setAuthorName("");
            $media->setCopyright("");
            $categoryManager = $this->getContainer()->get('sonata.classification.manager.category');
            $category = $categoryManager->findOneBy(array(
                'name' => $categoryName,
            ));
            $media->setCategory($category);
            $mediaManager->save($media, 'default', 'sonata.media.provider.image');
        }
        return $media;
    }
    
    public function createFile($dir, $fileName, $imgTitle, $categoryName) {
        $img = Finder::create()->name($fileName)->in($dir);
        foreach ($img as $file) {
            $mediaManager = $this->getMediaManager();
            $media = $mediaManager->create();
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            $media->setName($imgTitle);
            $media->setDescription("");
            $media->setAuthorName("");
            $media->setCopyright("");
            $categoryManager = $this->getContainer()->get('sonata.classification.manager.category');
            $category = $categoryManager->findOneBy(array(
                'name' => $categoryName,
            ));
            $media->setCategory($category);
            $mediaManager->save($media, 'default', 'sonata.media.provider.file');
        }
        return $media;
    }
    
    public function getMediaManager()
    {
        return $this->getContainer()->get('sonata.media.manager.media');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
