<?php

$uri = '/en_GB/';
$countryCode = "";

if(isset($_POST["lang"])){
	
	$language = $_POST["lang"];
	
	if(isset($_POST["ip"]) && $_POST["ip"] != ""){
	    $json = file_get_contents('https://www.iplocate.io/api/lookup/'.$_POST["ip"].'/');
		if(isset($json)){
			$geoip = json_decode($json);
			$countryCode = $geoip->country_code;
		}
	}
	
	
	if($countryCode == "IT" || $language == "it"){
		if($language == "en"){
			$uri = "/en_GB/";
		}
		else{
			$uri = "/it_IT/";
		}
	}
			
	if($countryCode == "GB"){
		$uri = "/en_GB/";
	}
			
	if($countryCode == "US"){
		$uri = "/en_GB/";
	}
			
	if($countryCode == "DE" || $language == "de"){
		if($language == "en"){
			$uri = "/en_DE/";
		}
		else{
			$uri = "/de_DE/";
		}
	}
	
	if($countryCode == "BE" || $countryCode == "LU" || $countryCode == "NL" || $countryCode == "FR" || $language == "fr" || $language == "nl"){
		if($language == "nl"){
		  $uri = "/nl_BX/";
		}
		else{
		  if($language == "fr"){
			  $uri = "/fr_BX/";
			}
			else{
			  $uri = "/en_BX/";
			}
		}
	}
	
	if($countryCode == "BD" || $countryCode == "MM" || $countryCode == "LA" || $countryCode == "TH" || $countryCode == "KH" || $countryCode == "VN" || $countryCode == "MY" || $countryCode == "SG" || $countryCode == "TW" || $countryCode == "PH" || $countryCode == "KP" || $countryCode == "KR" || $countryCode == "JP" || $countryCode == "BN" || $countryCode == "NZ" || $countryCode == "AU" || $countryCode == "TH" || $countryCode == "VN" || $countryCode == "MY" || $countryCode == "SG" || $countryCode == "BN" || $countryCode == "TW" || $countryCode == "JP" || $countryCode == "KP" || $countryCode == "KR" || $countryCode == "PH" || $countryCode == "KH" || $language == "th" || $language == "vi" || $language == "ms" || $language == "zh" || $language == "ja" || $language == "ko" || $language == "tl" || $language == "km"){
        $uri = "/en_APAC/";
	}
	
	if($countryCode == "ES" || $language == "es"){
        $uri = "/es_ES/";
	}
	
	if($countryCode == "PT" || $language == "pt"){
        $uri = "/pt_PT/";
	}
			
	header("location: ".$uri);
}


	
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	
	<form id="iplang" method="post" hidden>
		<input id="ip" type="text" name="ip" value="" />
		<input id="lang" type="text" name="lang" value="" />
	</form>

	<script type="application/javascript">
		var userip = "";

		function getIP(json) {
			var userip = json.ip;
			document.getElementById('ip').value = userip;
			document.getElementById('lang').value = String(navigator.language.substring(0, 2));
			//if(!document.getElementById('ip').value == "" && !document.getElementById('lang').value == ""){
				document.forms["iplang"].submit();
			//}
		}
		
		function getLanguage() {
			document.getElementById('lang').value = String(navigator.language.substring(0, 2));
			document.forms["iplang"].submit();
		}
		
	</script>

	<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP" onerror="getLanguage();" ></script>

	<body>
	</body>
</html>

