<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class DashboardGroupsEvent extends Event
{
    const EVENT = 'enioil.sonatacustomization.event.dashboardgroups';

    /**
     * @var array
     */
    protected $groups = array();

    public function getGroups() {
        return $this->groups;
    }

    public function setGroups($groups = array()) {
        $this->groups = $groups;
        return $this;
    }

    public function addGroup($name, $items = array(), $config = array()) {
        if (count($items) == 0) {
            throw new \InvalidArgumentException('Custom DashboardGroup must have at least one item');
        }

        if (isset($this->groups[$name])) {
            $items = array_merge($this->groups[$name]['items'], $items);
            $this->groups[$name]['items'] = $items;
        }
        else {
            $roles = array();
            foreach ($items as $item) {
                $roles = array_merge($roles, $item['roles']);
            }

            $default = array(
                'label' => $name,
                'roles' => $roles,
            );
            $group = array_merge($default, $config);
            $group['items'] = $items;
            $this->groups[$name] = $group;
        }

        return $this->groups[$name];
    }
}
