<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class created by CZA to replicate $translations = $repository->findTranslations($object)
 * method for translatable classes, not working with Gedmo\AbstractPersonalTranslatable :-(
 */
abstract class AbstractPersonalTranslationRepository extends EntityRepository
{
    // METHOD USEFUL TO CALCULATE LOCALE SWITCHER ROWS FOR ADVANCED LANGUAGE SWITCHER ...
    // TODO: improve with dinamic calculation of allowed languages
    public function findSlugTranslations($entityId, $slug)
    {

        $parameters = array();
        $parameters['entityId'] = $entityId;

        $query = $this->createQueryBuilder('trans')
            ->select('trans, entity')
            //->andWhere("trans.field = 'slug'")
            ->join('trans.object', 'entity')
            ->where('trans.object = :entityId')
            ->setParameters($parameters)
            ->getQuery();

        $data = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $data_lang = $this->findLangDistinct();
        $translations = array();
        //var_dump($entityId);
        //var_dump($slug);
        //\Doctrine\Common\Util\Debug::dump($query_it->getSql(),100000);
        //var_dump($data_lang);

        if ($data && is_array($data) && count($data)) {
            foreach ($data as $row) {
                $translations[$row['locale']][$row['field']] = $row['content'];
                $translations['it_IT']['slug'] = $row['object']['slug'];
            }
        } else {
            // TODO: for each language....
            foreach ($data_lang as $row) {
                $translations[$row['locale']]['slug'] = $slug;
            }
            //$translations['en_GB']['slug'] = $slug;
            $translations['it_IT']['slug'] = $slug;
        }

        return $translations;
    }

    public function findLangDistinct()
    {

        $query_lang = $this->createQueryBuilder('trans')
            ->select('trans')
            ->distinct()
            ->getQuery();

        return $query_lang->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

    }
}