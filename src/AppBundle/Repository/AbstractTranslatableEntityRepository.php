<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

abstract class AbstractTranslatableEntityRepository extends EntityRepository
{

    public function findOneByTranslatableField($fieldName, $fieldValue)
    {
        $parameters = array();
        $parameters[$fieldName] = $fieldValue;
        $query = $this->createQueryBuilder('t')
            ->andWhere("t." . $fieldName . " = :" . $fieldName)
            ->setParameters($parameters)
            ->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $results = $query->getResult();
        if (!$results) {
            return null;
        } else {
            return reset($results);
        }
    }
}
