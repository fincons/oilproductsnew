<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class AreaClassificationRowRepository extends AbstractTranslatableEntityRepository
{

    public function findDistinctAreaCategoriesOfLev2($current_locale = "")
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.areaLev2 = pc')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findDistinctAreaCategoriesOfLev3($current_locale = "",$catLev2)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.areaLev3 = pc')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.areaLev2 = :catLev2')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findDistinctAreaCategoriesOfLev4($current_locale = "",$catLev2, $catLev3)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.areaLev4 = pc')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.areaLev2 = :catLev2')
            ->andWhere('pcr.areaLev3 = :catLev3')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }


    public function findDistinctAreaCategoriesOfLev5($current_locale = "",$catLev2, $catLev3, $catLev4)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.areaLev5 = pc')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.areaLev2 = :catLev2')
            ->andWhere('pcr.areaLev3 = :catLev3')
            ->andWhere('pcr.areaLev4 = :catLev4')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findDistinctAreaCategoriesOfLev6($current_locale = "",$catLev2, $catLev3, $catLev4, $catLev5)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.areaLev6 = pc')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.areaLev2 = :catLev2')
            ->andWhere('pcr.areaLev3 = :catLev3')
            ->andWhere('pcr.areaLev4 = :catLev4')
            ->andWhere('pcr.areaLev5 = :catLev5')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->setParameter('catLev5', $catLev5)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }


    public function findDistinctProducts($current_locale = "",$catLev2, $catLev3, $catLev4, $catLev5, $catLev6)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->select('p')
            ->where('pcr.product = p')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.areaLev2 = :catLev2')
            ->andWhere('pcr.areaLev3 = :catLev3')
            ->andWhere('pcr.areaLev4 = :catLev4')
            ->andWhere('pcr.areaLev5 = :catLev5')
            ->andWhere('pcr.areaLev6 = :catLev6')
            ->orderBy('pcr.position, p.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->setParameter('catLev5', $catLev5)
            ->setParameter('catLev6', $catLev6)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }


    public function findAllByCategoriesAndProduct($catLev2, $catLev3, $catLev4, $catLev5, $catLev6, $product)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaClassificationRow', 'acr')
            ->select('acr')
            ->where('acr.areaLev2 = :catLev2')
            ->andWhere('acr.areaLev3 = :catLev3')
            ->andWhere('acr.areaLev4 = :catLev4')
            ->andWhere('acr.areaLev5 = :catLev5')
            ->andWhere('acr.areaLev6 = :catLev6')
            ->andWhere('acr.product = :product')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->setParameter('catLev5', $catLev5)
            ->setParameter('catLev6', $catLev6)
            ->setParameter('product', $product)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }
    
    public function findDistinctAreaCategoriesLanguages()
    {
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\AreaClassificationRow', 'pcr')
        ->select('pcr.language')
        ->distinct()
        ->getQuery();
        
        $languages = $query->getResult();
        
        return $languages;
        /*
         * restituisce tuttle le areaclassificationrow
        $query_lang = $this->createQueryBuilder('trans')
        ->select('trans')
        ->distinct()
        ->getQuery();
        
        return $query_lang->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        */
    }
    
    public function findSlugTranslationsByTitleCategories($title, $current_locale = "")
    {    
        $slugTranslations = "";
        
        if($current_locale == 'it_IT'){
          
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaCategoryTranslation', 'act')
            ->from('AppBundle\Entity\AreaCategory', 'ac')
            ->select('act.locale','act.content','ac.slug')
            ->where('ac.title = \''.$title.'\' ')
            ->andWhere('act.field = \'slug\' ')
            ->andWhere('ac.id = act.object_id')
            ->groupBy('act.locale')
            ->getQuery();
            
            $slugTranslations = $query->getResult();
        }
        else{
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\AreaCategoryTranslation', 'act')
            ->from('AppBundle\Entity\AreaCategory', 'ac')
            ->select('ac.title')
            ->where('act.content = \''.$title.'\' ')
            ->andWhere('act.field = \'title\' ')
            ->andWhere('ac.id = act.object_id')
            ->setMaxResults(1)
            ->getQuery();
            
            $titleIta = $query->getResult();
            
            if(isset($titleIta[0])){
                $titleIta = $titleIta[0]['title'];
                
                $query = $this->getEntityManager()
                ->createQueryBuilder()
                ->from('AppBundle\Entity\AreaCategoryTranslation', 'act')
                ->from('AppBundle\Entity\AreaCategory', 'ac')
                ->select('act.locale','act.content','ac.slug')
                ->where('ac.title = \''.$titleIta.'\' ')
                ->andWhere('act.field = \'slug\' ')            
                ->andWhere('ac.id = act.object_id')
                ->groupBy('act.locale')
                ->getQuery();
                
                $slugTranslations = $query->getResult();
            }
        }
        
        
        return $slugTranslations;
    }
    
    
    public function findNameAreaCategoryTranslations()
    {
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\PageTranslation', 'pgt')
        ->from('AppBundle\Entity\Page', 'pg')
        ->select('pgt.locale','pgt.content','pg.title')
        ->where('pg.routeId = \'area_list\' ')
        ->andWhere('pgt.field = \'title\' ')
        ->andWhere('pg.id = pgt.object_id')
        ->getQuery();
            
        $nameAreaTranslations = $query->getResult();        
        
        return $nameAreaTranslations;
    }


}
