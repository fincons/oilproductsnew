<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EpicArchiveRepository extends EntityRepository
{

    public function findLastEpicArchiveElaboratedByType($type)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where($qb->expr()->isNotNull('e.' . $type . 'ElaborationAt'))
            ->orderBy('e.' . $type . 'ElaborationAt', 'DESC')
            ->setMaxResults(1);
        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $ex) {
            return null;
        }
    }


    public function findEpicArchivesToElaborate($maxResults, $is_to_elaborate_products, $is_to_elaborate_product_classification, $is_to_elaborate_area_classification)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where($qb->expr()->eq('1', '2'));
        if ($is_to_elaborate_products) {
            $qb->orWhere($qb->expr()->isNull('e.productElaborationAt'));
        }
        if ($is_to_elaborate_product_classification) {
            $qb->orWhere($qb->expr()->isNull('e.productClassificationElaborationAt'));
        }
        if ($is_to_elaborate_area_classification) {
            $qb->orWhere($qb->expr()->isNull('e.areaClassificationElaborationAt'));
        }
        if ($maxResults > -1) {
            $qb->setMaxResults($maxResults);
        }
        $qb->orderBy('e.archiveName', 'ASC');
        return $qb->getQuery()->getResult();
    }


}
