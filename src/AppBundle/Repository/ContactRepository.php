<?php

namespace AppBundle\Repository;


use AppBundle\Entity\ContactGroup;

class ContactRepository extends AbstractTranslatableEntityRepository
{
    /**
     * @param ContactGroup $cg
     * @return mixed
     */
    public function findBySlugOrderBy(ContactGroup $cg)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\Contact', 'c')
            ->from('AppBundle\Entity\ContactGroup', 'cg')
            ->from('AppBundle\Entity\ContactSubGroup', 'csg')
            ->select('c')
            ->where('c.contactGroup = cg')
            ->andWhere('c.contactSubGroup = csg')
            ->andWhere('cg.id = :cgId')
            ->orderBy('csg.position', 'ASC')
            ->addOrderBy('c.title', 'ASC')
            ->addOrderBy('c.position', 'ASC')
            ->setParameter('cgId', $cg->getId())
            ->getQuery();

        $contact = $query->getResult();

        return $contact;
    }
}