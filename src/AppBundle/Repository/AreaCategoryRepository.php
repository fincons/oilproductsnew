<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class AreaCategoryRepository extends AbstractTranslatableEntityRepository
{
    
    public function findAllSlugAreaCategory()
    {
        
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\AreaCategory', 'ac')
        ->select('ac.slug')
        ->where('ac.slug != \'empty\' ')
        ->distinct()
        ->getQuery();
        $acSlugs = $query->getResult();
        
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\AreaCategoryTranslation', 'act')
        ->select('act.content')
        ->where('act.field = \'slug\' ')
        ->distinct()
        ->getQuery();
        $actSlugs = $query->getResult();
        
        
        $slugs =array();
        
        foreach($actSlugs as $actSlug) {
            $slugs[] = $actSlug['content'];
        }
        
        foreach($acSlugs as $acSlug) {
            if(!in_array($acSlug['slug'], $slugs)){
                $slugs[] = $acSlug['slug'];
            }
        }
        
        
        return $slugs;
        
    }
    
    
}
