<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class ProductRepository extends AbstractTranslatableEntityRepository
{
    public function findOlderProducts($codes)
    {
        $qb = $this->createQueryBuilder('p');

        $qb
            ->where($qb->expr()->notIn('p.code', ':codes'))
            ->setParameter('codes', $codes)
            ->orderBy('p.code', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }
    
    
    
    public function findAllSlugProducts()
    {
        
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\Product', 'p')
        ->select('p.slug')
        ->where('p.slug != \'empty\' ')
        ->distinct()
        ->getQuery();
        $pSlugs = $query->getResult();
        
        $slugs =array();
        
        foreach($pSlugs as $pSlug) {
            $slugs[] = $pSlug['slug'];
        }
        
        return $slugs ;
        
    }
    
    
    
}
