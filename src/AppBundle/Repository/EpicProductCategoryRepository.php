<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EpicProductCategoryRepository extends EntityRepository
{


    public function findDistinctAreaCategoriesOfAreaCategory()
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\EpicProductCategory', 'eac')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.productCategoryid = pc')
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findAllByCategory($cat)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\EpicProductCategory', 'epc')
            ->select('epc')
            ->where('epc.productCategoryid = :cat')
            ->setParameter('cat', $cat)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findAllByEpicKey($epicKey)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\EpicProductCategory', 'epc')
            ->select('epc')
            ->where('epc.epicKey = :epicKey')
            ->setParameter('epicKey', $epicKey)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

}
