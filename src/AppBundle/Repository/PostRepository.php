<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class PostRepository extends AbstractTranslatableEntityRepository
{
    public function findOneByTranslatableField($fieldName, $fieldValue, $current_locale="")
    {
        $parameters = array();
        $parameters[$fieldName] = $fieldValue;
        if($current_locale){
            $query = $this->createQueryBuilder('t')
            ->andWhere("t." . $fieldName . " = :" . $fieldName)
            ->andWhere('t.language LIKE \'%'.$current_locale.'%\' ')
            ->setParameters($parameters)
            ->getQuery();
        }
        else{
            $query = $this->createQueryBuilder('t')
            ->andWhere("t." . $fieldName . " = :" . $fieldName)
            ->setParameters($parameters)
            ->getQuery();
        }
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $results = $query->getResult();
        if (!$results) {
            return null;
        } else {
            return reset($results);
        }
    }
    
}