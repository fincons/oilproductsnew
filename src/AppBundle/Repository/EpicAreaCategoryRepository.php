<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Repository\AbstractTranslatableEntityRepository;

class EpicAreaCategoryRepository extends AbstractTranslatableEntityRepository
{


    public function findDistinctAreaCategoriesOfAreaCategory()
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\EpicAreaCategory', 'eac')
            ->from('AppBundle\Entity\AreaCategory', 'pc')
            ->select('pc')
            ->where('pcr.areaCategoryid = pc')
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findAllByCategory($cat)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\EpicAreaCategory', 'epc')
            ->select('epc')
            ->where('epc.areaCategoryid = :cat')
            ->setParameter('cat', $cat)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findAllByEpicKey($epicKey)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\EpicAreaCategory', 'epc')
            ->select('epc')
            ->where('epc.epicKey = :epicKey')
            ->setParameter('epicKey', $epicKey)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

}
