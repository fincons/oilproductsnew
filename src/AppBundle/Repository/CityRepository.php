<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CityRepository extends EntityRepository
{
    public function findOneByName($name) {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->where($qb->expr()->like('c.name', ':name'))
            ->setParameter('name', $name)
            ->setMaxResults(1)
        ;

        try {
            return $qb->getQuery()->getSingleResult();
        }
        catch (\Doctrine\ORM\NoResultException $ex) {
            return null;
        }
    }
}
