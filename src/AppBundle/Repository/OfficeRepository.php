<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * OfficeRepository
 */
class OfficeRepository extends EntityRepository
{
    public function findByCompanyCity($company, $city) {
        $qb = $this->createQueryBuilder('o')
            ->where('o.company = :company')
            ->andWhere('o.city = :city')
            ->setParameter('company', $company)
            ->setParameter('city', $city)
        ;
        return $qb->getQuery()->getResult();
    }

    /**
     * Trova Uffici per provincia di competenza in base alla città, tipo
     */
    public function searchByFormData(array $data) {
        $qb = $this->createQueryBuilder('o');

        if (isset($data['city']) && $data['city']) {
            $city = $data['city'];
            $qb
                ->join('o.provinces', 'p')
                ->andWhere('p = :provincia')
                ->setParameter('provincia', $city->getProvince())
            ;
        }
        if (isset($data['type']) && $data['type']) {
            $type = $data['type'];
            $qb
                ->join('o.company', 'c')
                ->andWhere(sprintf('c.%s = :has_type', $type))
                ->setParameter('has_type', true)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
