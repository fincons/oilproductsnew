<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class ProductCategoryRepository extends AbstractTranslatableEntityRepository
{
	
	public function findNotDuplicationByTitle($title) {
		return $this->findBy(array('duplicatedOf' => null, 'title' => $title));
	}
	
	
	public function findAllSlugProductCategory()
	{
	    
	    $query = $this->getEntityManager()
	    ->createQueryBuilder()
	    ->from('AppBundle\Entity\ProductCategory', 'pc')
	    ->select('pc.slug')
	    ->where('pc.slug != \'empty\' ')
	    ->distinct()
	    ->getQuery();
	    $pcSlugs = $query->getResult();
	    
	    $query = $this->getEntityManager()
	    ->createQueryBuilder()
	    ->from('AppBundle\Entity\ProductCategoryTranslation', 'pct')
	    ->select('pct.content')
	    ->where('pct.field = \'slug\' ')
	    ->distinct()
	    ->getQuery();
	    $pctSlugs = $query->getResult();
	    
	    
	    $slugs =array();
	    
	    foreach($pctSlugs as $pctSlug) {
	        $slugs[] = $pctSlug['content'];
	    }
	    
	    foreach($pcSlugs as $pcSlug) {
	        if(!in_array($pcSlug['slug'], $slugs)){
	            $slugs[] = $pcSlug['slug'];
	        }
	    }
	    
	    
	    return $slugs;
	    
	}
	
	
}