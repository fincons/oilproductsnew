<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class ProductClassificationRowRepository extends AbstractTranslatableEntityRepository
{

    public function findDistinctProductCategoriesOfLev2($current_locale = "")
    {   
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\ProductCategory', 'pc')
            ->select('pc')
            ->where('pcr.productCategoryLev2 = pc')
            // enabled == fromEpic
            ->andWhere('pcr.fromEpic = true')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            //->andWhere('pcr.language  = :current_locale ')
            //->setParameter('current_locale',  $current_locale)
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findDistinctProductCategoriesOfLev3($current_locale = "",$catLev2)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\ProductCategory', 'pc')
            ->select('pc')
            ->where('pcr.productCategoryLev3 = pc')
            ->andWhere('pcr.productCategoryLev2 = :catLev2')
            // enabled == fromEpic
            ->andWhere('pcr.fromEpic = true')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findDistinctProductCategoriesOfLev4($current_locale = "",$catLev2, $catLev3)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\ProductCategory', 'pc')
            ->select('pc')
            ->where('pcr.productCategoryLev4 = pc')
            ->andWhere('pcr.productCategoryLev2 = :catLev2')
            ->andWhere('pcr.productCategoryLev3 = :catLev3')
            // enabled == fromEpic
            ->andWhere('pcr.fromEpic = true')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }


    public function findDistinctProductCategoriesOfLev5($current_locale = "",$catLev2, $catLev3, $catLev4)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\ProductCategory', 'pc')
            ->select('pc')
            ->where('pcr.productCategoryLev5 = pc')
            ->andWhere('pcr.productCategoryLev2 = :catLev2')
            ->andWhere('pcr.productCategoryLev3 = :catLev3')
            ->andWhere('pcr.productCategoryLev4 = :catLev4')
            // enabled == fromEpic
            ->andWhere('pcr.fromEpic = true')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->orderBy('pcr.position, pc.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findDistinctProducts($current_locale = "",$catLev2, $catLev3, $catLev4, $catLev5)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->select('p')
            ->where('pcr.product = p')
            ->andWhere('pcr.productCategoryLev2 = :catLev2')
            ->andWhere('pcr.productCategoryLev3 = :catLev3')
            ->andWhere('pcr.productCategoryLev4 = :catLev4')
            ->andWhere('pcr.productCategoryLev5 = :catLev5')
            // enabled == fromEpic
            ->andWhere('pcr.fromEpic = true')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->orderBy('pcr.position, p.title', 'ASC')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->setParameter('catLev5', $catLev5)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }


    /**
     * @param \string $productCode
     * @return mixed
     */
    public function findByProductCode($current_locale = "",$productCode)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->select('pcr')
            ->where('pcr.product = p')
            ->andWhere('p.code = :pCode')
            // enabled == fromEpic
            ->andWhere('pcr.fromEpic = true')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->orderBy('pcr.position', 'ASC')
            ->setParameter('pCode', $productCode)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }

    public function findAllByCategoriesAndProduct($catLev2, $catLev3, $catLev4, $catLev5, $product)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->select('pcr')
            ->where('pcr.productCategoryLev2 = :catLev2')
            ->andWhere('pcr.productCategoryLev3 = :catLev3')
            ->andWhere('pcr.productCategoryLev4 = :catLev4')
            ->andWhere('pcr.productCategoryLev5 = :catLev5')
            ->andWhere('pcr.product = :product')
            ->setParameter('catLev2', $catLev2)
            ->setParameter('catLev3', $catLev3)
            ->setParameter('catLev4', $catLev4)
            ->setParameter('catLev5', $catLev5)
            ->setParameter('product', $product)
            ->getQuery();

        $categories = $query->getResult();

        return $categories;
    }
    
    public function findDistinctProductCategoriesLanguages()
    {
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
        ->select('pcr.language')
        ->distinct()
        ->getQuery();
        
        $languages = $query->getResult();
        
        return $languages;
    }
    
    public function findSlugTranslationsByTitleCategories($title, $current_locale = "")
    {
        $slugTranslations = "";
        
        if($current_locale == 'it_IT'){
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductCategoryTranslation', 'pct')
            ->from('AppBundle\Entity\ProductCategory', 'pc')
            ->select('pct.locale','pct.content','pc.slug')
            ->where('pc.title = \''.$title.'\' ')
            ->andWhere('pct.field = \'slug\' ')
            ->andWhere('pc.id = pct.object_id')
            ->groupBy('pct.locale')
            ->getQuery();
            
            $slugTranslations = $query->getResult();
        }
        else{
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductCategoryTranslation', 'pct')
            ->from('AppBundle\Entity\ProductCategory', 'pc')
            ->select('pc.title')
            ->where('pct.content = \''.$title.'\' ')
            ->andWhere('pct.field = \'title\' ')
            ->andWhere('pc.id = pct.object_id')
            ->setMaxResults(1)
            ->getQuery();
            
            $titleIta = $query->getResult();
            
            if(isset($titleIta[0])){
                $titleIta = $titleIta[0]['title'];
                
                $query = $this->getEntityManager()
                ->createQueryBuilder()
                ->from('AppBundle\Entity\ProductCategoryTranslation', 'pct')
                ->from('AppBundle\Entity\ProductCategory', 'pc')
                ->select('pct.locale','pct.content','pc.slug')
                ->where('pc.title = \''.$titleIta.'\' ')
                ->andWhere('pct.field = \'slug\' ')
                ->andWhere('pc.id = pct.object_id')
                ->groupBy('pct.locale')
                ->getQuery();
                
                $slugTranslations = $query->getResult();
            }
        }
        
        
        return $slugTranslations;
    }
    
    public function findFirstProductClassificationRowByProductAndLanguages($slug, $current_locale = "")
    {
        $mainLink = "";
        
        if($current_locale == 'it_IT'){
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->select('pcr')
            ->where('p.slug = \''.$slug.'\' ')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.product = p')
            //->setMaxResults(1)
            ->getQuery();
            
            $pcrs = $query->getResult();
            
            foreach ($pcrs as $pcr) {
                $mainLink = "";
                if($pcr->getProductCategoryLev5()->getSlug() != "empty"){ continue; }
                for ($numLev = 2; $numLev <= 5; $numLev++) {
                    $slugCategory = $pcr->{"getProductCategoryLev" . $numLev}()->getSlug();
                    if($slugCategory != "empty" && $slugCategory != ""){
                        if($numLev == 2){
                            $mainLink = $mainLink.$pcr->{"getProductCategoryLev" . $numLev}()->getSlug();
                        }
                        else{
                            $mainLink = $mainLink."/". $pcr->{"getProductCategoryLev" . $numLev}()->getSlug();
                        }
                    }
                }
                break;
            }
            
            
        }
        else{
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->from('AppBundle\Entity\ProductTranslation', 'pt')
            ->select('pcr')
            ->where('pt.content = \''.$slug.'\' ')
            ->andWhere('pt.field = \'slug\' ')
            ->andWhere('pt.locale LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('p.id = pt.object_id')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.product = p')
            //->setMaxResults(1)
            ->getQuery();
            
            $pcrs = $query->getResult();
            
            foreach ($pcrs as $pcr) {
                $mainLink = "";
                if($pcr->getProductCategoryLev5()->getSlug() != "empty"){ continue; }
                for ($numLev = 2; $numLev <= 5; $numLev++) {
                    $slugCategory = $pcr->{"getProductCategoryLev" . $numLev}()->getSlug();
                    if($slugCategory != "empty" && $slugCategory != ""){
                        if($numLev == 2){
                            $mainLink = $mainLink.$pcr->{"getProductCategoryLev" . $numLev}()->getSlug();
                        }
                        else{
                            $mainLink = $mainLink."/". $pcr->{"getProductCategoryLev" . $numLev}()->getSlug();
                        }
                    }
                }
                break;
            }
            
            
        }
        
        if($mainLink != ""){
            $mainLink = $mainLink."/".$slug;
        }
        
        return $mainLink;
    }
    
    
    public function findFirstProductClassificationRowByCategoryAndLanguages($slug, $current_locale = "")
    {
        $mainLink = "";
        if($slug == "lubrificanti"){ return $mainLink; }
        
       // if($current_locale == 'it_IT'){
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->select('pcr')
            ->where('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->getQuery();
            
            $pcrs = $query->getResult();
            
            $numCategory = 6;
            $pcrMain = "";
            //prende la pcr con il livello maggiore
            foreach ($pcrs as $pcr) {
                $slugCategory2 = $pcr->getProductCategoryLev2()->getSlug();
                $slugCategory3 = $pcr->getProductCategoryLev3()->getSlug();
                $slugCategory4 = $pcr->getProductCategoryLev4()->getSlug();
                $slugCategory5 = $pcr->getProductCategoryLev5()->getSlug();
                
                if($slugCategory2 != $slug && $slugCategory3 != $slug && $slugCategory4 != $slug && $slugCategory5 != $slug){ continue; }
                
                if($slugCategory2 == $slug && 2 < $numCategory){ $numCategory = 2; $pcrMain = $pcr; }
                if($slugCategory3 == $slug && 3 < $numCategory){ $numCategory = 3; $pcrMain = $pcr; }
                if($slugCategory4 == $slug && 4 < $numCategory){ $numCategory = 4; $pcrMain = $pcr; }
                if($slugCategory5 == $slug && 5 < $numCategory){ $numCategory = 5; $pcrMain = $pcr; }
            }
                
            if($pcrMain != ""){
                for($numLev = 2; $numLev <= $numCategory/*5*/; $numLev++) {
                    $slugCategory = $pcrMain->{"getProductCategoryLev" . $numLev}()->getSlug();
                    if($slugCategory != "empty" && $slugCategory != ""){
                        if($numLev == 4 && $numCategory < 5){ break; }
                        if($numLev == 2){
                            $mainLink = $mainLink.$pcrMain->{"getProductCategoryLev" . $numLev}()->getSlug();
                        }
                        else{
                            $mainLink = $mainLink."/". $pcrMain->{"getProductCategoryLev" . $numLev}()->getSlug();
                        }
                    }
                    if($numCategory == $numLev){ break; }
                }
            }
        
        
        return $mainLink;
    }
    
    public function findNameProductCategoryTranslations()
    {
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\PageTranslation', 'pgt')
        ->from('AppBundle\Entity\Page', 'pg')
        ->select('pgt.locale','pgt.content','pg.title')
        ->where('pg.routeId = \'product_list\' ')
        ->andWhere('pgt.field = \'title\' ')
        ->andWhere('pg.id = pgt.object_id')
        ->getQuery();
        
        $nameProductTranslations = $query->getResult();
        
        return $nameProductTranslations;
    }
    
    public function findAllProductsNames($current_locale = "")
    {
        if($current_locale == 'it_IT'){
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->select('p.title')
            ->where('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('p.code != \'EMPTY\' ')
            ->andWhere('pcr.product = p')
            ->distinct()
            ->getQuery();
            
            $pNames = $query->getResult();
            
        }
        else{
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->from('AppBundle\Entity\ProductTranslation', 'pt')
            ->select('pt.content')
            ->where('pt.field = \'title\' ')
            ->andWhere('p.code != \'EMPTY\' ')
            ->andWhere('pt.locale LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('p.id = pt.object_id')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.product = p')
            ->distinct()
            ->getQuery();
            
            $pNames = $query->getResult();
            
        }
        
        
        return $pNames;
    }
    
    public function findAllProductLinkByName($productName,$current_locale = "")
    {
        if($current_locale == 'it_IT'){
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->select('pcr')
            ->where('p.title LIKE \'%'.$productName.'%\' ')
            ->andWhere('p.code != \'EMPTY\' ')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.product = p')
            ->getQuery();
            
            $pcrs =  $query->getResult();
            
        }
        else{
            
            $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('AppBundle\Entity\ProductClassificationRow', 'pcr')
            ->from('AppBundle\Entity\Product', 'p')
            ->from('AppBundle\Entity\ProductTranslation', 'pt')
            ->select('pcr')
            ->where('pt.content LIKE \'%'.$productName.'%\' ')
            ->andWhere('pt.field = \'title\' ')
            ->andWhere('pt.locale LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('p.id = pt.object_id')
            ->andWhere('pcr.language LIKE \'%'.$current_locale.'%\' ')
            ->andWhere('pcr.product = p')
            ->getQuery();
            
            $pcrs = $query->getResult();
            
        }
        
        
        return $pcrs;
    }
 

}