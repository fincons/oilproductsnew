<?php

namespace AppBundle\Repository;

use AppBundle\Repository\AbstractTranslatableEntityRepository;

class PageRepository extends AbstractTranslatableEntityRepository
{
    public function findSlugTranslations($slug)
    {
        $query = $this->getEntityManager()
        ->createQueryBuilder()
        ->from('AppBundle\Entity\PageTranslation', 'pgt')
        ->from('AppBundle\Entity\Page', 'pg')
        ->select('pgt.locale','pgt.content','pg.slug')
        ->where('pg.routeId = \'page_show\' ')
        ->andWhere('pgt.field = \'slug\' ')        
        ->andWhere('pgt.content = \''.$slug.'\' OR pg.slug = \''.$slug.'\' ')
        ->andWhere('pg.id = pgt.object_id')
        ->getQuery();
        
        $slugTranslations = $query->getResult();
        
        return $slugTranslations;
    }
    
}