<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use AppBundle\Utils\TextUtils;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ContactSubGroupAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('iconName')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('iconName')
            ->add('position')
            ->add('published')
            ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('iconName')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
            ->add('name')
            ->add('iconName', ChoiceType::class, array(
                'choices'  => array(
                    ''=>'',
                    'italia' => 'italia',
                    'africa' => 'africa',
                    'asia' => 'asia',
                    'europe' => 'europe',
                ),
            ))
            ->add('email')
            ->add('position')
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published')
            ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
            ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
            ->end();

    }

}
