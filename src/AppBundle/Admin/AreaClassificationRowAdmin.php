<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AreaClassificationRowAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ->add('areaLev2')
        ->add('areaLev3')
        ->add('areaLev4')
        ->add('areaLev5')
        ->add('areaLev6')
        ->add('product')
        ->add('position')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('areaLev2')
            ->add('areaLev3')
            ->add('areaLev4')
            ->add('areaLev5')
            ->add('areaLev6')
	        ->add('product')
	        ->add('position')
	        ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('areaLev2')
            ->add('areaLev3')
            ->add('areaLev4')
            ->add('areaLev5')
            ->add('areaLev6')
        	->add('product')
        	->add('position')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('areaLev2')
                ->add('areaLev3')
                ->add('areaLev4')
                ->add('areaLev5')
                ->add('areaLev6')
        		->add('product')
        		->add('position')
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published', null, array('read_only' => true,
                'disabled'  => true))
             ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
             ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
             ->end();
    }

    public function getExportFields() {
    	return array('id', 'areaLev2', 'areaLev3', 'areaLev4', 'areaLev5', 'areaLev6', 'product', 'position');
    }

}
