<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Utils\TextUtils;
use AppBundle\Form\Type\PageTabsType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class AreaCategoryAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ->add('id')
        ->add('title')
        ->add('slug')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('slug')
            ->add('updatedAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'tabs' => ['template' => 'AppBundle:admin:list__action_page.html.twig']
                )
            ));
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        //$tl = $this->getRequest()->get('tl');
        //$productCategory = $this->getSubject();
        //$productCategory->setLocale($tl);

        $formMapper = $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('title')
                ->add('shortDescription', CKEditorType::class, array('required'=>false))
                ->add('description', CKEditorType::class, array('required'=>false))
                ->add('bannerImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                ->add('previewImage', 'sonata_type_model_list', array('required' => false), array(
                		'link_parameters' => array(
                				'context' => 'default',
                				'provider' => 'sonata.media.provider.image',
                )))
                ->add('thumbImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                ->add('detailImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                        ),
                        'class' => 'Application\Sonata\MediaBundle\Entity\Media',
                ))
                ->add('contactGroup', null, array('required' => false))
            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
                ->add('slug', null, array('required' => false))
                ->add('metaKeywords', null, array('required' => false))
                ->add('metaDescription', null, array('required' => false))
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
                ->add('published', null, array('required' => false))
                ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
                ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                        'disabled'  => true))
            ->end()
            ->with('Content', array('class' => 'col-md-12'))
                ->add('tabs', 'sonata_type_collection', array(
                    'by_reference'       => false,
                    'cascade_validation' => true,
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                ))
                ->add('includeProductSection', null, array(
                    'required' => false,
                    'label'    => 'Include Category details',
                    'help' => '<i>include a block with sub-categories or products on all the tabs</i>'
                ))
                ->add('includeGplToolSection', null, array(
                    'required' => false,
                    'label'    => 'Include GPL Tool',
                    'help' => '<i>add a tab with the GPL store locator</i>'
                ))
                ->add('includeConcessionaryToolSection', null, array(
                    'required' => false,
                    'label'    => 'Include Concessionary Tool',
                    'help' => '<i>add a tab with the Concessionary store locator</i>'
                ))
            ->end();

    }

    protected function configureRoutes(RouteCollection $collection) {
       $collection->add('tabs', '{id}/tabs');
    }

    public function preUpdate($object) {
    	parent::preUpdate($object);
    	foreach($object->getTabs() as $tab) {
            $this->setSlug($tab);
    	}
    }

    public function prePersist($object) {
    	parent::prePersist($object);
    	foreach($object->getTabs() as $tab) {
            $this->setSlug($tab);
    	}
    }

    public function getTabsFormOptions() {
        return array('type' => PageTabsType::TYPE_AREA);
    }
}
