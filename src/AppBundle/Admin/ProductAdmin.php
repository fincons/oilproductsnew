<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Utils\TextUtils;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class ProductAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ->add('title')
        ->add('slug')        
        ->add('code')       
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('slug')
            ->add('code')   
            ->add('fromEpic')
            ->add('fromAlis')
            ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
            ->add('code')     
            ->add('fromEpic')
            ->add('fromAlis')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('title')
                ->add('code', null)       
                ->add('shortDescription', CKEditorType::class, array('required'=>false))
                ->add('description', CKEditorType::class, array('required'=>false))
                ->add('gradient', null, array('required' => false))
                ->add('specifications', null, array('required' => false))
                ->add('pdf', null, array('required' => false))
                ->add('brochure', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.file',
                    ),
                    'class' => 'Application\Sonata\MediaBundle\Entity\Media',
                ))
                ->add('thumbImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                    ->add('detailImage', 'sonata_type_model_list', array('required' => false), array(
                        'link_parameters' => array(
                            'context' => 'default',
                            'provider' => 'sonata.media.provider.image',
                        ),
                        'class' => 'Application\Sonata\MediaBundle\Entity\Media',
                    ))   
                 ->add('fromEpic', null, array('required' => false))
                 ->add('epicVersion', null, array('required' => false))
                 ->add('fromAlis', null, array('required' => false))                    
            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
                ->add('slug', null, array('required' => false))
                ->add('metaKeywords', null, array('required' => false))
                ->add('metaDescription', null, array('required' => false))
                ->end()     
            ->with('Tracking Info', array('class' => 'col-md-6'))
                ->add('published', null, array('read_only' => true,
                    'disabled'  => true))
                ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
                ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                        'disabled'  => true))
            ->end();   
            
    }


}
