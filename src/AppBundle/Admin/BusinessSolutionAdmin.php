<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use AppBundle\Form\Type\PageTabsType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class BusinessSolutionAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('slug')
            ->add('description')
            ->add('language')
            ->add('published')
            
            ->add('enabled')
        
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('slug')
            ->add('description')
            ->add('position')
            ->add('language')
            ->add('published')
            
            ->add('enabled', 'choice', array(
                'choices' => array(
                    '1' => 'Yes',
                    '0' => 'No'                    
                )
            ))
            
            ->add('updatedAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'tabs' => ['template' => 'AppBundle:admin:list__action_page.html.twig']
                )
            ));
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
            ->add('published')
            
            ->add('enabled')
            
            ->add('description')
            ->add('language')
            ->add('position')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('title')
                ->add('description', CKEditorType::class)
                ->add('bannerImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                ->add('previewImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                 ->add('position', null, array('required' => true) )
                 ->add('language', null, array('required' => true,
                    		'help' => 'ES: ALL or de_DE|it_IT etc.'))

            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
                ->add('slug', null, array('required' => false))
                ->add('metaKeywords', null, array('required' => false))
                ->add('metaDescription', null, array('required' => false))
                ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
                ->add('published')
                
                ->add('enabled', 'choice', array(
                    'choices' => array(
                        '1' => 'Yes',
                        '0' => 'No'
                    )
                ))
                
                ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
                ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                        'disabled'  => true))
            ->end()
            ->with('Tab', array('class' => 'col-md-12'))
                ->add('tabs', 'sonata_type_collection', array(
            		'by_reference'       => false,
            		'cascade_validation' => true,
                ), array(
            		'edit' => 'inline',
            		'inline' => 'table',
                    'sortable' => 'position',
                ))
                ->add('includeConcessionaryToolSection', null, array(
                    'required' => false,
                    'label'    => 'Include Concessionary Tool',
                    'help' => '<i>add a tab with the Concessionary store locator</i>'
                ))
            ->end();

    }

    protected function configureRoutes(RouteCollection $collection) {
       $collection->add('tabs', '{id}/tabs');
    }

    public function preUpdate($object) {
        parent::preUpdate($object);
        foreach($object->getTabs() as $tab) {
            $this->setSlug($tab);
        }
    }
    
    public function prePersist($object) {
        parent::prePersist($object);
        foreach($object->getTabs() as $tab) {
            $this->setSlug($tab);
        }
    }

    public function getTabsFormOptions() {
        return array('type' => PageTabsType::TYPE_BUSINESS);
    }
}
