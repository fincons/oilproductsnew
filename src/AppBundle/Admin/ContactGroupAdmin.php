<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ContactGroupAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('slug')
            ->add('subitle')
            ->add('published');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('slug')
            ->add('language')
            ->add('position')
            ->add('published')
            ->add('updatedAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
            ->add('language')
            ->add('published');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {       
        $arrayType = array(
	        'marina' => 'marina',
	        'aviazione' => 'aviazione',
	        'agricoltura' => 'agricoltura',
	        'carburanti' => 'carburanti',
	        'bitumi' => 'bitumi',
	        'grassi' => 'grassi',
	        'additivi' => 'additivi',
	        'gpl' => 'gpl',
	        'sales_area_manager' => 'sales_area_manager',
	        'inside_sales' => 'inside_sales',
	        'technical_service' => 'technical_service'
        );

        $formMapper
            ->with('General', array('class' => 'col-md-6'))
            ->add('title')
            ->add('subtitle')
            ->add('language', null, array('required' => true,
            		'help' => 'ES: ALL or de_DE|it_IT etc.'))
            ->add('iconName', ChoiceType::class, array(
                'choices' =>
                    $arrayType,
            ))
            ->add('email')
            ->add('previewImage', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => array(
                    'context' => 'default',
                    'provider' => 'sonata.media.provider.image',
                )))
            ->add('bannerImage', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => array(
                    'context' => 'default',
                    'provider' => 'sonata.media.provider.image',
                )))
            ->add('pdf', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => array(
                    'context' => 'default',
                    'provider' => 'sonata.media.provider.file',
                ),
                'class' => 'Application\Sonata\MediaBundle\Entity\Media',
            ))
            ->add('position')
            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
            ->add('slug', null, array('required' => false))
            ->add('metaKeywords', null, array('required' => false))
            ->add('metaDescription', null, array('required' => false))
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published')
            ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled' => true))
            ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled' => true))
            ->end()
            ->with('Contacts', array('class' => 'col-md-12'))
            ->add('contacts', 'sonata_type_collection', array(
                'by_reference' => false,
                'cascade_validation' => true,
            ), array(
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->end();

    }


}
