<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Utils\TextUtils;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Route\RouteCollection;

class EpicArchiveAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ->add('archiveName')  
        ->add('downloadedAt')
        ->add('productElaborationAt')
        ->add('productClassificationElaborationAt')
        ->add('areaClassificationElaborationAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
        	->add('archiveName')  
        	->add('downloadedAt')
        	->add('productElaborationAt')
        	->add('productClassificationElaborationAt')
        	->add('areaClassificationElaborationAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('archiveName')  
        	->add('downloadedAt')
        	->add('productElaborationAt')
        	->add('productClassificationElaborationAt')
        	->add('areaClassificationElaborationAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('archiveName')     
                ->add('downloadedAt')
                ->add('productElaborationAt')
                ->add('productClassificationElaborationAt')
                ->add('areaClassificationElaborationAt')
            ->end();   
            
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
    	$collection->clearExcept(array('list'));
    }
    


}
