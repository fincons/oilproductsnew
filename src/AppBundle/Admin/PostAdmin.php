<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PostAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('slug')       
            ->add('type')
            ->add('abstract')
            ->add('language')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')        
            ->add('title')
            ->add('slug')
            ->add('type')
            ->add('referenceDate')
            ->add('highlight')
            ->add('showInHomePage')
            ->add('language')
            ->add('published')
            ->add('updatedAt')
            
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
            ->add('language')
            ->add('published')
            ->add('type')
            ->add('highlight')
            ->add('showInHomePage')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('title')
                ->add('subtitle')
                ->add('type', ChoiceType::class, array(
                		'choices'  => array(
                				'Event' => 'Event',
                				'News' => 'News',
                		),
                ))
                ->add('abstract', CKEditorType::class, array('required'=>true))
                ->add('main', CKEditorType::class, array('required'=>true))
                ->add('previewImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                ->add('detailImage', 'sonata_type_model_list', array('required' => false), array(
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                    )))
                ->add('language', null, array('required' => true,
                    'help' => 'ES: ALL or de_DE|it_IT etc.'))
                    // TODO: use sonata_type_datetime_picker to edit referenceDate... now it crashes
                    ->add('referenceDate')                    
                    ->add('highlight')
                    ->add('showInHomePage')

            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
                ->add('slug', null, array('required' => false))            
                ->add('metaKeywords', null, array('required' => false))
                ->add('metaDescription', null, array('required' => false))
                ->end()     
            ->with('Tracking Info', array('class' => 'col-md-6'))
                ->add('published')
                ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
                ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                        'disabled'  => true))
            ->end();                
            
    }
    

}
