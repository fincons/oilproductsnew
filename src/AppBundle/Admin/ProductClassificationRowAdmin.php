<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductClassificationRowAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ->add('productCategoryLev2')
        ->add('productCategoryLev3')
        ->add('productCategoryLev4')
        ->add('productCategoryLev5')
        ->add('product')
        ->add('position')
        ->add('fromEpic')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
	        ->add('productCategoryLev2')
	        ->add('productCategoryLev3')
	        ->add('productCategoryLev4')
	        ->add('productCategoryLev5')
	        ->add('product')
	        ->add('position')
	        ->add('fromEpic')
	        ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('productCategoryLev2')   
        	->add('productCategoryLev3')
        	->add('productCategoryLev4')
        	->add('productCategoryLev5')
        	->add('product')
        	->add('position')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
        		->add('productCategoryLev2') 
        		->add('productCategoryLev3')
        		->add('productCategoryLev4')
        		->add('productCategoryLev5')
        		->add('product')
        		->add('position')
        		->add('fromEpic')
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published', null, array('read_only' => true,
                'disabled'  => true))
             ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
             ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
             ->end();
    }

    public function getExportFields() {
    	return array('id', 'productCategoryLev2', 'productCategoryLev3', 'productCategoryLev4', 'productCategoryLev5', 'product', 'position');
    }


}
