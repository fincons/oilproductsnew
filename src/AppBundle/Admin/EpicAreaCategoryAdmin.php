<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EpicAreaCategoryAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('epicKey')
            ->add('areaCategoryid')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('epicKey')
            ->add('areaCategoryid')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('epicKey')
            ->add('areaCategoryid')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
            ->add('epicKey')
            ->add('areaCategoryid')
            ->end();   
            
    }
    /*
    protected function configureRoutes(RouteCollection $collection)
    {
    	$collection->clearExcept(array('list'));
    }*/

    public function getExportFields() {
        return array('id', 'epicKey', 'areaCategoryid');
    }

}
