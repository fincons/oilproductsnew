<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LinkGroupAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        	->add('type')        
        	->add('label')
        	->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('type')
            ->add('label')
            ->add('published')
            ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('type')
            ->add('label')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
            	->add('type')
                ->add('label', null, array('required' => false))
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
                ->add('published')
                ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
                ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
            ->end()            
            ->with('links', array('class' => 'col-md-12'))
            ->add('links', 'sonata_type_collection', array(
            		'by_reference'       => false,
            		'cascade_validation' => true,
            ), array(
            		'edit' => 'inline',
            		'inline' => 'table'
            ))
            ->end();            
            
    }


}
