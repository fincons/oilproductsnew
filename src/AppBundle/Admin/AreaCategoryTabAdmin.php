<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Utils\TextUtils;
use AppBundle\Entity\AreaCategoryTab;

class AreaCategoryTabAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
    	$filter
           ->add('title')
    	;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
    	$list
    	   ->addIdentifier('title')
           ->add('published')
    	;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        
        $em = $this->modelManager->getEntityManager('AppBundle:Link');
        
        $query = $em->createQueryBuilder("l")
        ->select("l")
        ->from("AppBundle:Link", "l")
        ->join('l.linkGroup','lg')
        ->where("lg.type = :lgType")
        ->setParameter('lgType',AreaCategoryTab::LINK_GROUP_TYPE)
        ->getQuery();
        
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('title')
                ->add('link', 'sonata_type_model', array(
                    'required' => false,
                    'multiple' => false,
                    'query' => $query
                ))
                ->add('published')
                ->add('position')
            ->end()
        ;
    }

}
