<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use AppBundle\Entity\Tab;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TabAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
    	$filter
           ->add('title')
    	;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
    	$list
    	   ->addIdentifier('title')
           ->add('published')
    	;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('AppBundle:Link');
        
        $query = $em->createQueryBuilder("l")
        ->select("l")
        ->from("AppBundle:Link", "l")
        ->join('l.linkGroup','lg')
        ->where("lg.type = :lgType")
        ->setParameter('lgType',Tab::LINK_GROUP_TYPE)
        ->getQuery();
        
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('title')
                ->add('link', 'sonata_type_model', array(
                    'required' => false,
                    'multiple' => false,
                    'query' => $query
                ))
                ->add('published')                
                ->add('position')
            ->end()
        ;
    }


}
