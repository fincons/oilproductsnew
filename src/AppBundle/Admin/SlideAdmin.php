<?php

namespace AppBundle\Admin;


use AppBundle\Entity\Slide;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

class SlideAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('firstLine')
            ->add('slider')
            ->add('link')
            ->add('link2')
            ->add('language')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('firstLine')
            ->add('badge')
            ->add('link')
            ->add('link2')
            ->add('slider')
            ->add('language')
            ->add('published')
            ->add('updatedAt')
            
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstLine')
            ->add('slider')
            ->add('language')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('AppBundle:Link');

        $query = $em->createQueryBuilder("l")
            ->select("l")
            ->from("AppBundle:Link", "l")
            ->join('l.linkGroup','lg')
            ->where("lg.type = :lgType")
            ->setParameter('lgType',Slide::LINK_GROUP_TYPE)
            ->getQuery();

        $formMapper
            ->with('General', array('class' => 'col-md-6 '))
            ->add('slider', ChoiceType::class, array(
                'choices'  => array(
                    Slide::JUMBO_SLIDER =>  Slide::JUMBO_SLIDER,
                    Slide::LINK_SLIDER => Slide::LINK_SLIDER,
                ),
            ))
            ->add('firstLine', null, array('required'=>true))
            ->add('secondLine', null, array('required'=>false))
            ->add('badge')

            ->add('image', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => array(
                    'context' => 'default',
                    'provider' => 'sonata.media.provider.image',
                )))
            ->add('link', 'sonata_type_model', array(
                'required' => false,
                'multiple' => false,
                'query' => $query
                )
            )
            ->add('link2', 'sonata_type_model', array(
                'required' => false,
                'multiple' => false,
                'query' => $query
                )
            )
            ->add('language', null, array('required' => true,
                'help' => 'ES: ALL or it_IT|en_GB or de_DE|en_DE etc.'))
            ->add('position')
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published')
            ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
            ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
            ->end();

    }
}
