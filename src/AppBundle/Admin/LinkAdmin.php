<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

class LinkAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('label')
        	->add('linkGroup')
            ->add('url')
            ->add('language')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('label')
        	->add('linkGroup')
            ->add('type')
            ->add('page')
            ->add('url')
            ->add('language')
            ->add('published')
            ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('label')
        	->add('linkGroup')
        	->add('type')
            ->add('page')
            ->add('url')
            ->add('language')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
    	
        $formMapper
        ->with('General', array('class' => 'col-md-6'))
            ->add('linkGroup', null, array('constraints' => new Assert\NotNull()))      
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'External' => 'External',
                    'Internal' => 'Internal',
                ),
            ))
            ->add('page', null, array('required' => false))
            ->add('url', null, array('required' => false))
            ->add('label', null, array('required' => false))
            ->add('format', null, array('required' => false))
            ->add('position', null, array('required' => false))
            ->add('language', null, array('required' => true,
            		'help' => 'ES: ALL or de_DE|it_IT etc.'))
        ->end()
        ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published')
            ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
             ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                    'disabled'  => true))
         ->end();
 
    }


}
