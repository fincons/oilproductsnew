<?php

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

class ContactAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('name')
            ->add('contactGroup')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')        
            ->add('title')
            ->add('name')
            ->add('contactGroup')
            ->add('contactSubGroup')
            ->add('Title')
            ->add('position')
            ->add('published')
            ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('name')
            ->add('contactGroup')
            ->add('contactSubGroup')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6 '))
            ->add('contactGroup', null, array('constraints' => new Assert\NotNull()))
            ->add('contactSubGroup', null, array('constraints' => new Assert\NotNull()))
            ->add('title')
            ->add('name')
            ->add('phone')
            ->add('phone2')
            ->add('email')
            ->add('email2')
            ->add('fax')
            ->add('fax2')
            ->add('mobile')
            ->add('mobile2')
            ->add('address')
            ->add('address2')
            ->add('site')
            ->add('site2')
            ->add('position')
            ->end()
            ->with('Company Contact Fields', array('class' => 'col-md-6'))
            ->add('company')
            ->add('intoplane')
            ->add('dayworkschedule')
            ->add('airportManager')
            ->end()
            ->with('Tracking Info', array('class' => 'col-md-6'))
            ->add('published')
            ->add('createdAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
            ->add('updatedAt', 'sonata_type_datetime_picker', array('read_only' => true,
                'disabled'  => true))
            ->end();

    }
}
