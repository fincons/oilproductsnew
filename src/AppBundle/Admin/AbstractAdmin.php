<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use AppBundle\Utils\TextUtils;


abstract class AbstractAdmin extends Admin
{
    public function preUpdate($object) {
        parent::preUpdate($object);
        $object->setUpdatedAt(new \DateTime);
        $this->setSlug($object);
    }
    
    public function prePersist($object) {
        parent::prePersist($object);
        $object->setCreatedAt(new \DateTime);
        $object->setUpdatedAt(new \DateTime);
        $this->setSlug($object);
    }
    
    protected function setSlug($object) {
        if(property_exists($object, 'slug')) {
            if (empty($object->getSlug())) {
                $object->setSlug(TextUtils::slugify($object->getTitle()));
            } else {
                $object->setSlug(TextUtils::slugify($object->getSlug()));
            }
        }        
    }
}
