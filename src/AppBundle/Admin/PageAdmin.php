<?php

namespace AppBundle\Admin;

use AppBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Utils\TextUtils;

class PageAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('routeId')
            ->add('slug')
            ->add('routeParams')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('routeId')
            ->add('slug')
            ->add('published')
            ->add('updatedAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'tabs' => ['template' => 'AppBundle:admin:list__action_page.html.twig']
                )
            ));
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('routeId')
            ->add('slug')
            ->add('published')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
	        ->with('General', array('class' => 'col-md-6'))
    	        ->add('title')
    	        ->add('routeId')
    	        ->add('routeParams')
    	        ->add('slug')
    	        ->add('bannerImage', 'sonata_type_model_list', array('required' => false), array(
    	            'link_parameters' => array(
    	                'context' => 'default',
    	                'provider' => 'sonata.media.provider.image',
    	            )
                ))
	        ->end()
	        ->with('Seo', array('class' => 'col-md-6'))
    	        ->add('metaKeywords', null, array('required' => false))
    	        ->add('metaDescription', null, array('required' => false))
	        ->end()
	        ->with('Tracking Info', array('class' => 'col-md-6'))
    	        ->add('published')
    	        ->add('createdAt', 'sonata_type_datetime_picker', array(
                    'read_only' => true,
    	            'disabled'  => true
                ))
                ->add('updatedAt', 'sonata_type_datetime_picker', array(
                    'read_only' => true,
                    'disabled'  => true
                ))
            ->end()
            ->with('Tab', array('class' => 'col-md-12'))
                ->add('tabs', 'sonata_type_collection', array(
            		'by_reference'       => false,
            		'cascade_validation' => true,
                ), array(
            		'edit' => 'inline',
            		'inline' => 'table',
                    'sortable' => 'position',
                ))
            ->end();
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('tabs', '{id}/tabs');
    }

    public function preUpdate($object) {
    	parent::preUpdate($object);
    	foreach($object->getTabs() as $tab) {
            $this->setSlug($tab);
    	}
    }

    public function prePersist($object) {
    	parent::prePersist($object);
    	foreach($object->getTabs() as $tab) {
            $this->setSlug($tab);
    	}
    }

    public function getTabsFormOptions() {
        return array();
    }
}
