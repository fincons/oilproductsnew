<?php

namespace AppBundle\Command;

use AppBundle\Bean\ProcessResult;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class SynchWithEpicCommandWeb extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:synchWithEpic')
            ->setDescription('Synch with Epic');
    }

    // sample calling with options
    // php app/console eop:synchWithEpic --download_from_remote=true --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=mail1@test.it --process_notification_to_email=mail2@test.it --elaborate_current=false --max_num_of_archives_to_elaborate=1

    //php app/console eop:synchWithEpic --download_from_remote=false --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=davide.tresoldi@finconsgroup.com --elaborate_current=false --max_num_of_archives_to_elaborate=1

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Synch with Epic START ...");

        $logger = $this->getContainer()->get('monolog.logger.enioil_admin');

        $environment = 'PROD';
        $logger->info("Environment: " . $environment);

        $process_commands = 'cd ' . __DIR__ . '/../../.. && ' . $this->getContainer()->getParameter('SynchWithEpicCommandWeb') . " " . $environment;
        $logger->info("Process commands: " . $process_commands . " START " . "(" . date('Y-m-d H:i:s') . ")");

        $process_output = '';
        $return_value = '';
        exec($process_commands, $process_output, $return_value);
        $output->writeln($process_output);

        if ($return_value > 0) {
            throw new \Exception("The procedure didn't complete correctly.");
        }

        $output->writeln($process_commands . " background call END ...");

    }

}