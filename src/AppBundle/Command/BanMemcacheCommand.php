<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class BanMemcacheCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:banMemcache')
        ->setDescription('Ban memcache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Ban memcache START ...");
        $logger = $this->getContainer()->get('monolog.logger.enioil_admin');
        $logger->info("Ban memcache (NB: actually memcached isn't used yet...)");
        $output->writeln("Ban memcache END ...");
    }


}
