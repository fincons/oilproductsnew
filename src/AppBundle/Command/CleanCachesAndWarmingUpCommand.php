<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class CleanCachesAndWarmingUpCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:cleanCachesAndWarmingUp')
        ->setDescription('Clean Caches and Warming up')
        ->addArgument(
            'urls_file_path',
            InputArgument::REQUIRED,
            'Path of file with all urls to call'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Clean caches and warming up background call START ...");
        
        $logger = $this->getContainer()->get('monolog.logger.enioil_admin');
        
        $urls_file_path = $input->getArgument('urls_file_path');
        $logger->info("Urls file path: " . $urls_file_path);
        
        $process_commands = 'cd ' . __DIR__. '/../../.. && ' . $this->getContainer()->getParameter('cleanCachesAndWarmingUpCommand') . " " . $urls_file_path ;
        $logger->info("Process commands: " . $process_commands);
        
//         $process = new Process($process_commands);
//         $process->start();

        $process_output= '';
        $return_value = '';
        exec($process_commands,$process_output,$return_value);
        $output->writeln($process_output);
        
        if ($return_value >0) {
            throw new \Exception("The procedure didn't complete correctly.");            
        }

        $output->writeln("Clean caches and warming up background call END ...");

    }


}
