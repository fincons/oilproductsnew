<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyProductsCommand extends ContainerAwareCommand
{
    private $em;
    private $sendMail;

    protected function configure()
    {
        $this->setName('eop:notifyProducts')
        ->setDescription('Notify products without product classification row');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->sendMail = $this->getContainer()->get('mailer');
        $email = $this->getContainer()->getParameter('notify_not_classified_products_to_email');
        $output->writeln("Notify products command START ...");
        $notifyProducts = [];
        $products = $this->em->getRepository('AppBundle:Product')->findAll();
        // enabled == fromEpic
        foreach($products as $product) {
            if($product->getProductClassificationRows()->isEmpty() || !$product->getProductClassificationRows()->fromEpic()) {
                $notifyProducts[] = $product;
            }
        }
        $output->writeln(sprintf("Found %d products without classification.", count($notifyProducts)));
        $this->notify($notifyProducts, $email);
        $output->writeln(sprintf("Details has been sent via email to %s", implode(',', $email)));

        $output->writeln("Notify products command END ...");
    }

    public function notify($products, $email)
    {
        $templating = $this->getContainer()->get('templating');
        $body = $templating->render('AppBundle:email:notify.txt.twig', array('products' => $products));
        
        $env = $this->getContainer()->getParameter('kernel.environment');
        
        $message = \Swift_Message::newInstance()
                ->setSubject('[ENIOIL][' . $env . '] Not classified products notification')
                ->setFrom($this->getContainer()->getParameter('oilproducts_from_email'))
                ->setTo($email)
                ->setBody($body, 'text/html')
        ;
        
        $logger = $this->getContainer()->get('monolog.logger.enioil_admin');
        $logger->info("Sent not classified products notification to " . json_encode($email));

        $this->sendMail->send($message);
    }
}
