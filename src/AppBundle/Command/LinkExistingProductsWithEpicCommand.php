<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LinkExistingProductsWithEpicCommand extends ContainerAwareCommand {
	protected function configure() {
		$this->setName ( 'eop:linkExistingProductsWithEpic' )->setDescription( 'Link existing products with Epic' );
    }

    // sample calling with options
    // php app/console eop:linkExistingCategoriesWithEpic --type=product 
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
    		
    	$output->writeln("Link existing products with Epic START ...");
    	$this->logger = $this->getContainer()->get('monolog.logger.enioil_admin');  
    	$this->epicProductSynchronizer = $this->getContainer()->get('epic_product_synchronizer');
    	$this->epicProductSynchronizer->linkExistingProductsWithEpic();
    }

    

    
}


