<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class CallSiteUrlsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:callSiteUrls')
        ->setDescription('Call site urls')
        ->addArgument(
            'urls_file_path',
            InputArgument::REQUIRED,
            'Path of file with all urls to call'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Call site urls START ...");

        $logger = $this->getContainer()->get('monolog.logger.enioil_admin');
        $client   = $this->getContainer()->get('guzzle.client.enioil');
        
        $urls_file_path = $input->getArgument('urls_file_path');
        $logger->info("Reading file: " . $urls_file_path);
        
        $base_url = $this->getContainer()->getParameter('fe_base_url');
        $urls = file($urls_file_path, FILE_IGNORE_NEW_LINES);

        foreach ($urls as $url) {
            try {
                $url = 'http://' . $base_url . $url;
                $response = $client->get($url);
                $output->writeln('Called url: ' . $url . " with status code: " . $response->getStatusCode());
                $logger->info('Called url: ' . $url . " with status code: " . $response->getStatusCode());
            } catch (\Exception $ex) {
                $output->writeln('Called url: ' . $url . " with error: " . $ex->getMessage());
                $logger->info('Called url: ' . $url . " with error: " . $ex->getMessage());
            }
        }
        $output->writeln("Call site urls END ...");
    }




}
