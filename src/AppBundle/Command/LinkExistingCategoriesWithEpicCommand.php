<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LinkExistingCategoriesWithEpicCommand extends ContainerAwareCommand {
	protected function configure() {
		$this->setName ( 'eop:linkExistingCategoriesWithEpic' )->setDescription( 'Link existing categories with Epic' )
		->addOption ( 'type', null, InputOption::VALUE_REQUIRED, null, null );
    }

    // sample calling with options
    // php app/console eop:linkExistingCategoriesWithEpic --type=product 
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
    		
    	$output->writeln("Link existing categories with Epic START ...");
    	$this->logger = $this->getContainer()->get('monolog.logger.enioil_admin');  
    	$this->epicProductCategorySynchronizer = $this->getContainer()->get('epic_product_category_synchronizer');
    	$this->epicProductCategorySynchronizer->linkExistingCategoriesWithEpic();
    }

    

    
}


