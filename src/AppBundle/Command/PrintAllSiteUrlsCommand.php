<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class PrintAllSiteUrlsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:printAllSiteUrls')
        ->setDescription('Print all site urls on urls.txt file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Print all site urls command START ...");
        $this->printAllSiteUrls();
        $output->writeln("Print all site urls command END ...");
    }

    private function printAllSiteUrls() {
        $urlsFile = "urls.txt";
        $urls = $this->getContainer()->get('url_manager')->calculateAllSiteUrls("");
        foreach ($urls as $url) {
            file_put_contents($urlsFile, $url . "\n", FILE_APPEND | LOCK_EX);
        }
    }


}
