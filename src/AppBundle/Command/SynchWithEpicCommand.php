<?php

namespace AppBundle\Command;

use AppBundle\Bean\ProcessResult;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class SynchWithEpicCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:synchWithEpic')->setDescription('Synch with Epic')
            ->addOption('download_from_remote', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('elaborate_products', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('elaborate_product_classification', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('elaborate_area_classification', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('elaborate_current', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('max_num_of_archives_to_elaborate', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('commit', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('sequential_file_check', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('older_file_check', null, InputOption::VALUE_REQUIRED, null, null)
            ->addOption('notification_to_email', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, null, null);
    }

    // sample calling with options
    // php app/console eop:synchWithEpic --download_from_remote=true --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=mail1@test.it --process_notification_to_email=mail2@test.it --elaborate_current=false --max_num_of_archives_to_elaborate=1

    //php app/console eop:synchWithEpic --download_from_remote=false --elaborate_products=true --elaborate_product_classification=true --elaborate_area_classification=true --sequential_file_check=true --older_file_check=true --commit=true --notification_to_email=davide.tresoldi@finconsgroup.com --elaborate_current=false --max_num_of_archives_to_elaborate=1

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {

            $timeStart = microtime(true);

            $output->writeln("Synch with Epic START ...");
            $this->logger = $this->getContainer()->get('monolog.logger.enioil_admin');
            $this->epicUtils = $this->getContainer()->get('epic_utils');

            $processResult = new ProcessResult();
            $processResult->setSubject("Epic synchronization " . "(" . date('Y-m-d H:i:s') . ")");

            $this->epicUtils->checkEpicDirs();

            $this->updateEpicParametersUsingInputOptions($input);
            $this->epicUtils->logInfoAndAppendToProcessResult($processResult, $this->logger, "Started elaboration with these parameters: " . $this->epicUtils->printEpicParameters());

            $epicFileManager = $this->getContainer()->get('epic_file_manager');
            $epicFileManager->setProcessResult($processResult);
            $epicFileManager->downloadFiles($processResult);
           
            $epicDataSynchronizer = $this->getContainer()->get('epic_data_synchronizer');
            $epicDataSynchronizer->setProcessResult($processResult);
            $epicDataSynchronizer->synch($processResult,$output);
            
            $processResult->setResult("OK");

        } catch (\Exception $e) {

            $output->writeln("An exception happened!");
            $processResult->setResult("KO");
            $this->epicUtils->logErrorAndAppendToProcessResult($processResult, $this->logger, $e);
            $output->writeln($processResult->printExceptions());

        } finally {

            $timeEnd = microtime(true);
            $executionTime = ($timeEnd - $timeStart) / 60;
            $this->epicUtils->logInfoAndAppendToProcessResult($processResult, $this->logger, "Total execution time " . $executionTime . " minutes");

            $this->getContainer()->get('notifier')->notify($this->epicUtils->getEpicProcessNotificationToEmail(), $processResult->getSubjectWithResult(), $processResult->getAbstractAndMessageAndExceptions(), $processResult->getFiles(), 'text/plain');
            
            $this->getContainer()->get("UrlManager")->sitemapGenerator("");
            $output->writeln("Memory usage after SiteMaps Generation: ".round(memory_get_usage()/1048576.0) . " Mb");
            
            $output->writeln("See app log for more details ...");
            $output->writeln("Synch with Epic STOP ...");

        }

    }


    protected function updateEpicParametersUsingInputOptions($input)
    {
        $this->epicUtils->updateEpicDownloadFromRemote($input->getOption('download_from_remote'));
        $this->epicUtils->updateEpicElaborateProducts($input->getOption('elaborate_products'));
        $this->epicUtils->updateEpicElaborateProductClassification($input->getOption('elaborate_product_classification'));
        $this->epicUtils->updateEpicElaborateAreaClassification($input->getOption('elaborate_area_classification'));
        $this->epicUtils->updateEpicElaborateCurrent($input->getOption('elaborate_current'));
        $this->epicUtils->updateEpicMaxNumOfArchivesToElaborate($input->getOption('max_num_of_archives_to_elaborate'));
        $this->epicUtils->updateEpicCommit($input->getOption('commit'));
        $this->epicUtils->updateEpicSequentialFileCheck($input->getOption('sequential_file_check'));
        $this->epicUtils->updateEpicOlderFileCheck($input->getOption('older_file_check'));
        $this->epicUtils->updateEpicProcessNotificationToEmail($input->getOption('notification_to_email'));
    }


}


