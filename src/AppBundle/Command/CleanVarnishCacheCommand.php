<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanVarnishCacheCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('eop:cleanVarnishCache')
        ->setDescription('Clean Varnish Cache');
    }
    
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //stampa a video
        $output->writeln("Clean Varnish Cache START ...");      
        
        //stampa log in app/logs/
        $logger = $this->getContainer()->get('monolog.logger.enioil_admin');
        
        $process_commands = 'cd ' . __DIR__ . '/../../.. && ' . $this->getContainer()->getParameter('CleanVarnishCacheCommand');
        $logger->info("Process commands: " . $process_commands . " START " . "(" . date('Y-m-d H:i:s') . ")");
        
        $process_output = '';
        $return_value = '';
        
        exec($process_commands, $process_output, $return_value);
        
        $output->writeln($process_output);
        
        if ($return_value > 0) {
            throw new \Exception("The procedure didn't complete correctly.");
        }
        
        $output->writeln($process_commands . " background call END ...");
            
    }
    
    
}

