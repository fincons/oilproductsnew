<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class AbstractCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('abstract')
        ->setDescription('abstract');
    }

}
