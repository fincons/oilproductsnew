<?php

namespace AppBundle\Command;

use \Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetPdfFromAlisCommand extends ContainerAwareCommand
{
    private $em;

    protected function configure()
    {
        $this->setName('eop:getPdfFromAlis')
            ->setDescription('Get pdf from Alis');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Get pdf from Alis START ...");

        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $products = $this->em->getRepository('AppBundle:Product')->findAll();


        $csv_file = fopen(sys_get_temp_dir() . "/alis/report.csv","w");
        fputcsv($csv_file, array("PROG", "CODE", "TITLE", "PDF",  "URL_ID", "PDF_SAVED", "EN_PDF", "EN_URL_ID", "EN_PDF_SAVED", "IDS_DIFFERENT"));

        $unique_url_id = array();
        $en_unique_url_id = array();

        $i = 1;
        foreach($products as $product) {
            if($product->getFromAlis() == 1) {

                $output->writeln("Elaborating product $i with code " . $product->getCode());

                $url  = $product->getPdf();
                $url_id = str_replace("https://nalis.eni.it/Alis/public/MainServlet?ACTION=SHOW_PDF&HidIdAllegato=","", $url);
                $url_id = str_replace("&HidLangAll=IT","", $url_id);


                $en_url = self::get_en_url($product, $this->em);
                $en_url_id = str_replace("https://nalis.eni.it/Alis/public/MainServlet?ACTION=SHOW_PDF&HidIdAllegato=","", $en_url);
                $en_url_id = str_replace("&HidLangAll=EN","", $en_url_id);
                $en_url_id = str_replace("&HidLangAll=IT","", $en_url_id);

                $diff_id = 0;
                if ($url_id != $en_url_id) {
                    $diff_id = 1;
                }

                $url_id = $url_id . "-" .  substr($url, -2) ;
                $unique_url_id[] = $url_id;

                $en_url_id = $en_url_id . "-" .  substr($en_url, -2) ;
                $en_unique_url_id[] = $en_url_id;

                $filename = "";
                if ($url) {
                    $filename = self::save_alis_file($url, $url_id, $output);
                }

                $en_filename = "";
                if ($en_url) {
                    $en_filename = self::save_alis_file($en_url, $en_url_id, $output);
                }

                fputcsv($csv_file, array($i, $product->getCode(), $product->getTitle(), $url, $url_id, $filename, $en_url, $en_url_id, $en_filename, $diff_id));

                $i++;

            }
        }

        fclose($csv_file);

        $output->writeln("Different unique url id " . count(array_unique($unique_url_id)));

        $output->writeln("Different en unique url id " . count(array_unique($en_unique_url_id)));

        $output->writeln("Get pdf from Alis END ...");

    }


    public static function get_en_url($product, $em) {
        $url = "";
        $productTranslations = $em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object'=> $product->getId(),
                'locale' => 'en_GB',
                'field' => 'PDF',
            ));

        foreach ($productTranslations as $productTranslation) {
            $url = $productTranslation->getContent();
        }

        return $url;
    }

    public static function save_alis_file($url, $url_id, $output) {

        $filename = "";
        $tmp_alis_dir = sys_get_temp_dir() . "/alis";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_COOKIEJAR, sys_get_temp_dir() . 'cookies.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, sys_get_temp_dir() . 'cookies.txt');

        $headerBuff = fopen(sys_get_temp_dir() . '/headers', 'w+');

        curl_setopt($ch, CURLOPT_WRITEHEADER, $headerBuff);

        $data = curl_exec($ch);

        if(!curl_errno($ch)) {
            rewind($headerBuff);

            $headers = stream_get_contents($headerBuff);

            $filename = self::get_filename_from_curl_response($headers);
        }

        curl_close($ch);

        $result = true;

        if ($filename) {
            $filename = $url_id . "-" . $filename;
            $path = $tmp_alis_dir . "/" . $filename;
            $result = file_put_contents($path, $data);
        }

        if(!$result){
            throw new Exception("Error saving file");
        }

        return $filename;
    }

    public static function get_filename_from_curl_response($response)
    {
        $header_text = $response;
        foreach (explode("\r\n", $header_text) as $i => $line) {
            $query = "Content-Disposition: attachment; filename=";
            if (substr($line, 0, strlen($query)) === $query) {
                $filename = str_replace($query,"", $line);
                $filename = str_replace('"',"", $filename);
            }
        }
        return $filename;
    }



}
