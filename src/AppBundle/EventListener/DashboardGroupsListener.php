<?php

namespace AppBundle\EventListener;

use AppBundle\Event\DashboardGroupsEvent;

class DashboardGroupsListener
{
    public function addDashboardGroup(DashboardGroupsEvent $event) {
        $items = array(
            array(
                'label' => 'Maintenance',
                'roles' => array('ROLE_ADMIN'),
                'items' => array(
                    array(
                        'label' => 'Run Script',
                        'route' => 'maintenance',
                        'icon'  => 'fa fa-cogs'
                    ),
                ),
            ),
        );

        $event->addGroup('Maintenance', $items, array('icon'  => '<i class="fa fa-cogs"></i>'));
    }
}
