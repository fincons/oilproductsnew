<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\BaseBlock;

/**
 * BlockContentListener
 */
class BlockContentListener
{
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof BaseBlock) {
            return;
        }

        $entity->loadContent($args->getEntityManager());
    }
}
