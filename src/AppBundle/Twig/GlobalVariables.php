<?php

namespace AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Event\DashboardGroupsEvent;

class GlobalVariables
{
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Set menu & dashboard custom groups.
     * Example return value: array(
            'Statistiche' => array(
                'label' => 'Statistiche',
                'icon'  => '<i class="fa fa-gift"></i>',
                'roles' => array(),
                'items' => array(
                        array(
                        'label' => 'Codici Promo',
                        'roles' => array(),
                        'items' => array(
                            array(
                                'label' => 'Lista',
                                'route' => 'admin_promo_list',
                            ),
                            array(
                                'label' => 'Carica nuovi',
                                'route' => 'admin_promo_upload',
                                'icon'  => 'fa fa-upload'
                            ),
                        ),
                    ),
                ),
            ),
        );
     * @return array
     */
    public function getCustomGroups()
    {
        $event = new DashboardGroupsEvent();
        $this->container->get('event_dispatcher')->dispatch(DashboardGroupsEvent::EVENT, $event);

        return $event->getGroups();
    }
}
