<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    private $urlManager;

    public function __construct($urlManager)
    {
        $this->urlManager = $urlManager;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('createUrlFromLink', array($this, 'createUrlFromLink')),
        );
    }

    public function createUrlFromLink($link)
    {
        $page = $link->getPage();
        if ($page) {
            $url = $this->urlManager->calculateUrlFromPage($page);
        } else {
            $url = $link->getUrl();
        }

        return $url;
    }

    public function getName()
    {
        return 'app_extension';
    }
}
