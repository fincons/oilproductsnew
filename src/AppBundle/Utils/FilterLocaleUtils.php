<?php
namespace AppBundle\Utils;

class FilterLocaleUtils{
	
	public static function linkEnabledForLocale($lang_permitted, $current_locale){
		return (strpos ( $lang_permitted, $current_locale ) !== false
				|| strpos ( $lang_permitted, 'ALL' ) !== false);
	}
	
}
