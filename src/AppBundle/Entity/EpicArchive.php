<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EpicArchiveRepository")
 * @ORM\Table(name="epic_archive")
 * @UniqueEntity("archiveName")
 */
class EpicArchive
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique = true)
     */
    private $archiveName;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $downloadedAt;
    
	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */


	private $productElaborationAt;
	
	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $productClassificationElaborationAt;
	
	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $areaClassificationElaborationAt;


    public function getId() {
        return $this->id;
    }

    public function setArchiveName($archiveName) {
        $this->archiveName = $archiveName;
    }

    public function getArchiveName() {
        return $this->archiveName;
    }

    public function setProductElaborationAt(\DateTime $elaboratedAt)
    {
    	$this->productElaborationAt = $elaboratedAt;
    }
    
    public function getProductElaborationAt()
    {
    	return $this->productElaborationAt;
    }
    
    public function setProductClassificationElaborationAt(\DateTime $elaboratedAt)
    {
    	$this->productClassificationElaborationAt = $elaboratedAt;
    }
    
    public function getProductClassificationElaborationAt()
    {
    	return $this->productClassificationElaborationAt;
    }
    
    public function setAreaClassificationElaborationAt(\DateTime $elaboratedAt)
    {
    	$this->areaClassificationElaborationAt = $elaboratedAt;
    }
    
    public function getAreaClassificationElaborationAt()
    {
    	return $this->areaClassificationElaborationAt;
    }
    
    public function setDownloadedAt(\DateTime $downloadedAt)
    {
    	$this->downloadedAt = $downloadedAt;
    }
    
    public function getDownloadedAt()
    {
    	return $this->downloadedAt;
    }    
    
    public function __toString()
    {
    	return $this->getArchiveName() . ", downloadedAt = " . ($this->getDownloadedAt() !=null ? $this->getDownloadedAt()->format('Y-m-d H:i:s') : ""). ", productElaborationAt = " . ($this->getProductElaborationAt() !=null ? $this->getProductElaborationAt()->format('Y-m-d H:i:s') : "")
    	. ", productClassificationElaborationAt = " . ($this->getProductClassificationElaborationAt() !=null ? $this->getProductClassificationElaborationAt()->format('Y-m-d H:i:s') : "") . ", areaClassificationElaborationAt = " . ($this->getAreaClassificationElaborationAt() !=null ? $this->getAreaClassificationElaborationAt()->format('Y-m-d H:i:s') : "");
    }
    
}
