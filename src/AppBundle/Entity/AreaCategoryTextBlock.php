<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\AreaCategoryBlock;
use AppBundle\Entity\Blocks\TextTrait;

/**
 * @ORM\Entity
 */
class AreaCategoryTextBlock extends AreaCategoryBlock
{
    use TextTrait;

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:text-default.html.twig';
    }
}
