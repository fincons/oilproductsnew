<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="tab")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\TabTranslation")
 */
class Tab extends BaseTab
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Page", inversedBy="tabs")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $page;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\Block",
     *      mappedBy="tab",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $blocks;
}
