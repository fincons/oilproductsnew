<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="link")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\LinkTranslation")
 */
class Link extends BaseEntity
{

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $label;

    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $url;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\LinkGroup", inversedBy="links")
     * @ORM\JoinColumn(nullable=false)
     */
    private $linkGroup;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Page", inversedBy="links")
     * @ORM\JoinColumn(nullable=true)
     */
    private $page;    
    
    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $type;
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $format;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;
    
    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    protected $language;
    
    public function getLanguage()
    {
    	return $this->language;
    }
    
    public function setLanguage($language)
    {
    	$this->language = $language;
    }    
    
    public function setType($type)
    {
    	$this->type = $type;
    }
    
    public function getType()
    {
    	return $this->type;
    }    
    
    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }
    
    public function setUrl($url)
    {
    	$this->url = $url;
    }
    
    public function getUrl()
    {
    	return $this->url;
    } 

    public function getLinkGroup()
    {
    	return $this->linkGroup;
    }
    
    public function setLinkGroup(LinkGroup $linkGroup)
    {
    	$this->linkGroup = $linkGroup;
    }
    
    public function getPage()
    {
    	return $this->page;
    }
    
    public function setPage(Page $page = null)
    {
    	$this->page = $page;
    }    
    
    public function setFormat($format)
    {
        $this->format = $format;
    }
    
    public function getFormat()
    {
        return $this->format;
    }
    
    public function __toString()
    {
    	return $this->getLabel() ?: $this->getType().':'. ($this->getPage() != null ? $this->getPage(): $this->getUrl());
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }
    
    public function getPosition()
    {
        return $this->position;
    }
        

}