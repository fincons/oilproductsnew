<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AreaClassificationRowRepository")
 * @ORM\Table(name="area_classification_row")
 */
class AreaClassificationRow extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="areaClassificationRowsLev2")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $areaLev2;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="areaClassificationRowsLev3")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $areaLev3;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="areaClassificationRowsLev4")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $areaLev4;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="areaClassificationRowsLev5")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $areaLev5;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="areaClassificationRowsLev6")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $areaLev6;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="areaClassificationRows")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $product;

    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $fromEpic;
    
    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    protected $language;
    
    public function getLanguage()
    {
        return $this->language;
    }
    
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function __construct()
    {
        $this->published = true;
    }

    public function setAreaLev2($areaLev2)
    {
        $this->areaLev2 = $areaLev2;
    }

    public function getAreaLev2()
    {
        return $this->areaLev2;
    }

    public function setAreaLev3($areaLev3)
    {
        $this->areaLev3 = $areaLev3;
    }

    public function getAreaLev3()
    {
        return $this->areaLev3;
    }

    public function setAreaLev4($areaLev4)
    {
        $this->areaLev4 = $areaLev4;
    }

    public function getAreaLev4()
    {
        return $this->areaLev4;
    }

    public function setAreaLev5($areaLev5)
    {
        $this->areaLev5 = $areaLev5;
    }

    public function getAreaLev5()
    {
        return $this->areaLev5;
    }

    public function setAreaLev6($areaLev6)
    {
        $this->areaLev6 = $areaLev6;
    }

    public function getAreaLev6()
    {
        return $this->areaLev6;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setProduct($product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setFromEpic($fromEpic)
    {
        $this->fromEpic = $fromEpic;
    }

    public function getFromEpic()
    {
        return $this->fromEpic;
    }

    public function __toString()
    {
        return "PCR " . $this->getId();
    }


}
