<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Blocks\SliderTrait;
use AppBundle\Entity\BusinessSolutionBlock;

/**
 * @ORM\Entity
 */
class BusinessSolutionSliderBlock extends BusinessSolutionBlock
{
    use SliderTrait;

    public function __construct()
    {
        $this->slider = new ArrayCollection();
    }

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:slider-default.html.twig';
    }
}
