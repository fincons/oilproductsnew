<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\ProductCategory;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductClassificationRowRepository")
 * @ORM\Table(name="product_classification_row")
 */
class ProductClassificationRow extends BaseEntity
{

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="productClassificationRowsLev2")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $productCategoryLev2;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="productClassificationRowsLev3")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $productCategoryLev3;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="productClassificationRowsLev4")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $productCategoryLev4;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="productClassificationRowsLev5")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $productCategoryLev5;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="productClassificationRows")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $product;

    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $fromEpic;
    
    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    protected $language;

    public function getLanguage()
    {
        return $this->language;
    }
    
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function __construct()
    {
        $this->published = true;
    }

    public function setProductCategoryLev2($productCategoryLev2)
    {
        $this->productCategoryLev2 = $productCategoryLev2;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategoryLev2()
    {
        return $this->productCategoryLev2;
    }

    public function setProductCategoryLev3($productCategoryLev3)
    {
        $this->productCategoryLev3 = $productCategoryLev3;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategoryLev3()
    {
        return $this->productCategoryLev3;
    }

    public function setProductCategoryLev4($productCategoryLev4)
    {
        $this->productCategoryLev4 = $productCategoryLev4;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategoryLev4()
    {
        return $this->productCategoryLev4;
    }

    public function setProductCategoryLev5($productCategoryLev5)
    {
        $this->productCategoryLev5 = $productCategoryLev5;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategoryLev5()
    {
        return $this->productCategoryLev5;
    }

    public function setProduct($product)
    {
        $this->product = $product;
    }

    /** @return Product */
    public function getProduct()
    {
        return $this->product;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setFromEpic($fromEpic)
    {
        $this->fromEpic = $fromEpic;
    }

    public function getFromEpic()
    {
        return $this->fromEpic;
    }

    public function __toString()
    {
        return "PCR " . $this->getId();
    }


}