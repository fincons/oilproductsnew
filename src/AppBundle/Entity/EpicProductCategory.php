<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EpicProductCategoryRepository")
 * @ORM\Table(name="epicKey_product_category")
 * @UniqueEntity("epicKey")
 */
class EpicProductCategory extends BaseEntity
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique = true, nullable=false)
     */
    protected $epicKey;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="epicProductCategory")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Column(name="productCategory_id")
     */
    protected $productCategoryid;

    /**
     * @ORM\Column(type="string", length=255, unique = true, nullable=false)
     */
    protected $published;

    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEpicKey()
    {
        return $this->epicKey;
    }

    /**
     * @param mixed $epicKey
     */
    public function setEpicKey($epicKey)
    {
        $this->epicKey = $epicKey;
    }

    /**
     * @return mixed
     */
    public function getProductCategoryId()
    {
        return $this->productCategoryid;
    }

    /**
     * @param mixed $productCategoryid
     */
    public function setProductCategoryId($productCategoryid)
    {
        $this->productCategoryid = $productCategoryid;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }
}
