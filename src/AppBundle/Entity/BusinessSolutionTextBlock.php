<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BusinessSolutionBlock;
use AppBundle\Entity\Blocks\TextTrait;

/**
 * @ORM\Entity
 */
class BusinessSolutionTextBlock extends BusinessSolutionBlock
{
    use TextTrait;

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:text-default.html.twig';
    }
}
