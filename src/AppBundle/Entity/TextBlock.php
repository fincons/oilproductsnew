<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Block;
use AppBundle\Entity\Blocks\TextTrait;

/**
 * @ORM\Entity
 */
class TextBlock extends Block
{
    use TextTrait;

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:text-default.html.twig';
    }
}
