<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Block;
use AppBundle\Entity\Blocks\EmailTrait;

/**
 * @ORM\Entity
 */
class EmailBlock extends Block
{
    use EmailTrait;

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:email-default.html.twig';
    }
}
