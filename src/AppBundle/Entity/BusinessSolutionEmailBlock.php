<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BusinessSolutionBlock;
use AppBundle\Entity\Blocks\EmailTrait;

/**
 * @ORM\Entity
 */
class BusinessSolutionEmailBlock extends BusinessSolutionBlock
{
    use EmailTrait;

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:email-default.html.twig';
    }
}
