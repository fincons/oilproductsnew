<?php

namespace AppBundle\Entity\Blocks;

use AppBundle\Entity\BrochureRow;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

trait BrochureTrait
{
	protected $docs;
	
	public function addDoc($doc)
	{
		$this->docs->add($doc);
		$this->saveContent();
	}
	
	public function removeDoc($doc)
	{
		$this->docs->removeElement($doc);
		$this->saveContent();
	}
	
	public function getDocs()
	{
		return $this->docs;
	}
	
	public function saveContent()
	{
		$content = ['docs' => []];
	
		foreach($this->docs as $docs) {
			$docsContent = [];
			if($docs->getText()) {
				$docsContent['text'] = $docs->getText();
			}
			if($docs->getPdf()) {
				$docsContent['pdf'] = $docs->getPdf()->getId();
			}
			if($docs->getPdfIcon()) {
				$docsContent['pdfIcon'] = $docs->getPdfIcon()->getId();
			}
			$content['docs'][] = $docsContent;
		}
	
		$this->content = $content;
	}
	
	public function loadContent(EntityManagerInterface $em)
	{
		$this->docs = new ArrayCollection();
		if (isset($this->content['docs'])) { 
			foreach($this->content['docs'] as $content) {
				$docs = new BrochureRow();
				if (isset($content['pdf'])) {
					$docs->setPdf($em->getRepository(Media::class)->find($content['pdf']));
				}
				if (isset($content['pdfIcon'])) {
					$docs->setPdfIcon($em->getRepository(Media::class)->find($content['pdfIcon']));
				}
				if (isset($content['text'])) {
					$docs->setText($content['text']);
				}
		
				$this->docs->add($docs);
				$docs->setBlock($this);
			}
		}
	}
}
