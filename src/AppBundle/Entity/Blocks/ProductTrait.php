<?php

namespace AppBundle\Entity\Blocks;

use Doctrine\ORM\EntityManagerInterface;

trait ProductTrait
{
    public function saveContent()
    {
        $this->content = array();
    }

    public function loadContent(EntityManagerInterface $em)
    {
    }
}
