<?php

namespace AppBundle\Entity\Blocks;

use Doctrine\ORM\EntityManagerInterface;

trait EmailTrait
{
    protected $text;
    protected $email;

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        $this->saveContent();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        $this->saveContent();
    }

    public function saveContent()
    {
        $this->content = array('text' => $this->getText(), 'email' => $this->getEmail());
    }

    public function loadContent(EntityManagerInterface $em)
    {
        if (isset($this->content['text'])) {
            $this->text = $this->content['text'];
        }
        if (isset($this->content['email'])) {
            $this->email = $this->content['email'];
        }
    }
}
