<?php

namespace AppBundle\Entity\Blocks;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Link;
use AppBundle\Entity\SliderRow;
use Application\Sonata\MediaBundle\Entity\Media;

trait SliderTrait
{
    protected $slider;
    protected $text;

    public function addSlider($slider)
    {
        $this->slider->add($slider);
        $this->saveContent();
    }

    public function removeSlider($slider)
    {
        $this->slider->removeElement($slider);
        $this->saveContent();
    }

    public function getSlider()
    {
        return $this->slider;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        $this->saveContent();
    }

    public function saveContent()
    {
        $content = ['slider' => []];

        foreach($this->slider as $slider) {
            $sliderContent = [];
            if($slider->getLink()) {
                $sliderContent['link'] = $slider->getLink()->getId();
            }
            if($slider->getImage()) {
                $sliderContent['image'] = $slider->getImage()->getId();
            }
            $content['slider'][] = $sliderContent;
        }

        $content['text'] = $this->text;

        $this->content = $content;
    }

    public function loadContent(EntityManagerInterface $em)
    {
        $this->slider = new ArrayCollection();
        foreach($this->content['slider'] as $content) {
            $slider = new SliderRow();
            if (isset($content['image'])) {
                $slider->setImage($em->getRepository(Media::class)->find($content['image']));
            }
            if (isset($content['link'])) {
                $slider->setLink($em->getRepository(Link::class)->find($content['link']));
            }

            $this->slider->add($slider);
            $slider->setBlock($this);
        }

        if(isset($this->content['text'])) {
            $this->text = $this->content['text'];
        }
    }
}
