<?php

namespace AppBundle\Entity\Blocks;

use Doctrine\ORM\EntityManagerInterface;

trait TextTrait
{
    protected $text;

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        $this->saveContent();
    }

    public function saveContent()
    {
        $this->content = array('text' => $this->getText());
    }

    public function loadContent(EntityManagerInterface $em)
    {
        if (isset($this->content['text'])) {
            $this->text = $this->content['text'];
        }
    }
}
