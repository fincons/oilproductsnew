<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * @ORM\MappedSuperclass
 */
class BaseEntity extends AbstractPersonalTranslatable implements TranslatableInterface
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $createdAt;

	/**
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $updatedAt;
	
	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	protected $published = false;

	
	public function getId()
	{
		return $this->id;
	}

	public function setCreatedAt(\DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
	}

	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	public function setUpdatedAt(\DateTime $updatedAt)
	{
	    $this->updatedAt = $updatedAt;
	}
	
	public function getUpdatedAt()
	{
	    return $this->updatedAt;
	}
	
	public function setPublished($published)
	{
	    $this->published = $published;
	}
	
	public function getPublished()
	{
	    return $this->published;
	}
	
	// TODO: use a listener to set this value from parameters
	protected function getDefaultLocale() {
	    return "it_IT";
	}
	
	protected function getCurrentLocale($locale = null) {
	    if ($locale == null) {
	        $locale = $this->getLocale();
	    }
	    if ($locale == null) {
	        $locale = $this->getDefaultLocale();
	    }
	    return $locale;
	}



}