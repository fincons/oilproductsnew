<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="block")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"text"="AppBundle\Entity\TextBlock","email"="AppBundle\Entity\EmailBlock", "brochure"="AppBundle\Entity\BrochureBlock","slider"="AppBundle\Entity\SliderBlock"})
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\BlockTranslation")
 */
abstract class Block extends BaseBlock
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tab", inversedBy="blocks")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $tab;
}
