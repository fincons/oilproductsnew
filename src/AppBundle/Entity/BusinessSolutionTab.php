<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseTab;

/**
 * @ORM\Entity
 * @ORM\Table(name="business_solution_tab")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\BusinessSolutionTabTranslation")
 */
class BusinessSolutionTab extends BaseTab
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BusinessSolution", inversedBy="tabs")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $page;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\BusinessSolutionBlock",
     *      mappedBy="tab",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $blocks;
}
