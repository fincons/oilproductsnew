<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EpicAreaCategoryRepository")
 * @ORM\Table(name="epicKey_area_category")
 * @UniqueEntity(fields={"epicKey", "areaCategoryid"})
 */
class EpicAreaCategory extends BaseEntity
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique = true, nullable=false)
     */
    protected $epicKey;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="epicAreaCategory")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Column(name="areaCategory_id")
     */
    protected $areaCategoryid;

    /**
     * @ORM\Column(type="string", length=255, unique = true, nullable=false)
     */
    protected $published;

    public function __construct()
    {

    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEpicKey()
    {
        return $this->epicKey;
    }

    /**
     * @param mixed $epicKey
     */
    public function setEpicKey($epicKey)
    {
        $this->epicKey = $epicKey;
    }

    /**
     * @return mixed
     */
    public function getAreaCategoryId()
    {
        return $this->areaCategoryid;
    }

    /**
     * @param mixed $areaCategory_id
     */
    public function setAreaCategoryId($areaCategory_id)
    {
        $this->areaCategoryid = $areaCategory_id;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }


}
