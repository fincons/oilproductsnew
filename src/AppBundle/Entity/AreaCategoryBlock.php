<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseBlock;

/**
 * @ORM\Entity
 * @ORM\Table(name="area_category_block")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"text"="AppBundle\Entity\AreaCategoryTextBlock","email"="AppBundle\Entity\AreaCategoryEmailBlock", "slider"="AppBundle\Entity\AreaCategorySliderBlock", "brochure"="AppBundle\Entity\AreaCategoryBrochureBlock"})
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\AreaCategoryBlockTranslation")
 */
abstract class AreaCategoryBlock extends BaseBlock
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategoryTab", inversedBy="blocks")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $tab;
}
