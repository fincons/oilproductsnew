<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class BaseLocalized
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", length=6, nullable=false)
	 */
	protected $locale;
	
	public function getId()
	{
	    return $this->id;
	}
	

	public function setLocale($locale)
	{
	    $this->locale = $locale;
	}
	
	public function getLocale()
	{
	    return $this->locale;
	}



}