<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactGroupTranslationRepository")
 * @ORM\Table(name="contact_group_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_contact_group_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class ContactGroupTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContactGroup", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}