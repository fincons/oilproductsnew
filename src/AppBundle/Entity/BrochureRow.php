<?php

namespace AppBundle\Entity;

class BrochureRow
{

    protected $text;
    protected $pdf;
    protected $pdfIcon;
    protected $block;
    
    public function getText()
    {
    	return $this->text;
    }
    
    public function setText($text)
    {
    	$this->text = $text;
    	if ($this->block) {
    		$this->block->saveContent();
    	}
    }
    
    public function getPdf()
    {
    	return $this->pdf;
    }
    
    public function setPdf($pdf)
    {
    	$this->pdf = $pdf;
    	if ($this->block) {
    		$this->block->saveContent();
    	}
    }

    public function getPdfIcon()
    {
    	return $this->pdfIcon;
    }

    public function setPdfIcon($pdfIcon)
    {
    	$this->pdfIcon = $pdfIcon;
    	if ($this->block) {
    		$this->block->saveContent();
    	}
    }


    public function setBlock($block) {
        $this->block = $block;
    }
}
