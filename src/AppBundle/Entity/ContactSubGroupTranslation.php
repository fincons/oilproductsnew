<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactSubGroupTranslationRepository")
 * @ORM\Table(name="contact_sub_group_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_contact_group_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class ContactSubGroupTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContactSubGroup", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}