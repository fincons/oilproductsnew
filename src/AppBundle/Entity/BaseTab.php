<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseBlock;
use AppBundle\Entity\BaseEditorialPage;

/**
 * @ORM\MappedSuperclass
 */
class BaseTab extends BasePage
{

    const LINK_GROUP_TYPE = 'tab_link';
    
    /**
     * @var ArrayCollection
     */
    protected $blocks;

    /**
     * @var Page
     */
    protected $page;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Link")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $link;
    
    /**
     * @var integer
     * @ORM\Column(name="position", type="integer")
     */
    protected $position = 0;


    public function __construct() {
        $this->published = true;
        $this->blocks = new ArrayCollection();
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function getPage()
    {
    	return $this->page;
    }

    public function setPage(BaseEditorialPage $page)
    {
    	$this->page = $page;
    }
    
    public function getLink()
    {
        return $this->link;
    }
    
    public function setLink($link)
    {
        $this->link = $link;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Add block
     *
     * @param \AppBundle\Entity\BaseBlock $block
     *
     * @return BaseTab
     */
    public function addBlock(BaseBlock $block)
    {
        $this->blocks[] = $block;
        $block->setTab($this);

        return $this;
    }

    /**
     * Remove block
     *
     * @param \AppBundle\Entity\BaseBlock $block
     */
    public function removeBlock(BaseBlock $block)
    {
        $this->blocks->removeElement($block);
    }

    /**
     * Get blocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlocks()
    {
        return $this->blocks;
    }
}
