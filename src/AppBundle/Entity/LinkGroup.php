<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="link_group")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\LinkGroupTranslation")
 */
class LinkGroup extends BaseEntity
{

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $label;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $type;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity="Link",
     *      mappedBy="linkGroup",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $links;
    
    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setType($type)
    {
    	$this->type = $type;
    }
    
    public function getType()
    {
    	return $this->type;
    } 
    
    public function getLinks()
    {
    	return $this->links;
    }
    
    public function addLink(Link $link)
    {
    	$this->links->add($link);
    	$link->setLinkGroup($this);
    }
    
    public function removeLink(Link $link)
    {
    	$this->links->removeElement($link);
    }  
    
    public function __toString()
    {
    	return $this->getType() ?: 'n/a';
    }

}