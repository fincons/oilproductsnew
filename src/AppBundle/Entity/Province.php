<?php

namespace AppBundle \Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="static_province")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProvinceRepository")
 */
class Province
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $short_name
     *
     * @ORM\Column(name="short_name", type="string", length=2)
     */
    private $shortName;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set short name
     *
     * @param string $short_name
     */
    public function setShortName($shortName) {
        $this->shortName = $shortName;
    }

    /**
     * Get short name
     *
     * @return string
     */
    public function getShortName() {
        return $this->shortName;
    }

    public function __toString() {
        return $this->name;
    }
}
