<?php

namespace AppBundle\Entity;

use AppBundle\Entity\BasePage;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseTab;


/**
 * @ORM\MappedSuperclass
 */
class BaseEditorialPage extends BasePage
{

    public function getTabs()
    {
        return $this->tabs;
    }
    
    public function addTab(BaseTab $tab)
    {
        $this->tabs->add($tab);
        $tab->setPage($this);
    }
    
    public function removeTab(BaseTab $tab)
    {
        $this->tabs->removeElement($tab);
    }
    
}
