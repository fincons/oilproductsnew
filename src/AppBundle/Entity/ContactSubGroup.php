<?php

namespace AppBundle\Entity;

use Application\Sonata\MediaBundle\PHPCR\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactSubGroupRepository")
 * @ORM\Table(name="contact_sub_group")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\ContactSubGroupTranslation")
 */
class ContactSubGroup extends BaseEntity
{

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=true)
     */

    protected $iconName;

    /**
     * @Assert\Email
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Contact",
     *      mappedBy="contactSubGroup",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @var ArrayCollection
     */
    private $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->published = true;
    }


    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact)
    {
        $this->contacts->add($contact);
        $contact->setContactGroup($this);
    }

    public function removeContact(Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getIconName()
    {
        return $this->iconName;
    }

    /**
     * @param string $iconName
     */
    public function setIconName($iconName)
    {
        $this->iconName = $iconName;
    }


    public function __toString()
    {
        return $this->getName() ?: 'n/a';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }


}