<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media;
use AppBundle\Entity\BaseLocalized;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_category_localized")
 */
class ProductCategoryLocalized extends BaseLocalized
{

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="productCategoriesLocalized", cascade={"persist"} )
     * @ORM\JoinColumn(nullable=false)
     */
    private $productCategory;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $brochure;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $brochureIcon;

    public function getBrochure()
    {
        return $this->brochure;
    }
    
    public function setBrochure(Media $brochure = null)
    {
        $this->brochure = $brochure;
    }
    
    public function getBrochureIcon()
    {
    	return $this->brochureIcon;
    }

    public function setBrochureIcon(Media $brochureIcon = null)
    {
    	$this->brochureIcon = $brochureIcon;
    }

    public function setProductCategory($productCategory)
    {
        $this->productCategory = $productCategory;
    }
    
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    

}