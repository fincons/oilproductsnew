<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Block;
use AppBundle\Entity\Blocks\SliderTrait;

/**
 * @ORM\Entity
 */
class SliderBlock extends Block
{
    use SliderTrait;

    public function __construct()
    {
        $this->slider = new ArrayCollection();
    }
    
    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:slider-default.html.twig';
    }
}
