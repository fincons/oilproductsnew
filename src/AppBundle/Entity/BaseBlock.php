<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseTab;

 /**
  * @ORM\MappedSuperclass
  */
abstract class BaseBlock extends BaseEntity
{
    /**
     * @var BaseTab
     */
    protected $tab;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="json", columnDefinition="TEXT")
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $template;

    /**
     * @var integer
     * @ORM\Column(name="position", type="integer")
     */
    protected $position = 0;


    public function __construct() {
        $this->published = true;
    }

    /**
     * @param $content
     * @return void
     */
    public function setContent($content)
    {
    	$this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
    	return $this->content;
    }

    /**
     * @param $template
     * @return void
     */
    public function setTemplate($template)
    {
    	$this->template = $template;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
    	return $this->template ?: $this->getDefaultTemplate();
    }

    public function getTab()
    {
    	return $this->tab;
    }

    public function setTab(BaseTab $tab)
    {
    	$this->tab = $tab;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    abstract public function loadContent(EntityManagerInterface $em);

    abstract public function saveContent();

    abstract protected function getDefaultTemplate();
    
    public function getType() {
        return null;
    }
}
