<?php

namespace AppBundle\Entity;

use AppBundle\Entity\BaseEditorialPage;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BusinessSolutionRepository")
 * @ORM\Table(name="business_solution")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\BusinessSolutionTranslation")
 */
class BusinessSolution extends BaseEditorialPage
{
    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $bannerImage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $previewImage;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\BusinessSolutionTab",
     *      mappedBy="page",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $tabs;
    
    /**
     * @ORM\Column(name="include_concessionary_tool", type="boolean", nullable=false, options={"default"="0"})
     */
    protected $includeConcessionaryToolSection = false;
    
    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    protected $language;
    
    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=false, options={"default"="1"})
     */
    protected $enabled = "1";
    
    
    public function getEnabled()
    {
        return $this->enabled;
    }

    
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function setPosition($position)
    {
    	$this->position = $position;
    }
    
    public function getPosition()
    {
    	return $this->position;
    }
    
    public function getLanguage()
    {
    	return $this->language;
    }
    
    public function setLanguage($language)
    {
    	$this->language = $language;
    }    

    public function __construct()
    {
        $this->tabs = new ArrayCollection();
        $this->published = true;
    }

    public function __toString()
    {
    	return $this->getTitle() ?: 'n/a';
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setBannerImage(Media $bannerImage = null)
    {
        $this->bannerImage = $bannerImage;
    }
    
    public function getBannerImage()
    {
        return $this->bannerImage;
    }
    
    public function setPreviewImage(Media $previewImage = null)
    {
        $this->previewImage = $previewImage;
    }

    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    public function setIncludeGplToolSection($includeGplToolSection)
    {
        $this->includeGplToolSection = $includeGplToolSection;
    }
    
    public function getIncludeGplToolSection()
    {
        return $this->includeGplToolSection;
    }
    
    public function setIncludeConcessionaryToolSection($includeConcessionaryToolSection)
    {
        $this->includeConcessionaryToolSection = $includeConcessionaryToolSection;
    }
    
    public function getIncludeConcessionaryToolSection()
    {
        return $this->includeConcessionaryToolSection;
    }

}
