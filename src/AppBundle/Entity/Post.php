<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BasePage;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\Table(name="post")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\PostTranslation")
 */
class Post extends BasePage
{

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $subtitle;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $abstract;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $type;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $previewImage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $detailImage;


    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    protected $language;


    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="LONGTEXT", nullable=true)
     */
    protected $main;
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $referenceDate;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $highlight;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $showInHomePage;



    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function __construct()
    {
        $this->highlight = false;
        $this->showInHomePage = false;
    }
    
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }
    
    public function getSubtitle()
    {
        return $this->subtitle;
    }    
    
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;
    }

    public function getAbstract()
    {
        return $this->abstract;
    }
    
    public function setType($type)
    {
    	$this->type = $type;
    }
    public function getType()
    {
    	return $this->type;
    }
    
    public function setPreviewImage(Media $previewImage = null)
    {
        $this->previewImage = $previewImage;
    }
    
    public function getPreviewImage()
    {
        return $this->previewImage;
    }  
    
    public function setDetailImage(Media $detailImage = null)
    {
        $this->detailImage = $detailImage;
    }
    
    public function getDetailImage()
    {
        return $this->detailImage;
    }    
    
    public function setMain($main)
    {
        $this->main = $main;
    }
    
    public function getMain()
    {
        return $this->main;
    }

    /**
     * @return mixed
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * @param mixed $highlight
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;
    }

    /**
     * @return mixed
     */
    public function getShowInHomePage()
    {
        return $this->showInHomePage;
    }

    /**
     * @param mixed $showInHomePage
     */
    public function setShowInHomePage($showInHomePage)
    {
        $this->showInHomePage = $showInHomePage;
    }

    public function setReferenceDate(\DateTime $referenceDate)
    {
        $this->referenceDate = $referenceDate;
    }
    
    public function getReferenceDate()
    {
        return $this->referenceDate;
    }    
    

}