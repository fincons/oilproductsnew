<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="slide")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\SlideTranslation")
 */
class Slide extends BaseEntity
{

    const JUMBO_SLIDER = 'jumboSlider';
    const LINK_SLIDER = 'linkSlider';
    const LINK_GROUP_TYPE = 'slider_link';

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, length=100)
     */
    protected $firstLine;


    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, length=100, nullable=true)
     */
    protected $secondLine;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $badge='';

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $slider;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Link")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $link;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Link")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $link2;

    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;
    
    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    protected $language;
    

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFirstLine() ?substr( $this->getFirstLine(),0,100): 'n/a';
    }

    /**
     * @return mixed
     */
    public function getFirstLine()
    {
        return $this->firstLine;
    }

    /**
     * @param mixed $firstLine
     */
    public function setFirstLine($firstLine)
    {
        $this->firstLine = $firstLine;
    }

    /**
     * @return mixed
     */
    public function getSecondLine()
    {
        return $this->secondLine;
    }

    /**
     * @param mixed $secondLine
     */
    public function setSecondLine($secondLine)
    {
        $this->secondLine = $secondLine;
    }

    /**
     * @return mixed
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * @param mixed $badge
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;
    }

    /**
     * @return Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Application\Sonata\MediaBundle\Entity\Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * @param string $slider
     */
    public function setSlider($slider)
    {
        $this->slider = $slider;
    }

    /**
     * @return Link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param Link $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
    
    /**
     * @return Link
     */
    public function getLink2()
    {
        return $this->link2;
    }

    /**
     * @param Link $link2
     */
    public function setLink2($link2)
    {
        $this->link2 = $link2;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }
    
    public function getLanguage()
    {
        return $this->language;
    }
    
    public function setLanguage($language)
    {
        $this->language = $language;
    }


}
