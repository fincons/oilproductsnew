<?php

namespace AppBundle\Entity;

class SliderRow
{
    const LINK_GROUP_TYPE = 'block_slider_link';

    protected $link;
    protected $image;
    protected $block;

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
        if ($this->block) {
            $this->block->saveContent();
        }
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        if ($this->block) {
            $this->block->saveContent();
        }
    }

    public function setBlock($block) {
        $this->block = $block;
    }
}
