<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\AreaCategoryBlock;
use AppBundle\Entity\Blocks\EmailTrait;

/**
 * @ORM\Entity
 */
class AreaCategoryEmailBlock extends AreaCategoryBlock
{
    use EmailTrait;

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:email-default.html.twig';
    }
}
