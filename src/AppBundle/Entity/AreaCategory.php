<?php

namespace AppBundle\Entity;

use AppBundle\Entity\BaseEditorialPage;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\BaseTab;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AreaCategoryRepository")
 * @ORM\Table(name="area_category")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\AreaCategoryTranslation")
 * @UniqueEntity("epicKey")
 */
class AreaCategory extends BaseEditorialPage
{

    //TODO: added already UniqueEntity... better add also unique = true as wrote in products
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $epicKey;

    /**
     * @ORM\OneToMany(
     *      targetEntity="EpicAreaCategory",
     *      mappedBy="areaCategoryid",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $epicAreaCategory;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AreaClassificationRow",
     *      mappedBy="areaLev2",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $areaClassificationRowsLev2;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AreaClassificationRow",
     *      mappedBy="areaLev3",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $areaClassificationRowsLev3;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AreaClassificationRow",
     *      mappedBy="areaLev4",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $areaClassificationRowsLev4;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AreaClassificationRow",
     *      mappedBy="areaLev5",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $areaClassificationRowsLev5;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AreaClassificationRow",
     *      mappedBy="areaLev6",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $areaClassificationRowsLev6;

    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $description;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $shortDescription;

    /**
     * @var ContactGroup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContactGroup", cascade={"persist"} )
     */
    protected $contactGroup;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $bannerImage;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $previewImage;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $thumbImage;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $detailImage;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\AreaCategoryTab",
     *      mappedBy="page",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $tabs;

    /**
     * @ORM\Column(name="include_gpl_tool", type="boolean", nullable=false, options={"default"="0"})
     */
    protected $includeGplToolSection = false;

    /**
     * @ORM\Column(name="include_concessionary_tool", type="boolean", nullable=false, options={"default"="0"})
     */
    protected $includeConcessionaryToolSection = false;

    /**
     * @ORM\Column(name="include_products", type="boolean", nullable=false, options={"default"="1"})
     */
    protected $includeProductSection = true;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $fromEpic;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="duplications", cascade={"persist"} )
     */
    protected $duplicatedOf;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductCategory", mappedBy="duplicatedOf")
     */
    protected $duplications;


    public function __construct()
    {
        $this->tabs = new ArrayCollection();
        $this->published = true;
        $this->fromEpic = false;
        $this->duplications = new ArrayCollection();
    }

    public function setEpicKey($epicKey)
    {
        $this->epicKey = $epicKey;
    }

    public function getEpicKey()
    {
        return $this->epicKey;
    }

    public function setFromEpic($fromEpic)
    {
        $this->fromEpic = $fromEpic;
    }

    public function getFromEpic()
    {
        return $this->fromEpic;
    }

    public function getDuplicatedOf()
    {
        return $this->duplicatedOf;
    }

    public function getDuplications()
    {
        return $this->duplications;
    }

    public function addDuplication(ProductCategory $duplication)
    {
        $this->duplications[] = $duplication;
        $duplication->setDuplicatedOf($this);
    }

    public function setDuplicatedOf(ProductCategory $duplicatedOf)
    {
        $this->duplicatedOf = $duplicatedOf;
    }

    public function __toString()
    {
        return $this->getTitle() ?: 'n/a';
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @return ContactGroup
     */
    public function getContactGroup()
    {
        return $this->contactGroup;
    }

    /**
     * @param ContactGroup $contactGroup
     */
    public function setContactGroup($contactGroup)
    {
        $this->contactGroup = $contactGroup;
    }

    public function setBannerImage(Media $bannerImage = null)
    {
        $this->bannerImage = $bannerImage;
    }

    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    public function setPreviewImage(Media $previewImage = null)
    {
        $this->previewImage = $previewImage;
    }

    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    public function setThumbImage(Media $thumbImage = null)
    {
        $this->thumbImage = $thumbImage;
    }

    public function getThumbImage()
    {
        return $this->thumbImage;
    }

    public function setDetailImage(Media $detailImage = null)
    {
        $this->detailImage = $detailImage;
    }

    public function getDetailImage()
    {
        return $this->detailImage;
    }

    public function setIncludeGplToolSection($includeGplToolSection)
    {
        $this->includeGplToolSection = $includeGplToolSection;
    }

    public function getIncludeGplToolSection()
    {
        return $this->includeGplToolSection;
    }

    public function setIncludeConcessionaryToolSection($includeConcessionaryToolSection)
    {
        $this->includeConcessionaryToolSection = $includeConcessionaryToolSection;
    }

    public function getIncludeConcessionaryToolSection()
    {
        return $this->includeConcessionaryToolSection;
    }


    public function setIncludeProductSection($includeProductSection)
    {
        $this->includeProductSection = $includeProductSection;
    }

    public function getIncludeProductSection()
    {
        return $this->includeProductSection;
    }

    /**
     * @return mixed
     */
    public function getEpicAreaCategory()
    {
        return $this->epicAreaCategory;
    }

    /**
     * @param mixed $epicAreaCategory
     */
    public function setEpicAreaCategory($epicAreaCategory)
    {
        $this->epicAreaCategory = $epicAreaCategory;
    }


}
