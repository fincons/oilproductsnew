<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Office", mappedBy="company")
     */
    protected $offices;

    /**
     * @ORM\Column
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $cylinder;

    /**
    * @ORM\Column(type="boolean")
     */
    protected $littleBarrel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->provinces = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cylinder
     *
     * @param boolean $cylinder
     *
     * @return Company
     */
    public function setCylinder($cylinder)
    {
        $this->cylinder = $cylinder;

        return $this;
    }

    /**
     * Get cylinder
     *
     * @return boolean
     */
    public function getCylinder()
    {
        return $this->cylinder;
    }

    /**
     * Set littleBarrel
     *
     * @param boolean $littleBarrel
     *
     * @return Company
     */
    public function setLittleBarrel($littleBarrel)
    {
        $this->littleBarrel = $littleBarrel;

        return $this;
    }

    /**
     * Get littleBarrel
     *
     * @return boolean
     */
    public function getLittleBarrel()
    {
        return $this->littleBarrel;
    }

    /**
     * Add office
     *
     * @param \AppBundle\Entity\Office $office
     *
     * @return Company
     */
    public function addOffice(\AppBundle\Entity\Office $office)
    {
        $this->offices[] = $office;

        return $this;
    }

    /**
     * Remove office
     *
     * @param \AppBundle\Entity\Office $office
     */
    public function removeOffice(\AppBundle\Entity\Office $office)
    {
        $this->offices->removeElement($office);
    }

    /**
     * Get offices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffices()
    {
        return $this->offices;
    }
}
