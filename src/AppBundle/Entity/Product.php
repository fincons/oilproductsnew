<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\ProductTranslation")
 * @UniqueEntity("code")
 */
class Product extends BaseEditorialPage
{

    //TODO: added already UniqueEntity... better add also unique = true when db is cleaned from duplicates...
    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $code;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $gradient;

    /**
 	* @ORM\Column(type="text", columnDefinition="TEXT", nullable=true)
    */
    protected $specifications;

    /**
    * @ORM\Column(type="string", length=2, nullable=false)
    */
    protected $country = 'IT';

    /**
    * @Gedmo\Translatable
    * @ORM\Column(type="string", length=1000, nullable=true)
    */
    protected $pdf;

    /**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", columnDefinition="TEXT", nullable=true)
	 */
	protected $description;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
	 */
	protected $shortDescription;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
	 */
	protected $thumbImage;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
	 */
	protected $detailImage;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
	 */
	protected $bannerImage;

    /**
    * @ORM\Column(type="boolean")
    */
    protected $fromAlis;

    /**
     * @ORM\OneToMany(
     *      targetEntity="ProductClassificationRow",
     *      mappedBy="product",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $productClassificationRows;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\AreaClassificationRow",
     *      mappedBy="product",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $areaClassificationRows;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductLocalized", cascade={"persist"}, mappedBy="product")
     */
    protected $productsLocalized;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $brochure;
    
    // NB: now is a fake field
    protected $tabs;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $fromEpic;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $epicVersion;


    public function __construct()
    {
            $this->productClassificationRows = new ArrayCollection();
            $this->productsLocalized = new ArrayCollection();
            $this->fromAlis = true;
            $this->published = true;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setGradient($gradient)
    {
        $this->gradient = $gradient;
    }

    public function getGradient()
    {
        return $this->gradient;
    }

    public function setSpecifications($specifications)
    {
        $this->specifications = $specifications;
    }

    public function getSpecifications()
    {
        return $this->specifications;
    }

    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    public function getPdf()
    {
        return $this->pdf;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    public function setFromAlis($fromAlis)
    {
        $this->fromAlis = $fromAlis;
    }

    public function getFromAlis()
    {
        return $this->fromAlis;
    }

    public function getProductClassificationRows()
    {
    	return $this->productClassificationRows;
    }

    public function addProductClassificationRows($productClassificationRows)
    {
    	$this->productClassificationRows->add($productClassificationRows);
    	$productClassificationRows->setProduct($this);
    }

    public function removeProductClassificationRows($productClassificationRows)
    {
    	$this->productClassificationRowsLev2->removeElement(productClassificationRows);
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setThumbImage(Media $thumbImage = null )
    {
        $this->thumbImage = $thumbImage;
    }

    public function getThumbImage()
    {
        return $this->thumbImage;
    }

    public function setDetailImage(Media $detailImage = null)
    {
        $this->detailImage = $detailImage;
    }

    public function getDetailImage()
    {
        return $this->detailImage;
    }

    public function setBannerImage(Media $bannerImage = null)
    {
        $this->bannerImage = $bannerImage;
    }

    public function getBannerImage()
    {
        return $this->bannerImage;
    }
    
    public function setEpicVersion($version)
    {
    	$this->epicVersion = $version;
    }
    
    public function getEpicVersion()
    {
    	return $this->epicVersion;
    }   

    public function setFromEpic($fromEpic)
    {
    	$this->fromEpic = $fromEpic;
    }
    
    public function getFromEpic()
    {
    	return $this->fromEpic;
    }

    public function getLocalizedBrochure($locale) {
        return null;
    }

    public function getBrochure($locale = null) {
        $locale = $this->getCurrentLocale($locale);
        $productLocalized = null;
        foreach ($this->productsLocalized as $productLocalizedFor) {
            if ($productLocalizedFor->getLocale() == $locale) {
                $productLocalized = $productLocalizedFor;
                return $productLocalized->getBrochure();
            }
        }
        if ($productLocalized == null) {
            return $this->brochure;
        }
    }

    public function setBrochure(Media $brochure = null) {
        $locale = $this->getCurrentLocale();
        $productLocalized = null;
        if ($locale == $this->getDefaultLocale()) {
            $this->brochure = $brochure;
        } else {
            foreach ($this->productsLocalized as $productLocalizedFor) {
                if ($productLocalizedFor->getLocale() == $locale) {
                    $productLocalized = $productLocalizedFor;
                    $productLocalized->setBrochure($brochure);
                }
            }
            if ($productLocalized == null) {
                $productLocalized = new ProductLocalized();
                $productLocalized->setLocale($locale);
                $productLocalized->setBrochure($brochure);
                $this->productsLocalized->add($productLocalized);
                $productLocalized->setProduct($this);
            }
        }
    }
    
    
    public function getTechFileDownloadId() {
    	$url_id = $this->pdf;
    	// old alis pdf
    	if (strpos($url_id, 'https://nalis.eni.it') !== false) {
    		$url_id = str_replace("https://nalis.eni.it/Alis/public/MainServlet?ACTION=SHOW_PDF&HidIdAllegato=","", $url_id);
    		$url_id = str_replace("&HidLangAll=IT","", $url_id);
    		$url_id = str_replace("&HidLangAll=EN","", $url_id);
    		$url_id = $url_id . "-" .  substr($this->pdf, -2) ;
    		$url_id = "a-" . $url_id;
    	} else if ($url_id){
    		$url_id = "e-" . $url_id;
    	}
    	// epic pdf
    	return $url_id;
    }
    
    public function __toString()
    {
    	return $this->getTitle() . " (" . $this->getCode() . ")";
    }


}
