<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Application\Sonata\MediaBundle\Entity\Media;
use AppBundle\Entity\BaseTab;
use AppBundle\Entity\BaseEditorialPage;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 * @ORM\Table(name="page")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\PageTranslation")
 */
class Page extends BaseEditorialPage
{

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $routeId;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $routeParams;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $bannerImage;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Tab",
     *      mappedBy="page",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $tabs;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Link",
     *      mappedBy="page",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    private $links;

    public function __construct()
    {
    	$this->tabs = new ArrayCollection();
        $this->links = new ArrayCollection();
    }

    public function setRouteId($routeId)
    {
    	$this->routeId = $routeId;
    }

    public function getRouteId()
    {
        return $this->routeId;
    }

    public function getRouteParams()
    {
    	return $this->routeParams;
    }

    public function setRouteParams($routeParams)
    {
    	$this->routeParams = $routeParams;
    }

    public function __toString()
    {
        return $this->getTitle() ?: 'n/a';
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function addLink(Link $link)
    {
        $this->link->add($link);
        $link->setPage($this);
    }

    public function removeLink(Link $link)
    {
        $this->links->removeElement($link);
    }

    public function setBannerImage(Media $bannerImage = null)
    {
        $this->bannerImage = $bannerImage;
    }

    public function getBannerImage()
    {
        return $this->bannerImage;
    }

}
