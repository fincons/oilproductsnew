<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseBlock;

/**
 * @ORM\Entity
 * @ORM\Table(name="business_solution_block")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"text"="AppBundle\Entity\BusinessSolutionTextBlock","email"="AppBundle\Entity\BusinessSolutionEmailBlock", "slider"="AppBundle\Entity\BusinessSolutionSliderBlock", "brochure"="AppBundle\Entity\BusinessSolutionBrochureBlock"})
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\BusinessSolutionBlockTranslation")
 */
abstract class BusinessSolutionBlock extends BaseBlock
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BusinessSolutionTab", inversedBy="blocks")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $tab;
}
