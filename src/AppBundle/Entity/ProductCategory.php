<?php

namespace AppBundle\Entity;

use AppBundle\Entity\BasePage;
use AppBundle\Entity\ProductCategoryLocalized;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductCategoryRepository")
 * @ORM\Table(name="product_category")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\ProductCategoryTranslation")
 * @UniqueEntity("epicKey")
 */
class ProductCategory extends BaseEditorialPage
{

	//TODO: added already UniqueEntity... better add also unique = true as wrote in products
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $epicKey;

    /**
     * @ORM\OneToMany(
     *      targetEntity="EpicProductCategory",
     *      mappedBy="productCategoryid",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $epicProductCategory;
	
    /**
     * @ORM\OneToMany(
     *      targetEntity="ProductClassificationRow",
     *      mappedBy="productCategoryLev2",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $productClassificationRowsLev2;

    /**
     * @ORM\OneToMany(
     *      targetEntity="ProductClassificationRow",
     *      mappedBy="productCategoryLev3",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $productClassificationRowsLev3;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity="ProductClassificationRow",
     *      mappedBy="productCategoryLev4",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $productClassificationRowsLev4;    
    
    /**
     * @ORM\OneToMany(
     *      targetEntity="ProductClassificationRow",
     *      mappedBy="productCategoryLev5",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     */
    protected $productClassificationRowsLev5;    
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $description;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $shortDescription;

    /**
     * @var ContactGroup
     * @ORM\ManyToOne(targetEntity="ContactGroup", cascade={"persist"} )
     */
    protected $contactGroup;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $bannerImage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $previewImage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $thumbImage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $detailImage;   
    
    /**
     * @ORM\Column(name="include_concessionary_tool", type="boolean", nullable=false, options={"default"="0"})
     */
    protected $includeConcessionaryToolSection = false;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductCategoryLocalized", cascade={"persist"}, mappedBy="productCategory")
     */
    protected $productCategoriesLocalized;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $brochure;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $brochureIcon;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $fromEpic;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="duplications", cascade={"persist"} )
     */
    protected $duplicatedOf;
    
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductCategory", mappedBy="duplicatedOf")
     */
    protected $duplications;
    
    
    public function __construct() {
        $this->published = true;
        $this->fromEpic = false;
        $this->productCategoriesLocalized = new ArrayCollection();
        $this->duplications = new ArrayCollection();
    }
    
    public function setEpicKey($epicKey)
    {
    	$this->epicKey = $epicKey;
    }
    
    public function getEpicKey()
    {
    	return $this->epicKey;
    }

    public function getEpicProductCategory()
    {
        return $this->epicProductCategory;
    }

    public function addEepicProductCategory($epicProductCategory)
    {
        $this->epicProductCategory->add($epicProductCategory);
        $epicProductCategory->setProductCategory($this);
    }

    public function removeEpicProductCategory($epicProductCategory)
    {
        $this->epicProductCategory->removeElement($epicProductCategory);
    }

    
    public function getProductClassificationRowsLev2()
    {
    	return $this->productClassificationRowsLev2;
    }
    
    public function addProductClassificationRowsLev2($productClassificationRowsLev2)
    {
    	$this->productClassificationRowsLev2->add($productClassificationRowsLev2);
    	$productClassificationRowsLev2->setProductCategory($this);
    }
    
    public function removeProductClassificationRowsLev2($productClassificationRowsLev2)
    {
    	$this->productClassificationRowsLev2->removeElement($productClassificationRowsLev2);
    }
    
    
    public function getProductClassificationRowsLev3()
    {
    	return $this->productClassificationRowsLev3;
    }
    
    public function addProductClassificationRowsLev3($productClassificationRowsLev3)
    {
    	$this->productClassificationRowsLev3->add($productClassificationRowsLev3);
    	$productClassificationRowsLev3->setProductCategory($this);
    }
    
    public function removeProductClassificationRowsLev3($productClassificationRowsLev3)
    {
    	$this->productClassificationRowsLev3->removeElement(productClassificationRowsLev3);
    }    
    
    public function getProductClassificationRowsLev4()
    {
    	return $this->productClassificationRowsLev4;
    }
    
    public function addProductClassificationRowsLev4($productClassificationRowsLev4)
    {
    	$this->productClassificationRowsLev4->add($productClassificationRowsLev4);
    	$productClassificationRowsLev4->setProductCategory($this);
    }
    
    public function removeProductClassificationRowsLev4($productClassificationRowsLev4)
    {
    	$this->productClassificationRowsLev4->removeElement(productClassificationRowsLev4);
    }    
    
    public function getProductClassificationRowsLev5()
    {
    	return $this->productClassificationRowsLev5;
    }
    
    public function addProductClassificationRowsLev5($productClassificationRowsLev5)
    {
    	$this->productClassificationRowsLev5->add($productClassificationRowsLev5);
    	$productClassificationRowsLev5->setProductCategory($this);
    }
    
    public function removeProductClassificationRowsLev5($productClassificationRowsLev5)
    {
    	$this->productClassificationRowsLev5->removeElement(productClassificationRowsLev5);
    }   
    
    public function __toString()
    {
    	return $this->getTitle() . ($this->getEpicKey() ? (" (" . $this->getEpicKey() . ")") : "");
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }
    
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @return ContactGroup
     */
    public function getContactGroup()
    {
        return $this->contactGroup;
    }

    /**
     * @param ContactGroup $contactGroup
     */
    public function setContactGroup($contactGroup)
    {
        $this->contactGroup = $contactGroup;
    }

    /**
     * @return mixed
     */
    public function getProductCategoriesLocalized()
    {
        return $this->productCategoriesLocalized;
    }

    /**
     * @param mixed $productCategoriesLocalized
     */
    public function setProductCategoriesLocalized($productCategoriesLocalized)
    {
        $this->productCategoriesLocalized = $productCategoriesLocalized;
    }


    
    public function setBannerImage(Media $bannerImage = null)
    {
        $this->bannerImage = $bannerImage;
    }
    
    public function getBannerImage()
    {
        return $this->bannerImage;
    }
    
    public function setPreviewImage(Media $previewImage = null)
    {
        $this->previewImage = $previewImage;
    }
    
    public function getPreviewImage()
    {
        return $this->previewImage;
    }
    
    public function setThumbImage(Media $thumbImage = null)
    {
        $this->thumbImage = $thumbImage;
    }
    
    public function getThumbImage()
    {
        return $this->thumbImage;
    }
    
    public function setDetailImage(Media $detailImage = null)
    {
        $this->detailImage = $detailImage;
    }
    
    public function getDetailImage()
    {
        return $this->detailImage;
    }
    
    public function setIncludeConcessionaryToolSection($includeConcessionaryToolSection)
    {
        $this->includeConcessionaryToolSection = $includeConcessionaryToolSection;
    }
    
    public function getIncludeConcessionaryToolSection()
    {
        return $this->includeConcessionaryToolSection;
    }
    
    public function getBrochure($locale = null) {
        $locale = $this->getCurrentLocale($locale);
        $productCategoryLocalized = null;
        foreach ($this->productCategoriesLocalized as $productCategoryLocalizedFor) {
            if ($productCategoryLocalizedFor->getLocale() == $locale) {
                $productCategoryLocalized = $productCategoryLocalizedFor;
                return $productCategoryLocalized->getBrochure();
            }
        }
        if ($productCategoryLocalized == null) {
            return $this->brochure;            
        }
    }
    
    public function setBrochure(Media $brochure = null) {
        $locale = $this->getCurrentLocale();
        $productCategoryLocalized = null;
        if ($locale == $this->getDefaultLocale()) {
           $this->brochure = $brochure; 
        } else {
            foreach ($this->productCategoriesLocalized as $productCategoryLocalizedFor) {
                if ($productCategoryLocalizedFor->getLocale() == $locale) {
                    $productCategoryLocalized = $productCategoryLocalizedFor;
                    $productCategoryLocalized->setBrochure($brochure);
                }
            }
            if ( $productCategoryLocalized == null) {
                $productCategoryLocalized = new ProductCategoryLocalized();
                $productCategoryLocalized->setLocale($locale);
                $productCategoryLocalized->setBrochure($brochure);
                $this->productCategoriesLocalized->add($productCategoryLocalized);
                $productCategoryLocalized->setProductCategory($this);
            }            
        }
    }


    public function getBrochureIcon($locale = null) {
        $locale = $this->getCurrentLocale($locale);
        $productCategoryLocalized = null;
        foreach ($this->productCategoriesLocalized as $productCategoryLocalizedFor) {
            if ($productCategoryLocalizedFor->getLocale() == $locale) {
                $productCategoryLocalized = $productCategoryLocalizedFor;
                return $productCategoryLocalized->getBrochureIcon();
            }
        }
        if ($productCategoryLocalized == null) {
            return $this->brochureIcon;
        }
    }

    public function setBrochureIcon(Media $brochureIcon = null) {
        $locale = $this->getCurrentLocale();
        $productCategoryLocalized = null;
        if ($locale == $this->getDefaultLocale()) {
            $this->brochureIcon = $brochureIcon;
        } else {
            foreach ($this->productCategoriesLocalized as $productCategoryLocalizedFor) {
                if ($productCategoryLocalizedFor->getLocale() == $locale) {
                    $productCategoryLocalized = $productCategoryLocalizedFor;
                    $productCategoryLocalized->setBrochureIcon($brochureIcon);
                }
            }
            if ( $productCategoryLocalized == null) {
                $productCategoryLocalized = new ProductCategoryLocalized();
                $productCategoryLocalized->setLocale($locale);
                $productCategoryLocalized->setBrochureIcon($brochureIcon);
                $this->productCategoriesLocalized->add($productCategoryLocalized);
                $productCategoryLocalized->setProductCategory($this);
            }
        }
    }
    public function setFromEpic($fromEpic)
    {
    	$this->fromEpic = $fromEpic;
    }
    
    public function getFromEpic()
    {
    	return $this->fromEpic;
    }
    
    public function getDuplicatedOf()
    {
    	return $this->duplicatedOf;
    }
    
    public function getDuplications() {
    	return $this->duplications;
    }
    
    public function addDuplication(ProductCategory $duplication) {
    	$this->duplications[] = $duplication;
    	$duplication->setDuplicatedOf($this);
    }
    
    public function setDuplicatedOf(ProductCategory $duplicatedOf) {
    	$this->duplicatedOf = $duplicatedOf;
    }
    

}