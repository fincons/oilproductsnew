<?php

namespace AppBundle\Entity;

use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\MappedSuperclass
 */
class BasePage extends BaseEntity
{

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	protected $title;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	protected $slug;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $metaKeywords;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $metaDescription;


	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setSlug($slug)
	{
		$this->slug = $slug;
	}

	public function getSlug()
	{
		return $this->slug;
	}

	public function setMetaKeywords($metaKeywords)
	{
	    $this->metaKeywords = $metaKeywords;
	}

	public function getMetaKeywords()
	{
	    return $this->metaKeywords;
	}

	public function setMetaDescription($metaDescription)
	{
	    $this->metaDescription = $metaDescription;
	}

	public function getMetaDescription()
	{
	    return $this->metaDescription;
	}

	public function __toString()
	{
		return $this->getTitle() ?: 'n/a';
	}

}
