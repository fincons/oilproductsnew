<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\ContactTranslation")
 */
class Contact extends BaseEntity
{
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    protected $title;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    protected $name;

    /**
     * @Assert\Email
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $email;
    
    /**
     * @Assert\Email
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $email2;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $phone;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $phone2;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $fax;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $fax2;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $mobile;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $mobile2;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $address;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $address2;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $site;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $site2;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $company;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $intoplane;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $dayworkschedule;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $airportManager;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContactGroup", inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var ContactGroup
     */
    private $contactGroup;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContactSubGroup", inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var contactSubGroup
     */
    private $contactSubGroup;

    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;


    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @return string
     */
    public function getSite2()
    {
        return $this->site2;
    }

    /**
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @param string $site2
     */
    public function setSite2($site2)
    {
        $this->site2 = $site2;
    }

    /**
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * @return string
     */
    public function getFax2()
    {
        return $this->fax2;
    }

    /**
     * @return string
     */
    public function getMobile2()
    {
        return $this->mobile2;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $email2
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;
    }

    /**
     * @param string $phone2
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;
    }

    /**
     * @param string $fax2
     */
    public function setFax2($fax2)
    {
        $this->fax2 = $fax2;
    }

    /**
     * @param string $mobile2
     */
    public function setMobile2($mobile2)
    {
        $this->mobile2 = $mobile2;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }



    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function __toString()
    {
        return $this->getName() ?: 'n/a';
    }

    /**
     * @return ContactGroup
     */
    public function getContactGroup()
    {
        return $this->contactGroup;
    }

    /**
     * @param ContactGroup $contactGroup
     */
    public function setContactGroup($contactGroup)
    {
        $this->contactGroup = $contactGroup;
    }
    /**
     * @return ContactSubGroup
     */
    public function getContactSubGroup()
    {
        return $this->contactSubGroup;
    }

    /**
     * @param ContactSubGroup $contactSubGroup
     */
    public function setContactSubGroup($contactSubGroup)
    {
        $this->contactSubGroup = $contactSubGroup;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDayworkschedule()
    {
        return $this->dayworkschedule;
    }

    /**
     * @param string $dayworkschedule
     */
    public function setDayworkschedule($dayworkschedule)
    {
        $this->dayworkschedule = $dayworkschedule;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getIntoplane()
    {
        return $this->intoplane;
    }

    /**
     * @param string $intoplane
     */
    public function setIntoplane($intoplane)
    {
        $this->intoplane = $intoplane;
    }

    /**
     * @return string
     */
    public function getAirportManager()
    {
        return $this->airportManager;
    }

    /**
     * @param string $airportManager
     */
    public function setAirportManager($airportManager)
    {
        $this->airportManager = $airportManager;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

}