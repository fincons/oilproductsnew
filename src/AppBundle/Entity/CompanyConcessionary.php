<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompanyConcessionary
 *
 * @ORM\Table(name="companyconcessionary")
 * @ORM\Entity
 */
class CompanyConcessionary {
	/**
	 *
	 * @var int @ORM\Column(name="id", type="integer")
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="Office", mappedBy="companyconcessionary")
	 */
	protected $offices;
	
	/**
	 * @ORM\Column
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $cylinder;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $littleBarrel;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->provinces = new \Doctrine\Common\Collections\ArrayCollection ();
	}
	
	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name        	
	 *
	 * @return CompanyConcessionary
	 */
	public function setName($name) {
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Set cylinder
	 *
	 * @param boolean $cylinder        	
	 *
	 * @return CompanyConcessionary
	 */
	public function setCylinder($cylinder) {
		$this->cylinder = $cylinder;
		
		return $this;
	}
	
	/**
	 * Get cylinder
	 *
	 * @return boolean
	 */
	public function getCylinder() {
		return $this->cylinder;
	}
	
	/**
	 * Set littleBarrel
	 *
	 * @param boolean $littleBarrel        	
	 *
	 * @return CompanyConcessionary
	 */
	public function setLittleBarrel($littleBarrel) {
		$this->littleBarrel = $littleBarrel;
		
		return $this;
	}
	
	/**
	 * Get littleBarrel
	 *
	 * @return boolean
	 */
	public function getLittleBarrel() {
		return $this->littleBarrel;
	}
	
	/**
	 * Add office
	 *
	 * @param \AppBundle\Entity\Office $office        	
	 *
	 * @return CompanyConcessionary
	 */
	public function addOffice(\AppBundle\Entity\Office $office) {
		$this->offices [] = $office;
		
		return $this;
	}
	
	/**
	 * Remove office
	 *
	 * @param \AppBundle\Entity\Office $office        	
	 */
	public function removeOffice(\AppBundle\Entity\Office $office) {
		$this->offices->removeElement ( $office );
	}
	
	/**
	 * Get officesConcessionary
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getOfficesConcessionary() {
		return $this->offices;
	}
}
