<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media;
use AppBundle\Entity\BaseLocalized;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_localized")
 */
class ProductLocalized extends BaseLocalized
{

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="productsLocalized", cascade={"persist"} )
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $brochure;
    
    public function getBrochure()
    {
        return $this->brochure;
    }
    
    public function setBrochure(Media $brochure = null)
    {
        $this->brochure = $brochure;
    }
    
    public function setProduct($product)
    {
        $this->product = $product;
    }
    
    public function getProduct()
    {
        return $this->product;
    }

    

}