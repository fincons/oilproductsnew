<?php

namespace AppBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactGroupRepository")
 * @ORM\Table(name="contact_group")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\ContactGroupTranslation")
 */
class ContactGroup extends BasePage
{

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     */
    protected $subtitle;

    /**
     * @var string
     * @ORM\Column (type="text", columnDefinition="TEXT", nullable=true)
     * @Accessor(getter="getLanguageAsArray")
     * @Type("array")
     */
    protected $language;

    /**
     * @var string
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $iconName;

    /**
     * @Assert\Email
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $email;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $previewImage;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $bannerImage;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ContactGroupLocalized", cascade={"persist"}, mappedBy="contactGroup")
     */
    protected $contactGroupsLocalized;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $pdf;

    /**
     * @ORM\Column(name="position", type="float", nullable=false)
     */
    protected $position = 0;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Contact",
     *      mappedBy="contactGroup",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     * @var ArrayCollection
     */
    private $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->published = true;
        $this->contactGroupsLocalized = new ArrayCollection();
    }


    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact)
    {
        $this->contacts->add($contact);
        $contact->setContactGroup($this);
    }

    public function removeContact(Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

  
    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    private function is_JSON()
    {
        call_user_func_array('json_decode', func_get_args());
        return (json_last_error() === JSON_ERROR_NONE);
    }

    /**
     * @return string
     */
    public function getIconName()
    {
        return $this->iconName;
    }

    /**
     * @param string $iconName
     */
    public function setIconName($iconName)
    {
        $this->iconName = $iconName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return Media
     */
    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    /**
     * @param Media $previewImage
     */
    public function setPreviewImage(Media $previewImage = null)
    {
        $this->previewImage = $previewImage;
    }

    /**
     * @return Media
     */
    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    /**
     * @param Media $bannerImage
     */
    public function setBannerImage(Media $bannerImage = null)
    {
        $this->bannerImage = $bannerImage;
    }


    public function __toString()
    {
        return $this->getTitle() ?: 'n/a';
    }

    public function getPdf($locale = null)
    {
        if ($locale == null) {
            $locale = $this->getLocale();
        }
        if ($locale == null) {
            $locale = $this->getDefaultLocale();
        }
        $contactGroupsLocalized = null;
        foreach ($this->contactGroupsLocalized as $contactGroupsLocalizedFor) {
            if ($contactGroupsLocalizedFor->getLocale() == $locale) {
                $contactGroupsLocalized = $contactGroupsLocalizedFor;
                return $contactGroupsLocalized->getPdf();
            }
        }
        if ($contactGroupsLocalized == null) {
            return $this->pdf;
        }
    }

    public function setPdf(Media $pdf = null)
    {
        $locale = $this->getLocale();
        $contactGroupsLocalized = null;
        if ($locale == $this->getDefaultLocale()) {
            $this->pdf = $pdf;
        } else {
            foreach ($this->contactGroupsLocalized as $contactGroupLocalizedFor) {
                if ($contactGroupLocalizedFor->getLocale() == $locale) {
                    $contactGroupsLocalized = $contactGroupLocalizedFor;
                    $contactGroupsLocalized->setPdf($pdf);
                }
            }
            if ($contactGroupsLocalized == null) {
                $contactGroupsLocalized = new ContactGroupLocalized();
                $contactGroupsLocalized->setLocale($locale);
                $contactGroupsLocalized->setPdf($pdf);
                $this->contactGroupsLocalized->add($contactGroupsLocalized);
                $contactGroupsLocalized->setContactGroup($this);
            }
        }
    }

// TODO: use a listener to set this value from parameters
    public function getDefaultLocale()
    {
        return "it_IT";
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

}