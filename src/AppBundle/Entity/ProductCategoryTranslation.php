<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductCategoryTranslationRepository")
 * @ORM\Table(name="product_category_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="product_category_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class ProductCategoryTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}