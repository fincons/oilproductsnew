<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Store
{
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @ORM\Column(type="string")
    */
    protected $name;

    /**
    * @ORM\Column(type="string")
    */
    protected $lat;

    /**
    * @ORM\Column(type="string")
    */
    protected $lng;
}
