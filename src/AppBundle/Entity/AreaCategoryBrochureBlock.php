<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\AreaCategoryBlock;
use AppBundle\Entity\Blocks\BrochureTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class AreaCategoryBrochureBlock extends AreaCategoryBlock
{
    use BrochureTrait;
    
    public function __construct()
    {
    	$this->docs = new ArrayCollection();
    }

    protected function getDefaultTemplate()
    {
        return 'AppBundle:frontend/block:brochure-default.html.twig';
    }
}
