<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * @ORM\Entity
 * @ORM\Table(name="contact_localized")
 */
class ContactGroupLocalized
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=6, nullable=false)
     */
    protected $locale;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContactGroup", inversedBy="contactGroupsLocalized", cascade={"persist"} )
     * @ORM\JoinColumn(nullable=false)
     */
    private $contactGroup;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $pdf;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getPdf()
    {
        return $this->pdf;
    }
    
    public function setPdf(Media $pdf = null)
    {
        $this->pdf = $pdf;
    }
    
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
    
    public function getLocale()
    {
        return $this->locale;
    }
    
    public function setContactGroup($contactGroup)
    {
        $this->contactGroup = $contactGroup;
    }
    
    public function getContactGroup()
    {
        return $this->contactGroup;
    }

    

}