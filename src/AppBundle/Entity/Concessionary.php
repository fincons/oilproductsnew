<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Concessionary
 *
 * @ORM\Table(name="concessionary")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConcessionaryRepository")
 */
class Concessionary {
	/**
	 *
	 * @var int @ORM\Column(name="id", type="integer")
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Province")
	 * @ORM\JoinTable(name="concessionary_provinces",
	 * joinColumns={@ORM\JoinColumn(name="office_id", referencedColumnName="id")},
	 * inverseJoinColumns={@ORM\JoinColumn(name="province_id", referencedColumnName="id")}
	 * )
	 */
	private $provinces;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CompanyConcessionary", inversedBy="offices")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $company;
	
	/**
	 * @ORM\ManyToOne(targetEntity="City")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $city;
	
	/**
	 *
	 * @var string @ORM\Column(name="address", type="string", length=255)
	 */
	private $address;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Province")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $province;
	
	/**
	 *
	 * @var string @ORM\Column(name="cap", type="string", length=5)
	 */
	private $cap;
	
	/**
	 *
	 * @var string @ORM\Column(name="email", type="string", length=255)
	 */
	private $email;
	
	/**
	 *
	 * @var string @ORM\Column(name="phone", type="string", length=255)
	 */
	private $phone;
	
	/**
	 *
	 * @var string @ORM\Column(name="fax", type="string", length=255)
	 */
	private $fax;
	
	/**
	 *
	 * @var string @ORM\Column(name="lat", type="float")
	 */
	private $lat;
	
	/**
	 *
	 * @var string @ORM\Column(name="lng", type="float")
	 */
	private $lng;
	
	/**
	 *
	 * @var string @ORM\Column(name="sezione",  type="string", length=45)
	 */
	private $sezione;
	
	/**
     * @return string
     */
    public function getSezione()
    {
        return $this->sezione;
    }

    /**
     * @param string $sezione
     */
    public function setSezione($sezione)
    {
        $this->sezione = $sezione;
    }

    /**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set address
	 *
	 * @param string $address        	
	 *
	 * @return Concessionary
	 */
	public function setAddress($address) {
		$this->address = $address;
		
		return $this;
	}
	
	/**
	 * Get address
	 *
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}
	
	/**
	 * Set cap
	 *
	 * @param string $cap        	
	 *
	 * @return Concessionary
	 */
	public function setCap($cap) {
		$this->cap = $cap;
		
		return $this;
	}
	
	/**
	 * Get cap
	 *
	 * @return string
	 */
	public function getCap() {
		return $this->cap;
	}
	
	/**
	 * Set email
	 *
	 * @param string $email        	
	 *
	 * @return Concessionary
	 */
	public function setEmail($email) {
		$this->email = $email;
		
		return $this;
	}
	
	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * Set phone
	 *
	 * @param string $phone        	
	 *
	 * @return Concessionary
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
		
		return $this;
	}
	
	/**
	 * Get phone
	 *
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}
	
	/**
	 * Set fax
	 *
	 * @param string $fax        	
	 *
	 * @return Concessionary
	 */
	public function setFax($fax) {
		$this->fax = $fax;
		
		return $this;
	}
	
	/**
	 * Get fax
	 *
	 * @return string
	 */
	public function getFax() {
		return $this->fax;
	}
	
	/**
	 * Set city
	 *
	 * @param \AppBundle\Entity\City $city        	
	 *
	 * @return Concessionary
	 */
	public function setCity(\AppBundle\Entity\City $city = null) {
		$this->city = $city;
		
		return $this;
	}
	
	/**
	 * Get city
	 *
	 * @return \AppBundle\Entity\City
	 */
	public function getCity() {
		return $this->city;
	}
	
	/**
	 * Set province
	 *
	 * @param \AppBundle\Entity\Province $province        	
	 *
	 * @return Concessionary
	 */
	public function setProvince(\AppBundle\Entity\Province $province = null) {
		$this->province = $province;
		
		return $this;
	}
	
	/**
	 * Get province
	 *
	 * @return \AppBundle\Entity\Province
	 */
	public function getProvince() {
		return $this->province;
	}
	
	/**
	 * Set company Concessionary
	 *
	 * @param \AppBundle\Entity\CompanyConcessionary $company        	
	 *
	 * @return Concessionary
	 */
	public function setCompanyConcessionary(\AppBundle\Entity\CompanyConcessionary $company) {
		$this->company = $company;
		
		return $this;
	}
	
	/**
	 * Get company Concessionary
	 *
	 * @return \AppBundle\Entity\CompanyConcessionary
	 */
	public function getCompanyConcessionary() {
		return $this->company;
	}
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->provinces = new \Doctrine\Common\Collections\ArrayCollection ();
	}
	
	/**
	 * Add province
	 *
	 * @param \AppBundle\Entity\Province $province        	
	 *
	 * @return Concessionary
	 */
	public function addProvince(\AppBundle\Entity\Province $province) {
		$this->provinces [] = $province;
		
		return $this;
	}
	
	/**
	 * Remove province
	 *
	 * @param \AppBundle\Entity\Province $province        	
	 */
	public function removeProvince(\AppBundle\Entity\Province $province) {
		$this->provinces->removeElement ( $province );
	}
	
	/**
	 * Get provinces
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProvinces() {
		return $this->provinces;
	}
	
	/**
	 * Set lat
	 *
	 * @param float $lat        	
	 *
	 * @return Concessionary
	 */
	public function setLat($lat) {
		$this->lat = $lat;
		
		return $this;
	}
	
	/**
	 * Get lat
	 *
	 * @return float
	 */
	public function getLat() {
		return $this->lat;
	}
	
	/**
	 * Set lng
	 *
	 * @param float $lng        	
	 *
	 * @return Concessionary
	 */
	public function setLng($lng) {
		$this->lng = $lng;
		
		return $this;
	}
	
	/**
	 * Get lng
	 *
	 * @return float
	 */
	public function getLng() {
		return $this->lng;
	}
}
