<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\BaseTab;

/**
 * @ORM\Entity
 * @ORM\Table(name="area_category_tab")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\AreaCategoryTabTranslation")
 */
class AreaCategoryTab extends BaseTab
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AreaCategory", inversedBy="tabs")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $page;

    /**
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\AreaCategoryBlock",
     *      mappedBy="tab",
     *      orphanRemoval=true, cascade={"persist"}
     * )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $blocks;
}
