<?php

namespace AppBundle\DataFixtures\ORM;

use \DateTime;
use AppBundle\Entity\Post;
use AppBundle\Utils\TextUtils;
use Symfony\Component\Yaml\Parser;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Symfony\Component\DependencyInjection\ContainerInterface;
class LoadPostData extends AbstractLoadData 
{
    private $container;

    public function getOrder()
    {
        return 5;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getContainer()
    {
         return $this->container;
    }

    public function load(ObjectManager $manager)
    {
    	$fileLocator = $this->container->get('file_locator');
    	$yamlParser = new Parser();
    	$this->loadPosts($manager, $fileLocator, $yamlParser);
    }

    private function loadPosts(ObjectManager $manager, $fileLocator, $yamlParser) {
        
        $path = $fileLocator->locate('@AppBundle/DataFixtures/data/posts.yml');
    	$datas = $yamlParser->parse(file_get_contents($path));
    	$i = 0;
    	foreach ($datas as $data) {
    	    
    	    $i++;
    	    
    		$post = new Post();
    		$post->setPublished($data['it']['published']);
    		$post->setTitle($data['it']['title']);
    		$post->setSubtitle($data['it']['subtitle']);
    		$post->setSlug(TextUtils::slugify($post->getTitle()));
    		$post->setType($data['it']['type']);
    		$post->setAbstract($data['it']['abstract']);
    		$post->setMain($data['it']['main']);
    		$post->setLocale('it_IT');
    		
    		if (array_key_exists('previewImage', $data['it'])) {
    		    $img = $this->createImage(__DIR__.'/../data/media/img', $data['it']['previewImage'], "post preview " . $i, 'post');
    		    $post->setPreviewImage($img);
    		}
    		
    		if (array_key_exists('detailImage', $data['it'])) {
    		    $img = $this->createImage(__DIR__.'/../data/media/img', $data['it']['detailImage'], "post detail " . $i, 'post');
    		    $post->setDetailImage($img);
    		}

    		$manager->persist($post);
    		$manager->flush();
    		$post = $manager->find('AppBundle\Entity\post', $post->getId());
    		$post->setTitle($data['en']['title']);
    		$post->setSubtitle($data['en']['subtitle']);
    		$post->setSlug(TextUtils::slugify($post->getTitle()));
    		$post->setAbstract($data['en']['abstract']);
    		$post->setMain($data['en']['main']);
    		$post->setLocale('en_GB');
    		$manager->persist($post);
    		$manager->flush();			    		
    	}
    	
    }
    

}
