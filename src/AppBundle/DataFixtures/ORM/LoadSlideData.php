<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Link;
use AppBundle\Entity\LinkGroup;
use AppBundle\Entity\Slide;

use Symfony\Component\Yaml\Parser;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Symfony\Component\DependencyInjection\ContainerInterface;
class LoadSlideData extends AbstractLoadData
{
    private $container;

    public function getOrder()
    {
        return 8;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    protected function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }
        return $this->container->get('doctrine');
    }

    public function load(ObjectManager $manager)
    {
        $fileLocator = $this->container->get('file_locator');
        $yamlParser = new Parser();
        $this->loadSlides($manager, $fileLocator, $yamlParser);
    }

    private function loadSlides(ObjectManager $manager, $fileLocator, $yamlParser) {
        //Creo il gruppo usato per i link nello slider
        $sliderLinkGroup= new LinkGroup();
        $sliderLinkGroup->setPublished(true);
        $sliderLinkGroup->setLabel('Link for HP slider');
        $sliderLinkGroup->setType(Slide::LINK_GROUP_TYPE);
        $manager->persist($sliderLinkGroup);
        $manager->flush();

        $link1 = new Link();
        $link1->setType('Internal');
        $link1->setPage($manager->find('AppBundle\Entity\Page',2));
        $link1->setLabel('Link1 per linkSlider');
        $link1->setPublished(true);
        $link1->setLinkGroup($sliderLinkGroup);
        $manager->persist($link1);
        $manager->flush();

        $link2 = new Link();
        $link2->setType('External');
        $link2->setUrl('http://www.enistation.com');
        $link2->setLabel('Link2 per linkSlider');
        $link2->setPublished(true);
        $link2->setLinkGroup($sliderLinkGroup);
        $manager->persist($link2);
        $manager->flush();

        $path = $fileLocator->locate('@AppBundle/DataFixtures/data/slides.yml');
        $datas = $yamlParser->parse(file_get_contents($path));
        $i = 0;

        foreach ($datas as $data) {

            $i++;

            $newSlide = new Slide();
            $newSlide->setPublished($data['it']['published']);
            $newSlide->setFirstLine($data['it']['firstLine']);
            $newSlide->setSecondLine($data['it']['secondLine']);
            if ($data['it']['badge']) {
                $newSlide->setBadge($data['it']['badge']);
            }
            $newSlide->setSlider($data['it']['slider']);
            if ($data['it']['image']) {
                $img = $this->createImage(__DIR__ . '/../data/media/img', $data['it']['image'], "slide" . $i, 'slide');
                $newSlide->setImage($img);
            }

            if ($data['it']['link']) {
                /** @var Link $link */
                $link = $data['it']['link']=='predefined2'? $link2 : $link1;
                $newSlide->setLink($link);
            }

            if ($data['it']['position']) {
                $newSlide->setPosition($data['it']['position']);
            }


            $manager->persist($newSlide);
            $manager->flush();
            $newSlide = $manager->find('AppBundle\Entity\Slide', $newSlide->getId());
            $newSlide->setFirstLine($data['en']['firstLine']);
            $newSlide->setSecondLine($data['en']['secondLine']);
            if ($data['en']['badge']) {
                $newSlide->setBadge($data['en']['badge']);
            }
            $newSlide->setLocale('en_GB');
            $manager->persist($newSlide);
            $manager->flush();
        }
    }
}
