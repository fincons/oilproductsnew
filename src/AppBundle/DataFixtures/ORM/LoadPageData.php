<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\EmailBlock;
use AppBundle\Entity\Page;
use AppBundle\Entity\Tab;
use AppBundle\Entity\TextBlock;
use Symfony\Component\Yaml\Parser;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Symfony\Component\DependencyInjection\ContainerInterface;
class LoadPageData extends AbstractLoadData
{
    private $container;

    public function getOrder()
    {
        return 3;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function load(ObjectManager $manager)
    {
    	$fileLocator = $this->container->get('file_locator');
    	$yamlParser = new Parser();
    	$this->loadPages($manager, $fileLocator, $yamlParser);
    }

    private function loadPages(ObjectManager $manager, $fileLocator, $yamlParser) {
    	$path = $fileLocator->locate('@AppBundle/DataFixtures/data/pages.yml');
    	$datas = $yamlParser->parse(file_get_contents($path));
    	foreach ($datas as $data) {
            $this->loadPage($manager, $data);
    	}
    }

    private function loadPage(ObjectManager $manager, $data) {
        $page = new Page();
        $page->setPublished($data['it']['published']);
        $page->setTitle($data['it']['title']);
        $page->setSlug($data['it']['slug']);
        $page->setRouteId($data['it']['routeId']);

        if (array_key_exists ('bannerImage', $data['it'])) {
            $img = $this->createImage(__DIR__.'/../data/media/img', $data['it']['bannerImage'], "page banner " . $page->getTitle(), 'page');
            $page->setBannerImage($img);
        }

        $page->setLocale('it_IT');
        $manager->persist($page);
        $manager->flush();

        $page = $manager->find('AppBundle\Entity\Page', $page->getId());
        $page->setTitle($data['en']['title']);
        $page->setSlug($data['en']['slug']);
        $page->setLocale('en_GB');
        $manager->persist($page);
        $manager->flush();

        if($data['tabs']) {
            foreach($data['tabs'] as $data_tab) {
                $this->loadTab($manager, $page, $data_tab);
            }
        }
    }

    private function loadTab(ObjectManager $manager, $page, $data_tab) {
        $tab = new Tab();
        $tab->setPosition($data_tab['position']);
        $tab->setTitle($data_tab['it']['title']);
        $tab->setSlug($data_tab['it']['slug']);
        $tab->setLocale('it_IT');
        $tab->setPage($page);
        $manager->persist($tab);
        $manager->flush();

        $tab = $manager->find(sprintf('AppBundle\Entity%s\Tab', ''), $tab->getId());
        $tab->setTitle($data_tab['en']['title']);
        $tab->setSlug($data_tab['en']['slug']);
        $tab->setLocale('en_GB');
        $manager->persist($tab);
        $manager->flush();

        if ($data_tab['blocks']) {
            foreach ($data_tab['blocks'] as $data_block) {
                $this->loadBlock($manager, $tab, $data_block);
            }
        }
    }

    private function loadBlock(ObjectManager $manager, $tab, $data_block) {
        switch($data_block['type']) {
            case 'text':
                $block = new TextBlock();
                $block->setText($data_block["it"]["text"]);
                break;
            case 'email':
                $block = new EmailBlock();
                $block->setText($data_block["it"]["text"]);
                $block->setEmail($data_block["it"]["email"]);
                break;
            default:
                throw new \Exception(sprintf("Block type '%s' not found", $data_block['type']));
        }
        $block->setLocale('it_IT');
        $tab->addBlock($block);
        $manager->persist($block);
        $manager->flush();

        $block = $manager->find(sprintf('AppBundle\Entity%s\Block', ''), $block->getId());
        switch($data_block['type']) {
            case 'text':
                $block->setText($data_block["en"]["text"]);
                break;
            case 'email':
                $block->setText($data_block["en"]["text"]);
                $block->setEmail($data_block["en"]["email"]);
                break;
            default:
                throw new \Exception(sprintf("Block type '%s' not found", $data_block['type']));
        }
        $block->setLocale('en_GB');
        $manager->persist($block);
        $manager->flush();
    }
}
