<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\Finder\Finder;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;

abstract class AbstractLoadData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{

    public function getMediaManager()
    {
        return $this->getContainer()->get('sonata.media.manager.media');
    }
    
    public function createImage($dir, $fileName, $imgTitle, $categoryName) {
        $img = Finder::create()->name($fileName)->in($dir);
        foreach ($img as $file) {
            $mediaManager = $this->getMediaManager();
            $media = $mediaManager->create();
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            $media->setName($imgTitle);
            $media->setDescription("");
            $media->setAuthorName("");
            $media->setCopyright("");
            $categoryManager = $this->getContainer()->get('sonata.classification.manager.category');
            $category = $categoryManager->findOneBy(array(
            	'name' => $categoryName,
        	));
            $media->setCategory($category);
            $mediaManager->save($media, 'default', 'sonata.media.provider.image');
        }
        return $media;
    }
    
    public function createFile($dir, $fileName, $imgTitle, $categoryName) {
        $img = Finder::create()->name($fileName)->in($dir);
        foreach ($img as $file) {
            $mediaManager = $this->getMediaManager();
            $media = $mediaManager->create();
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            $media->setName($imgTitle);
            $media->setDescription("");
            $media->setAuthorName("");
            $media->setCopyright("");
            $categoryManager = $this->getContainer()->get('sonata.classification.manager.category');
            $category = $categoryManager->findOneBy(array(
                'name' => $categoryName,
            ));
            $media->setCategory($category);
            $mediaManager->save($media, 'default', 'sonata.media.provider.file');
        }
        return $media;
    }
    
}
