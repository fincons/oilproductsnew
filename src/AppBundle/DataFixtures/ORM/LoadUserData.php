<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractLoadData 
{

    private $container;

    function getOrder()
    {
        return 1;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getContainer()
    {
        return $this->container;
    }

    public function load(ObjectManager $manager)
    {
        // TODO: use a crypted password
        $to_change_password = 'dyXhMAUz5p';
        
        $manager = $this->getUserManager();
        
        $user = $manager->createUser();
        $user->setUsername('enioilsupport@digitalmill.it');
        $user->setEmail('enioilsupport@digitalmill.it');
        $user->setPlainPassword($to_change_password);
        $user->setEnabled(true);
        $user->setSuperAdmin(true);
        $user->setLocked(false);
        $manager->updateUser($user);
        
        $user = $manager->createUser();
        $user->setUsername('marco.fontana@doing.com');
        $user->setEmail('marco.fontana@doing.com');
        $user->setPlainPassword($to_change_password);
        $user->setEnabled(true);
        $user->setSuperAdmin(true);
        $user->setLocked(false);
        $manager->updateUser($user);

    }

    /**
     * @return \FOS\UserBundle\Model\UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->container->get('fos_user.user_manager');
    }

}
