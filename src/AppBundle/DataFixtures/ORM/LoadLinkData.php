<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use AppBundle\Entity\Link;
use AppBundle\Entity\LinkGroup;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;

use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadLinkData extends AbstractLoadData
{
    private $container;

    public function getOrder()
    {
        return 4;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function load(ObjectManager $manager)
    {
    	$fileLocator = $this->container->get('file_locator');
    	$yamlParser = new Parser();
    	$this->loadLinks($manager, $fileLocator, $yamlParser);
    }

    private function loadLinks(ObjectManager $manager, $fileLocator, $yamlParser) {
    	$path = $fileLocator->locate('@AppBundle/DataFixtures/data/links.yml');
    	$datas = $yamlParser->parse(file_get_contents($path));
    	foreach ($datas as $data) {

	    	$linkGroup = new LinkGroup();
	    	$linkGroup->setPublished($data['it']['published']);
	    	$linkGroup->setType($data['it']['type']);
	    	$linkGroup->setLabel($data['it']['label']);
	    	$linkGroup->setLocale('it_IT');
	    	$manager->persist($linkGroup);
	    	$manager->flush();

	    	$linkGroup = $manager->find('AppBundle\Entity\LinkGroup', $linkGroup->getId());
	    	$linkGroup->setLabel($data['en']['label']);
	    	$linkGroup->setLocale('en_GB');
	    	$manager->persist($linkGroup);
	    	$manager->flush();

	    	foreach ($data['links'] as $data_link) {

	    		$link = new Link();
	    		$link->setPublished($data_link['it']['published']);
	    		$link->setLabel($data_link['it']['label']);
	    		$link->setType($data_link['it']['type']);
	    		$link->setUrl($data_link['it']['url']);
	    		$link->setPosition($data_link['it']['position']);

	    		if (array_key_exists('format' , $data_link['it'])) {
	    		    $link->setFormat($data_link['it']['format']);
	    		}

	    		if ($data_link['it']['page_routeId']) {
	    			$repository = $this->container->get("doctrine")
	    			->getRepository('AppBundle:Page');
					$page = $repository->findOneBy(
    					array('routeId' => $data_link['it']['page_routeId'],
								'slug' => $data_link['it']['page_slug']));
                    if ($page) {
		                $link->setPage($page);
                    }
	    		}

	    		$link->setLinkGroup($linkGroup);
	    		$manager->persist($link);
	    		$manager->flush();

	    		$link = $manager->find('AppBundle\Entity\Link', $link->getId());
	    		$link->setLabel($data_link['en']['label']);
	    		$link->setUrl($data_link['en']['url']);
	    		$link->setLocale('en_GB');
	    		$manager->persist($link);
	    		$manager->flush();
	    	}

    	}
    }


}
