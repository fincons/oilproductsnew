<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Doctrine\Common\Persistence\ObjectManager;
use Sonata\ClassificationBundle\Model\ContextInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadContextData extends AbstractLoadData 
{
    private $container;

    public function getOrder()
    {
        return 2;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getContainer()
    {
        return $this->container;
    }

    public function load(ObjectManager $manager)
    {
    	$contextManager = $this->container->get('sonata.classification.manager.context');
    	$defaultContext = $contextManager->findOneBy(array(
    	    'id' => ContextInterface::DEFAULT_CONTEXT,
    	));
    	
    	if (!$defaultContext) {
    	    $defaultContext = $contextManager->create();
    	    $defaultContext->setId(ContextInterface::DEFAULT_CONTEXT);
    	    $defaultContext->setName('Default');
    	    $defaultContext->setEnabled(true);
    	    $contextManager->save($defaultContext);
    	} 
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category = $categoryManager->create();
    	$category->setName("default");
    	$category->setEnabled(true);
    	$category->setContext($defaultContext);
    	$categoryManager->save($category);
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category = $categoryManager->create();
    	$category->setName("default");
    	$category->setEnabled(true);
    	$category->setContext($defaultContext);
    	$categoryManager->save($category);
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category2 = $categoryManager->create();
    	$category2->setName("post");
    	$category2->setEnabled(true);
    	$category2->setContext($defaultContext);
    	$category2->setParent($category);
    	$categoryManager->save($category2);
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category3 = $categoryManager->create();
    	$category3->setName("product_category");
    	$category3->setEnabled(true);
    	$category3->setContext($defaultContext);
    	$category3->setParent($category);
    	$categoryManager->save($category3);
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category3 = $categoryManager->create();
    	$category3->setName("product");
    	$category3->setEnabled(true);
    	$category3->setContext($defaultContext);
    	$category3->setParent($category);
    	$categoryManager->save($category3);
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category3 = $categoryManager->create();
    	$category3->setName("page");
    	$category3->setEnabled(true);
    	$category3->setContext($defaultContext);
    	$category3->setParent($category);
    	$categoryManager->save($category3);
    	
    	$categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category3 = $categoryManager->create();
    	$category3->setName("contact_group");
    	$category3->setEnabled(true);
    	$category3->setContext($defaultContext);
    	$category3->setParent($category);
    	$categoryManager->save($category3);
        $categoryManager = $this->container->get('sonata.classification.manager.category');
    	$category3 = $categoryManager->create();
    	$category3->setName("slide");
    	$category3->setEnabled(true);
    	$category3->setContext($defaultContext);
    	$category3->setParent($category);
    	$categoryManager->save($category3);
    	
    }
    

}
