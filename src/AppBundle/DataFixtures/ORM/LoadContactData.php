<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactGroup;
use AppBundle\Entity\ContactSubGroup;
use AppBundle\Entity\ProductCategory;
use AppBundle\Utils\TextUtils;
use Symfony\Component\Yaml\Parser;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Symfony\Component\DependencyInjection\ContainerInterface;
class LoadContactData extends AbstractLoadData
{
    private $container;

    public function getOrder()
    {
        return 7;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    protected function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }
        return $this->container->get('doctrine');
    }

    public function load(ObjectManager $manager)
    {
        $fileLocator = $this->container->get('file_locator');
        $yamlParser = new Parser();
        $this->loadContactGroups($manager, $fileLocator, $yamlParser);
    }

    private function loadContactGroups(ObjectManager $manager, $fileLocator, $yamlParser) {

        $path = $fileLocator->locate('@AppBundle/DataFixtures/data/contacts.yml');
        $datas = $yamlParser->parse(file_get_contents($path));
        $i = 0;
        foreach ($datas as $data) {

            $i++;

            $newContactGroup = new ContactGroup();
            $newContactGroup->setPublished($data['it']['published']);
            $newContactGroup->setTitle($data['it']['title']);
            $newContactGroup->setSubtitle($data['it']['subtitle']);
            $newContactGroup->setSlug(TextUtils::slugify($newContactGroup->getTitle()));
            $newContactGroup->setIconName($data['it']['iconName']);
            $newContactGroup->setEmail($data['it']['email']);
            $newContactGroup->setLocale('it_IT');

            if ($data['it']['previewImage']) {
                $img = $this->createImage(__DIR__.'/../data/media/img', $data['it']['previewImage'], "contact_group preview " . $i, 'contact_group');
                $newContactGroup->setPreviewImage($img);
            }

            if ($data['it']['bannerImage']) {
                $img = $this->createImage(__DIR__.'/../data/media/img', $data['it']['bannerImage'], "contact_group detail " . $i, 'contact_group');
                $newContactGroup->setBannerImage($img);
            }
            if (array_key_exists('pdf',$data['it'])) {
                $sampleBrochureIT = $this->createFile(__DIR__ . '/../data/media/file/', 'SAMPLE-BROCHURE-IT.pdf', "sample brochure it", 'contact_group');
                $newContactGroup->setPdf($sampleBrochureIT);
            }


            $manager->persist($newContactGroup);
            $manager->flush();
            /** @var ContactGroup $newContactGroup */
            $newContactGroup = $manager->find('AppBundle\Entity\ContactGroup', $newContactGroup->getId());
            $newContactGroup->setTitle($data['en']['title']);
            $newContactGroup->setSubtitle($data['en']['subtitle']);
            $newContactGroup->setSlug(TextUtils::slugify($newContactGroup->getTitle()));
            $newContactGroup->setLocale('en_GB');
            // Set $newContactGroup in the declared ProductCategory
            if (array_key_exists('pdf',$data['it'])) {
                $sampleBrochureEN = $this->createFile(__DIR__ . '/../data/media/file/', 'SAMPLE-BROCHURE-EN.pdf', "sample brochure en", 'contact_group');
                $newContactGroup->setPdf($sampleBrochureEN);
            }
            // Set $newContactGroup in the declared ProductCategory
            if (array_key_exists('position',$data['it'])) {
                $newContactGroup->setPosition($data['it']['position']);
            }
            $manager->persist($newContactGroup);
            $manager->flush();

            // Set $newContactGroup in the declared ProductCategory
            if (array_key_exists('productCategory',$data['it'])) {
                /** @var ProductCategory $category */
                $category = $manager->getRepository('AppBundle:ProductCategory')->findOneByTitle($data['it']['productCategory']);
                $category->setContactGroup($newContactGroup);
                $manager->persist($category);
                $manager->flush();
            }

            if ($data['contacts']) {
                foreach ($data['contacts'] as $data_contact) {

                    $subGroup = $this->getDoctrine()
                        ->getRepository('AppBundle:ContactSubGroup')
                        ->findOneByTranslatableField('name', $data_contact["subGroup"]['it']['name']);
                    if(!$subGroup){
                        $subGroup = $this->createContactSubGroup($manager, $data_contact);
                    }
                    if (array_key_exists('position',$data_contact["subGroup"]['it'])) {
                        $subGroup->setPosition($data_contact["subGroup"]['it']['position']);
                    }
                    if(array_key_exists('iconName',$data_contact["subGroup"]['it'])){
                        $this->createAviationContact($manager, $data_contact, $newContactGroup, $subGroup);
                    } else {
                        $this->createContact($manager, $data_contact, $newContactGroup, $subGroup);
                    }
                }
            }
        }

    }

    /**
     * @param ObjectManager $manager
     * @param $data_contact
     * @return ContactSubGroup
     */
    private function createContactSubGroup(ObjectManager $manager, $data_contact)
    {
        $subGroup = new ContactSubGroup();
        $subGroup->setName($data_contact["subGroup"]['it']['name']);
        if(array_key_exists('iconName',$data_contact["subGroup"]['it'])){
            $subGroup->setIconName($data_contact["subGroup"]["it"]['iconName']);
        }
        $manager->persist($subGroup);
        $manager->flush();

        //Translate field
        /** @var ContactSubGroup $subGroup */
        $subGroup = $manager->find('AppBundle\Entity\ContactSubGroup', $subGroup->getId());
        $subGroup->setName($data_contact["subGroup"]["en"]['name']);
        $subGroup->setLocale('en_GB');
        $manager->persist($subGroup);
        $manager->flush();
        return $subGroup;
    }

    /**
     * @param ObjectManager $manager
     * @param $data_block
     * @param $newContactGroup
     * @return Contact
     */
    private function createContact(ObjectManager $manager, $data_block, $newContactGroup, $subGroup)
    {
        $newContact = new Contact();
        $newContact->setPublished($data_block["it"]["published"]);
        $newContact->setTitle($data_block["it"]["title"]);
        $newContact->setName($data_block["it"]["name"]);
        $newContact->setPhone($data_block["it"]["phone"]);
        $newContact->setEmail($data_block["it"]["email"]);
        $newContact->setFax($data_block["it"]["fax"]);
        $newContact->setMobile($data_block["it"]["mobile"]);
        // Set $newContactGroup in the declared ProductCategory
        if (array_key_exists('position',$data_block['it'])) {
            $newContact->setPosition($data_block['it']['position']);
        }
        $newContact->setContactGroup($newContactGroup);
        $newContact->setContactSubGroup($subGroup);
        $manager->persist($newContact);
        $manager->flush();

        /** @var Contact $newContact */
        $newContact = $manager->find('AppBundle\Entity\Contact', $newContact->getId());
        $newContact->setTitle($data_block["en"]["title"]);
        $newContact->setLocale('en_GB');
        $newContact->setContactGroup($newContactGroup);
        $manager->persist($newContact);
        $manager->flush();
        return $newContact;
    }
    /**
     * @param ObjectManager $manager
     * @param $data_block
     * @param $newContactGroup
     * @return Contact
     */
    private function createAviationContact(ObjectManager $manager, $data_block, $newContactGroup, $subGroup)
    {
        $newContact = new Contact();
        $newContact->setPublished($data_block["it"]["published"]);
        $newContact->setTitle($data_block["it"]["title"]);
        $newContact->setName($data_block["it"]["name"]);
        $newContact->setPhone($data_block["it"]["phone"]);
        $newContact->setCompany($data_block["it"]["company"]);
        $newContact->setIntoplane($data_block["it"]["intoplane"]);
        $newContact->setDayworkSchedule($data_block["it"]["dayworkschedule"]);
        $newContact->setAirportManager($data_block["it"]["airportManager"]);
        $newContact->setContactGroup($newContactGroup);
        $newContact->setContactSubGroup($subGroup);
        $manager->persist($newContact);
        $manager->flush();

        /** @var Contact $newContact */
        $newContact = $manager->find('AppBundle\Entity\Contact', $newContact->getId());
        $newContact->setTitle($data_block["en"]["title"]);
        $newContact->setLocale('en_GB');
        $newContact->setContactGroup($newContactGroup);
        $manager->persist($newContact);
        $manager->flush();
        return $newContact;
    }


}
