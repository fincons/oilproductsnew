<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use AppBundle\Utils\Constants;
use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\ProductClassificationRow;
use AppBundle\Utils\TextUtils;
use Ddeboer\DataImport\Reader\CsvReader;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\ORM\AbstractLoadData;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadProductData extends AbstractLoadData
{

    private $container;

    public function getOrder()
    {
        return 6;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
         return $this->container;
    }

    public function load(ObjectManager $manager)
    {

        $logger = $this->container->get('monolog.logger.enioil');
        
    	$fileLocator = $this->container->get('file_locator');
    	$path = $fileLocator->locate('@AppBundle/DataFixtures/data/official_product_classification_rows.csv');
    	$file = new \SplFileObject($path);
    	$reader = new CsvReader($file, '|');
    	$reader->setHeaderRowNumber(0);

    	$isMissingCreated = true;
    	$this->createProductCategoryIfMissing(Constants::EMPTY_CATEGORY_TITLE, null, null, null, null, null, null, $isMissingCreated, $manager);

    	$sampleBannerImgCategory = $this->createImage(__DIR__.'/../data/media/img/', 'CATEGORY-BANNER.jpg', "category banner sample", 'product_category');
    	$samplePreviewImgCategory = $this->createImage(__DIR__.'/../data/media/img/', 'CATEGORY-PREVIEW.jpg', "category preview sample", 'product_category');
    	$sampleThumbImgCategory = $this->createImage(__DIR__.'/../data/media/img/', 'CATEGORY-THUMB.jpg', "category thumb sample", 'product_category');
    	$sampleDetailImgCategory = $this->createImage(__DIR__.'/../data/media/img/', 'CATEGORY-DETAIL.jpg', "category detail sample", 'product_category');
    	$sampleBrochureCategoryIT = $this->createFile(__DIR__.'/../data/media/file/', 'SAMPLE-BROCHURE-IT.pdf', "sample brochure it", 'product_category');
    	$sampleBrochureCategoryEN = $this->createFile(__DIR__.'/../data/media/file/', 'SAMPLE-BROCHURE-EN.pdf', "sample brochure en", 'product_category');
    	$sampleBrochureProductIT = $this->createFile(__DIR__.'/../data/media/file/', 'SAMPLE-BROCHURE-IT.pdf', "sample brochure it", 'product');
    	$sampleBrochureProductEN = $this->createFile(__DIR__.'/../data/media/file/', 'SAMPLE-BROCHURE-EN.pdf', "sample brochure en", 'product');
    	 
    	
    	$isFirstLev2Created = false;
    	$isFirstLev3Created = false;
    	$isFirstLev4Created = false;
    	$isFirstLev5Created = false;
    	$isFirstProductCreated = false;

    	$i=1;
    	foreach ($reader as $row) {
    	        $logger->info('Importing row ' . $i);
    	        $productCategoryLev2 = $this->createProductCategoryIfMissing($row['lev2'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev2Created, $manager);
    	        $productCategoryLev3 = $this->createProductCategoryIfMissing($row['lev3'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev3Created, $manager);
    	        $productCategoryLev4 = $this->createProductCategoryIfMissing($row['lev4'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev4Created, $manager);
    	        $productCategoryLev5 = $this->createProductCategoryIfMissing($row['lev5'], $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureCategoryIT, $sampleBrochureCategoryEN, $isFirstLev5Created, $manager);
    	        $product = $this->createProductIfMissing($row['prodcode'], $row['prodname'], $sampleBrochureProductIT, $sampleBrochureProductEN, $isFirstProductCreated, $manager);
    	        $this->createProductClassificationRowIfMissing($productCategoryLev2, $productCategoryLev3, $productCategoryLev4, $productCategoryLev5, $product, $row['position'], $manager);
    	        $i++;    	        
    	}
    }

    private function createProductCategoryIfMissing($title, $sampleBannerImgCategory, $samplePreviewImgCategory, $sampleThumbImgCategory, $sampleDetailImgCategory, $sampleBrochureIT, $sampleBrochureEN, &$isFirstCreated, $manager) {
    	if (!$title) {
    		$title = Constants::EMPTY_CATEGORY_TITLE;
    	}
    	$category = $manager->getRepository('AppBundle:ProductCategory')->findOneByTitle($title);
    	if (!$category) {
       		$category = new ProductCategory();
       		$category->setLocale('it_IT');
    		$category->setTitle($title);
    		$category->setSlug(TextUtils::slugify($title));
    		if (!$isFirstCreated) {
    		    $category->setBannerImage($sampleBannerImgCategory);
    		    $category->setPreviewImage($samplePreviewImgCategory);
    		    $category->setThumbImage($sampleThumbImgCategory);
    		    $category->setDetailImage($sampleDetailImgCategory);
    		    $category->setBrochure($sampleBrochureIT);
    		    $category->setDescription("<p>Descrizione della categoria <strong>" . $title . "</strong>...</p><p>Eni &egrave; da sempre impegnata....</p>");
    		    $category->setShortDescription("<p>Descrizione breve della categoria <strong>" . $title . "</strong>...</p>");
    		}
    		
    		$manager->persist($category);
    		$manager->flush();

    		if (!$isFirstCreated) {
    		    $category = $manager->find('AppBundle\Entity\ProductCategory', $category->getId());
    		    $category->setLocale('en_GB');
    		    $category->setTitle($title . " EN");
    		    $category->setSlug(TextUtils::slugify($category->getTitle()));
    		    $category->setDescription("<p>Description of category <strong>" . $title . "</strong>...</p><p>Eni &egrave; da sempre impegnata....</p>");
    		    $category->setShortDescription("<p>Short description of category <strong>" . $title . "</strong>...</p>");
    		    $category->setBrochure($sampleBrochureEN);
    		    $manager->persist($category);
    		    $manager->flush();
    		}

    		$isFirstCreated = true;
    	}
    	return $category;
    }

    private function createProductIfMissing($code, $name, $sampleBrochureIT, $sampleBrochureEN, &$isFirstCreated, $manager) {
    	if (!$code) {
    		return null;
    	}
    	$product = $manager->getRepository('AppBundle:Product')->findOneByCode($code);
    	if (!$product) {
    		$product = new Product();
    		$product->setLocale('it_IT');
    		$product->setTitle($name);
    		$product->setSlug(TextUtils::slugify($product->getTitle()));
    		$product->setCode($code);
    		$product->setCountry('IT');
    		if (!$isFirstCreated) {
    		    $product->setBrochure($sampleBrochureIT);    		    
    		}
            $product->setFromAlis(false);
    		$manager->persist($product);
    		$manager->flush();
    		
    		if (!$isFirstCreated) {
    		    $product = $manager->find('AppBundle\Entity\Product', $product->getId());
    		    $product->setLocale('en_GB');
    		    $product->setTitle($name . " EN");
    		    $product->setSlug(TextUtils::slugify($product->getTitle()));
    		    $product->setBrochure($sampleBrochureEN);   
    		    $manager->persist($product);
    		    $manager->flush();
    		}
    		
    		$isFirstCreated = true;
    	}
    	return $product;
    }


    private function createProductClassificationRowIfMissing($productCategoryLev2, $productCategoryLev3, $productCategoryLev4, $productCategoryLev5, $product, $position, $manager) {
    	$productClassificationRow = $manager->getRepository('AppBundle:ProductClassificationRow')->findOneBy(
    		array(	'published'=> true,
    				'productCategoryLev2' => $productCategoryLev2,
    				'productCategoryLev3' => $productCategoryLev3,
    				'productCategoryLev4' => $productCategoryLev4,
    				'productCategoryLev5' => $productCategoryLev5,
    		        'product' => $product,
    		)
    	);

    	if (!$productClassificationRow) {
    		$productClassificationRow = new ProductClassificationRow();
    		$productClassificationRow->setProductCategoryLev2($productCategoryLev2);
    		$productClassificationRow->setProductCategoryLev3($productCategoryLev3);
    		$productClassificationRow->setProductCategoryLev4($productCategoryLev4);
    		$productClassificationRow->setProductCategoryLev5($productCategoryLev5);
    		$productClassificationRow->setProduct($product);
    		$productClassificationRow->setPosition($position);
    		$manager->persist($productClassificationRow);
    		$manager->flush();
    	} else {
    	    $logger = $this->container->get('monolog.logger.enioil');
    	    $logger->info('Row exists...');
    	}
    }






}
