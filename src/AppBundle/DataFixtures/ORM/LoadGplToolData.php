<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\City;
use AppBundle\Entity\Company;
use AppBundle\Entity\Office;
use AppBundle\Entity\Province;

class LoadGplToolData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 10;
    }

    public function load(ObjectManager $manager) {
        $fileLocator = $this->container->get('file_locator');
        $path = $fileLocator->locate('@AppBundle/DataFixtures/data/offices.csv');
        $em = $this->container->get('doctrine.orm.entity_manager');

        $repoCompany = $em->getRepository('AppBundle:Company');
        $repoOffice = $em->getRepository('AppBundle:Office');
        $repoCity = $em->getRepository('AppBundle:City');
        $repoProvince = $em->getRepository('AppBundle:Province');

        $fp = fopen($path, 'r');
        while ($row = fgetcsv($fp, null, ';')) {
            list($ownedProvNames, $companyName, $cyl, $lb, $cityName, $addr, $provName, $cap, $email, $phone, $fax, $lat, $lng) = $row;

            $ownedProvinces = [];
            foreach(explode(';', $ownedProvNames) as $p){
                $p = trim($p);
                $ownedProvince = $repoProvince->findOneByName($p);
                if(empty($ownedProvince)){
                    throw new \Exception(sprintf('Provincia non trovata nel database: %s', $p));
                    continue;
                }
                $ownedProvinces[] = $ownedProvince;
            }

            $city = $repoCity->findOneByName($cityName);
            if(empty($city)) {
                throw new \Exception(sprintf('Città non trovata nel database: %s', $cityName));
            }

            $province = $repoProvince->findOneByShortName($provName);
            if (empty($province)) {
                throw new \Exception(sprintf('Provincia non trovata nel database: %s', $provName));
            }

            $company = $repoCompany->findOneByName($companyName);
            if (empty($company)) {
                $company = (new Company)
                    ->setName($companyName)
                    ->setCylinder($cyl == 'true' ? true : false)
                    ->setLittleBarrel($lb == 'true' ? true : false)
                ;
                $em->persist($company);
                $em->flush();
            }

            $office = $repoOffice->findByCompanyCity($company, $city);
            if (empty($office)) {
                $office = (new Office)
                    ->setCompany($company)
                    ->setCity($city)
                    ->setProvince($province)
                    ->setAddress($addr)
                    ->setCap($cap)
                    ->setEmail($email)
                    ->setPhone($phone)
                    ->setFax($fax)
                    ->setLat(floatval($lat))
                    ->setLng(floatval($lng))
                ;
                $em->persist($office);
            }
            foreach ($ownedProvinces as $p) {
                $office->addProvince($p);
            }
            $em->flush();
        }
        fclose($fp);
    }
}
