-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Apr 24, 2015 alle 13:34
-- Versione del server: 5.5.43-0ubuntu0.14.04.1
-- Versione PHP: 5.5.9-1ubuntu4.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucidatabase`
--

--
-- Dump dei dati per la tabella `static_province`
--
TRUNCATE `static_province`;
INSERT INTO `static_province` (`id`, `name`, `short_name`) VALUES
(1, 'Chieti', 'CH'),
(2, 'L''Aquila', 'AQ'),
(3, 'Pescara', 'PE'),
(4, 'Teramo', 'TE'),
(5, 'Matera', 'MT'),
(6, 'Potenza', 'PZ'),
(7, 'Catanzaro', 'CZ'),
(8, 'Cosenza', 'CS'),
(9, 'Crotone', 'KR'),
(10, 'Reggio Calabria', 'RC'),
(11, 'Vibo Valentia', 'VV'),
(12, 'Avellino', 'AV'),
(13, 'Benevento', 'BN'),
(14, 'Caserta', 'CE'),
(15, 'Napoli', 'NA'),
(16, 'Salerno', 'SA'),
(17, 'Bologna', 'BO'),
(18, 'Ferrara', 'FE'),
(19, 'Forlì - Cesena', 'FC'),
(20, 'Modena', 'MO'),
(21, 'Parma', 'PR'),
(22, 'Piacenza', 'PC'),
(23, 'Ravenna', 'RA'),
(24, 'Reggio Emilia', 'RE'),
(25, 'Rimini', 'RN'),
(26, 'Gorizia', 'GO'),
(27, 'Trieste', 'TS'),
(28, 'Udine', 'UD'),
(29, 'Frosinone', 'FR'),
(30, 'Latina', 'LT'),
(31, 'Rieti', 'RI'),
(32, 'Roma', 'RM'),
(33, 'Viterbo', 'VT'),
(34, 'Genova', 'GE'),
(35, 'Imperia', 'IM'),
(36, 'La Spezia', 'SP'),
(37, 'Savona', 'SV'),
(38, 'Bergamo', 'BG'),
(39, 'Brescia', 'BS'),
(40, 'Como', 'CO'),
(41, 'Cremona', 'CR'),
(42, 'Lecco', 'LC'),
(43, 'Lodi', 'LO'),
(44, 'Mantova', 'MN'),
(45, 'Milano', 'MI'),
(46, 'Pavia', 'PV'),
(47, 'Sondrio', 'SO'),
(48, 'Varese', 'VA'),
(49, 'Ancona', 'AN'),
(50, 'Ascoli Piceno', 'AP'),
(51, 'Macerata', 'MC'),
(52, 'Pesaro Urbino', 'PU'),
(53, 'Campobasso', 'CB'),
(54, 'Isernia', 'IS'),
(55, 'Alessandria', 'AL'),
(56, 'Asti', 'AT'),
(57, 'Biella', 'BI'),
(58, 'Cuneo', 'CN'),
(59, 'Novara', 'NO'),
(60, 'Torino', 'TO'),
(61, 'Vercelli', 'VC'),
(62, 'Bari', 'BA'),
(63, 'Brindisi', 'BR'),
(64, 'Foggia', 'FG'),
(65, 'Lecce', 'LE'),
(66, 'Taranto', 'TA'),
(67, 'Cagliari', 'CA'),
(68, 'Nuoro', 'NU'),
(69, 'Oristano', 'OR'),
(70, 'Sassari', 'SS'),
(71, 'Agrigento', 'AG'),
(72, 'Caltanissetta', 'CL'),
(73, 'Catania', 'CT'),
(74, 'Enna', 'EN'),
(75, 'Messina', 'ME'),
(76, 'Palermo', 'PA'),
(77, 'Ragusa', 'RG'),
(78, 'Siracusa', 'SR'),
(79, 'Trapani', 'TP'),
(80, 'Arezzo', 'AR'),
(81, 'Firenze', 'FI'),
(82, 'Grosseto', 'GR'),
(83, 'Livorno', 'LI'),
(84, 'Lucca', 'LU'),
(85, 'Massa Carrara', 'MS'),
(86, 'Pisa', 'PI'),
(87, 'Pistoia', 'PT'),
(88, 'Prato', 'PO'),
(89, 'Siena', 'SI'),
(90, 'Bolzano', 'BZ'),
(91, 'Trento', 'TN'),
(92, 'Perugia', 'PG'),
(93, 'Terni', 'TR'),
(94, 'Aosta', 'AO'),
(95, 'Belluno', 'BL'),
(96, 'Padova', 'PD'),
(97, 'Pordenone', 'PN'),
(98, 'Rovigo', 'RO'),
(99, 'Treviso', 'TV'),
(100, 'Venezia', 'VE'),
(101, 'Verona', 'VR'),
(102, 'Vicenza', 'VI'),
(103, 'San Marino', 'RS'),
(104, 'Olbia - Tempio', 'OT'),
(105, 'Verbano - Cusio - Ossola', 'VB'),
(106, 'Ogliastra', 'OG'),
(107, 'Medio Campidano', 'VS'),
(108, 'Carbonia - Iglesias', 'CI'),
(109, 'Monza Brianza', 'MB'),
(110, 'Fermo', 'FM'),
(111, 'Barletta - Andria - Trani', 'BT'),
(112, 'Estero', 'XX');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
