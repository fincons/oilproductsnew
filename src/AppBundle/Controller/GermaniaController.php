<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;

class GermaniaController extends AbstractController {
	
	public function dataSheetBannerAction() {
    	$logger = $this->get('monolog.logger.enioil');
    	$logger->debug('Data Sheet Banner Action');
    	
    	$request = $this->get('request');
    	
    	$current_locale = $request->getLocale();
    	
    	$locale_de = array('de_DE', 'en_DE');

    	if( in_array($current_locale, $locale_de) ) {
    	
	    	//valori di default
	    	$title = 'Schmierstoffe Datenblatt';
	    	$url = 'http://www.eni-datenblatt.de/'; //safety datasheet link: http://www.eni-datenblatt.de/.
	    	
	    	//Provo a cercarli dal Db se non lo trovo uso i valori di default
	    	$linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
	    			array('type'=> 'dataSheetBanner', 'published' => true)
	    			);
	    	
	    	if ($linkGroup) {
				
				$links = $linkGroup->getLinks ();
				
				foreach ( $links as $link ) {
					$lang_permitted = $link->getLanguage ();
					
					if ( (strpos ( $lang_permitted, $current_locale ) !== false
							|| strpos ( $lang_permitted, 'ALL' ) !== false)
							&& $link->getPublished ()) {
						$url = $link->getUrl ();
						$title = $link->getLabel ();
						$logger->debug ( 'Link found url: ' + $url + ' - title: ' + $title );
						// prendo solo il primo. in teoria dovrei averne soltanto uno
						break;
						
					}
				}
			}
    	      
            return $this->render('AppBundle:frontend/partial:dataSheetBannerDe.html.twig', array(
                'title' => $title,
                'url' => $url,
            ));
        }else{
        	$logger->debug( 'Locale diverso da Germania. Locale: ' + $request->getLocale() );
        	return $this->render('AppBundle:frontend:totalEmpty.html.twig', array());
        }
    }
}
