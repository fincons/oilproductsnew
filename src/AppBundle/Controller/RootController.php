<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RootController extends AbstractController
{

	public function rootAction(Request $request)
	{
		$defaultProtocol = $this->getParameter('default_protocol');
		
		$defaultLocale = $this->container->getParameter('locale');
				
		$uri = $this->generateUrl('homepage', array('_locale'=>$defaultLocale));
		
		//esegue il redirect su una pagina html che in base al GEO IP e lingua del browser indirizza nella sezione(Nazione) corretta
		$uri = "/index.php";
		
		$absoluteurl = $defaultProtocol . '://' . $request->getHttpHost() . $uri;
		$baseurl = $request->getScheme() . '://' . $request->getHttpHost();
		
		$response = new RedirectResponse($absoluteurl, 301);
		
		$response->setContent($this->render(
		    'AppBundle:frontend:redirect.html.twig', array(
		        'absoluteurl' => $absoluteurl,
		        'baseurl' => $baseurl,
		        'uri' => $uri
		    )
		    ));

		
		
		return $response;

	}
}
