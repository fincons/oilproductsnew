<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use AppBundle\Utils\FilterLocaleUtils;

class PostController extends AbstractController
{
    
    public function listAction()
    {
    	
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('post list action');
        
        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByRouteId($this->container->get('request')->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }

    	$posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findBy(
            array('published'=> true),
            array('referenceDate' => 'DESC')
    	);

        $current_locale =  $this->get('request')->getLocale();
        $posts_all = array();
        //Filtro per locale
        if ($posts) {
            foreach ($posts as $posts_temp) {
                $lang_permitted = $posts_temp->getLanguage ();
                if( FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale)){
                    $posts_all[] = $posts_temp;
                }
            }
        }

        //creo il link con lo slug tradotto preso dal file i18n.xml
        $link = array();
        $link = $this->container->get("UrlManager")->getRoutesByRouteId("post_list", $current_locale);
        
        $this->customizeSeoForBasePage($page, $link);
     	
    	$adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, null);
    	
    	$breadcrumb = $this->get("breadcrumb_manager")->addHome()->addItem(array('title' => $page->getTitle(), 'url' => ""));
    	
    	return $this->render('AppBundle:frontend/post:post_list.html.twig', 
    	    array('posts' => $posts_all,
    	        'adv_language_switcher_rows' => $adv_language_switcher_rows, 
    	        'breadcrumb' => $breadcrumb,
    	        'page' =>$page));
    }
 
    public function detailAction($slug)
    {

        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('post view action');
        
        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByRouteId($this->container->get('request')->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }
        
        $current_locale =  $this->get('request')->getLocale();        
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->findOneByTranslatableField('slug',$slug,$current_locale); 
    	
        if (!$post || !$post->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }
        
    	$this->customizeSeoForPost($post);

    	$adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, $slug, 'Post', $post);
    	
    	$breadcrumb = $this->get("breadcrumb_manager")->addHome()->addPageByRouteId('post_list')->addItem(array('title' => $post->getTitle(), 'url' => ""));
    	
    	return $this->render('AppBundle:frontend/post:post_detail.html.twig', array(
    			'post' => $post,
    	        'adv_language_switcher_rows' => $adv_language_switcher_rows,
    	        'breadcrumb' => $breadcrumb,
    			'form' => false,
    	));
    }

    public function highlightsPostsAction()
    {
        $highlightedPosts = $this->getDoctrine()->getRepository('AppBundle:Post')->findBy(
            array('published'=> true),
            array('highlight' => 'DESC','referenceDate' => 'DESC')
        );

        $current_locale =  $this->get('request')->getLocale();
        $highlightedPosts_all = array();
        $highlightedPosts_count = 0;
        //Filtro per locale
        if ($highlightedPosts) {
            foreach ($highlightedPosts as $highlightedPosts_temp) {
                $lang_permitted = $highlightedPosts_temp->getLanguage ();
                if( FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale) && $highlightedPosts_count < 1){
                    $highlightedPosts_all[] = $highlightedPosts_temp;
                    $highlightedPosts_count++;
                }
            }
        }

        $otherPosts = $this->getDoctrine()->getRepository('AppBundle:Post')->findBy(
            array('published'=> true),
            array('showInHomePage' => 'DESC','referenceDate' => 'DESC')
        );

        $current_locale =  $this->get('request')->getLocale();
        $otherPosts_all = array();
        $otherPosts_count = 0;
        //Filtro per locale
        if ($otherPosts) {
            foreach ($otherPosts as $otherPosts_temp) {
                $lang_permitted = $otherPosts_temp->getLanguage ();
                if( FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale) && $otherPosts_count < 3){
                    $otherPosts_all[] = $otherPosts_temp;
                    $otherPosts_count++;
                }
            }
        }

        return $this->render('AppBundle:frontend/partial:highlights_posts.html.twig', array(
            'highlightedPosts' => $highlightedPosts_all,
            'otherPosts' => $otherPosts_all,
        ));
    }

    private function customizeSeoForPost($post) {
        
        $title = $post->getTitle();
        $description = (empty($post->getMetaDescription()) ? $post->getAbstract() : $post->getMetaDescription());
        $keywords = $post->getMetaKeywords();
        
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->setTitle($title.' | Eni Oil Products');
        $seoPage->addMeta('property', 'og:title', $title);
        if ($description) {
            $seoPage->addMeta('name', 'description', $description);
            $seoPage->addMeta('property', 'og:description', $description);
        }
        if($keywords) {
            $seoPage->addMeta('name', 'keywords', $keywords);
        }
    }

    

}

