<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Sonata\TranslationBundle\Admin\Extension\AbstractTranslatableAdminExtension;
use AppBundle\Form\Type\PageTabsType;

/**
 * PageController
 */
class PageController extends CRUDController
{
    public function tabsAction($id) {
        $request = $this->getRequest();
        $id      = $request->get($this->admin->getIdParameter());
        $object  = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $this->admin->checkAccess('edit', $object);

        $form = $this->createForm(PageTabsType::class, $object, $this->admin->getTabsFormOptions());
        $form->handleRequest($request);

        if ($form->isValid()) {
            // check the csrf token
            $this->validateCsrfToken('sonata.tabs');
            try {
                // Save tabs
                $this->admin->update($object);

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans(
                        'flash_edit_success',
                        array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                        'SonataAdminBundle'
                    )
                );

                // redirect to edit mode
                return $this->redirectTo($object);
            }
            catch (ModelManagerException $e) {
                $this->handleModelManagerException($e);
                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans(
                        'flash_edit_error',
                        array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                        'SonataAdminBundle'
                    )
                );
            }
        }

        $view = $form->createView();
        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, 'AppBundle:admin:tabs_form_fields.html.twig');

        return $this->render('AppBundle:admin:page_tabs.html.twig', array(
            'object'     => $object,
            'form'       => $view,
            'action'     => 'tabs',
            'csrf_token' => $this->getCsrfToken('sonata.tabs')
        ));
    }

    // Added 'btn_update_and_tabs' redirect target
    protected function redirectTo($object)
    {
        $request = $this->getRequest();

        $url = false;

        if (null !== $request->get('btn_update_and_list')) {
            $url = $this->admin->generateUrl('list');
        }

        if (null !== $request->get('btn_update_and_tabs')) {
            $url = $this->admin->generateObjectUrl('tabs', $object);
        }

        if (null !== $request->get('btn_create_and_create')) {
            $params = array();
            if ($this->admin->hasActiveSubClass()) {
                $params['subclass'] = $request->get('subclass');
            }
            $url = $this->admin->generateUrl('create', $params);
        }

        if ($this->getRestMethod() === 'DELETE') {
            $url = $this->admin->generateUrl('list');
        }

        if (!$url) {
            foreach (array('edit', 'show') as $route) {
                if ($this->admin->hasRoute($route) && $this->admin->isGranted(strtoupper($route), $object)) {
                    $url = $this->admin->generateObjectUrl($route, $object);
                    break;
                }
            }
        }

        if (!$url) {
            $url = $this->admin->generateUrl('list');
        }

        return new RedirectResponse($url);
    }
}
