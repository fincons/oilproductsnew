<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Finder\Finder;
use Exception;

class DownloadController extends AbstractController
{
   
    public function techFileAction($fileid)
    {
    
    	$logger = $this->get('monolog.logger.enioil');
    	$logger->debug('download tech file action for file id ' . $fileid);
    	
    	$matching_term = "";
    	$storage_dir = "";
    	
    	// old alis pdf
    	if (substr($fileid, 0, 2 ) === "a-") {
    		$alisfileid = substr($fileid, 2); 
    		//$alis_storage_dir = $this->get('kernel')->getRootDir(). "/../web/uploads/media/alis/";
    		$storage_dir = $this->getParameter('assetic.write_to') . "/uploads/media/alis/";
    		$matching_term = $alisfileid .'-';
    		$file_name_suffix = $alisfileid .'-';
    		
    	// epic pdf	
    	} else if (substr($fileid, 0, 2 ) === "e-") {
    		$epicfileid = substr($fileid, 2); 
    		$storage_dir = $this->get('epic_utils')->getEpicPdfDir();
    		$matching_term = $epicfileid;
    		$file_name_suffix = "";
    		
    	}
    	
    	$filename = "";
    	if($matching_term && $storage_dir){
        	$finder = new Finder();
        	$finder->name($matching_term .'*');
        	
        	foreach ($finder->in($storage_dir) as $file) {
        		// only one file for id...
        		$filename = $file;
        	}
    	}
    	
    	if ($filename) {
    		$download_filename = str_replace($file_name_suffix,"", basename($filename));
    		$response = new BinaryFileResponse($filename);
    		$response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $download_filename);
    		return $response;
    	} else {
    	    $defaultLocale = $this->container->getParameter('locale');    	    
    	    $uri = $this->generateUrl('homepage', array('_locale'=>$defaultLocale));
    	    $response = new RedirectResponse($uri, 302);
    	    
    	    return $response;
    		//throw new Exception("File for fileid " . $fileid  . " is missing" );
    	}

    	
     }
     

    
    
}
