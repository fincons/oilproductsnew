<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactGroup;
use AppBundle\Entity\Page;
use AppBundle\Utils\FilterLocaleUtils;

class ContactController extends AbstractController
{

    public function listAction()
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('Contact list action');

        $page = $this->configurePage();
        
        $current_locale = $this->get('request')->getLocale();
        
        //creo il link con lo slug tradotto preso dal file i18n.xml
        $link = array();
        $link = $this->container->get("UrlManager")->getRoutesByRouteId("contact_list", $current_locale);
        
        $this->customizeSeoForBasePage($page, $link);
        
        
        $breadcrumb = $this->get("breadcrumb_manager")->addHome()->addItem(array('title' => $page->getTitle(), 'url' => ""));

        $contactGroups_all = $this->getDoctrine()->getRepository('AppBundle:ContactGroup')->findBy(
            array('published' => true),
            array(
                'position' => 'ASC',
                'updatedAt' => 'DESC')
        );

        
        $contactGroups = array();
        //Filtro per locale
        if ($contactGroups_all) {
            foreach ($contactGroups_all as $contactGroups_temp) {
                $lang_permitted = $contactGroups_temp->getLanguage();
                if (FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale)) {
                    $contactGroups[] = $contactGroups_temp;
                }
            }
        }


        $adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, null);

        $render_path = 'AppBundle:frontend/contact:contact_list.html.twig';

        /* AS: Dopo aver fatto la modifica, gli utenti hanno cambiato idea e siamo tornati indietro.
         * Lascio il codice per eventali usi futuri.
         $request = $this->get('request');
        if ($request->getLocale() === 'de_DE' or $request->getLocale() === 'en_DE') {
            $render_path = 'AppBundle:frontend/contact:contact_list_de.html.twig';
        }*/

        return $this->render($render_path, array(
            'page' => $page,
            'breadcrumb' => $breadcrumb,
            'adv_language_switcher_rows' => $adv_language_switcher_rows,
            'contactGroups' => $contactGroups
        ));
    }

    public function detailAction($slug)
    {

        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('ContactGroup view action');

        $repository = $this->getDoctrine()->getRepository('AppBundle:ContactGroup');
        $contactGroup = $repository->findOneByTranslatableField('slug', $slug);
        if (!$contactGroup || !$contactGroup->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }


        // Configure Page stuff
        $page = $this->configurePage();
        $this->customizeSeoForContactGroup($contactGroup);
        $breadcrumb = $this->get("breadcrumb_manager")->addHome()->addPageByRouteId('contact_list')->addItem(array('title' => $contactGroup->getTitle(), 'url' => ""));
        $adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, $slug, 'ContactGroup', $contactGroup);

        // Build a map like [subgroup][title][contact]
        $contactsSubGroupTitleMap = $this->groupBySubgroupAndTitle($contactGroup);

        $countriesClass = array_values($contactsSubGroupTitleMap)[0]['subGroupObj']->getIconName() == '' ? '' : 'countries';

        $render_path = 'AppBundle:frontend/contact:contact_detail.html.twig';

        /* AS: Dopo aver fatto la modifica, gli utenti hanno cambiato idea e siamo tornati indietro.
         * Lascio il codice per eventali usi futuri.
         * $request = $this->get('request');
        if ($request->getLocale() === 'de_DE' or $request->getLocale() === 'en_DE') {
            $render_path = 'AppBundle:frontend/contact:contact_detail_de.html.twig';
        }
        */

        return $this->render($render_path, array(
            'contactGroup' => $contactGroup,
            'countriesClass' => $countriesClass,
            'contactsSubGroupTitleMap' => $contactsSubGroupTitleMap,
            'adv_language_switcher_rows' => $adv_language_switcher_rows,
            'breadcrumb' => $breadcrumb
        ));
    }

    /**
     * @return Page
     */
    private function configurePage()
    {
        //Carico la pagina
        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByRouteId($this->container->get('request')->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }
        return $page;
    }

    /**
     * @param ContactGroup $contactGroup
     */
    private function customizeSeoForContactGroup($contactGroup)
    {

        $title = $contactGroup->getTitle();
        $description = (empty($contactGroup->getMetaDescription()) ? $contactGroup->getSubtitle() : $contactGroup->getMetaDescription());
        $keywords = $contactGroup->getMetaKeywords();

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->setTitle($title . ' | Eni Oil Products');
        $seoPage->addMeta('property', 'og:title', $title);
        if ($description) {
            $seoPage->addMeta('name', 'description', $description);
            $seoPage->addMeta('property', 'og:description', $description);
        }
        if ($keywords) {
            $seoPage->addMeta('name', 'keywords', $keywords);
        }
    }

    // incredible complex method to order contacts by subgroup (position asc), title (alphabet asc), contact (position asc) 
    private function groupBySubgroupAndTitle(ContactGroup $contactGroup)
    {
        $contactsMap = [];
        $contactsArrayCollection = $contactGroup->getContacts()->toArray();
        //uasort($contactsArrayCollection,array($this,'orderASCBySubGroupPosition'));

        /** @var Contact $contact */
        foreach ($contactsArrayCollection as $contact) {
            if ($contact->getPublished()) {
                $contactSubGroupName = $contact->getContactSubGroup()->getName();

                $contactTitle = $contact->getTitle();

                if (!array_key_exists($contactSubGroupName, $contactsMap)) {
                    $contactsMap[$contactSubGroupName] = [
                        'subGroupObj' => $contact->getContactSubGroup(),
                        'byTitle' => []
                    ];
                }
                if (!array_key_exists($contactTitle, $contactsMap[$contactSubGroupName]['byTitle'])) {
                    $contactsMap[$contactSubGroupName]['byTitle'][$contactTitle] = [];
                }
                $contactsMap[$contactSubGroupName]['byTitle'][$contactTitle][] = $contact;
            }
        }

        $contactsMapSorted = [];
        foreach (array_keys($contactsMap) as $contactSubGroupName) {
            //ksort($contactsMap[$contactSubGroupName]['byTitle']);
            $subGroupObject = $contactsMap[$contactSubGroupName]['subGroupObj'];
            $contactsMapSorted[str_pad($subGroupObject->getPosition(), 20, "0", STR_PAD_LEFT) . "-" . $contactSubGroupName] = $contactsMap[$contactSubGroupName];
        }

        ksort($contactsMapSorted);

        $contactsMap = [];
        foreach (array_keys($contactsMapSorted) as $contactSubGroupNameSorted) {
            $subGroupObject = $contactsMapSorted[$contactSubGroupNameSorted]['subGroupObj'];
            $contactsMap[$subGroupObject->getName()] = $contactsMapSorted[$contactSubGroupNameSorted];
        }

        return $contactsMap;

    }

    protected function orderASCBySubGroupPosition(Contact $a, Contact $b)
    {
        if ($a == $b) {
            return 0;
        }
        $aPosition = $a->getContactSubGroup()->getPosition();
        $bPosition = $b->getContactSubGroup()->getPosition();
        return $aPosition < $bPosition ? -1 : 1;
    }


}

