<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use AppBundle\Utils\FilterLocaleUtils;

class MenuController extends AbstractController
{
    
    public function header1SitesMenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'header1_sites', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:header1_sites_menu.html.twig', array("menu"=>$menu));
    }  
    
    public function header1MenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'header1', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:header1_menu.html.twig', array("menu"=>$menu));
    }  
    
    public function header2MenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'header2', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:header2_menu.html.twig', array("menu"=>$menu));
    }   
    
    public function headerMobileMenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'header_mobile', 'published' => true)
            );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:mobile_menu.html.twig', array("menu"=>$menu));
    }
    
    public function footer1MenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'footer1', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:footer_menu.html.twig', array("menu"=>$menu));
    }

    public function footer2MenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'footer2', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:footer_menu.html.twig', array("menu"=>$menu));
    }
    
    public function footer3MenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'footer3', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:footer_menu.html.twig', array("menu"=>$menu));
    }
    
    public function footerSitesMenuAction()
    {
        $linkGroup = $this->container->get('doctrine')->getRepository('AppBundle:LinkGroup')->findOneBy(
            array('type'=> 'footer_sites', 'published' => true)
        );
        $menu = $this->createMenuFromLinkGroup($linkGroup);
        return $this->render('AppBundle:frontend/partial:footer_sites_menu.html.twig', array("menu"=>$menu));
    }
    
    private function createMenuFromLinkGroup($linkGroup) {
        $menu = array();
        $menu["label"] = "";
        $menu["items"] = array();
               
        if ($linkGroup) {
            if ($linkGroup->getLabel()) {
                $menu["label"] = $linkGroup->getLabel();
            }
    
            $links = $linkGroup->getLinks();
            $current_locale =  $this->get('request')->getLocale();
            
            foreach ($links as $link) {
                if ($link->getPublished()) {   	
                	$lang_permitted = $link->getLanguage ();
                	
                	if(  FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale)){
	                    $page = $link->getPage();
	                    if ($page) {
	                        $url = $this->get("url_manager")->calculateUrlFromPage($page);
	                        $menu["items"][] = array('title' => ($link->getLabel() ? $link->getLabel(): $page->getTitle()), 'url' => $url, 'format'=>$link->getFormat(), 'type'=>$link->getType(), 'position'=>$link->getPosition());
	                    } else {
	                        $url = $link->getUrl();
	                        $menu["items"][] = array('title' => $link->getLabel(), 'url' => $url, 'format'=>$link->getFormat(), 'type'=>$link->getType(), 'position'=>$link->getPosition() );
	                    }     
                	}
                }
            }
            
            $positions = array();
            foreach ($menu["items"] as $key => $row){
                $positions[$key] = $row['position'];
            }
            array_multisort($positions, SORT_ASC, $menu["items"]);
            
        }
       
        return $menu;

    }
}