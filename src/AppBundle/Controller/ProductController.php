<?php

namespace AppBundle\Controller;

use AppBundle\Utils\Constants;
use AppBundle\Utils\TextUtils;
use AppBundle\Utils\FilterLocaleUtils;
use AppBundle\Controller\AbstractController;
use AppBundle\Bean\ClassificationBranch;
use AppBundle\Form\Type\ConcessionaryToolType;

class ProductController extends AbstractController
{

    public function listAction()
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('product list action');

        $page = $this->getDoctrine()
            ->getRepository('AppBundle:Page')
            ->findOneByRouteId($this->container->get('request')
                ->get('_route'));

        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')
                ->trans('exception.not_found'));
        }
        
        $current_locale =  $this->get('request')->getLocale();
        
        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:ProductClassificationRow')
            ->findDistinctProductCategoriesOfLev2($current_locale);
        
        
        $classificationBranches = array();
        foreach ($categories as $category) {
           // $lang_permitted = $category->getLanguage ();
            //if( FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale)){               
                $classificationBranch = new ClassificationBranch(array($category));
                $classificationBranches[] = $classificationBranch;
           // }
        }
        
        

        $link = array();
        $link = $this->container->get("UrlManager")->getRoutesByRouteId("product_list", $current_locale);
        
        $this->customizeSeoForBasePage($page, $link);

        $adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, null);

        $breadcrumb = $this->get("breadcrumb_manager")
            ->addHome()
            ->addItem(array(
                'title' => $page->getTitle(),
                'url' => ""
            ));

        return $this->render('AppBundle:frontend/product:product_list.html.twig', array(
            'classificationBranches' => $classificationBranches,
            'adv_language_switcher_rows' => $adv_language_switcher_rows,
            'breadcrumb' => $breadcrumb,
            'page' => $page
        ));
    }

    public function detailAction($slug)
    {

        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('product detail action');

        $page = $this->getDoctrine()
            ->getRepository('AppBundle:Page')
            ->findOneByRouteId($this->container->get('request')
                ->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')
                ->trans('exception.not_found'));
        }
        
        //prendo la lingua in uso
        $current_locale =  $this->get('request')->getLocale();
        
        $classificationBranchToRender = $this->container->get("product_classification_url_manager")->getClassificationBranchFromDynamicSlug($slug,$current_locale);

        if ($classificationBranchToRender == null) {
            throw $this->createNotFoundException($this->get('translator')
                    ->trans('exception.not_found') . ' :: ' . $slug);
        }

        /*
        //creo il link con lo slug tradotto del product category
        $link = array();
        $link['it_IT'] = "/it_IT/";
        $nameProductTranslations = $this->getDoctrine()->getRepository('AppBundle:ProductClassificationRow')->findNameProductCategoryTranslations();
        foreach ($nameProductTranslations as $nameProductTranslation) {
            $link[$nameProductTranslation['locale']] = "/".$nameProductTranslation['locale']."/";
            $link[$nameProductTranslation['locale']] = $link[$nameProductTranslation['locale']].TextUtils::slugify($nameProductTranslation['content']);
        }
        $link['it_IT'] = $link['it_IT'].TextUtils::slugify($nameProductTranslations[0]['title']);
        */
        
        
        //creo il link con lo slug tradotto del product category preso dal file i18n.xml 
        $link = array();
        $link = $this->container->get("UrlManager")->getRoutesByRouteId("product_list", $current_locale);
        
        //per ogni categoria nello slug, cerco lo slug tradotto
        foreach ($classificationBranchToRender->getWalk() as $step) {
            if($step->getTitle() != "EMPTY"){
                $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:ProductClassificationRow')->findSlugTranslationsByTitleCategories($step->getTitle(),$current_locale);
                if($slugTranslations != ""){
                    foreach ($slugTranslations as $slugTranslation) {
                        $link[$slugTranslation['locale']] = $link[$slugTranslation['locale']]."/".$slugTranslation['content'];
                    }
                    if(isset($slugTranslations[0])){
                        $link['it_IT'] = $link['it_IT']."/".$slugTranslations[0]['slug'];
                    }
                }
            }
        }
        
        
        
        
        $logger->debug('slug ' . $classificationBranchToRender->getSlug());
        foreach ($classificationBranchToRender->getWalk() as $step) {
            $logger->debug('walk ' . $step->getTitle());
        }
        foreach ($classificationBranchToRender->getDoors() as $door) {
            $logger->debug('door ' . $door->getLastStep()->getTitle());
        }
        $logger->debug('template ' . $classificationBranchToRender->getProductTemplate());

        $this->customizeSeoForClassificationBranch($classificationBranchToRender, $link);

        // TODO: calculate if advanced language switcher must be used
        $adv_language_switcher_rows = array();

        $breadcrumb = $this->get("breadcrumb_manager")
            ->addHome()->addPageByRouteId('product_list')->addClassificationBranch($classificationBranchToRender);

        return $this->render($classificationBranchToRender->getProductTemplate(), array(
            'classificationBranch' => $classificationBranchToRender,
            'adv_language_switcher_rows' => $adv_language_switcher_rows,
            'breadcrumb' => $breadcrumb,
            'form' => false,
        ));

    }
    
    public function concessionaryToolAction($title)
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Concessionary');
        
        $form = $this->createForm(ConcessionaryToolType::class);
        $form->handleRequest($request);
        
        $offices = array();
        $found = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $offices = $repo->searchByFormData($form->getData());
            
            if (count($offices)) {
                $found = true;
            }
        }
        if (!count($offices)) {
            
            $offices = $repo->findAll();
            $officesTemp = array();
            foreach($offices as $office) {
                if ( (strpos($office->getSezione(), $title) !== false) ) {
                    $officesTemp[] = $office;
                }
            }
            $offices =  $officesTemp;
        }
        
        return $this->render('AppBundle:frontend/area:concessionarytool.html.twig', [
            'form' => $form->createView(),
            'offices' => $offices,
            'found' => $found,
            'title' => $title,
        ]);
    }

    public function menuAction($type, $root, $startLevel, $depth, $activeClassificationBranch)
    {
        $categories = ($root != null) ? $root->getWalk() : array();
        $catLev2Root = (count($categories) > 0) ? $categories[0] : null;
        $catLev3Root = (count($categories) > 0) ? $categories[1] : null;
        $catLev4Root = (count($categories) > 0) ? $categories[2] : null;
        $catLev5Root = (count($categories) > 0) ? $categories[3] : null;

        $rootClassificationBranch = new ClassificationBranch();
        
        $current_locale =  $this->get('request')->getLocale();
        
        
        if ($catLev2Root == null || $startLevel < 1) {           
            $catsLev2 = $this->getDoctrine()
                ->getRepository('AppBundle:ProductClassificationRow')
                ->findDistinctProductCategoriesOfLev2($current_locale);
        } else {
            $catsLev2 = array(
                $catLev2Root
            );
        }
        
        
        
        foreach ($catsLev2 as $catLev2) {
           
                $catLev2ClassificationBranch = new ClassificationBranch();
                $catLev2ClassificationBranch->addStep($catLev2);
                $this->checkActive($catLev2ClassificationBranch, $activeClassificationBranch);
    
                if ($catLev3Root == null || $startLevel < 2) {
                    $catsLev3 = $this->getDoctrine()
                        ->getRepository('AppBundle:ProductClassificationRow')
                        ->findDistinctProductCategoriesOfLev3($current_locale,$catLev2);
                } else {
                    $catsLev3 = array(
                        $catLev3Root
                    );
                }
    
                foreach ($catsLev3 as $catLev3) {
                    $catLev3ClassificationBranch = new ClassificationBranch($catLev2ClassificationBranch->getWalk());
                    $catLev3ClassificationBranch->addStep($catLev3);
                    $this->checkActive($catLev3ClassificationBranch, $activeClassificationBranch);
    
                    if ($catLev4Root == null || $startLevel < 3) {
                        $catsLev4 = $this->getDoctrine()
                            ->getRepository('AppBundle:ProductClassificationRow')
                            ->findDistinctProductCategoriesOfLev4($current_locale,$catLev2, $catLev3);
                    } else {
                        $catsLev4 = array(
                            $catLev4Root
                        );
                    }
    
                    foreach ($catsLev4 as $catLev4) {
                        $catLev4ClassificationBranch = new ClassificationBranch($catLev3ClassificationBranch->getWalk());
                        $catLev4ClassificationBranch->addStep($catLev4);
                        $this->checkActive($catLev4ClassificationBranch, $activeClassificationBranch);
    
                        if ($catLev4Root == null || $startLevel < 4) {
                            $catsLev5 = $this->getDoctrine()
                                ->getRepository('AppBundle:ProductClassificationRow')
                                ->findDistinctProductCategoriesOfLev5($current_locale,$catLev2, $catLev3, $catLev4);
                        } else {
                            $catsLev5 = array(
                                $catLev5Root
                            );
                        }
    
                        foreach ($catsLev5 as $catLev5) {
    
                            if ($catLev5->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                                $catLev5ClassificationBranch = new ClassificationBranch($catLev4ClassificationBranch->getWalk());
                                $catLev5ClassificationBranch->addStep($catLev5);
                                $this->checkActive($catLev5ClassificationBranch, $activeClassificationBranch);
                                $catLev4ClassificationBranch->addDoor($catLev5ClassificationBranch);
    
                            } else {
    
                                $products = $this->getDoctrine()
                                    ->getRepository('AppBundle:ProductClassificationRow')
                                    ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev4, $catLev5);
    
                                foreach ($products as $product) {
                                    $productClassificationBranch = new ClassificationBranch($catLev4ClassificationBranch->getWalk());
                                    $productClassificationBranch->addStep($product);
                                    $this->checkActive($productClassificationBranch, $activeClassificationBranch);
                                    $catLev4ClassificationBranch->addDoor($productClassificationBranch);
                                }
    
                            }
    
                        }
    
                        $catLev3ClassificationBranch->addDoor($catLev4ClassificationBranch);
                    }
    
                    if ($catLev3->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                        $catLev2ClassificationBranch->addDoor($catLev3ClassificationBranch);
                    }
    
                }
    
                $rootClassificationBranch->addDoor($catLev2ClassificationBranch);
            
        }

        if ($type == 'left') {
            return $this->render('AppBundle:frontend/product:product_left_menu.html.twig', array(
                'classificationBranch' => $rootClassificationBranch,
                'depth' => $depth,
                'startLevel' => $startLevel
            ));
        } else if ($type == 'header') {
            return $this->render('AppBundle:frontend/product:product_full_menu.html.twig', array(
                'classificationBranch' => $rootClassificationBranch,
                'depth' => $depth,
                'startLevel' => $startLevel
            ));
        } else if ($type == 'mobile') {
            return $this->render('AppBundle:frontend/product:product_mobile_menu.html.twig', array(
                'classificationBranch' => $rootClassificationBranch,
                'depth' => $depth,
                'startLevel' => $startLevel
            ));
        }


    }

    private function checkActive($classificationBranch, $activeClassificationBranch)
    {
        if ($activeClassificationBranch) {
            if ($classificationBranch->contains($activeClassificationBranch)) {
                $classificationBranch->setActive(true);
            }
        }
    }

    private function customizeSeoForClassificationBranch($classificationBranchToRender, $link = "")
    {

        $title = "";
        $description = "";
        $keywords = "";

        $titleSeparator = " - ";
        $metaKeywordsSeparator = ", ";

        $walk = $classificationBranchToRender->getWalk();
        $numItems = count($walk);
        $i = 0;
        foreach ($walk as $category) {
            if ($category->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                $title .= ($title ? $titleSeparator : "") . $category->getTitle();
            }
            $keywords .= (($keywords && $category->getMetaKeywords()) ? $metaKeywordsSeparator : "") . $category->getMetaKeywords();
            if (++$i === $numItems) {
                $description = (empty($category->getMetaDescription()) ? $category->getDescription() : $category->getMetaDescription());
            }
        }

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->setTitle($title . ' | Eni Oil Products');
        $seoPage->addMeta('property', 'og:title', $title);
        if ($description) {
            $seoPage->addMeta('name', 'description', $description);
            $seoPage->addMeta('property', 'og:description', $description);
        }
        if ($keywords) {
            $seoPage->addMeta('name', 'keywords', $keywords);
        }
        
        
        $current_locale =  $this->get('request')->getLocale();
        
        //inserimento link canonical
        if(strpos($classificationBranchToRender->getProductTemplate(), 'lev5') !== false && $classificationBranchToRender->getIsSingleProduct()){
            
            $linkReference = $this->getDoctrine()->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByProductAndLanguages(end($walk)->getSlug(),$current_locale);
            
            if($linkReference != $classificationBranchToRender->getSlug()){
                
                $linkRoute = $this->container->get("UrlManager")->getRouteByRouteId("product_list", $current_locale);
                $linkReference = $linkRoute."/".$linkReference;
                
                $seoPage->setLinkCanonical($linkReference);
            }
        }
        else{
            $linkReference = $this->getDoctrine()->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByCategoryAndLanguages(end($walk)->getSlug(),$current_locale);
            if($linkReference != $classificationBranchToRender->getSlug()){
                if($linkReference != ""){
                    $linkRoute = $this->container->get("UrlManager")->getRouteByRouteId("product_list", $current_locale);
                    $linkReference = $linkRoute."/".$linkReference;
                    $seoPage->setLinkCanonical($linkReference);
                }
            }
        }
        
        
        if($link){
            
            foreach ($link as $hrefLang => $href) {
                
                $hrefLang = strtolower($hrefLang);
                $hrefLang = str_replace("_","-",$hrefLang);
                
                if($hrefLang == "it-it"){
                    $seoPage->addLangAlternate($href, "x-default");
                }
                
                if($hrefLang == "en-gb"){
                    $hrefLang = "en-it";
                    $seoPage->addLangAlternate($href, substr($hrefLang,0,2));
                }
                
                if($hrefLang == "nl-bx"){
                    $hrefLang = "nl-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "nl-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "nl-nl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if($hrefLang == "fr-bx"){
                    $hrefLang = "fr-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "fr-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if($hrefLang == "en-bx"){
                    $hrefLang = "en-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-nl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if ( substr($hrefLang,0,2) != "en" && strpos($href, substr($hrefLang,0,2)."_" ) !== false) {
                    $seoPage->addLangAlternate($href, substr($hrefLang,0,2));
                }
                
                if($hrefLang == "en-apac"){
                    $hrefLang = "th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "vi";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "zh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ja";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "tl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "km";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-bd";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-mm";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-la";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-vn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-my";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-sg";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-tw";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-ph";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kr";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-jp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-bn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-nz";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-au";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "th-th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "vi-vn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-my";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-sg";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-bn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "zh-tw";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ja-jp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko-kp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko-kr";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "tl-ph";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "km-kh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    
                    continue;
                }
                
                $seoPage->addLangAlternate($href, $hrefLang);
            }
        }
        
        //$seoPage->addLangAlternate("https://oilproducts.eni.com/it_IT/", "x-default");
        
    }
    
    
}
