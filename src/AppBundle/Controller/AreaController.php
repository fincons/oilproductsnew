<?php

namespace AppBundle\Controller;

use AppBundle\Bean\AreaClassificationBranch;
use AppBundle\Controller\AbstractController;
use AppBundle\Utils\Constants;
use AppBundle\Utils\TextUtils;
use AppBundle\Form\Type\GplToolType;
use AppBundle\Form\Type\ConcessionaryToolType;

class AreaController extends AbstractController
{

    public function listAction()
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('area list action');

        $page = $this->getDoctrine()
            ->getRepository('AppBundle:Page')
            ->findOneByRouteId($this->container->get('request')
                ->get('_route'));

        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')
                ->trans('exception.not_found'));
        }
        
        $current_locale =  $this->get('request')->getLocale();
        
        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:AreaClassificationRow')
            ->findDistinctAreaCategoriesOfLev2($current_locale);

        $classificationBranches = array();
        foreach ($categories as $category) {
            $classificationBranch = new AreaClassificationBranch(array($category));
            $classificationBranches[] = $classificationBranch;
        }

        $link = array();
        $link = $this->container->get("UrlManager")->getRoutesByRouteId("area_list", $current_locale);
        
        $this->customizeSeoForBasePage($page, $link);

        $adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, null);

        $breadcrumb = $this->get("breadcrumb_manager")
            ->addHome()
            ->addItem(array(
                'title' => $page->getTitle(),
                'url' => ""
            ));

        return $this->render('AppBundle:frontend/area:area_list.html.twig', array(
            'classificationBranches' => $classificationBranches,
            'adv_language_switcher_rows' => $adv_language_switcher_rows,
            'breadcrumb' => $breadcrumb,
            'page' => $page
        ));
    }

    public function detailAction($slug)
    {

        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('area detail action');

        $page = $this->getDoctrine()
            ->getRepository('AppBundle:Page')
            ->findOneByRouteId($this->container->get('request')
                ->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')
                ->trans('exception.not_found'));
        }
        
        //prendo la lingua in uso
        $current_locale =  $this->get('request')->getLocale();
        
        $classificationBranchToRender = $this->container->get("area_classification_url_manager")->getClassificationBranchFromDynamicSlug($slug,$current_locale);

        if ($classificationBranchToRender == null) {
            throw $this->createNotFoundException($this->get('translator')
                ->trans('exception.not_found'));
        }
        
        
        /*
        //prendo le lingue presenti nell'alberatura delle aree
        $languages = $this->getDoctrine()->getRepository('AppBundle:AreaClassificationRow')->findDistinctAreaCategoriesLanguages();
        echo "<script>console.log( 'Debug Objects: " . $slug . "' );</script>";
        $languageArray = array();
        foreach ($languages as $language) {
            foreach (explode('|',$language['language']) as $singleLang) {
                array_push($languageArray, $singleLang); 
            }            
        }
        echo "<script>console.log( 'lang " . print_r($languageArray,true) . "' );</script>";
        */        
                
        /*
        //creo il link con lo slug tradotto dell'area category
        $link = array();
        $link['it_IT'] = "/it_IT/";
        $nameAreaTranslations = $this->getDoctrine()->getRepository('AppBundle:AreaClassificationRow')->findNameAreaCategoryTranslations();
        foreach ($nameAreaTranslations as $nameAreaTranslation) {
            $link[$nameAreaTranslation['locale']] = "/".$nameAreaTranslation['locale']."/";
            $link[$nameAreaTranslation['locale']] = $link[$nameAreaTranslation['locale']].TextUtils::slugify($nameAreaTranslation['content']);
        }
        $link['it_IT'] = $link['it_IT'].TextUtils::slugify($nameAreaTranslations[0]['title']);
        */
        
        
        //creo il link con lo slug tradotto dell'area category preso dal file i18n.xml 
        $link = array();
        $link = $this->container->get("UrlManager")->getRoutesByRouteId("area_list",$current_locale);
        
        //per ogni categoria nello slug, cerco lo slug tradotto        
        //$cont = 0;
        foreach ($classificationBranchToRender->getWalk() as $step) {
            if($step->getTitle() != "EMPTY"){
                $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:AreaClassificationRow')->findSlugTranslationsByTitleCategories($step->getTitle(),$current_locale);
                if($slugTranslations != ""){
                    foreach ($slugTranslations as $slugTranslation) {
                        //if($cont == 0){
                        //    $link[$slugTranslation['locale']] = "";
                        //}
                        $link[$slugTranslation['locale']] = $link[$slugTranslation['locale']]."/".$slugTranslation['content'];
                    }
                    if(isset($slugTranslations[0])){
                        $link['it_IT'] = $link['it_IT']."/".$slugTranslations[0]['slug'];
                    }
                   // $cont++;
                }
            }
        }
        //echo "<script>console.log( 'slug:  ". print_r($link,true) . "' );</script>";
                

        $logger->debug('slug ' . $classificationBranchToRender->getSlug());
        foreach ($classificationBranchToRender->getWalk() as $step) {
            $logger->debug('walk ' . $step->getTitle());
        }
        foreach ($classificationBranchToRender->getDoors() as $door) {
            $logger->debug('door ' . $door->getLastStep()->getTitle());
        }
        $logger->debug('template ' . $classificationBranchToRender->getProductTemplate());

        $this->customizeSeoForClassificationBranch($classificationBranchToRender, $link);

        // TODO: calculate if advanced language switcher must be used
        $adv_language_switcher_rows = array();
        /*
        $page_slug = $this->getDoctrine()
            ->getRepository('AppBundle:Page')
            ->findOneByRouteId('area_detail');
        $adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page_slug, null);
        */
        $breadcrumb = $this->get("breadcrumb_manager")
            ->addHome()->addPageByRouteId('area_list')->addAreaClassificationBranch($classificationBranchToRender);

        return $this->render('AppBundle:frontend/area:template1.html.twig', array(
            'classificationBranch' => $classificationBranchToRender,
            'adv_language_switcher_rows' => $adv_language_switcher_rows,
            'breadcrumb' => $breadcrumb,
            'form' => false,
        ));

    }

    public function gplToolAction()
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Office');

        $form = $this->createForm(GplToolType::class);
        $form->handleRequest($request);

        $offices = array();
        $found = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $offices = $repo->searchByFormData($form->getData());

            if (count($offices)) {
                $found = true;
            }
        }
        if (!count($offices)) {
            $offices = $repo->findAll();
        }

        return $this->render('AppBundle:frontend/area:gpltool.html.twig', [
            'form' => $form->createView(),
            'offices' => $offices,
            'found' => $found,
        ]);
    }

    public function concessionaryToolAction($title)
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Concessionary');

        $form = $this->createForm(ConcessionaryToolType::class);
        $form->handleRequest($request);

        $offices = array();
        $found = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $offices = $repo->searchByFormData($form->getData());

            if (count($offices)) {
                $found = true;
            }
        }
        if (!count($offices)) {
            
            $offices = $repo->findAll();
            $officesTemp = array();
            foreach($offices as $office) {
                if ( (strpos($office->getSezione(), $title) !== false) ) {
                    $officesTemp[] = $office;
                }
            }
            $offices =  $officesTemp;
        }

        return $this->render('AppBundle:frontend/area:concessionarytool.html.twig', [
            'form' => $form->createView(),
            'offices' => $offices,
            'found' => $found,
            'title' => $title,
        ]);
    }

    public function gplContactAction()
    {

        $repoProvince = $this->getDoctrine()->getManager()->getRepository('AppBundle:Province');

        $provinces = $repoProvince->findAll();

        return $this->render('AppBundle:frontend/area:gplcontact.html.twig', [
            'provinces' => $provinces,
        ]);
    }

    private function checkActive($classificationBranch, $activeClassificationBranch)
    {
        if ($activeClassificationBranch) {
            if ($classificationBranch->contains($activeClassificationBranch)) {
                $classificationBranch->setActive(true);
            }
        }
    }

    private function customizeSeoForClassificationBranch($classificationBranchToRender, $link = "")
    {
        
        $title = "";
        $description = "";
        $keywords = "";

        $titleSeparator = " - ";
        $metaKeywordsSeparator = ", ";

        $walk = $classificationBranchToRender->getWalk();
        $numItems = count($walk);
        $i = 0;        
        foreach ($walk as $category) {            
            if ($category->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                $title .= ($title ? $titleSeparator : "") . $category->getTitle();
            }
            $keywords .= (($keywords && $category->getMetaKeywords()) ? $metaKeywordsSeparator : "") . $category->getMetaKeywords();
            if (++$i === $numItems) {
                $description = (empty($category->getMetaDescription()) ? $category->getDescription() : $category->getMetaDescription());
            }
        }

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->setTitle($title . ' | Eni Oil Products');
        $seoPage->addMeta('property', 'og:title', $title);
        if ($description) {
            $seoPage->addMeta('name', 'description', $description);
            $seoPage->addMeta('property', 'og:description', $description);
        }
        if ($keywords) {
            $seoPage->addMeta('name', 'keywords', $keywords);
        }
        
        
        
        
        //inserimento link canonical
        $current_locale =  $this->get('request')->getLocale();
        if(strpos($classificationBranchToRender->getProductTemplate(), 'lev5') !== false && $classificationBranchToRender->getIsSingleProduct()){
            $linkReference = $this->getDoctrine()->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByProductAndLanguages(end($walk)->getSlug(),$current_locale);
            if($linkReference != ""){
                $linkRoute = $this->container->get("UrlManager")->getRouteByRouteId("product_list", $current_locale);
                $linkReference = $linkRoute."/".$linkReference;
                $seoPage->setLinkCanonical($linkReference);
            }
        }
        else{
            $linkReference = $this->getDoctrine()->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByCategoryAndLanguages(end($walk)->getSlug(),$current_locale);
            if($linkReference != ""){
                $linkRoute = $this->container->get("UrlManager")->getRouteByRouteId("product_list", $current_locale);
                $linkReference = $linkRoute."/".$linkReference;
                $seoPage->setLinkCanonical($linkReference);
            }
        }
            
        
        
        if($link){
            
            foreach ($link as $hrefLang => $href) {
                
                $hrefLang = strtolower($hrefLang);
                $hrefLang = str_replace("_","-",$hrefLang);
                
                if($hrefLang == "it-it"){
                    $seoPage->addLangAlternate($href, "x-default");
                }
                
                if($hrefLang == "en-gb"){
                    $hrefLang = "en-it";
                    $seoPage->addLangAlternate($href, substr($hrefLang,0,2));
                }
                
                if($hrefLang == "nl-bx"){
                    $hrefLang = "nl-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "nl-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "nl-nl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if($hrefLang == "fr-bx"){
                    $hrefLang = "fr-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "fr-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if($hrefLang == "en-bx"){
                    $hrefLang = "en-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-nl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if ( substr($hrefLang,0,2) != "en" && strpos($href, substr($hrefLang,0,2)."_" ) !== false) {
                    $seoPage->addLangAlternate($href, substr($hrefLang,0,2));
                }
                
                if($hrefLang == "en-apac"){
                    $hrefLang = "th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "vi";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "zh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ja";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "tl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "km";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-bd";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-mm";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-la";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-vn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-my";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-sg";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-tw";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-ph";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kr";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-jp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-bn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-nz";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-au";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "th-th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "vi-vn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-my";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-sg";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-bn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "zh-tw";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ja-jp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko-kp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko-kr";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "tl-ph";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "km-kh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    
                    continue;
                }
                
                $seoPage->addLangAlternate($href, $hrefLang);
            }
        }
       
        //$seoPage->addLangAlternate("https://oilproducts.eni.com/it_IT/", "x-default");
        
    }
    
    
}
