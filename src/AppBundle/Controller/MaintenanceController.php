<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MaintenanceController extends AbstractController
{
    private $availableCommands = [
    /*'cleanVarnishCache' => [
        'name' => 'Clean Varnish Cache:',
        'class' => '\AppBundle\Command\CleanVarnishCacheCommand',
        'params' => []
    ],*/
    'notify' => [
        'name' => 'Notify not classified products',
        'class' => '\AppBundle\Command\NotifyProductsCommand',
        'params' => []
    ],
    'cleanCachesAndWarmingUp' => [
        'name' => 'Clean all caches and warming up for url:',
        'class' => '\AppBundle\Command\CleanCachesAndWarmingUpCommand',
        'params' => ['url' => '/it_IT']
    ],        
        'synchWithEpic' => [
            'name' => 'Synch with Epic:',
            'class' => '\AppBundle\Command\SynchWithEpicCommand',
            'params' => ['env' => 'prod']
        ]
        
    ];
    public function updateclassificationAction(Request $request)
    {
    	$classificationType=trim($request->request->get("fileType"));
    	$uploadedFile=$request->files->get("csvFile");
    	$classificationUpdaterService = $this->container->get('classification_updater');
    	if($classificationType == 'pcr'){
    		$responseMessage = $classificationUpdaterService->updatePCRPositions($uploadedFile->getPathName());
    	}elseif($classificationType == 'acr'){
    		$responseMessage = $classificationUpdaterService->updateACRPositions($uploadedFile->getPathName());
    	}
    	$this->get('session')->getFlashBag()->add('success', $responseMessage);
    	return $this->indexAction($request);
    }
    public function indexAction(Request $request)
    {
        $logger = $this->get('monolog.logger.enioil_admin');
        
        if($request->isXmlHttpRequest()) {
            $class = $request->get('command');
            $maintenanceParams = $request->get('params');
            $commandParams = array();
            
            // if CleanCachesAndWarmingUpCommand build urls_file_path with url from request
            if ((strpos($class, "CleanCachesAndWarmingUpCommand") !== false) && array_key_exists('url', $maintenanceParams)) {
                $temp = tmpfile();
                $logger->info("Added url for clean caches and warming up: " . $maintenanceParams['url']);
                fwrite($temp, $maintenanceParams['url']);
                fseek($temp, 0);
                $commandParams['urls_file_path'] = stream_get_meta_data($temp)['uri'];
            }
            
            $command = new $class;
            $command->setContainer($this->container);
            
            $input = new ArrayInput($commandParams);
            if ((strpos($class, "SynchWithEpicCommand") !== false)) {
                ini_set('max_execution_time', 3600);
            }

            $output = new BufferedOutput();
            try {
                $command->run($input, $output);
                $content = $output->fetch();
                $success = true;
            } catch(\Exception $ex) {
                $success = false;
                $content = 'Error: ' .  $ex->getMessage();
            }

            return new JsonResponse(array('content' => nl2br($content), 'success' => $success));
        }
        return $this->render('AppBundle:admin:maintenance.html.twig', array(
            'availableCommands' => $this->availableCommands
        ));
    }

    public function render($view, array $parameters = array(), \Symfony\Component\HttpFoundation\Response $response = null) {
        $adminPool = $this->get('sonata.admin.pool');

        return parent::render($view, array_merge($parameters, ['admin_pool' => $adminPool, 'base_template' => $adminPool->getTemplate('layout')]), $response);
   }
}
