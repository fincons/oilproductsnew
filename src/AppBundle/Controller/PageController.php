<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;

class PageController extends AbstractController
{

    public function showAction($slug)
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('page view action');

        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByTranslatableField('slug',$slug);

        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }
        
        
        $host = $this->container->get("UrlManager")->getHost();
        if (!strpos($host, 'http') !== false) {
            $host = "https://".$host;
        }
        $link = array();
        
        $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:Page')->findSlugTranslations($slug);
        
        
        foreach ($slugTranslations as $slugTranslation) {
            $link[$slugTranslation['locale']] = $host."/".$slugTranslation['locale']."/";
            $link[$slugTranslation['locale']] = $link[$slugTranslation['locale']].$slugTranslation['content'];
        }
        if(isset($slugTranslations[0])){
            $link['it_IT'] = $host."/it_IT/";
            $link['it_IT'] = $link['it_IT'].$slugTranslations[0]['slug'];
            
            $this->customizeSeoForBasePage($page,$link);
        }
        else{
            $this->customizeSeoForBasePage($page,null);
        }
        
        

    	$adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, $slug);

    	$breadcrumb = $this->get("breadcrumb_manager")->addHome()->addPage($page);

    	return $this->render('AppBundle:frontend/page:template1.html.twig', array(
    			'page' => $page,
    			'adv_language_switcher_rows' => $adv_language_switcher_rows,
    	        'breadcrumb' => $breadcrumb,
    			'form' => false,
    	));
    }
    
}
