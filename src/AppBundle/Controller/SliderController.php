<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Slide;

class SliderController extends AbstractController
{

    public function jumboSliderAction()
    {
        $slider = $this->getDoctrine()->getRepository('AppBundle:Slide')->findBy(
            array('published'=> true,
                'slider'=> Slide::JUMBO_SLIDER
                ),
            array('position' => 'ASC',
                'updatedAt' => 'DESC')
        );

        return $this->render('AppBundle:frontend/partial:jumbo_slider_ver2.html.twig', array(
                        'slider' => $slider
        ));
    }

    public function linkSliderAction()
    {   
        $current_locale =  $this->get('request')->getLocale();
        
        $slider = $this->getDoctrine()->getRepository('AppBundle:Slide')->findBy(
            array(
                'published'=> true,
                'slider'=> Slide::LINK_SLIDER
                ),
            array('position' => 'ASC',
                'updatedAt' => 'DESC')
        );
        $sliderForView = $this->buildSLiderForView($slider, $current_locale);

        return $this->render('AppBundle:frontend/partial:link_slider.html.twig', array(
                        'slider' => $sliderForView
        ));
    }

    private function buildSLiderForView($slider, $current_locale)
    {
        $slideForView = [];
        /** @var Slide $slide */
        foreach ($slider as $slide) {            
            $language = $slide->getLanguage();
            $link = $slide->getLink();            
            if($language == "ALL" || strpos($language, $current_locale) !== false){
                if ($link) {
                    if ($link->getPublished()) {
                        $page = $link->getPage();
                        if ($page) {
                            $url = $this->get("url_manager")->calculateUrlFromPage($page);
                            $link = array('external'=>false,
                                'title' => $page->getTitle(),
                                'url' => $url,
                                'format'=>$link->getFormat(),
                                'label'=> $link->getLabel());
                        } else {
                            $url = $link->getUrl();
                            $link = array(
                                'external'=>true,
                                'title' => $link->getLabel(),
                                'url' => $url,
                                'format'=>$link->getFormat(),
                                'label'=> $link->getLabel());
                        }
                    }                
                }
                
                
                $link2 = $slide->getLink2();
                if ($link2) {
                    $languageLink2 = $link2->getLanguage();
                    if ($link2->getPublished() && ($languageLink2 == "ALL" || strpos($languageLink2, $current_locale) !== false)) {
                        $page = $link2->getPage();
                        if ($page) {
                            $url = $this->get("url_manager")->calculateUrlFromPage($page);
                            $link2 = array('external'=>false,
                                'title' => $page->getTitle(),
                                'url' => $url,
                                'format'=>$link2->getFormat(),
                                'label'=> $link2->getLabel());
                        } else {
                            $url = $link2->getUrl();
                            $link2 = array(
                                'external'=>true,
                                'title' => $link2->getLabel(),
                                'url' => $url,
                                'format'=>$link2->getFormat(),
                                'label'=> $link2->getLabel());
                        }
                    }
                    else{
                        $link2 = "";
                    }
                }
                
                $slideForView[] = [
                    'slide' => $slide,
                    'link' => $link,
                    'link2' => $link2,
                ];
            }
        }
        return $slideForView;
    }

}