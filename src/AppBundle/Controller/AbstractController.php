<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class AbstractController extends Controller
{

    // CALCULATE LANGUAGE SWITCHER ROWS FOR ADVANCED LANGUAGE SWITCHER ...
    // TODO: improve with dinamic calculation of allowed languages
    // TODO per la gestione avanzata guarda il metodo sotto
    protected function calculateAdvLanguageSwitcherRows($page, $slug, $objectType = null, $object = null)
    {
        $adv_language_switcher_rows = array();
        if ($page->getRouteId() == "page_show") {
            $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:PageTranslation')->findSlugTranslations($page->getId(), $slug);
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate('page_show', array('_locale' => 'it_IT', 'slug' => $slugTranslations["it_IT"]["slug"]));
            if(isset($slugTranslations["en_GB"])){
                $adv_language_switcher_rows['EN'] = $this->get('router')->generate('page_show', array('_locale' => 'en_GB', 'slug' => $slugTranslations["en_GB"]["slug"]));
            }
        } else if ($page->getRouteId() == "product_detail") {
            $adv_language_switcher_rows['IT'] = "";
            $adv_language_switcher_rows['EN'] = "";
            foreach ($object as $category) {
                $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:' . $objectType . 'Translation')->findSlugTranslations($category->getId(), $category->getSlug());
                $adv_language_switcher_rows['IT'] .= ($adv_language_switcher_rows['IT'] ? "/" : "") . $slugTranslations["it_IT"]["slug"];
                $adv_language_switcher_rows['EN'] .= ($adv_language_switcher_rows['EN'] ? "/" : "") . $slugTranslations["en_GB"]["slug"];
            }
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT', 'slug' => $adv_language_switcher_rows['IT']));
            $adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB', 'slug' => $adv_language_switcher_rows['EN']));
        } elseif ($slug) {
            $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:' . $objectType . 'Translation')->findSlugTranslations($object->getId(), $slug);
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT', 'slug' => $slugTranslations["it_IT"]["slug"]));
            if(isset($slugTranslations["en_GB"])){
                $adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB', 'slug' => $slugTranslations["en_GB"]["slug"]));
            }
        } else {
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT'));
            $adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB'));
        }
        return $adv_language_switcher_rows;
    }

    //TODO metodo per gestire lo switch di lingua degli slug
    /*
     * Manca la gestione degli indirizzi con sottocategorie
     * ES. /en_GB/areas/fuels/combustion/kerosene
     *
     * Come fare a capire che "combustion" fa riferimento a quella categoria e non ad un'altra con lo stesso slug?
     */
    protected function calculateAdvLanguageSwitcherRows_advanced($page, $slug, $objectType = null, $object = null)
    {
        //var_dump($page);
        $adv_language_switcher_rows = array();
        if (!isset($page)) {
            return $adv_language_switcher_rows;
        }
        $route_id = $page->getRouteId();
        $page_id = $page->getId();
        //var_dump('var :: ' . $route_id . ' - ' . $slug . ' -- ' . $objectType . ' - ' . $object);
        if (isset($object) && isset($objectType) && ($route_id === "product_detail" || $route_id === "area_detail")) {
            var_dump('sono in product details');
            var_dump($object);
            foreach ($object as $category) {
                $page_id = $category->getId();
                $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:' . $objectType . 'Translation')->findSlugTranslations($page_id, $category->getSlug());
                $adv_language_switcher_rows['IT'] .= ($adv_language_switcher_rows['IT'] ? "/" : "") . $slugTranslations["it_IT"]["slug"];
                $adv_language_switcher_rows['EN'] .= ($adv_language_switcher_rows['EN'] ? "/" : "") . $slugTranslations["en_GB"]["slug"];
            }
        }

        if (isset($slug)) {
            var_dump('sto gestendo lo slug :: ' . $slug);
            $page_translation = (isset($objectType)) ? $objectType . 'Translation' : 'PageTranslation';
            $page_id = (isset($objectType)) ? $object->getId() : $page->getId();
            //var_dump($page_translation);
            $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:' . $page_translation)->findSlugTranslations($page_id, $slug);

            //var_dump($slugTranslations);
            foreach ($slugTranslations as $lang => $slugTranslation) {
                //var_dump($lang . ' - ' . $slugTranslation['slug']);
                $adv_language_switcher_rows[$lang] = $this->get('router')->generate($route_id, array('locale' => $lang, 'slug' => $slugTranslation['slug']));
            }
            //$adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT', 'slug' => $slugTranslations["it_IT"]["slug"]));
            //$adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB', 'slug' => $slugTranslations["en_GB"]["slug"]));
        }

        if(isset($route_id) && !isset($object) && !isset($objectType)){

            $slugLangs = $this->getDoctrine()->getRepository('AppBundle:PageTranslation')->findLangDistinct();
            //var_dump($slugLangs[0]);

            foreach ($slugLangs as $lang) {
                $adv_language_switcher_rows[$lang['locale']] = $this->get('router')->generate($route_id, array('locale' => $lang['locale']));
            }
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT'));
        }

        /*
        if ($page->getRouteId() == "page_show") {
            $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:PageTranslation')->findSlugTranslations($page->getId(), $slug);
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate('page_show', array('_locale' => 'it_IT', 'slug' => $slugTranslations["it_IT"]["slug"]));
            $adv_language_switcher_rows['EN'] = $this->get('router')->generate('page_show', array('_locale' => 'en_GB', 'slug' => $slugTranslations["en_GB"]["slug"]));
        } else if ($page->getRouteId() == "product_detail") {
            $adv_language_switcher_rows['IT'] = "";
            $adv_language_switcher_rows['EN'] = "";
            foreach ($object as $category) {
                $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:' . $objectType . 'Translation')->findSlugTranslations($category->getId(), $category->getSlug());
                $adv_language_switcher_rows['IT'] .= ($adv_language_switcher_rows['IT'] ? "/" : "") . $slugTranslations["it_IT"]["slug"];
                $adv_language_switcher_rows['EN'] .= ($adv_language_switcher_rows['EN'] ? "/" : "") . $slugTranslations["en_GB"]["slug"];
            }
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT', 'slug' => $adv_language_switcher_rows['IT']));
            $adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB', 'slug' => $adv_language_switcher_rows['EN']));
        } elseif ($slug) {
            $slugTranslations = $this->getDoctrine()->getRepository('AppBundle:' . $objectType . 'Translation')->findSlugTranslations($object->getId(), $slug);
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT', 'slug' => $slugTranslations["it_IT"]["slug"]));
            $adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB', 'slug' => $slugTranslations["en_GB"]["slug"]));
        } else {
            $adv_language_switcher_rows['IT'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'it_IT'));
            $adv_language_switcher_rows['EN'] = $this->get('router')->generate($page->getRouteId(), array('locale' => 'en_GB'));
        }
        */
        return $adv_language_switcher_rows;
    }

    protected function customizeSeoForBasePage($basePage,$link = "")
    {

        $title = $basePage->getTitle();
        $description = $basePage->getMetaDescription();
        $keywords = $basePage->getMetaKeywords();

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->setTitle($title . ' | Eni Oil Products');
        $seoPage->addMeta('property', 'og:title', $title . ' | Oil Products');
        if ($description) {
            $seoPage->addMeta('name', 'description', $description);
            $seoPage->addMeta('property', 'og:description', $description);
        }
        if ($keywords) {
            $seoPage->addMeta('name', 'keywords', $keywords);
        }
        
        if($link){
            
            foreach ($link as $hrefLang => $href) {
                
                $hrefLang = strtolower($hrefLang);
                $hrefLang = str_replace("_","-",$hrefLang);
                
                if($hrefLang == "it-it"){
                    $seoPage->addLangAlternate($href, "x-default");
                }
              
                if($hrefLang == "en-gb"){ 
                    $hrefLang = "en-it";
                    $seoPage->addLangAlternate($href, substr($hrefLang,0,2));
                }                
                
                if($hrefLang == "nl-bx"){
                    $hrefLang = "nl-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "nl-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "nl-nl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if($hrefLang == "fr-bx"){
                    $hrefLang = "fr-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "fr-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if($hrefLang == "en-bx"){
                    $hrefLang = "en-be";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-lu";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-nl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                }
                
                if ( substr($hrefLang,0,2) != "en" && strpos($href, substr($hrefLang,0,2)."_" ) !== false) {
                    $seoPage->addLangAlternate($href, substr($hrefLang,0,2));
                }
                
                if($hrefLang == "en-apac"){
                    $hrefLang = "th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "vi";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "zh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ja";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "tl";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "km";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-bd";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-mm";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-la";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-vn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-my";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-sg";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-tw";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-ph";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-kr";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-jp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-bn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-nz";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "en-au";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "th-th";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "vi-vn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-my";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-sg";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ms-bn";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "zh-tw";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ja-jp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko-kp";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "ko-kr";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "tl-ph";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    $hrefLang = "km-kh";
                    $seoPage->addLangAlternate($href, $hrefLang);
                    
                    continue;
                }
                
                $seoPage->addLangAlternate($href, $hrefLang);
            }
        }
        
        //$seoPage->addLangAlternate("https://oilproducts.eni.com/it_IT/", "x-default");
        
    }


}