<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    
    public function homeAction(Request $request)
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('home action');
    	return $this->render('AppBundle:frontend/home:home.html.twig', array());
    }
}

