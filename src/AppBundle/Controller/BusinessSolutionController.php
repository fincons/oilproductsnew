<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use AppBundle\Utils\FilterLocaleUtils;
use AppBundle\Form\Type\ConcessionaryToolType;

class BusinessSolutionController extends AbstractController
{
    
    public function listAction()
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('business solution list action');
        
        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByRouteId($this->container->get('request')->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }

    	$businesssolutions_all = $this->getDoctrine()->getRepository('AppBundle:BusinessSolution')->findBy(
            array('published'=> true),
            array('position' => 'ASC')
    	);
    	
    	$current_locale =  $this->get('request')->getLocale();
    	$businesssolutions = array();
    	//Filtro per locale
    	if ($businesssolutions_all) {
    		foreach ($businesssolutions_all as $businesssolution_temp) {
    			$lang_permitted = $businesssolution_temp->getLanguage ();
    			if( FilterLocaleUtils::linkEnabledForLocale($lang_permitted, $current_locale)){
    				$businesssolutions[] = $businesssolution_temp;
    			}
    		}
    	}
    	
    	//creo il link con lo slug tradotto preso dal file i18n.xml
    	$link = array();
    	$link = $this->container->get("UrlManager")->getRoutesByRouteId("businesssolution_list", $current_locale);
    	
    	$this->customizeSeoForBasePage($page, $link);
     	
    	$adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, null);
    	
    	$breadcrumb = $this->get("breadcrumb_manager")->addHome()->addItem(array('title' => $page->getTitle(), 'url' => ""));
    	
    	return $this->render('AppBundle:frontend/businesssolution:businesssolution_list.html.twig', 
    	    array('businesssolutions' => $businesssolutions, 
    	        'adv_language_switcher_rows' => $adv_language_switcher_rows, 
    	        'breadcrumb' => $breadcrumb,
    	        'page' =>$page));
    }
   
    public function detailAction($slug) {
        
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('business solution view action');

        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByRouteId($this->container->get('request')->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }

        $businessSolution = $this->getDoctrine()->getRepository('AppBundle:BusinessSolution')->findOneByTranslatableField('slug',$slug); 
    	
        if (!$businessSolution || !$businessSolution->getEnabled()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }
        
    	//$this->customizeSeoForBusinessSolution($businessSolution);

        $adv_language_switcher_rows = array();
    	// $adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, $slug, 'Post', $businessSolution);
    	
    	$breadcrumb = $this->get("breadcrumb_manager")->addHome()->addPageByRouteId('businesssolution_list')->addItem(array('title' => $businessSolution->getTitle(), 'url' => ""));

    	return $this->render('AppBundle:frontend/businesssolution:template1.html.twig', array(
    			'businesssolution' => $businessSolution,
    			'adv_language_switcher_rows' => $adv_language_switcher_rows,
    	        'breadcrumb' => $breadcrumb,
    			'form' => false,
    	));
        
    }
    
   
    
    public function concessionaryToolAction()
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Concessionary');
        
        $form = $this->createForm(ConcessionaryToolType::class);
        $form->handleRequest($request);
        
        $offices = array();
        $found = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $offices = $repo->searchByFormData($form->getData());
            
            if (count($offices)) {
                $found = true;
            }
        }
        if (!count($offices)) {
            $offices = $repo->findBy( array('sezione' => "businessSolution"));
        }
        
        return $this->render('AppBundle:frontend/businesssolution:concessionarytool.html.twig', [
            'form' => $form->createView(),
            'offices' => $offices,
            'found' => $found,
        ]);
    }
    
}

