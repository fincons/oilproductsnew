<?php

namespace AppBundle\Controller;

use AppBundle\Controller\AbstractController;
use AppBundle\Entity\Page;
use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\ProductClassificationRow;
use AppBundle\Utils\Constants;

class SearchProductController extends AbstractController
{
    public function searchAction($tab = null)
    {

        $categories = $this->getCategories();
        $filters = $this->getFilters();
        $productsNames = $this->getProductsNames();

        return $this->render('AppBundle:frontend/partial:search_product.html.twig', array(
            'categories' => $categories,
            'filters' => $filters,
            'productsNames' => $productsNames,
            'tab'=>$tab,
            'url_service' => $this->get('product_classification_url_manager'),
        ));
    }

    /**
     * @param \string $productCode
     */
    public function searchByProductCodeAction($productCode){
        $page = $this->configurePage();
        $this->customizeSeoForBasePage($page,null);
        $breadcrumb = $this->get("breadcrumb_manager")->addHome()->addItem(array('title' => $page->getTitle(), 'url' => ""));
        //$adv_language_switcher_rows = $this->calculateAdvLanguageSwitcherRows($page, $productCode);
        
        $current_locale =  $this->get('request')->getLocale();
        
        $em = $this->getDoctrine()->getManager();
        $productClassificationRows = $em->getRepository('AppBundle:ProductClassificationRow')->findByProductCode($current_locale,$productCode);
        $searchResults = [];
        /** @var ProductClassificationRow $productClassificationRow */
        foreach ($productClassificationRows as $productClassificationRow) {
            $searchResult = [
                'image' => $productClassificationRow->getProduct()->getThumbImage(),
                'title' => $productClassificationRow->getProduct()->getTitle(),
                'abstract' => $productClassificationRow->getProduct()->getShortDescription(),
                'uri' => $this->dummyProductUrlBuilder($productClassificationRow),
            ];
            $searchResults[]=$searchResult;
        }

        return $this->render('AppBundle:frontend/search:searchresult_list.html.twig', array(
            'page'=> $page,
            'breadcrumb' => $breadcrumb,
            'tab'=>'code-tab-activator',
            'resultsCount' => count($searchResults),
            'searchString' => $productCode,
            'results' => $searchResults,
        ));
    }
    
    public function searchByProductNameAction($productName){
        $page = $this->configurePage();
        $this->customizeSeoForBasePage($page,null);
        $breadcrumb = $this->get("breadcrumb_manager")->addHome()->addItem(array('title' => $page->getTitle(), 'url' => ""));
        
        $current_locale =  $this->get('request')->getLocale();
        
        $em = $this->getDoctrine()->getManager();
        $productClassificationRows = $em->getRepository('AppBundle:ProductClassificationRow')->findAllProductLinkByName($productName,$current_locale);
        $searchResults = [];
        /** @var ProductClassificationRow $productClassificationRow */
        foreach ($productClassificationRows as $productClassificationRow) {
            $searchResult = [
                'image' => $productClassificationRow->getProduct()->getThumbImage(),
                'title' => $productClassificationRow->getProduct()->getTitle(),
                'abstract' => $productClassificationRow->getProduct()->getShortDescription(),
                'uri' => $this->dummyProductUrlBuilder($productClassificationRow),
            ];
            $searchResults[]=$searchResult;
        }
        
        return $this->render('AppBundle:frontend/search:searchresult_list2.html.twig', array(
            'page'=> $page,
            'breadcrumb' => $breadcrumb,
            'tab'=>'code-tab-activator',
            'resultsCount' => count($searchResults),
            'searchString' => $productName,
            'results' => $searchResults,
        ));
    }

    protected function getCategories()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = [];
        
        $current_locale =  $this->get('request')->getLocale();
        
        foreach($em->getRepository('AppBundle:ProductClassificationRow')->findDistinctProductCategoriesOfLev2($current_locale) as $category) {
            $categories[$category->getId()] = $category;
        }

        return $categories;
    }

    protected function getFilters()
    {
        
        $filters = [];
        $em = $this->getDoctrine()->getManager();
        
        $current_locale =  $this->get('request')->getLocale();
        
        $categoriesLev2ToFilter = $em->getRepository('AppBundle:ProductClassificationRow')->findDistinctProductCategoriesOfLev2($current_locale);
        foreach($categoriesLev2ToFilter as $categoryLev2) {
            $categoriesLev3 = $em->getRepository('AppBundle:ProductClassificationRow')->findDistinctProductCategoriesOfLev3($current_locale,$categoryLev2);
            $categoriesLev3ToFilter = array();
            foreach($categoriesLev3 as $categoryLev3) {
                if ($categoryLev3->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                    $categoriesLev3ToFilter[] = $categoryLev3;
                }
            }
            $filters[$categoryLev2->getId()] = $categoriesLev3ToFilter;
        }

        return $filters;
    }
     
    protected function getProductsNames()
    {
        $em = $this->getDoctrine()->getManager();
        $productsNames = [];
        
        $current_locale =  $this->get('request')->getLocale();
        
        $productsNames = $em->getRepository('AppBundle:ProductClassificationRow')->findAllProductsNames($current_locale);
        
        return $productsNames;
    }
    

    /**
     * @return Page
     */
    private function configurePage()
    {
        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository('AppBundle:Page')->findOneByRouteId($this->container->get('request')->get('_route'));
        if (!$page || !$page->getPublished()) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.not_found'));
        }
        return $page;
    }

    protected function customizeSeoForBasePage($basePage, $link = "") {

        $title = $basePage->getTitle();
        $description = $basePage->getMetaDescription();
        $keywords = $basePage->getMetaKeywords();

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage->setTitle($title.'  | Eni Oil Products');
        $seoPage->addMeta('property', 'og:title', $title);
        if ($description) {
            $seoPage->addMeta('name', 'description', $description);
            $seoPage->addMeta('property', 'og:description', $description);
        }
        if($keywords) {
            $seoPage->addMeta('name', 'keywords', $keywords);
        }
    }

    /**
     * Build an url starting from a ProductClassificationRow and this dummy rule:
     * Level6: cat
     * @param \AppBundle\Entity\ProductClassificationRow $productClassificationRow
     * @return string
     */
    protected function dummyProductUrlBuilder(\AppBundle\Entity\ProductClassificationRow $productClassificationRow){
        $productUriWithOutLocale = '';
        $level = 2;
        foreach ([2,3,4,5] as $catLevel) {
            $reflectionMethod = new \ReflectionMethod(\AppBundle\Entity\ProductClassificationRow::class, 'getProductCategoryLev' . $catLevel);
            /** @var ProductCategory $category */
            $category = $reflectionMethod->invoke($productClassificationRow);
            if(strtolower($category->getSlug()) != 'empty'){
                $level++;
                $productUriWithOutLocale = $productUriWithOutLocale .'/'.$category->getSlug();
            }
        }
        switch ($level) {
            case 6: //Product are listed in accordion
                return $productUriWithOutLocale;
            case 5: //Product has single specific page
                return $productUriWithOutLocale
                .'/'.$productClassificationRow->getProduct()->getSlug();
            case 4: //Products sigle page are under last category
                return $productUriWithOutLocale
                .'/'.$productClassificationRow->getProductCategoryLev3()->getSlug()
                .'/'.$productClassificationRow->getProduct()->getSlug();
            default : $productClassificationRow;
        }

    }
}
