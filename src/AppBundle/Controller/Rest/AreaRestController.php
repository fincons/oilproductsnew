<?php
namespace AppBundle\Controller\Rest;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;

class AreaRestController extends FOSRestController
{

    /**
     * @Annotations\RequestParam(name="name", nullable=true)
     * @Annotations\RequestParam(name="email", nullable=true)
     * @Annotations\RequestParam(name="phone", nullable=true)
     * @Annotations\RequestParam(name="city", nullable=true)
     * @Annotations\RequestParam(name="province", nullable=true)
     * @Annotations\RequestParam(name="offerType", nullable=true)
     * @Annotations\RequestParam(name="message", nullable=true)
     * @Annotations\RequestParam(name="privacyAccepted", nullable=true)
     */
    public function postGplcontactsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $logger = $this->get('monolog.logger.enioil');
        $logger->debug('area gpl contact action ');
        $logger->debug('locale ' . $request->getLocale());
        $logger->debug('name ' . $paramFetcher->get("name"));
        $logger->debug('email ' . $paramFetcher->get("email"));
        $logger->debug('phone ' . $paramFetcher->get("phone"));
        $logger->debug('city ' . $paramFetcher->get("city"));
        $logger->debug('province ' . $paramFetcher->get("province"));
        $logger->debug('offerType ' . $paramFetcher->get("offerType"));
        $logger->debug('message ' . $paramFetcher->get("message"));
        $logger->debug('privacyAccepted ' . $paramFetcher->get("privacyAccepted"));
        
        $templating = $this->get('templating');
        $body = $templating->render('AppBundle:email:gplcontact.txt.twig', array(
            'name' => $paramFetcher->get("name"),
            'email' => $paramFetcher->get("email"),
            'phone' => $paramFetcher->get("phone"),
            'city' => $paramFetcher->get("city"),
            'province' => $paramFetcher->get("province"),
            'offerType' => $paramFetcher->get("offerType"),
            'message' => $paramFetcher->get("message"),
            'privacyAccepted' => $paramFetcher->get("privacyAccepted"),            
        ));
        $toEmail = $this->getParameter('gplcontact_to_email');
        $fromEmail = $this->getParameter('oilproducts_from_email');
        
        $env = $this->getParameter('kernel.environment');
        
        $message = \Swift_Message::newInstance()->setSubject('[ENIOIL][' . $env . '] Gpl contact mail')
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBody($body, 'text/html');
        
        $this->get('mailer')->send($message);
        $logger->debug('mail sent');
        
        return $this->get('translator')->trans('gplcontact.sentcorrectly');
    }
}

