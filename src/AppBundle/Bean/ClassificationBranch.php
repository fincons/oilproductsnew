<?php
namespace AppBundle\Bean;

use AppBundle\Bean\AbstractClassificationBranch;

class ClassificationBranch extends AbstractClassificationBranch
{

    public function getHasDedicatedPage() {
        if ($this->hasDedicatedPage != -1) {
            return $this->hasDedicatedPage;
        }
        if (count($this->walk) == 3) {
            return false;
        } else {
            return true;
        }
    }
}
