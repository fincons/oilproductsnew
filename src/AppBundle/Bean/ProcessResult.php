<?php
namespace AppBundle\Bean;

class ProcessResult
{

	private $result = "";
	private $subject = "";
	private $message = "";
	private $abstract = "";
	private $exceptions = array();
	private $warnings = array();
	private $files = array();
	
	public function getResult() {
		return $this->result;
	}
	
	public function setResult($result) {
		$this->result = $result;
	}
	
	public function getSubject() {
		return $this->subject;
	}
	
	public function setSubject($subject) {
		$this->subject = $subject;
	}
	
    public function getMessage() {
    	return $this->message;
    }
    
    public function appendMessage($message) {
    	$this->message .= $message . "\n";
    }
    
    public function getAbstract() {
    	return $this->abstract;
    }
    
    public function appendAbstract($abstract) {
    	$this->abstract .= $abstract . "; ";
    }
    
    public function getExceptions() {
    	return $this->exceptions;
    }
    
    public function appendException($exception) {
    	$this->exceptions[] = $exception;
    }
    
    public function getWarnings() {
    	return $this->warnings;
    }
    
    public function appendWarning($warning) {
    	$this->warnings[] = $warning;
    }
    
    public function getFiles() {
    	return $this->files;
    }
    
    public function appendFile($file) {
    	$this->files[] = $file;
    }
    
    public function printExceptions() {
    	$string = "";
    	foreach ($this->getExceptions() as $exception) {
    		$string .= "\nException:\n:";
    		$string.= $exception->getMessage() . " at file " . $exception->getFile() . " at line " . $exception->getLine();
    		$string.= " with stacktrace:\n" . $exception->getTraceAsString();
    	}
    	return $string;
    }
    
    public function getAbstractAndMessageAndExceptions() {
    	$string = "Abstract:\n";
    	$string .= $this->getAbstract();
    	$string .= "\n\nDetail:\n";
    	$string .= $this->getMessage();
    	$string .= "\n\n";
		$string.= $this->printExceptions();
    	return $string;
    }
    
    public function getSubjectWithResult() {
    	return $this->getResult() . ((count($this->warnings) >0)? " WITH WARN" : "") . " " . $this->getSubject();
    }
    

    
    
}
