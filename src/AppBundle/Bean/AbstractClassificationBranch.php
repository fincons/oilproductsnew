<?php
namespace AppBundle\Bean;

use AppBundle\Utils\Constants;

class AbstractClassificationBranch
{

    protected $walk;

    protected $doors;

    protected $active;

    protected $isSingleProduct;
    
    protected $productTemplate;
    
    protected $menuStart;
    
    protected $menuDepth;
    
    protected $hasDedicatedPage = -1;

    public function __construct($walk = array(), $doors = array(), $active = false, $isSingleProduct = false)
    {
        $this->walk = $walk;
        $this->doors = $doors;
        $this->active = $active;
        $this->isSingleProduct = $isSingleProduct;
    }

    public function setWalk($walk)
    {
        $this->walk = $walk;
    }

    public function getWalk()
    {
        return $this->walk;
    }

    public function addStep($step) {
        $this->walk[] = $step;
    }

    public function setDoors($doors)
    {
        $this->doors = $doors;
    }

    public function getDoors()
    {
        return $this->doors;
    }

    public function addDoor($door) {
        $this->doors[] = $door;
    }

    public function getLastStep() {
        return end($this->walk);
    }

    public function getFirstStep() {
        return  reset($this->walk);;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function equals($classificationBranch) {
        if ($this->getSlug() == $classificationBranch->getSlug()) {
            return true;
        }
        return false;
    }

    public function contains($classificationBranch) {
        if ($this->startsWith($classificationBranch->getSlug()."/", $this->getSlug()."/")) {
            return true;
        }
        return false;
    }

    public function setHasDedicatedPage($hasDedicatedPage) {
        $this->hasDedicatedPage = $hasDedicatedPage;
    }

    public function isLastStepEmpty() {
        if ($this->getLastStep()->getTitle() == Constants::EMPTY_CATEGORY_TITLE) {
            return true;
        }
        return false;
    }

    private function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function setIsSingleProduct($isSingleProduct)
    {
        $this->isSingleProduct = $isSingleProduct;
    }

    public function getIsSingleProduct()
    {
        return $this->isSingleProduct;
    }

    public function getBannerImage() {
        foreach (array_reverse($this->walk) as $step) {
            if(property_exists($step,'bannerImage')){
                if ($step->getBannerImage()) {
                    return $step->getBannerImage();
                }
            }
        }
        return null;
    }
    
	public function setProductTemplate($productTemplate)
	{
		$this->productTemplate = $productTemplate;
	}

	public function getProductTemplate()
	{
		return $this->productTemplate;
	}
	
	public function setMenuStart($menuStart)
	{
	    $this->menuStart = $menuStart;
	}
	
	public function getMenuStart()
	{
	    return $this->menuStart;
	}
	
	public function setMenuDepth($menuDepth)
	{
	    $this->menuDepth = $menuDepth;
	}
	
	public function getMenuDepth()
	{
	    return $this->menuDepth;
	}
	
	// NB: when category is EMPTY this calculation jumps the slug level
	public function getSlug() {
	    $slug = "";
	    $i = 1;
	    foreach($this->walk as $step) {
	        if ($step->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                $stepSlug = $step->getSlug();
                $slug = $slug . ($slug ? "/" : "") . $stepSlug;
	        }
	        $i++;
	    }
	    return $slug;
	}

	// NB: when category is EMPTY this calculation sets previous category slug
// 	public function getSlug() {
// 	    $slug = "";
// 	    $previousStepSlug = "";
// 	    $i = 1;
// 	    foreach($this->walk as $step) {
// 	        $stepSlug = $step->getSlug();
// 	        if ($step->getTitle() == Constants::EMPTY_CATEGORY_TITLE) {
// 	            $stepSlug = $previousStepSlug;
// 	        }
// 	        $slug = $slug . ($slug ? "/" : "") . $stepSlug;
// 	        $previousStepSlug = $stepSlug;
// 	        $i++;
// 	    }
// 	    return $slug;
// 	}
	
	public function getHasDedicatedPage() {
	    return false;
	}


}
