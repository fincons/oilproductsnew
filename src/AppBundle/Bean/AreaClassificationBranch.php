<?php
namespace AppBundle\Bean;

use AppBundle\Bean\AbstractClassificationBranch;

class AreaClassificationBranch extends AbstractClassificationBranch
{
    
    public function getHasDedicatedPage() {
        if ($this->hasDedicatedPage != -1) {
            return $this->hasDedicatedPage;
        }
        if (count($this->walk) == 4) {
            return false;
        } else {
            return true;
        }
    }

}
