<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class TextBlockType extends AbstractBlockType
{
    protected $dataClass = 'AppBundle\\Entity\\TextBlock';
    protected $businessDataClass = 'AppBundle\\Entity\\BusinessSolutionTextBlock';
    protected $areaDataClass = 'AppBundle\\Entity\\AreaCategoryTextBlock';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('text', CKEditorType::class, ['label' => 'Text']);
    }

    public function getName()
    {
        return 'block_text_type';
    }
}
