<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use Infinite\FormBundle\Form\Type\PolyCollectionType;
use AppBundle\Form\Type\PageTabsType;
use AppBundle\Form\Type\TextBlockType;
use AppBundle\Form\Type\EmailBlockType;
use AppBundle\Form\Type\SliderBlockType;
use AppBundle\Form\Type\BrochureBlockType;

/**
 * TabType
 */
class TabType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('blocks', PolyCollectionType::class, array(
            'types'              => array(
                TextBlockType::class,
                EmailBlockType::class,
                BrochureBlockType::class,
                SliderBlockType::class,
            ),
            'options'            => ['type' => $options['type']],
            'allow_add'          => true,
            'allow_delete'       => true,
            'cascade_validation' => true,
            'label'              => false,
            'by_reference'       => false,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('type', PageTabsType::TYPE_STANDARD);
        $resolver->setDefault('data_class', function (Options $options) {
            switch($options['type']) {
                case PageTabsType::TYPE_BUSINESS:
                    return 'AppBundle\\Entity\\BusinessSolutionTab';
                    break;
                case PageTabsType::TYPE_AREA:
                    return 'AppBundle\\Entity\\AreaCategoryTab';
                    break;
                default:
                    return 'AppBundle\\Entity\\Tab';
                    break;
            }
        });
    }

    public function getName()
    {
        return 'tab_type';
    }
}
