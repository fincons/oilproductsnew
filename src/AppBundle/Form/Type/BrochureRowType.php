<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\BrochureRow;
use Sonata\AdminBundle\Form\Type\ModelTypeList;
use Sonata\DoctrineORMAdminBundle\Admin\FieldDescription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BrochureRowType extends AbstractType
{
    protected $admin;

    public function __construct($admin)
    {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $fieldDescription = new FieldDescription();
        $fieldDescription->setAdmin($this->admin);
        $fieldDescription->setAssociationAdmin($this->admin);
        $fieldDescription->setOption('link_parameters', array(
        		'context' => 'default',
        		'provider' => 'sonata.media.provider.file',
        ));
        
        $pdfIconFieldDescription = new FieldDescription();
        $pdfIconFieldDescription->setAdmin($this->admin);
        $pdfIconFieldDescription->setAssociationAdmin($this->admin);
        $pdfIconFieldDescription->setOption('link_parameters', array(
        		'context' => 'default',
        		'provider' => 'sonata.media.provider.image',
        ));

        $builder->add('text', 'text', ['label' => 'Title']);
        $builder->add('pdf', ModelTypeList::class, [
        		'model_manager' => $this->admin->getModelManager(),
        		'class'  => $this->admin->getClass(),
        		'label' => 'Pdf',
        		'sonata_field_description' => $pdfIconFieldDescription,
        ]);
        $builder->add('pdfIcon', ModelTypeList::class, [
        		'model_manager' => $this->admin->getModelManager(),
        		'class'  => $this->admin->getClass(),
        		'label' => 'PdfIcon',
        		'sonata_field_description' => $pdfIconFieldDescription,
        		'required'    => false,
        ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', BrochureRow::class);
    }

    public function getName()
    {
        return 'brochure_row_type';
    }
}

