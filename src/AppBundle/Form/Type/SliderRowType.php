<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelTypeList;
use Sonata\DoctrineORMAdminBundle\Admin\FieldDescription;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\SliderRow;

class SliderRowType extends AbstractType
{
    protected $admin;

    public function __construct($admin)
    {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $fieldDescription = new FieldDescription();
        $fieldDescription->setAdmin($this->admin);
        $fieldDescription->setAssociationAdmin($this->admin);
        $fieldDescription->setOption('link_parameters', array(
            'context' => 'default',
            'provider' => 'sonata.media.provider.image',
        ));

        $builder->add('link', EntityType::class, [
            'class' => 'AppBundle:Link',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('l')
                    ->join('l.linkGroup', 'g')
                    ->where('g.type = :link')
                    ->setParameter('link', SliderRow::LINK_GROUP_TYPE)
                    ;
                },
        ]);

        $builder->add('image', ModelTypeList::class, [
            'model_manager' => $this->admin->getModelManager(),
            'class'  => $this->admin->getClass(),
            'label' => 'Image',
            'sonata_field_description' => $fieldDescription,
            'constraints' => array(
                new NotBlank,
            )
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', SliderRow::class);
    }

    public function getName()
    {
        return 'slider_row_type';
    }
}
