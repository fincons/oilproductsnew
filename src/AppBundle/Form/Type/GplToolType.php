<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * GplToolType
 */
class GplToolType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'expanded'      => true,
                'required'      => false,
                'label'         => false,
                'placeholder'   => false,
                'choices'       => ['cylinder' => 'gpltool.tank', 'littleBarrel' => 'gpltool.barrel'],
            ))
            ->add('city', EntityType::class, array(
                'required'      => false,
                'label'         => false,
                'class'         => 'AppBundle:City',
                'attr'          => ['class' => 'select2', 'style' => 'width: 100%'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                },
                'choice_label'  => function ($city) {
                    return sprintf("%s (%s)", $city->getName(), $city->getProvince()->getShortName());
                },
            ))
            ->add('search', SubmitType::class, array(
                'attr'  => ['class' => 'btn btn-default btn-submit btn-cerca'],
                'label' => 'gpltool.btn.search'
            ))
        ;
    }

    public function getName()
    {
        return 'gpl_tool_type';
    }
}
