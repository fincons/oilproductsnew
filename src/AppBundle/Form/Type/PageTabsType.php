<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\Type\TabType;

/**
 * PageTabsType
 */
class PageTabsType extends BaseType
{
    const TYPE_STANDARD = 'standard';
    const TYPE_BUSINESS = 'business';
    const TYPE_AREA = 'area';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tabs', CollectionType::class, array(
            'cascade_validation' => true,
            'label'              => false,
            'entry_type'         => TabType::class,
            'entry_options'      => ['type' => $options['type']],
            'by_reference'       => false,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('type', self::TYPE_STANDARD);
        $resolver->setDefault('data_class', function (Options $options) {
            switch($options['type']) {
                case PageTabsType::TYPE_BUSINESS:
                    return 'AppBundle\\Entity\\BusinessSolution';
                    break;
                case PageTabsType::TYPE_AREA:
                    return 'AppBundle\\Entity\\AreaCategory';
                    break;
                default:
                    return 'AppBundle\\Entity\\Page';
                    break;
            }
        });
    }

    public function getName()
    {
        return 'page_tabs_type';
    }
}
