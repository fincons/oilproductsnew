<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class EmailBlockType extends AbstractBlockType
{
    protected $dataClass = 'AppBundle\\Entity\\EmailBlock';
    protected $businessDataClass = 'AppBundle\\Entity\\BusinessSolutionEmailBlock';
    protected $areaDataClass = 'AppBundle\\Entity\\AreaCategoryEmailBlock';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('text', CKEditorType::class, ['label' => 'Text']);
        $builder->add('email', EmailType::class, ['label' => 'Email']);
    }

    public function getName()
    {
        return 'block_email_type';
    }
}
