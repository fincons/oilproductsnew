<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

abstract class AbstractBlockType extends BaseType
{
    protected $dataClass = 'AppBundle\\Entity\\Block';
    protected $businessDataClass = 'AppBundle\\Entity\\BusinessSolutionBlock';
    protected $areaDataClass = 'AppBundle\\Entity\\AreaCategoryBlock';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('published', CheckboxType::class, ['required' => false, 'label' => false]);
        $builder->add('position', HiddenType::class, ['attr' => ['class' => 'sortable-position']]);
        $builder->add('_type', HiddenType::class, array(
            'data'   => get_class($this),
            'mapped' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('type', PageTabsType::TYPE_STANDARD);
        $resolver->setDefault('data_class', function (Options $options) {
            switch($options['type']) {
                case PageTabsType::TYPE_BUSINESS:
                    return $this->businessDataClass;
                    break;
                case PageTabsType::TYPE_AREA:
                    return $this->areaDataClass;
                    break;
                default:
                    return $this->dataClass;
            }
        });
        $resolver->setDefault('model_class', function (Options $options) {
            switch($options['type']) {
                case PageTabsType::TYPE_BUSINESS:
                    return $this->businessDataClass;
                    break;
                case PageTabsType::TYPE_AREA:
                    return $this->areaDataClass;
                    break;
                default:
                    return $this->dataClass;
            }
        });
    }
}
