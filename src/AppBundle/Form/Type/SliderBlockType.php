<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Form\Type\CollectionType;
use AppBundle\Form\Type\SliderRowType;

class SliderBlockType extends AbstractBlockType
{
    protected $dataClass = 'AppBundle\\Entity\\SliderBlock';
    protected $businessDataClass = 'AppBundle\\Entity\\BusinessSolutionSliderBlock';
    protected $areaDataClass = 'AppBundle\\Entity\\AreaCategorySliderBlock';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('text', CKEditorType::class, ['label' => 'Text']);

        $builder->add('slider', CollectionType::class, [
            'cascade_validation' => true,
            'label'              => false,
            'entry_type'         => SliderRowType::class,
            'by_reference'       => false,
            'allow_add'          => true,
            'allow_delete'       => true,
        ]);
    }

    public function getName()
    {
        return 'block_slider_type';
    }
}
