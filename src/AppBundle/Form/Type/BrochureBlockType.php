<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\Type\BrochureRowType;
use Sonata\AdminBundle\Form\Type\CollectionType;

class BrochureBlockType extends AbstractBlockType
{
    protected $dataClass = 'AppBundle\\Entity\\BrochureBlock';
    protected $businessDataClass = 'AppBundle\\Entity\\BusinessSolutionBrochureBlock';
    protected $areaDataClass = 'AppBundle\\Entity\\AreaCategoryBrochureBlock';

    protected $admin;

    public function __construct($admin)
    {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('docs', CollectionType::class, [
        		'cascade_validation' => true,
        		'label'              => false,
        		'entry_type'         => BrochureRowType::class,
        		'by_reference'       => false,
        		'allow_add'          => true,
        		'allow_delete'       => true,
        ]);

    }

    public function getName()
    {
        return 'block_brochure_type';
    }
}
