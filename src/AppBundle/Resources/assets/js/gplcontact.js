$(document).ready(function() {
	$('#province').select2({
		  placeholder: $('#labelProvince').html()
	});
});

$("#gplcontact-form").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
    } else {
        event.preventDefault();
        submitForm();
    }
});

function submitForm(){
    $("#msgSubmit").addClass("hidden");
	var url = $("#gplcontact-form").attr("action");
	var name = $("#name").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var city = $("#city").val();
    var province = $("#province").val();
    var offerType = $('input[id=offerType]:checked').val();
    var message = $("#message").val();
    var privacyAccepted = $("#privacyAccepted").val();
    $('#btnSubmit').attr('disabled', 'disabled');
    
    $.ajax({
        type: "POST",
        url: url,
        data: "name=" + name + "&email=" + email + "&phone=" + phone + "&city=" + city + "&province=" + province + 
        "&offerType=" + offerType + "&message=" + message + "&privacyAccepted=" + privacyAccepted,
        error: function(text) {
        	$("#msgSubmit").html($('#labelSentIncorrectly').html());
            $("#msgSubmit").removeClass("hidden");
            $('#btnSubmit').removeAttr('disabled');  
        },
        success : function(text){
        	$("#msgSubmit").html(text);
            $("#msgSubmit").removeClass("hidden");
            $('#btnSubmit').removeAttr('disabled');  
        }
    });
}

