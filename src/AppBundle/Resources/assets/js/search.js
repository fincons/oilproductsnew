var SearchProduct = SearchProduct || {
    categories: { },
    filters: { }
}

$(document).ready(function() {
	
	current_language = window.location.href.split('/').slice(-2,-1)[0].substring(0, 2);
	
    $('#product-category').on('change', function() {
        if($('#product-category').val()) {
            $('#product-filters').prop('disabled', false);
            $('#search-product').prop('disabled', false);
        } else {
            $('#product-filters').prop('disabled', true);
            $('#search-product').prop('disabled', true);
        }
        var filters = SearchProduct.filters[$(this).val()];
        var counter= 0;
        for(key in filters) {
              counter++;
        }    
        $('#product-filters').empty();
        if ($('#product-category').val() && counter==0) {
        	$('#product-filters').append('<option value="" selected="selected">--</option>'); 
            $('#product-filters').prop('disabled', true);
        } else {
        	
        	if(current_language == 'it'){
        		var filterlabel = 'Applica un filtro';
        	}
        	if(current_language == 'en'){
        		var filterlabel = 'Apply filter';
        	}
        	if(current_language == 'de'){
                var filterlabel = 'Filter anwenden';
            }
        	if(current_language == 'fr'){
                var filterlabel = 'Appliquer le filtre';
            }
        	if(current_language == 'nl'){
                var filterlabel = 'Pas het filter toe';
            }
        	if(current_language == 'es'){
                var filterlabel = 'Aplicar filtro';
            }
        	if(current_language == 'pt'){
                var filterlabel = 'Aplicar filtro';
            }
        	
            $('#product-filters').append('<option value="" selected="selected">'+filterlabel+'</option>');
            for(key in filters) {
                  $('#product-filters').append('<option value="' + key + '">' + filters[key].label + '</option>');
            }    
        }
        $('#product-filters').val('').change();
    });

    $('#search-product').on('click', function(event) {
        event.preventDefault();
        var category = $('#product-category').val();
        var filter = $('#product-filters').val();
        if(filter) {
            window.location.href = SearchProduct.filters[category][filter].href;
        } else if(category) {
            window.location.href = SearchProduct.categories[category].href;
        }
    });
    $('#product-category').trigger('change');

    $('#search-product-code').on('click', function(event) {
        event.preventDefault();
        var productCode = $('#product-code').val();
        window.location.href = $('#search-by-code').attr('data-action')+productCode;
    });
});
