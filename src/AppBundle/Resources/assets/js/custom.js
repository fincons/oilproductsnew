// Global Variables
var $_window,
header,
header_height,
header_upper_row_height,
header_middle_row_height,
first_country_offset_top,
readinglist,
this_offset_top,
readinglist_offset_top,
readinglist_markup,
readinglist_children,
content_nav_menu_has_1_lvl = false;


// Functions


// Manages the Reading List in the Contacts page as well as the 'Back to top' elements
function scroll_to (this_offset_top) {
    $('html, body').animate({
            scrollTop: this_offset_top
        }, 300
    );
}


// Adjusts the Reading list 'top' value as the user scrolls the page
function adjust_readinglist_top_value () {
    if ($_window.scrollTop() >= first_country_offset_top) {
        readinglist_offset_top = $_window.scrollTop() - first_country_offset_top;
        readinglist.css('top', readinglist_offset_top);
    }
    else {
        readinglist.css('top', 0);
    }
}


// Shift header as the user scrolls the page
function shift_header () {
    if ($_window.scrollTop() > 0) {
        header.addClass('shifted');
    }
    else {
        header.removeClass('shifted');
    }
}


$(document).ready(function(){
    // Variables overwrites
    $_window = $(window),
    header = $("header"),
    header_height = header.outerHeight(),
    header_upper_row_height = header.children('.upper').outerHeight(true),
    header_middle_row_height = header.children('.middle').outerHeight(true),
    readinglist = $('#readinglist');


    if ($('ul.countries').length > 0) {
        first_country_offset_top = $('ul.countries > li:first-child').offset().top;
    }

    if ($(".content-nav-sidebar").length > 0 && $(".content-nav-sidebar").hasClass("menu-level-1")) {
        content_nav_menu_has_1_lvl = true;
    }

    // Add padding to the body as the header is in position: fixed
    $("body").css("padding-top", header_height);

    $_window.scroll(function () {
        shift_header ();
        adjust_readinglist_top_value ();
    });

    // Menu Prodotti in HP
    $(".products-menu-toggle").click(function() {
        var total_menu_height = $_window.height() - header_upper_row_height - header_middle_row_height + 45;
        $(this).next(".products-menu-wrapper-set-height").height(total_menu_height);
        $(this).next(".products-menu-wrapper").toggle();
        return false;
    });

    $(".products-menu > ul > li > div a").click(function() {
    	if ($(this).parents("li").children("ul").length>0) {
    	  $(this).closest("ul").children().addClass("no-border").children("div").add($(this).parents(".products-menu").find(' > .link:not(".back")')).hide();
    	  $(this).parents("li").children("ul").add($(this).parent("div").prev("div")).show();
    	  $(this).parents(".products-menu").children(".back").show();
    	  if (!$(this).parent("div").is(":first-child")) {
    	      return false;
    	  }
    	}
    });

    $(".products-menu .back a").click(function() {
        $(this).parent(".back").add($(this).parents(".products-menu").find("ul li ul, > ul > li > div:first-child")).hide();
        $(this).parents(".products-menu").find('> .link:not(".back"), > ul > li > div:not(":first-child")').show();
        $("ul li.no-border").removeClass("no-border");
        return false;
    });

    $("#show-mobile-menu").click(function(){
        $("nav.main-mobile").toggleClass("main-mobile-visible");
    });

    // Close menu when the user clicks outside of it
    $("html").click(function() {
      $(".main .products-menu-wrapper").hide();
    });

    $(".main .products-menu").click(function(e) {
      e.stopPropagation();
    });

    // Menu on left sidebar
    /*
    $(".content-nav-sidebar > ul > li > a").click(function(){
        $(".content-nav-sidebar ul li a").removeClass("active");

        if ($(this).next("ul").is(":visible")) {
            $(".content-nav-sidebar ul li ul").slideUp();
        }

        else {
            $(".content-nav-sidebar ul li ul").not($(this).next("ul")).slideUp();
            $(this).next("ul").slideDown();
            $(this).addClass("active");
        }

        if (content_nav_menu_has_1_lvl == false) {
            return false;
        }
    });

    $(".content-nav-sidebar ul li ul li a").click(function(){
        $(".content-nav-sidebar ul li ul li a").removeClass("active");
        $(this).addClass("active");
    });
    */

    // Accordions
    $(".accordion-animate").click(function(){
        $(this).parents('.panel-heading').toggleClass('active');
    });

    // Responsive Accordions in Contacts section
    $("section.contact-us ul li h3 .icon-wrapper").click(function(){
        $(this).parent().toggleClass("active").next("ul").toggleClass("visible");
    });


    // Back to Top
    $(".back-to-top .icon").click(function(){
        scroll_to(0);
    });


    // Reading list
    $('ul.countries > li').each(function () {
        this_offset_top = $(this).offset().top;
        readinglist_markup = '<span onclick="scroll_to(' + this_offset_top + ');"><span class="bullet"><span>' + $(this).children('h3').text() + '</span></span></span>';
        readinglist.append(readinglist_markup);
        readinglist_children = readinglist.children();
        readinglist_children.click(function () {
            readinglist_children.removeClass('active');
            $(this).addClass('active');
        });
    });


    // Cookie bar
    if (Cookies.get('cookie-policy-accepted') !== 'true') {
        $("#cookie-bar").fadeIn();
    }

    $("#cookie-bar .btn-ylw").click(function() {
        $("#cookie-bar").fadeOut();
        Cookies.set('cookie-policy-accepted', 'true', { expires: 365 });
    });


    // Page Tabs
    if( $(".content-nav-tabs .page-tabs li").length) {
	    var bann_contact = $(".banner-contacts .btn-ylw").attr("href");
	    $(".content-nav-tabs .page-tabs li").each(function() {
		if ($(this).children().attr("title") == 'Contatti' || $(this).children().attr("title") == 'Contacts' || $(this).children().attr("title") == 'Kontakte' || $(this).children().attr("title") == 'Contacten') {
		    $(this).children().attr("data-toggle", "");
		    $(this).children().attr("href", bann_contact);
		}
	    });
    }


    if( $(".row.preview-list").length) {
        var my_index = 1;
        $(".row.preview-list li").each(function() {
            console.log((my_index % 4) +"-"+ (my_index % 2));
            if ( ((my_index % 4) == 0) && ((my_index % 2) == 0) ){
                $(this).after( '<li class="col-xs-12"></li>' );
            }else if ( ((my_index % 4) != 0) && ((my_index % 2) == 0) ){
                $(this).after( '<li class="col-xs-12 hidden-md hidden-lg"></li>' );
            }
            my_index = my_index+1;
        });
    }

    // Search
    $('#product-category').select2({
      minimumResultsForSearch: -1
    });

    $('#product-filters').select2({
      minimumResultsForSearch: -1
    });
    
    //Automatic Areas link Tab
	$('li a').click(function(event){
		if($(this).attr('href') == '##') { 
			event.preventDefault();
			event.stopPropagation();
			var foundLink = jQuery("a.link-to-content[title$='"+jQuery(this).attr('title')+"']").attr('href');
			if(foundLink !== undefined){
				window.location.href = foundLink;
			}
		}
	});


});
    if(window.location.hash) {
        var hash = window.location.hash.substring(1);
        var linkdaaprire = jQuery('a[href="#'+ hash +'"]');
        if(linkdaaprire.length > 0){
            linkdaaprire.trigger('click');
        };
    };
