<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\ProductClassificationRow;

class EpicProductClassificationRowSynchronizer extends EpicAbstractSynchronizer
{

    private $processResult;

    private $notEpicPcrs = [];
    
    private $countPosition = 0;

    public function __construct($em, $logger, $epicUtils)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function synchProductClassificationRows($epicArchiveToElaborate, $pcrs, $Nation)
    {

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Elaborate product classification for archive " . $epicArchiveToElaborate->getArchiveName());

        // disable all db pcrs
        /* Modifica paolo da testare
        $pcrsFromDb = $this->em->getRepository('AppBundle:ProductClassificationRow')->findAll();

        foreach($pcrsFromDb as $pcrFromDb) {

            if ($pcrFromDb->getFromEpic()) {
                $this->notEpicPcrs[$pcrFromDb->getId()] = self::NOT_EPIC_BECOME;
            } else {
                $this->notEpicPcrs[$pcrFromDb->getId()] = self::NOT_EPIC_ALREADY;
            }
            $pcrFromDb->setPosition(0);
            $pcrFromDb->setFromEpic(false);
        }
        */
        // Remove all db pcrs
        
        $language ="it_IT|en_GB";
        
        if($Nation == "Germany"){
            $language ="de_DE|en_DE";
        }
        
        if($Nation == "Benelux"){
            $language ="en_BX|fr_BX|nl_BX";
        }
        
        if($Nation == "Apac"){
            $language ="en_APAC";
        }
        
        if($Nation == "Iberia"){
            $language ="es_ES|pt_PT";
        }
        
        $query = $this->em->createQuery("DELETE AppBundle:ProductClassificationRow p WHERE p.language = '".$language."'");
        $query->execute();

        
        //prende la position max esistente per usarla nelle nei prodotti senza position
        foreach ($pcrs as &$pcr2) {
            if((isset($pcr2['Product_Position']) && is_numeric($pcr2['Product_Position'])) && $pcr2['Product_Position'] > $this->countPosition){
                $this->countPosition = $pcr2['Product_Position'];
            }
        }

        // calculate new pcrs
        $fake_position = 0;
        foreach ($pcrs as &$pcr) {
            $fake_position++;
            $pcr['fake_position'] = $fake_position;

            $this->logger->info("Elaborating row $fake_position: " . json_encode($pcr));

            $emptyCat = $this->em->getRepository('AppBundle:ProductCategory')->findOneByTitle('EMPTY');

            //Cerco Categoria 2
            if ($pcr['ProductCategoryLev2_epicKey'] !== null) {
                /**
                 * Attenzione, vado a cercare nella tabella di associazione con l'epickey altrimenti non ho quella aggiornata e
                 * l'alberatura sfasa
                 */
                $EpicCatLev2 = $this->em->getRepository('AppBundle:EpicProductCategory')->findOneByEpicKey($pcr['ProductCategoryLev2_epicKey']);
                $catLev2 = $this->getProductCategoryByEpicId($EpicCatLev2);
                if (!$catLev2) {
                    $catLev2 = $emptyCat;
                }
            } else {
                $catLev2 = $emptyCat;
            }
            $pcr['ProductCategoryLev2_id'] = $catLev2->getId();
            $pcr['ProductCategoryLev2_title'] = $catLev2->getTitle();

            //Cerco Categoria 3
            if ($pcr['ProductCategoryLev3_epicKey'] !== null) {
                $EpicCatLev3 = $this->em->getRepository('AppBundle:EpicProductCategory')->findOneByEpicKey($pcr['ProductCategoryLev3_epicKey']);
                $catLev3 = $this->getProductCategoryByEpicId($EpicCatLev3);
                if (!$catLev3) {
                    $catLev3 = $emptyCat;
                }
            } else {
                $catLev3 = $emptyCat;
            }
            $pcr['ProductCategoryLev3_id'] = $catLev3->getId();
            $pcr['ProductCategoryLev3_title'] = $catLev3->getTitle();

            //Cerco Categoria 4
            if ($pcr['ProductCategoryLev4_epicKey'] !== null) {
                $EpicCatLev4 = $this->em->getRepository('AppBundle:EpicProductCategory')->findOneByEpicKey($pcr['ProductCategoryLev4_epicKey']);
                $catLev4 = $this->getProductCategoryByEpicId($EpicCatLev4);
                if (!$catLev4) {
                    $catLev4 = $emptyCat;
                }
            } else {
                $catLev4 = $emptyCat;
            }
            $pcr['ProductCategoryLev4_id'] = $catLev4->getId();
            $pcr['ProductCategoryLev4_title'] = $catLev4->getTitle();

            //Cerco Categoria 5
            if ($pcr['ProductCategoryLev5_epicKey'] !== null) {
                $EpicCatLev5 = $this->em->getRepository('AppBundle:EpicProductCategory')->findOneByEpicKey($pcr['ProductCategoryLev5_epicKey']);
                $catLev5 = $this->getProductCategoryByEpicId($EpicCatLev5);
                if (!$catLev5) {
                    $catLev5 = $emptyCat;
                }
            } else {
                $catLev5 = $emptyCat;
            }

            $pcr['ProductCategoryLev5_id'] = $catLev5->getId();
            $pcr['ProductCategoryLev5_title'] = $catLev5->getTitle();
            
            if( $pcr['Product'] == "EMPTY") {
                $product = $this->em->getRepository('AppBundle:Product')->findOneByCode( $pcr['Product']);
            }
            else{
                //Cerco Prodotto
                $product = $this->em->getRepository('AppBundle:Product')->findOneByCode(str_pad($pcr['Product'], 6, "0", STR_PAD_LEFT));
            }
            
            

            if (isset($product)) {
                $this->logger->info("Found product :: " . str_pad($pcr['Product'], 6, "0", STR_PAD_LEFT));
                $pcrsFromDb = $this->em->getRepository('AppBundle:ProductClassificationRow')->findAllByCategoriesAndProduct($catLev2, $catLev3, $catLev4, $catLev5, $product);

                $pcr['ProductCategoryLev2_duplicated_of_id'] = "";
                $pcr['ProductCategoryLev3_duplicated_of_id'] = "";
                $pcr['ProductCategoryLev4_duplicated_of_id'] = "";
                $pcr['ProductCategoryLev5_duplicated_of_id'] = "";

                if (count($pcrsFromDb) == 0) {
                    $this->logger->info("NOT Found in ProductClassificationRow :: " . $catLev2 . ', ' . $catLev3 . ', ' . $catLev4 . ', ' . $catLev5 . ', ' . $product);

                    // statistics for duplicated
                    $catLev2MaybeDupl = $catLev2;
                    if ($catLev2MaybeDupl->getDuplicatedOf() != null) {
                        $catLev2MaybeDupl = $catLev2MaybeDupl->getDuplicatedOf();
                        $pcr['ProductCategoryLev2_duplicated_of_id'] = $catLev2MaybeDupl->getId();
                    }
                    $catLev3MaybeDupl = $catLev3;
                    if ($catLev3MaybeDupl->getDuplicatedOf() != null) {
                        $catLev3MaybeDupl = $catLev3MaybeDupl->getDuplicatedOf();
                        $pcr['ProductCategoryLev3_duplicated_of_id'] = $catLev3MaybeDupl->getId();
                    }
                    $catLev4MaybeDupl = $catLev4;
                    if ($catLev4MaybeDupl->getDuplicatedOf() != null) {
                        $catLev4MaybeDupl = $catLev4MaybeDupl->getDuplicatedOf();
                        $pcr['ProductCategoryLev4_duplicated_of_id'] = $catLev4MaybeDupl->getId();
                    }
                    $catLev5MaybeDupl = $catLev5;
                    if ($catLev5MaybeDupl->getDuplicatedOf() != null) {
                        $catLev5MaybeDupl = $catLev5MaybeDupl->getDuplicatedOf();
                        $pcr['ProductCategoryLev5_duplicated_of_id'] = $catLev5MaybeDupl->getId();
                    }
                    $pcrsFromDb = $this->em->getRepository('AppBundle:ProductClassificationRow')->findAllByCategoriesAndProduct($catLev2MaybeDupl, $catLev3MaybeDupl, $catLev4MaybeDupl, $catLev5MaybeDupl, $product);

                    //$pcrFromDb = new ProductClassificationRow();
                    $pcrFromDb = null;
                    if (count($pcrsFromDb) == 1) {
                        $pcr['type'] = self::DUPLICATED;
                        $pcrFromDb = new ProductClassificationRow();
                        $pcrFromDb->setProductCategoryLev2($catLev2);
                        $pcrFromDb->setProductCategoryLev3($catLev3);
                        $pcrFromDb->setProductCategoryLev4($catLev4);
                        $pcrFromDb->setProductCategoryLev5($catLev5);
                        $pcrFromDb->setLanguage($language);
                        //$pcrFromDb->setPosition($pcr['Product_Position']);
                        $pcrFromDb->setProduct($product);
                    } else if (count($pcrsFromDb) == 0) {
                        $pcr['type'] = self::EPIC_CREATED;
                        if ($product) {
                            $pcrFromDb = new ProductClassificationRow();
                            $pcrFromDb->setProductCategoryLev2($catLev2);
                            $pcrFromDb->setProductCategoryLev3($catLev3);
                            $pcrFromDb->setProductCategoryLev4($catLev4);
                            $pcrFromDb->setProductCategoryLev5($catLev5);
                            $pcrFromDb->setLanguage($language);
                            //$pcrFromDb->setPosition($pcr['Product_Position']);
                            $pcrFromDb->setProduct($product);
                        }

                    } else {
                        throw new Exception ("More than one duplicated row!");
                    }

                } else if (count($pcrsFromDb) == 1) {
                    $this->logger->info("Found in ProductClassificationRow :: " . $catLev2 . ', ' . $catLev3 . ', ' . $catLev4 . ', ' . $catLev5 . ', ' . $product);
                    $pcr['type'] = self::EPIC_UPDATED;
                    $pcrFromDb = $pcrsFromDb[0];
                } else {
                    $pcr['type'] = self::DUPLICATED;
                    throw new Exception ("Pcr duplicated!");
                }

                if (isset($pcrFromDb)) {
                    $this->savePcrOnDb($pcrFromDb, $pcr['Product_Position']);
                }

                $pcr['id'] = $pcrFromDb->getId();

                if (in_array($pcrFromDb->getId(), array_keys($this->notEpicPcrs))) {
                    if ($this->notEpicPcrs[$pcrFromDb->getId()] == self::NOT_EPIC_ALREADY) {
                        if ($pcr['type'] == self::EPIC_UPDATED || $pcr['type'] == self::DUPLICATED) {
                            $pcr['type'] = self::EPIC_CREATED;
                        }
                    }
                    $this->notEpicPcrs = array_diff_key($this->notEpicPcrs, array($pcrFromDb->getId() => 0));

                }
            } else {
                $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Product Code not found in DB " . $pcr['Product']);
            }
        }

        foreach (array_keys($this->notEpicPcrs) as $id) {
            $pcr = array();
            $pcr['position'] = 0;
            $pcr['id'] = $id;
            $pcr['type'] = $this->notEpicPcrs[$id];
            $pcrs[] = $pcr;
        }

        $pcrsFile = $this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "pcrs.csv");
        $pcrsKeys = ['position', 'id', 'ProductCategoryLev2_id', 'ProductCategoryLev2_duplicated_of_id', 'ProductCategoryLev2_title', 'ProductCategoryLev2_epicKey', 'ProductCategoryLev3_id', 'ProductCategoryLev3_duplicated_of_id', 'ProductCategoryLev3_title', 'ProductCategoryLev3_epicKey', 'ProductCategoryLev4_id', 'ProductCategoryLev4_duplicated_of_id', 'ProductCategoryLev4_title', 'ProductCategoryLev4_epicKey', 'ProductCategoryLev5_id', 'ProductCategoryLev5_duplicated_of_id', 'ProductCategoryLev5_title', 'ProductCategoryLev5_epicKey', 'Product', 'Product_Title', 'type'];
        $this->exportToCsv($pcrsFile, $pcrs, $pcrsKeys);

    }


    private function savePcrOnDb(&$pcrFromDb, $position)
    {
        if(((isset($position) && is_numeric($position)) ? $position : 0) == 0){
            $this->countPosition++;
            $position = $this->countPosition;
        }
        
        $pcrFromDb->setFromEpic(true);
        $pcrFromDb->setPosition($position);
        $this->em->persist($pcrFromDb);
        $this->em->flush();
    }

    private function getProductCategoryByEpicId($EpicId)
    {
        if (count($EpicId) > 0) {
            $this->logger->info("Found Epic Product category on db by epic key with this id => " . $EpicId->getProductCategoryId());

            $productCategoriesFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findOneById($EpicId->getProductCategoryId());

        }
        return $productCategoriesFromDb;
    }
}