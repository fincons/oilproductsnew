<?php

namespace AppBundle\Services;

use AppBundle\Entity\AreaCategory;
use AppBundle\Entity\AreaClassificationRow;

class EpicAreaClassificationRowSynchronizer extends EpicAbstractSynchronizer
{

    private $processResult;

    private $notEpicPcrs = [];
    
    private $countPosition = 0;

    public function __construct($em, $logger, $epicUtils)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function synchAreaClassificationRows($epicArchiveToElaborate, $acrs, $Nation)
    {

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Elaborate product classification for archive " . $epicArchiveToElaborate->getArchiveName());

        // disable all db pcrs
        /* Modifica paolo da testare
        $pcrsFromDb = $this->em->getRepository('AppBundle:ProductClassificationRow')->findAll();

        foreach($pcrsFromDb as $pcrFromDb) {

            if ($pcrFromDb->getFromEpic()) {
                $this->notEpicPcrs[$pcrFromDb->getId()] = self::NOT_EPIC_BECOME;
            } else {
                $this->notEpicPcrs[$pcrFromDb->getId()] = self::NOT_EPIC_ALREADY;
            }
            $pcrFromDb->setPosition(0);
            $pcrFromDb->setFromEpic(false);
        }
        */
        // Remove all db pcrs
        
        
        $language ="it_IT|en_GB";
        
        if($Nation == "Germany"){
            $language ="de_DE|en_DE";
        }
        
        if($Nation == "Benelux"){
            $language ="en_BX|fr_BX|nl_BX";
        }
        
        if($Nation == "Apac"){
            $language ="en_APAC";
        }
        
        if($Nation == "Iberia"){
            $language ="es_ES|pt_PT";
        }
        
        $query = $this->em->createQuery("DELETE AppBundle:AreaClassificationRow a WHERE a.language = '".$language."'");
        $query->execute();
        
        //prende la position max esistente per usarla nelle nei prodotti senza position
        foreach ($acrs as &$acr2) {            
            if((isset($acr2['Product_Position']) && is_numeric($acr2['Product_Position'])) && $acr2['Product_Position'] > $this->countPosition){
                $this->countPosition = $acr2['Product_Position'];
            }
        }
        
        //dump($acrs);
        //exit();
        // calculate new acrs
        $fake_position = 0;
        foreach ($acrs as &$acr) {
            $fake_position++;
            $pcr['fake_position'] = $fake_position;

            $this->logger->info("Elaborating row $fake_position: " . json_encode($acr));

            $emptyCat = $this->em->getRepository('AppBundle:AreaCategory')->findOneByTitle('EMPTY');
            //Cerco Categoria 2
            if ($acr['AreaLev2_epicKey'] !== null) {
                /**
                 * Attenzione, vado a cercare nella tabella di associazione con l'epickey altrimenti non ho quella aggiornata e
                 * l'alberatura sfasa
                 */
                //$catLev2 = $this->em->getRepository('AppBundle:AreaCategory')->findOneByEpicKey($acr['AreaLev2_epicKey']);
                $EpicCatLev2 = $this->em->getRepository('AppBundle:EpicAreaCategory')->findOneByEpicKey($acr['AreaLev2_epicKey']);
                $catLev2 = $this->getAreaCategoryByEpicId($EpicCatLev2);
                if (!$catLev2) {
                    $catLev2 = $emptyCat;
                }
            } else {
                $catLev2 = $emptyCat;
            }
            $acr['AreaLev2_id'] = $catLev2->getId();
            $acr['AreaLev2_title'] = $catLev2->getTitle();

            //Cerco Categoria 3
            if ($acr['AreaLev3_epicKey'] !== null) {
                $EpicCatLev3 = $this->em->getRepository('AppBundle:EpicAreaCategory')->findOneByEpicKey($acr['AreaLev3_epicKey']);
                $catLev3 = $this->getAreaCategoryByEpicId($EpicCatLev3);
                if (!$catLev3) {
                    $catLev3 = $emptyCat;
                }
            } else {
                $catLev3 = $emptyCat;
            }
            $acr['AreaLev3_id'] = $catLev3->getId();
            $acr['AreaLev3_title'] = $catLev3->getTitle();

            //Cerco Categoria 4
            if ($acr['AreaLev4_epicKey'] !== null) {
                $EpicCatLev4 = $this->em->getRepository('AppBundle:EpicAreaCategory')->findOneByEpicKey($acr['AreaLev4_epicKey']);
                $catLev4 = $this->getAreaCategoryByEpicId($EpicCatLev4);
                if (!$catLev4) {
                    $catLev4 = $emptyCat;
                }
            } else {
                $catLev4 = $emptyCat;
            }
            $acr['AreaLev4_id'] = $catLev4->getId();
            $acr['AreaLev4_title'] = $catLev4->getTitle();

            //Cerco Categoria 5
            if ($acr['AreaLev5_epicKey'] !== null) {
                $EpicCatLev5 = $this->em->getRepository('AppBundle:EpicAreaCategory')->findOneByEpicKey($acr['AreaLev5_epicKey']);
                $catLev5 = $this->getAreaCategoryByEpicId($EpicCatLev5);
                if (!$catLev5) {
                    $catLev5 = $emptyCat;
                }
            } else {
                $catLev5 = $emptyCat;
            }
            $acr['AreaLev5_id'] = $catLev5->getId();
            $acr['AreaLev5_title'] = $catLev5->getTitle();

            //Cerco Categoria 6
            if ($acr['AreaLev6_epicKey'] !== null) {
                $EpicCatLev6 = $this->em->getRepository('AppBundle:EpicAreaCategory')->findOneByEpicKey($acr['AreaLev6_epicKey']);
                $catLev6 = $this->getAreaCategoryByEpicId($EpicCatLev6);
                if (!$catLev6) {
                    $catLev6 = $emptyCat;
                }
            } else {
                $catLev6 = $emptyCat;
            }
            $acr['AreaLev6_id'] = $catLev6->getId();
            $acr['AreaLev6_title'] = $catLev6->getTitle();
            
            if($acr['Product'] == "EMPTY"){
                $product = $this->em->getRepository('AppBundle:Product')->findOneByCode($acr['Product']);
            }
            else{
                //Cerco Prodotto
                $product = $this->em->getRepository('AppBundle:Product')->findOneByCode(str_pad($acr['Product'], 6, "0", STR_PAD_LEFT));
            }
            


            if (isset($product)) {
                $this->logger->info("Found product :: " . str_pad($acr['Product'], 6, "0", STR_PAD_LEFT));
                $acrsFromDb = $this->em->getRepository('AppBundle:AreaClassificationRow')->findAllByCategoriesAndProduct($catLev2, $catLev3, $catLev4, $catLev5, $catLev6, $product);

                $acr['AreaLev2_duplicated_of_id'] = "";
                $acr['AreaLev3_duplicated_of_id'] = "";
                $acr['AreaLev4_duplicated_of_id'] = "";
                $acr['AreaLev5_duplicated_of_id'] = "";
                $acr['AreaLev6_duplicated_of_id'] = "";

                if (count($acrsFromDb) == 0) {
                    $this->logger->info("NOT Found in AreaClassificationRow :: " . $catLev2 . ', ' . $catLev3 . ', ' . $catLev4 . ', ' . $catLev5 . ', ' . $catLev6 . ', ' . $product);

                    // statistics for duplicated
                    $catLev2MaybeDupl = $catLev2;
                    if ($catLev2MaybeDupl->getDuplicatedOf() != null) {
                        $catLev2MaybeDupl = $catLev2MaybeDupl->getDuplicatedOf();
                        $acr['AreaLev2_duplicated_of_id'] = $catLev2MaybeDupl->getId();
                    }
                    $catLev3MaybeDupl = $catLev3;
                    if ($catLev3MaybeDupl->getDuplicatedOf() != null) {
                        $catLev3MaybeDupl = $catLev3MaybeDupl->getDuplicatedOf();
                        $acr['AreaLev3_duplicated_of_id'] = $catLev3MaybeDupl->getId();
                    }
                    $catLev4MaybeDupl = $catLev4;
                    if ($catLev4MaybeDupl->getDuplicatedOf() != null) {
                        $catLev4MaybeDupl = $catLev4MaybeDupl->getDuplicatedOf();
                        $acr['AreaLev4_duplicated_of_id'] = $catLev4MaybeDupl->getId();
                    }
                    $catLev5MaybeDupl = $catLev5;
                    if ($catLev5MaybeDupl->getDuplicatedOf() != null) {
                        $catLev5MaybeDupl = $catLev5MaybeDupl->getDuplicatedOf();
                        $acr['AreaLev5_duplicated_of_id'] = $catLev5MaybeDupl->getId();
                    }
                    $catLev6MaybeDupl = $catLev6;
                    if ($catLev6MaybeDupl->getDuplicatedOf() != null) {
                        $catLev6MaybeDupl = $catLev6MaybeDupl->getDuplicatedOf();
                        $acr['AreaLev6_duplicated_of_id'] = $catLev6MaybeDupl->getId();
                    }
                    $acrsFromDb = $this->em->getRepository('AppBundle:AreaClassificationRow')->findAllByCategoriesAndProduct($catLev2MaybeDupl, $catLev3MaybeDupl, $catLev4MaybeDupl, $catLev5MaybeDupl, $catLev6MaybeDupl, $product);

                    if (count($acrsFromDb) == 1) {
                        $acr['type'] = self::DUPLICATED;
                        $acrFromDb = new AreaClassificationRow();
                        $acrFromDb->setAreaLev2($catLev2);
                        $acrFromDb->setAreaLev3($catLev3);
                        $acrFromDb->setAreaLev4($catLev4);
                        $acrFromDb->setAreaLev5($catLev5);
                        $acrFromDb->setAreaLev6($catLev6);
                        $acrFromDb->setLanguage($language);
                        //$acrFromDb->setPosition($acr['Product_Position']);
                        $acrFromDb->setProduct($product);
                    } else if (count($acrsFromDb) == 0) {
                        $acr['type'] = self::EPIC_CREATED;
                        if ($product) {
                            $acrFromDb = new AreaClassificationRow();
                            $acrFromDb->setAreaLev2($catLev2);
                            $acrFromDb->setAreaLev3($catLev3);
                            $acrFromDb->setAreaLev4($catLev4);
                            $acrFromDb->setAreaLev5($catLev5);
                            $acrFromDb->setAreaLev6($catLev6);
                            $acrFromDb->setLanguage($language);
                            //$acrFromDb->setPosition($acr['Product_Position']);
                            $acrFromDb->setProduct($product);
                        }

                    } else {
                        throw new Exception ("More than one duplicated row!");
                    }

                } else if (count($acrsFromDb) == 1) {
                    $this->logger->info("Found in AreaClassificationRow :: " . $catLev2 . ', ' . $catLev3 . ', ' . $catLev4 . ', ' . $catLev5 . ', ' . $catLev6 . ', ' . $product);
                    $acr['type'] = self::EPIC_UPDATED;
                    $acrFromDb = $acrsFromDb[0];
                } else {
                    $acr['type'] = self::DUPLICATED;
                    throw new Exception ("Acr duplicated!");
                }

                if (isset($acrFromDb) && $acrFromDb) {
                    $this->saveAcrOnDb($acrFromDb, $acr['Product_Position']);
                }

                $acr['id'] = $acrFromDb->getId();

                if (in_array($acrFromDb->getId(), array_keys($this->notEpicPcrs))) {
                    if ($this->notEpicPcrs[$acrFromDb->getId()] == self::NOT_EPIC_ALREADY) {
                        if ($acr['type'] == self::EPIC_UPDATED || $acr['type'] == self::DUPLICATED) {
                            $acr['type'] = self::EPIC_CREATED;
                        }
                    }
                    $this->notEpicPcrs = array_diff_key($this->notEpicPcrs, array($acrFromDb->getId() => 0));

                }
            } else {
                $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "[AreaClassificationRow] - Product Code not found in DB " . $acr['Product']);
            }
        }

        foreach (array_keys($this->notEpicPcrs) as $id) {
            $acr = array();
            $acr['position'] = 0;
            $acr['id'] = $id;
            $acr['type'] = $this->notEpicPcrs[$id];
            $acrs[] = $acr;
        }

        $acrsFile = $this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "acrs.csv");
        $acrsKeys = ['position', 'id',
            'AreaLev2_id',
            'AreaLev2_duplicated_of_id',
            'AreaLev2_title',
            'AreaLev2_epicKey',
            'AreaLev3_id',
            'AreaLev3_duplicated_of_id',
            'AreaLev3_title',
            'AreaLev3_epicKey',
            'AreaLev4_id',
            'AreaLev4_duplicated_of_id',
            'AreaLev4_title',
            'AreaLev4_epicKey',
            'AreaLev5_id',
            'AreaLev5_duplicated_of_id',
            'AreaLev5_title',
            'AreaLev5_epicKey',
            'AreaLev6_id',
            'AreaLev6_duplicated_of_id',
            'AreaLev6_title',
            'AreaLev6_epicKey',
            'Area', 'Area_Title', 'type'];
        $this->exportToCsv($acrsFile, $acrs, $acrsKeys);

    }


    private function saveAcrOnDb(&$acrFromDb, $position)
    {
        if(((isset($position) && is_numeric($position)) ? $position : 0) == 0){
            $this->countPosition++;
            $position = $this->countPosition;
        }
        
        $acrFromDb->setFromEpic(true);
        $acrFromDb->setPosition($position);
        $this->em->persist($acrFromDb);
        $this->em->flush();
    }

    private function getAreaCategoryByEpicId($EpicId){
        $areaCategoriesFromDb = "";
        if (count($EpicId) > 0) {
            $this->logger->info("Found Epic Area category on db by epic key with this id => " . $EpicId->getAreaCategoryId());

            $areaCategoriesFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findOneById($EpicId->getAreaCategoryId());
        }
        return $areaCategoriesFromDb;
    }

}