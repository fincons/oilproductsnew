<?php

namespace AppBundle\Services;

use AppBundle\Utils\TextUtils;
use Exception;
use AppBundle\Entity\EpicArchive;
use AppBundle\Entity\Product;
use Ddeboer\DataImport\Reader\CsvReader;


class EpicProductSynchronizer
{

    const EPIC_UPDATED = "EPIC_UPDATED";
    const EPIC_CREATED = "EPIC_CREATED";
    const EPIC_UNTOUCHED = "EPIC_UNTOUCHED";
    const EPIC_MISSING_XML = "EPIC_MISSING_XML";
    const NOT_EPIC_ALREADY = "NOT_EPIC_ALREADY";
    const NOT_EPIC_BECOME = "NOT_EPIC_BECOME";

    const TRIMMARKER = '...';

    const LENGTH_65535 = 65.535;
    const LENGTH_255 = 255;
    const LENGTH_1000 = 1000;
    const LENGTH_2 = 2;

    private $processResult;
    private $epicProductsCode = [];
    private $notEpicProductsCode = [];

    public function __construct($em, $logger, $epicUtils)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function elaborateProducts($epicArchiveToElaborate, $product_datas, $Nation)
    {
        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Elaborate products for archive " . $epicArchiveToElaborate->getArchiveName());
        $this->synchProducts($epicArchiveToElaborate, $product_datas, $Nation);
        $this->moveFilesToCurrentAndPdfDir($epicArchiveToElaborate);
        $this->updateEpicArchiveTable($epicArchiveToElaborate);
    }

    private function synchProducts($epicArchiveToElaborate, $product_datas, $Nation)
    {

        $this->em->getConnection()->beginTransaction();

        try {
            //TODO toglie il limite di memoria, cambiare durante lo sviluppo di germania gestendo meglio il parser di xml
            ini_set('memory_limit', '-1');

            $keys = array();
            $keys[] = 'type';
            $keys[] = 'code';
            $keys[] = 'title';
            $keys[] = 'inepic';
            $keys[] = 'gradient';
            $keys[] = 'specifications';
            $keys[] = 'pdf';
            $keys[] = 'description';
            $keys[] = 'epicVersion';

            $keys[] = 'inalis';
            $keys[] = 'title (old)';
            $keys[] = 'gradient (old)';
            $keys[] = 'specifications (old)';
            $keys[] = 'pdf (old)';
            $keys[] = 'description (old)';
            $keys[] = 'epicVersion (old)';
            $keys[] = 'shortDescription (old)';

            $csv_to_export_en = array();
            $csv_to_export_it = array();
            $csv_to_export_de = array();
            $csv_to_export_fr_bx = array();
            $csv_to_export_nl_bx = array();
            $csv_to_export_es_es = array();
            $csv_to_export_pt_pt = array();

            ksort($product_datas);

            foreach ($product_datas as $product) {

                $this->logger->info("Elaborating product " . json_encode($product));

                // comon lang data
                $row = array();
                // it data
                $row_it = array();
                // en data
                $row_en = array();
                // de data
                $row_de = array();
                // en_de data
                $row_en_de = array();
                // en_bx data
                $row_en_bx = array();
                // fr_bx data
                $row_fr_bx = array();
                // nl_bx data
                $row_nl_bx = array();
                // en_apac data
                $row_en_apac = array();
                // es_es data
                $row_es_es = array();
                // pt_pt data
                $row_pt_pt = array();
                
                if($product->Code == "EMPTY"){
                    $row['code'] = $product->Code;
                }
                else{
                    // NB: code str pad left
                    $row['code'] = (strlen($product->Code) <= 6) ? str_pad($product->Code, 6, "0", STR_PAD_LEFT) : $product->Code;
                }
                
                

                $row['epicVersion'] = $product->Version;
                $row['inepic'] = 1;

                // data from db

                //controllo se esiste già il codice a db
                $productFromDb = $this->em->getRepository('AppBundle:Product')->findOneByCode($row['code']);

                if ($productFromDb != null) {

                    $this->logger->info("Found product on db with code " . $productFromDb->getCode() . " and epicVersion " . $productFromDb->getEpicVersion());

                    /**
                     * Controllo rimosso per dare pieno controllo a epic
                     */
                    //if (strlen($productFromDb->getEpicVersion()) == 0 || intval($productFromDb->getEpicVersion()) < intval($row['epicVersion'])) {

                    $this->logger->info("Product is to be updated");

                    $this->extractOldDbData($productFromDb, $Nation, $row, $row_it, $row_en, $row_de, $row_fr_bx, $row_nl_bx, $row_es_es, $row_pt_pt);

                    $getInfoFromProductXml = $this->getInfoFromProductXml($epicArchiveToElaborate, $product, $row, $row_it, $row_en, $row_de, $row_fr_bx, $row_nl_bx, $row_es_es, $row_pt_pt);

                    if ($getInfoFromProductXml) {

                        $this->epicProductsCode[$row['code']] = self::EPIC_UPDATED;
                        $row['type'] = $this->epicProductsCode[$row['code']];


                        $row_it = array_merge($row, $row_it);
                        $row_en = array_merge($row, $row_en);
                        
                        //controllo rimosso perche impediva update del campo pdf in italiano quando era gia pieno
                        //if($Nation == "Italy"){
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_it);
                        //}
                        
                        if($Nation == "Germany"){
                            
                            $row_de = array_merge($row, $row_de);
                            $row_en_de =  array_merge($row_en, $row_en_de);
                            $row_en_de['locale'] = 'en_DE';
                            
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_de);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en_de);
                        }
                        
                        if($Nation == "Benelux"){
                            
                            $row_fr_bx = array_merge($row, $row_fr_bx);
                            $row_nl_bx = array_merge($row, $row_nl_bx);
                            $row_en_bx =  array_merge($row_en, $row_en_bx);
                            $row_en_bx['locale'] = 'en_BX';
                            
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_fr_bx);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_nl_bx);
                            //se copia di en_GB non posizionare dopo un'altra copia di en_GB
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en_bx);
                        }
                        
                        if($Nation == "Apac"){
                            
                            $row_en_apac = array_merge($row_en, $row_en_apac);
                            $row_en_apac['locale'] = 'en_APAC';
                            //se copia di en_GB non posizionare dopo un'altra copia di en_GB
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en_apac);
                        }
                        
                        if($Nation == "Iberia"){
                            
                            $row_es_es = array_merge($row, $row_es_es);
                            $row_pt_pt = array_merge($row, $row_pt_pt);
                            
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_es_es);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_pt_pt);
                        }


                    } else {

                        $this->logger->info("Missing product xml");
                        $this->epicProductsCode["$row[code]"] = self::EPIC_MISSING_XML;
                        $row['type'] = $this->epicProductsCode["$row[code]"];

                        $row = $row;

                        $row_it = array_merge($row, $row_it);
                        $row_en = array_merge($row, $row_en);
                        $row_de = array_merge($row, $row_de);
                        $row_fr_bx = array_merge($row,$row_fr_bx);
                        $row_nl_bx = array_merge($row, $row_nl_bx);
                        $row_es_es = array_merge($row, $row_es_es);
                        $row_pt_pt = array_merge($row, $row_pt_pt);
                        
                    }

                    $csv_to_export_it[] = $row_it;
                    $csv_to_export_en[] = $row_en;
                    $csv_to_export_de[] = $row_de;
                    $csv_to_export_fr_bx[] = $row_fr_bx;
                    $csv_to_export_nl_bx[] = $row_nl_bx;
                    $csv_to_export_es_es[] = $row_es_es;
                    $csv_to_export_pt_pt[] = $row_pt_pt;

                    /*
                    } else {

                        $this->logger->info("Product is to be untouched");

                        $this->epicProductsCode[$row['code']] = self::EPIC_UNTOUCHED;
                        $row['type'] = $this->epicProductsCode[$row['code']];

                    }*/

                } else {
                                                         
                    $this->logger->info("Product is to be created");

                    $getInfoFromProductXml = $this->getInfoFromProductXml($epicArchiveToElaborate, $product, $row, $row_it, $row_en, $row_de, $row_fr_bx, $row_nl_bx, $row_es_es, $row_pt_pt);

                    if ($getInfoFromProductXml) {

                        $this->epicProductsCode[$row['code']] = self::EPIC_CREATED;
                        $row['type'] = $this->epicProductsCode[$row['code']];

                        $row = $row;

                        $row_it = array_merge($row, $row_it);
                        $row_en = array_merge($row, $row_en);
                        
                        $row_de = array_merge($row, $row_de);
                        $row_en_de =  array_merge($row_en, $row_en_de);
                        $row_en_de['locale'] = 'en_DE';
                        
                        
                        $row_fr_bx = array_merge($row, $row_fr_bx);
                        $row_nl_bx = array_merge($row, $row_nl_bx);
                        $row_en_bx =  array_merge($row_en, $row_en_bx);
                        $row_en_bx['locale'] = 'en_BX';
                        
                        $row_en_apac =  array_merge($row_en, $row_en_apac);
                        $row_en_apac['locale'] = 'en_APAC';
                        
                        $row_es_es = array_merge($row, $row_es_es);
                        $row_pt_pt = array_merge($row, $row_pt_pt);

                        // create product
                        
                        $productFromDb = new Product();
                        
                        //controlli rimossi per inserimento unico del prodotto
                        //if($Nation == "Italy"){
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_it);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en);
                       //}
                        
                        //if($Nation == "Germany"){
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_de);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en_de);
                        //}
                        
                        //if($Nation == "Benelux"){
                            
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_fr_bx);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en_bx);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_nl_bx);
                            
                        //}
                            //se copia di en_GB non posizionare dopo un'altra copia di en_GB
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_en_apac);
                            
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_es_es);
                            $this->updateProductFieldsWithEpicData($productFromDb, $row_pt_pt);

                    } else {

                        $this->logger->info("Missing product xml");
                        $this->epicProductsCode[$row['code']] = self::EPIC_MISSING_XML;
                        $row['type'] = $this->epicProductsCode[$row['code']];
                    }

                    $csv_to_export_it[] = $row_it;
                    $csv_to_export_en[] = $row_en;
                    $csv_to_export_de[] = $row_de;
                    $csv_to_export_fr_bx[] = $row_fr_bx;
                    $csv_to_export_nl_bx[] = $row_nl_bx;
                    $csv_to_export_es_es[] = $row_es_es;
                    $csv_to_export_pt_pt[] = $row_pt_pt;

                }

            }

            // export to csv

            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_it.csv"), $csv_to_export_it, $keys, 'it');
            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_en.csv"), $csv_to_export_en, $keys, 'en');
            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_de.csv"), $csv_to_export_de, $keys, 'de');
            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_fr_bx.csv"), $csv_to_export_fr_bx, $keys, 'fr_bx');
            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_nl_bx.csv"), $csv_to_export_nl_bx, $keys, 'nl_bx');
            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_es_es.csv"), $csv_to_export_es_es, $keys, 'es_es');
            $this->exportToCsv($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_pt_pt.csv"), $csv_to_export_pt_pt, $keys, 'pt_pt');
            
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_it.csv"));
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_en.csv"));
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_de.csv"));
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_fr_bx.csv"));
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_nl_bx.csv"));
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_es_es.csv"));
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_pt_pt.csv"));

            // set not epic all the other products
            $this->logger->info("Set not Epic all older products");

            $olderProducts = $this->em->getRepository('AppBundle:Product')->findOlderProducts(array_keys($this->epicProductsCode));

            foreach ($olderProducts as $olderProduct) {
                if ($olderProduct->getFromEpic()) {
                    $this->notEpicProductsCode[$olderProduct->getCode()] = self::NOT_EPIC_BECOME;
                    $olderProduct->setFromEpic(false);
                    $this->em->flush();
                } else {
                    $this->notEpicProductsCode[$olderProduct->getCode()] = self::NOT_EPIC_ALREADY;
                }
            }

            // notify not epic become products
            if (count(array_keys($this->notEpicProductsCode, self::NOT_EPIC_BECOME)) > 0) {
                $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Send not epic become products mail for " . count(array_keys($this->notEpicProductsCode, self::NOT_EPIC_BECOME)) . " products");
                $this->epicUtils->notifyNotEpicBecome($epicArchiveToElaborate, array_keys($this->notEpicProductsCode, self::NOT_EPIC_BECOME));
            }

            // stats
            $usefulStats = $this->usefulStats();
            $this->logger->info($usefulStats);
            file_put_contents($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_stats.txt"), $usefulStats);
            $this->processResult->appendFile($this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "products_stats.txt"));

            if ($this->epicUtils->getEpicCommit()) {
                $this->em->getConnection()->commit();
            } else {
                $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "No db changes because commit is disabled");
            }

        } catch (Exception $ex) {
            $this->em->getConnection()->rollback();
            throw new Exception($ex->getMessage());
        }

    }


    private function extractOldDbData($productFromDb, $Nation, &$row, &$row_it, &$row_en, &$row_de, &$row_fr_bx, &$row_nl_bx, &$row_es_es, &$row_pt_pt)
    {

        $row['inalis'] = $productFromDb->getFromAlis();
        $row['epicVersion (old)'] = $productFromDb->getEpicVersion();

        $row_it['title (old)'] = $productFromDb->getTitle();
        $row_it['gradient (old)'] = $productFromDb->getGradient();
        $row_it['specifications (old)'] = $productFromDb->getSpecifications();
        $row_it['pdf (old)'] = $productFromDb->getPdf();
        $row_it['description (old)'] = $productFromDb->getDescription();
        $row_it['shortDescription (old)'] = $productFromDb->getShortDescription();

        $row_en['title (old)'] = $this->get_en_field($productFromDb, 'title', $productFromDb->getTitle());
        $row_en['gradient (old)'] = $this->get_en_field($productFromDb, 'gradient', $productFromDb->getGradient());
        $row_en['specifications (old)'] = $this->get_en_field($productFromDb, 'specifications', $productFromDb->getSpecifications());
        $row_en['pdf (old)'] = $this->get_en_field($productFromDb, 'pdf', $productFromDb->getPdf());
        $row_en['description (old)'] = $this->get_en_field($productFromDb, 'description', $productFromDb->getDescription());
        $row_en['shortDescription (old)'] = $this->get_en_field($productFromDb, 'shortDescription', $productFromDb->getShortDescription());        
        
        if($Nation == "Germany"){
            $row_de['title (old)'] = $this->get_de_field($productFromDb, 'title', $productFromDb->getTitle());
            $row_de['gradient (old)'] = $this->get_de_field($productFromDb, 'gradient', $productFromDb->getGradient());
            $row_de['specifications (old)'] = $this->get_de_field($productFromDb, 'specifications', $productFromDb->getSpecifications());
            $row_de['pdf (old)'] = $this->get_de_field($productFromDb, 'pdf', $productFromDb->getPdf());
            $row_de['description (old)'] = $this->get_de_field($productFromDb, 'description', $productFromDb->getDescription());
            $row_de['shortDescription (old)'] = $this->get_de_field($productFromDb, 'shortDescription', $productFromDb->getShortDescription());
        }
        
        if($Nation == "Benelux"){
            $row_fr_bx['title (old)'] = $this->get_fr_bx_field($productFromDb, 'title', $productFromDb->getTitle());
            $row_fr_bx['gradient (old)'] = $this->get_fr_bx_field($productFromDb, 'gradient', $productFromDb->getGradient());
            $row_fr_bx['specifications (old)'] = $this->get_fr_bx_field($productFromDb, 'specifications', $productFromDb->getSpecifications());
            $row_fr_bx['pdf (old)'] = $this->get_fr_bx_field($productFromDb, 'pdf', $productFromDb->getPdf());
            $row_fr_bx['description (old)'] = $this->get_fr_bx_field($productFromDb, 'description', $productFromDb->getDescription());
            $row_fr_bx['shortDescription (old)'] = $this->get_fr_bx_field($productFromDb, 'shortDescription', $productFromDb->getShortDescription());
            
            $row_nl_bx['title (old)'] = $this->get_nl_bx_field($productFromDb, 'title', $productFromDb->getTitle());
            $row_nl_bx['gradient (old)'] = $this->get_nl_bx_field($productFromDb, 'gradient', $productFromDb->getGradient());
            $row_nl_bx['specifications (old)'] = $this->get_nl_bx_field($productFromDb, 'specifications', $productFromDb->getSpecifications());
            $row_nl_bx['pdf (old)'] = $this->get_nl_bx_field($productFromDb, 'pdf', $productFromDb->getPdf());
            $row_nl_bx['description (old)'] = $this->get_nl_bx_field($productFromDb, 'description', $productFromDb->getDescription());
            $row_nl_bx['shortDescription (old)'] = $this->get_nl_bx_field($productFromDb, 'shortDescription', $productFromDb->getShortDescription());
        }
        
        if($Nation == "Iberia"){
            $row_es_es['title (old)'] = $this->get_es_es_field($productFromDb, 'title', $productFromDb->getTitle());
            $row_es_es['gradient (old)'] = $this->get_es_es_field($productFromDb, 'gradient', $productFromDb->getGradient());
            $row_es_es['specifications (old)'] = $this->get_es_es_field($productFromDb, 'specifications', $productFromDb->getSpecifications());
            $row_es_es['pdf (old)'] = $this->get_es_es_field($productFromDb, 'pdf', $productFromDb->getPdf());
            $row_es_es['description (old)'] = $this->get_es_es_field($productFromDb, 'description', $productFromDb->getDescription());
            $row_es_es['shortDescription (old)'] = $this->get_es_es_field($productFromDb, 'shortDescription', $productFromDb->getShortDescription());
            
            $row_pt_pt['title (old)'] = $this->get_pt_pt_field($productFromDb, 'title', $productFromDb->getTitle());
            $row_pt_pt['gradient (old)'] = $this->get_pt_pt_field($productFromDb, 'gradient', $productFromDb->getGradient());
            $row_pt_pt['specifications (old)'] = $this->get_pt_pt_field($productFromDb, 'specifications', $productFromDb->getSpecifications());
            $row_pt_pt['pdf (old)'] = $this->get_pt_pt_field($productFromDb, 'pdf', $productFromDb->getPdf());
            $row_pt_pt['description (old)'] = $this->get_pt_pt_field($productFromDb, 'description', $productFromDb->getDescription());
            $row_pt_pt['shortDescription (old)'] = $this->get_pt_pt_field($productFromDb, 'shortDescription', $productFromDb->getShortDescription());
        }
        

    }


    private function getInfoFromProductXml($epicArchiveToElaborate, $product, &$row, &$row_it, &$row_en, &$row_de, &$row_fr_bx, &$row_nl_bx, &$row_es_es, &$row_pt_pt)
    {
        
        if( $row['code'] == "EMPTY") {            
            return false;            
        }

        $dirPath = $this->epicUtils->getEpicElaborationDirOfArchive($epicArchiveToElaborate);

        $productFilePath = $dirPath . $product->FilePath;

        if (file_exists($productFilePath)) {

            $xml = simplexml_load_file($productFilePath, 'SimpleXMLElement', LIBXML_NOCDATA);

            
            if($xml->Name->ita){
                $row_it['title'] = $xml->Name->ita;
            } else{
                $row_it['title'] = $xml->Name;
            }
            $row_it['title'] = str_replace("&amp;"," & ", $row_it['title']);
            
            
            if($xml->Name->enu){
                $row_en['title'] = $xml->Name->enu;
            } else{
                $row_en['title'] = $xml->Name;
            }
            $row_en['title'] = str_replace("&amp;"," & ", $row_en['title']);
            
            
            if($xml->Name->deu){
                $row_de['title'] = $xml->Name->deu;
            } else{
                $row_de['title'] = $xml->Name;
            }
            $row_de['title'] = str_replace("&amp;"," & ", $row_de['title']);
            
            
            if($xml->Name->fra){
                $row_fr_bx['title'] = $xml->Name->fra;
            } else{
                $row_fr_bx['title'] = $xml->Name;
            }
            $row_fr_bx['title'] = str_replace("&amp;"," & ", $row_fr_bx['title']);
            
            
            if($xml->Name->nld){
                $row_nl_bx['title'] = $xml->Name->nld;
            } else{
                $row_nl_bx['title'] = $xml->Name;
            }
            $row_nl_bx['title'] = str_replace("&amp;"," & ", $row_nl_bx['title']);
            
            
            if($xml->Name->esn){
                $row_es_es['title'] = $xml->Name->esn;
            } else{
                $row_es_es['title'] = $xml->Name;
            }
            $row_es_es['title'] = str_replace("&amp;"," & ", $row_es_es['title']);
            
            
            if($xml->Name->ptb){
                $row_pt_pt['title'] = $xml->Name->ptb;
            } else{
                $row_pt_pt['title'] = $xml->Name;
            }
            $row_pt_pt['title'] = str_replace("&amp;"," & ", $row_pt_pt['title']);
            
            
            $row['gradient'] = "";
            $row['specifications'] = TextUtils::cleanXmlString(implode("| ", array_values((array)$xml->PerformanceLevels->PerformanceLevel)));

            $row_it['pdf'] = $this->extractPdf($xml, 'ITA');
            if(isset($xml->Description->ita)){
                $row_it['description'] = $xml->Description->ita;
            }
            else{
                $row_it['description'] = "";
            }
            $row_it['locale'] = 'it_IT';

            
            $row_en['pdf'] = $this->extractPdf($xml, 'ENU');
            if(isset($xml->Description->enu)){
                $row_en['description'] = $xml->Description->enu;
            }
            else{
                $row_en['description'] = "";
            }
            $row_en['locale'] = 'en_GB';
 
            
            $row_de['pdf'] = $this->extractPdf($xml, 'DEU');
            if(isset($xml->Description->deu)){
                $row_de['description'] = $xml->Description->deu;
            }
            else{
                $row_de['description'] = " ";
            }
            $row_de['locale'] = 'de_DE';
            
            
            $row_fr_bx['pdf'] = $this->extractPdf($xml, 'FRA');
            if(isset($xml->Description->fra)){
                $row_fr_bx['description'] = $xml->Description->fra;
            }
            else{
                $row_fr_bx['description'] = " ";
            }
            $row_fr_bx['locale'] = 'fr_BX';
            
            
            $row_nl_bx['pdf'] = $this->extractPdf($xml, 'NLD');
            if(isset($xml->Description->nld)){
                $row_nl_bx['description'] = $xml->Description->nld;
            }
            else{
                $row_nl_bx['description'] = "  ";
            }
            $row_nl_bx['locale'] = 'nl_BX';
            
            
            $row_es_es['pdf'] = $this->extractPdf($xml, 'ESN');
            if(isset($xml->Description->esn)){
                $row_es_es['description'] = $xml->Description->esn;
            }
            else{
                $row_es_es['description'] = " ";
            }
            $row_es_es['locale'] = 'es_ES';
            
            
            $row_pt_pt['pdf'] = $this->extractPdf($xml, 'PTB');
            if(isset($xml->Description->ptb)){
                $row_pt_pt['description'] = $xml->Description->ptb;
            }
            else{
                $row_pt_pt['description'] = "  ";
            }
            $row_pt_pt['locale'] = 'pt_PT';
            

            return true;

        } else {

            return false;

        }

    }

    private function updateProductFieldsWithEpicData($product, $row)
    {   
                
        //$textproduct = print_r($row,true);
        //$this->logger->info("New Product ".$textproduct);
        
        $product->setCode($row['code']);
        //if(strlen(trim($row['title']))){
        $product->setTitle(mb_strimwidth($row['title'], 0, self::LENGTH_255, self::TRIMMARKER));
        //}
        $product->setSlug(TextUtils::slugify($row['title']));
        // if (strlen(trim($row['description']))) {
        $product->setDescription($row['description']);
        $product->setShortDescription((isset($row['shortDescription (old)'])) ? $row['shortDescription (old)'] : '');
        // }
        // if (strlen(trim($row['gradient']))) {
        $product->setGradient(mb_strimwidth($row['gradient'], 0, self::LENGTH_255, self::TRIMMARKER));
        // }
        // if (strlen(trim($row['specifications']))) {
        $product->setSpecifications(TextUtils::cleanXmlString($row['specifications']));
        // }
        $pdf = "";
        if (strlen(trim($row['pdf']))) {
            $pdf = (mb_strimwidth($row['pdf'], 0, self::LENGTH_1000, self::TRIMMARKER));
        }
        $product->setPdf($pdf);
        
        $product->setFromEpic(true);
        $product->setLocale($row['locale']);
        
        $product->setEpicVersion($row['epicVersion']);
        
        $this->em->persist($product);
        $this->em->flush();

        
    }


    private function usefulStats()
    {

        $products = $this->em->getRepository('AppBundle:Product')->findAll();

        $output = "Total products in oilproducts db " . count($products);
        $output .= "\nTotal " . count(array_keys($this->epicProductsCode)) . " products from Epic";
        $output .= "\nCreated " . count(array_keys($this->epicProductsCode, self::EPIC_CREATED)) . " products " . json_encode(array_keys($this->epicProductsCode, self::EPIC_CREATED));
        $output .= "\nUpdated " . count(array_keys($this->epicProductsCode, self::EPIC_UPDATED)) . " products " . json_encode(array_keys($this->epicProductsCode, self::EPIC_UPDATED));
        $output .= "\nUntouched " . count(array_keys($this->epicProductsCode, self::EPIC_UNTOUCHED)) . " products " . json_encode(array_keys($this->epicProductsCode, self::EPIC_UNTOUCHED));
        $output .= "\nMissing xml " . count(array_keys($this->epicProductsCode, self::EPIC_MISSING_XML)) . " products " . json_encode(array_keys($this->epicProductsCode, self::EPIC_MISSING_XML));
        $output .= "\nAlready not Epic " . count(array_keys($this->notEpicProductsCode, self::NOT_EPIC_ALREADY)) . " products " . json_encode(array_keys($this->notEpicProductsCode, self::NOT_EPIC_ALREADY));
        $output .= "\nFrom Epic to not Epic " . count(array_keys($this->notEpicProductsCode, self::NOT_EPIC_BECOME)) . " products " . json_encode(array_keys($this->notEpicProductsCode, self::NOT_EPIC_BECOME));

        $notClassifiedProducts = [];
        /* Rimozione dei prodotti non clasificati
        foreach($products as $product) {
            if($product->getProductClassificationRows()->isEmpty()) {
                $notClassifiedProducts[] = $product->getCode();
            }
        }
        $output .= "\nNot classified " .  count($notClassifiedProducts) . " products " . json_encode($notClassifiedProducts);
         */
        return $output;
    }

    private function extractPdf($xml, $lang)
    {
        $pdf = "";
        if(null !== $xml->FilePaths){
            foreach ($xml->FilePaths->children() as $filePath) {
                if ($filePath->Language == $lang) {
                    $pdf = $filePath->Path;
                    break;
                }
            }
        }
        return $pdf;
    }

    private function exportToCsv($outputFile, $csv_to_export, $keys, $lang)
    {
        $fp = fopen($outputFile, 'w');
        fputcsv($fp, $keys);

        foreach ($csv_to_export as $fields) {
            $ordered_fields = array();
            foreach ($keys as $key) {
                if (array_key_exists($key, $fields)) {
                    $ordered_fields[$key] = $fields[$key];
                }
            }
            fputcsv($fp, $ordered_fields);
        }

        fclose($fp);
    }


    private function get_en_field($product, $field_name, $field_value_it)
    {
        $field_value = $field_value_it;
        $productTranslations = $this->em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object' => $product->getId(),
                'locale' => 'en_GB',
                'field' => $field_name,
            ));

        foreach ($productTranslations as $productTranslation) {
            $field_value = $productTranslation->getContent();
        }

        return $field_value;
    }
    
    private function get_de_field($product, $field_name, $field_value_it)
    {
        $field_value = $field_value_it;
        $productTranslations = $this->em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object' => $product->getId(),
                'locale' => 'de_DE',
                'field' => $field_name,
            ));
        
        foreach ($productTranslations as $productTranslation) {
            $field_value = $productTranslation->getContent();
        }
        
        return $field_value;
    }
    
    private function get_fr_bx_field($product, $field_name, $field_value_it)
    {
        $field_value = $field_value_it;
        $productTranslations = $this->em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object' => $product->getId(),
                'locale' => 'fr_BX',
                'field' => $field_name,
            ));
        
        foreach ($productTranslations as $productTranslation) {
            $field_value = $productTranslation->getContent();
        }
        
        return $field_value;
    }
    
    private function get_nl_bx_field($product, $field_name, $field_value_it)
    {
        $field_value = $field_value_it;
        $productTranslations = $this->em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object' => $product->getId(),
                'locale' => 'nl_BX',
                'field' => $field_name,
            ));
        
        foreach ($productTranslations as $productTranslation) {
            $field_value = $productTranslation->getContent();
        }
        
        return $field_value;
    }
    
    private function get_es_es_field($product, $field_name, $field_value_it)
    {
        $field_value = $field_value_it;
        $productTranslations = $this->em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object' => $product->getId(),
                'locale' => 'es_ES',
                'field' => $field_name,
            ));
        
        foreach ($productTranslations as $productTranslation) {
            $field_value = $productTranslation->getContent();
        }
        
        return $field_value;
    }

    private function get_pt_pt_field($product, $field_name, $field_value_it)
    {
        $field_value = $field_value_it;
        $productTranslations = $this->em->getRepository('AppBundle:ProductTranslation')->findBy(
            array(
                'object' => $product->getId(),
                'locale' => 'pt_PT',
                'field' => $field_name,
            ));
        
        foreach ($productTranslations as $productTranslation) {
            $field_value = $productTranslation->getContent();
        }
        
        return $field_value;
    }
    
    private function updateEpicArchiveTable($epicArchiveToElaborate)
    {
        if ($epicArchiveToElaborate->getArchiveName() == "current") {
            return;
        }
        if (!$this->epicUtils->getEpicCommit()) {
            return;
        }
        $this->logger->info("Update EpicArchive row for file " . $epicArchiveToElaborate->getArchiveName());
        $epicArchiveToElaborate->setProductElaborationAt(new \DateTime("now"));
        $this->em->persist($epicArchiveToElaborate);
        $this->em->flush();
        $this->epicUtils->logAbstractAndAppendToProcessResult($this->processResult, $this->logger, "SYNCHRONIZED PRODUCTS FOR " . $epicArchiveToElaborate->getArchiveName());
    }

    private function moveFilesToCurrentAndPdfDir($epicArchiveToElaborate)
    {
        if ($epicArchiveToElaborate->getArchiveName() == "current") {
            return;
        }
        if (!$this->epicUtils->getEpicCommit()) {
            return;
        }
        $reports = array_map("basename", $this->processResult->getFiles());
        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Moving files to current dir");
        $source = $this->epicUtils->getEpicElaborationDirOfArchive($epicArchiveToElaborate);
        $currentDir = $this->epicUtils->getEpicCurrentDir();
        $pdfDir = $this->epicUtils->getEpicPdfDir();
        $files = scandir($source);
        foreach ($files as $file) {
            if (in_array($file, array(".", ".."))) continue;
            if (in_array($file, $reports)) continue;
            $file_parts = pathinfo($file);
            if (strtoupper($file_parts['extension']) == "XML") {
                if (copy($source . $file, $currentDir . $file)) {
                } else {
                    $this->epicUtils->logWarningAndAppendToProcessResult($this->processResult, $this->logger, "Error when copying $source.$file to current dir");
                }
            } else {
                if (copy($source . $file, $pdfDir . $file)) {
                } else {
                    $this->epicUtils->logWarningAndAppendToProcessResult($this->processResult, $this->logger, "Error when copying $source.$file to pdf dir");
                }
            }
        }
    }

    public function linkExistingProductsWithEpic()
    {
        $this->logger->info("Reading file ");

        $file = new \SplFileObject($this->epicUtils->getEpicElaborationDir() . 'products_link.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        $i = 0;
        foreach ($reader as $row) {
            $this->logger->info("Row " . json_encode($row));

            $code = str_pad($row['old_code'], 6, "0", STR_PAD_LEFT);
            if (substr($row['old_code'], 0, 1) === 'N') {
                $code = $row['old_code'];
            }

            $productFromDb = $this->em->getRepository('AppBundle:Product')->findOneByCode($code);
            if ($productFromDb != null) {
                $productFromDb->setCode(str_pad($row['new_code'], 6, "0", STR_PAD_LEFT));
                $this->em->persist($productFromDb);
                $this->em->flush();
                $i++;
            } else {
                $this->logger->info("Product not found!");
            }

        }

        $this->logger->info("Updated " . $i . " products");
    }


}