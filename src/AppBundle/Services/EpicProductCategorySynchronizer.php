<?php

namespace AppBundle\Services;

use AppBundle\Entity\EpicProductCategory;
use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\ProductCategoryTranslation;
use AppBundle\Utils\TextUtils;
use Ddeboer\DataImport\Reader\CsvReader;

class EpicProductCategorySynchronizer extends EpicAbstractSynchronizer
{

    private $processResult;

    private $notEpicProductCategories = [];

    public function __construct($em, $logger, $epicUtils)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function synchProductCategories($epicArchiveToElaborate, $product_category_datas, $Nation)
    {

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Elaborate product categories for archive " . $epicArchiveToElaborate->getArchiveName());

        $product_category_datas_enriched = array();

        // disable all db categories
        $categoriesFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findAll();
        foreach ($categoriesFromDb as $categoryFromDb) {
            if ($categoryFromDb->getFromEpic()) {
                $this->notEpicProductCategories[$categoryFromDb->getId()] = self::NOT_EPIC_BECOME;
            } else {
                $this->notEpicProductCategories[$categoryFromDb->getId()] = self::NOT_EPIC_ALREADY;
            }
            $categoryFromDb->setFromEpic(false);
        }

        foreach ($product_category_datas as $product_category_data) {
            $this->logger->info("Product category: " . json_encode($product_category_data));

            $productCategoryFromDb = null;
            $product_category_data['linked_to_db_id'] = "";
            $product_category_data['linked_to_db_title'] = "";
            $product_category_data['duplicated_of_db_id'] = "";
            $product_category_data['duplicated_of_db_title'] = "";

            //$productCategoriesFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findByEpicKey($product_category_data['key']);
            $productCategoriesFromDb = null;
            //Se trovo la chiave nella tabella di associazione -> prendo l'id della categoria e proseguo
            //Se non trovo la chiave nella tabella di associaz -> salvo la nuova categoria e salvo l'id nelle associazioni
            $add_assoc = false;
            $epicProductCategoriesFromDb = $this->em->getRepository('AppBundle:EpicProductCategory')->findByEpicKey($product_category_data['key']);
            if (count($epicProductCategoriesFromDb) > 0) {
                $this->logger->info("Found Epic Product category on db by epic key with this id => " . $epicProductCategoriesFromDb[0]->getProductCategoryId());

                $productCategoriesFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findById($epicProductCategoriesFromDb[0]->getProductCategoryId());
            }

            if (count($productCategoriesFromDb) == 1) {

                $this->logger->info("Found category on db by epic key");
                $product_category_data['type'] = self::EPIC_UPDATED_BY_KEY;
                $productCategoryFromDb = $productCategoriesFromDb[0];
                $product_category_data['linked_to_db_id'] = $productCategoriesFromDb[0]->getId();
                $product_category_data['linked_to_db_title'] = $productCategoriesFromDb[0]->getTitle();

            } else if (count($productCategoriesFromDb) == 0 || $productCategoriesFromDb == null) {
                //TODO aggiungo anche quando è null così creo la nuova categoria
                //TODO ricordarsi anche di creare l'elemento nella tabella di associazione
                $this->logger->info("NOT Found Product category on db by epic key");

                if ($this->epicUtils->getEpicSynchCategoriesAutoDuplicates()) {

                    $productCategoriesFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findNotDuplicationByTitle($product_category_data['ita']);

                    if (count($productCategoriesFromDb) == 1) {
                        $this->logger->info("Found category on db by title");
                        if ($productCategoriesFromDb[0]->getEpicKey() == null) {
                            $product_category_data['type'] = self::EPIC_UPDATED_BY_TITLE;
                            $productCategoryFromDb = $productCategoriesFromDb[0];
                            $product_category_data['linked_to_db_id'] = $productCategoriesFromDb[0]->getId();
                            $product_category_data['linked_to_db_title'] = $productCategoriesFromDb[0]->getTitle();
                        } else {
                            $this->logger->info("Found category on db with same title but epicKey different -> duplication....");
                            $product_category_data['type'] = self::DUPLICATED;
                            $productCategoryFromDb = new ProductCategory();
                            // TODO: add a property to disable that setting...
                            $productCategoryFromDb->setDuplicatedOf($productCategoriesFromDb[0]);
                            $product_category_data['duplicated_of_db_id'] = $productCategoriesFromDb[0]->getId();
                            $product_category_data['duplicated_of_db_title'] = $productCategoriesFromDb[0]->getTitle();
                        }
                    } else if (count($productCategoriesFromDb) == 0) {
                        $this->logger->info("Missing  category on db");
                        $product_category_data['type'] = self::EPIC_CREATED;
                        $productCategoryFromDb = new ProductCategory();
                    } else {
                        $this->logger->info("Found more than one category with that title on db!");
                        $product_category_data['type'] = self::ERROR;
                        $productCategoryFromDb = new ProductCategory();
                        // TODO: create but not set duplicated of anything (it will be done manually)
                    }

                } else {

                    $this->logger->info("Missing  category on db");
                    $product_category_data['type'] = self::EPIC_CREATED;

                    // Se sono qua significa che la categoria non è tra quelle associate
                    // Quindi creo la nuova categoria e l'associazione
                    $productCategoryFromDb = new ProductCategory();

                    $add_assoc = true;
                    //Controllo se esiste già una categoria con Titolo identico ma senza EpicKey assegnata
                    $productCategoriesFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findBy(
                        array('title' => $product_category_data['ita'], 'epicKey' => null),
                        array()
                    );

                    //dump($productCategoriesFromDb);

                    $this->logger->info("Get category from db");
                    if (count($productCategoriesFromDb) > 0) {
                        $this->logger->info("FOUND! Found category on db by title # " . sizeof($productCategoriesFromDb));
                        $productCategoryFromDb = $productCategoriesFromDb[0];
                    } else {
                        $productCategoryFromDb = new ProductCategory();
                    }
                }


            } else {
                $this->logger->info("Found more than one category with that epicKey on db!");
                $product_category_data['type'] = self::ERROR;
            }

            // it data
            $row_it = array();
            $row_it['epicKey'] = $product_category_data['key'];
            $row_it['title'] = str_replace("'","’",$product_category_data['ita']);
            $row_it['locale'] = 'it_IT';

            // en data
            $row_en = array();
            $row_en['epicKey'] = $product_category_data['key'];
            $row_en['title'] = str_replace("'","’",$product_category_data['enu']);
            $row_en['locale'] = 'en_GB';
            
            if($Nation == "Germany"){
                
                // de data
                $row_de = array();
                $row_de['epicKey'] = $product_category_data['key'];
                $row_de['title'] = str_replace("'","’",$product_category_data['deu']);
                $row_de['locale'] = 'de_DE';
                
                // en_de data
                $row_en_de = array();
                $row_en_de = $row_en;
                $row_en_de['locale'] = 'en_DE';
            }
            
            if($Nation == "Benelux"){
                
                // en_bx data
                $row_en_bx = array();
                $row_en_bx = $row_en;
                $row_en_bx['locale'] = 'en_BX';
                
                // fr_bx data
                $row_fr_bx = array();
                $row_fr_bx['epicKey'] = $product_category_data['key'];
                $row_fr_bx['title'] = str_replace("'","’",$product_category_data['fra']);
                $row_fr_bx['locale'] = 'fr_BX';
                
                // nl_bx data
                $row_nl_bx = array();
                $row_nl_bx['epicKey'] = $product_category_data['key'];
                $row_nl_bx['title'] = str_replace("'","’",$product_category_data['nld']);
                $row_nl_bx['locale'] = 'nl_BX';
            }
            
            if($Nation == "Apac"){
                // en_apac data
                $row_en_apac = array();
                $row_en_apac = $row_en;
                $row_en_apac['locale'] = 'en_APAC';
            }
            
            if($Nation == "Iberia"){
                
                // es_es data
                $row_es_es = array();
                $row_es_es['epicKey'] = $product_category_data['key'];
                $row_es_es['title'] = str_replace("'","’",$product_category_data['esn']);
                $row_es_es['locale'] = 'es_ES';
                
                // pt_pt data
                $row_pt_pt = array();
                $row_pt_pt['epicKey'] = $product_category_data['key'];
                $row_pt_pt['title'] = str_replace("'","’",$product_category_data['ptb']);
                $row_pt_pt['locale'] = 'pt_PT';
            }

            if ($productCategoryFromDb) {
                if ($this->epicUtils->getEpicCommit()) {
                    
                    //if($Nation == "Italy"){
                        $updatedDataIt = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_it);
                        $updatedDataEn = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_en);
                    //}
                    
                    if($Nation == "Germany"){
                        $updatedDataDe = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_de);
                        $updatedDataEnDe = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_en_de);
                    }
                    
                    if($Nation == "Benelux"){
                        $updatedDataEnBX = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_en_bx);
                        $updatedDataFrBX = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_fr_bx);
                        $updatedDataNlBX = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_nl_bx);
                    }
                    
                    if($Nation == "Apac"){
                        $updatedDataEnApac = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_en_apac);
                    }
                    
                    if($Nation == "Iberia"){
                        $updatedDataEsES = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_es_es);
                        $updatedDataPtPT = $this->updateProductCategoryFieldsWithEpicData($productCategoryFromDb, $row_pt_pt);
                    }
                    
                    //$this->logger->info("Print prodotto categoria");
                    //$this->logger->info($productCategoriesFromDb);

                    //crea l'associazione
                    if ($add_assoc) {
                        $this->logger->info("Add assoc category on db");
                        $epicProductCategoriy = new EpicProductCategory();
                        $updatedDataEpicProductCategory = $this->insertEpicProductCategoryFields($epicProductCategoriy, $product_category_data['key'], $productCategoryFromDb->getId());
                    }

                }
                if ($Nation == "Italy" && !$updatedDataIt && !$updatedDataEn) {
                    $product_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Germany" && !$updatedDataDe && !$updatedDataEnDe) {
                    $product_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Benelux" && !$updatedDataEnBX && !$updatedDataFrBX && !$updatedDataNlBX) {
                    $product_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Apac" && !$updatedDataEnApac) {
                    $product_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Iberia" && !$updatedDataEsES && !$updatedDataPtPT) {
                    $product_category_data['type'] = self::EPIC_UNTOUCHED;
                }

                $product_category_data['id'] = $productCategoryFromDb->getId();
            }

            $product_category_datas_enriched[] = $product_category_data;

            if (in_array($productCategoryFromDb->getId(), array_keys($this->notEpicProductCategories))) {
                $this->notEpicProductCategories = array_diff_key($this->notEpicProductCategories, array($productCategoryFromDb->getId() => 0));
            }

        }

        foreach (array_keys($this->notEpicProductCategories) as $id) {
            $pc = array();
            $pc['id'] = $id;
            $pc['type'] = $this->notEpicProductCategories[$id];
            $product_category_datas_enriched[] = $pc;
        }

        $categoriesFile = $this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "product_categories.csv");
        $categoriesKeys = ['id', 'key', 'ptb', 'esn', 'nld', 'fra', 'deu', 'enu', 'ita', 'linked_to_db_id', 'linked_to_db_title', 'duplicated_of_db_id', 'duplicated_of_db_title', 'type'];
        $this->exportToCsv($categoriesFile, $product_category_datas_enriched, $categoriesKeys);

    }

    private function removeUntouched($product_category_datas)
    {
        $product_category_datas_to_return = array();
        foreach ($product_category_datas as $product_category_data) {
            if ($product_category_data['type'] != self::EPIC_UNTOUCHED) {
                $product_category_datas_to_return[] = $product_category_data;
            }
        }
        return $product_category_datas_to_return;
    }

    private function insertEpicProductCategoryFields($epicProductCategoriy, $Epickey, $categoryId)
    {

        $epicProductCategoriy->setProductCategoryId($categoryId);
        $epicProductCategoriy->setEpicKey($Epickey);
        $epicProductCategoriy->setPublished(1);
        $this->em->persist($epicProductCategoriy);
        if ($this->epicUtils->getEpicCommit()) {
            $this->em->flush();
        }
        return true;
        
    }

    private function updateProductCategoryFieldsWithEpicData($productCategory, $row)
    {   
        $textproductCategory = print_r($row,true);
        $this->logger->info("product category: ".$textproductCategory);
        
        $this->logger->info("Update values for product category");
        $new_title = mb_strimwidth($row['title'], 0, self::LENGTH_255, self::TRIMMARKER);
        $new_epicKey = mb_strimwidth($row['epicKey'], 0, self::LENGTH_255, self::TRIMMARKER);
        $old_title = $productCategory->getTitle();
        $old_epicKey = $productCategory->getEpicKey();
        $old_slug = $productCategory->getSlug();
        $new_slug = TextUtils::slugify($row['title']);
        $productCategoriesTranslationFromDb = null;
        //if ($row['locale'] == 'en_GB') {
        if ($row['locale'] !== 'it_IT') {
            // TODO: get en field from db
            $this->logger->info("Not italian so i need to find the correct title");

            $productCategoriesTranslationFromDb = $this->em->getRepository('AppBundle:ProductCategoryTranslation')->findOneBy(
                array('locale' => $row['locale'], 'field' => 'title', 'object' => $productCategory->getId()),
                array()
            );
            if (isset($productCategoriesTranslationFromDb)) {
                $old_title = $productCategoriesTranslationFromDb->getContent();
            } else {
                $old_title = "";
            }
            $this->logger->info("title in " . $row['locale'] . ' -> ' . $old_title);
        }

        if (strcmp($new_title, $old_title) == 0 && strcmp($new_slug, $old_slug) == 0 && strcmp($new_epicKey, $old_epicKey) == 0) {

            $this->logger->info("Update data in Italian " . $row['locale'] . " - NO change ");
            $this->logger->info("new title " . $new_title . " - old title " . $old_title);
            $productCategory->setFromEpic(true);
            $productCategory->setLocale($row['locale']);
            $this->em->persist($productCategory);
            if ($this->epicUtils->getEpicCommit()) {
                $this->em->flush();
            }
            return false;

        } else {

            if ($row['locale'] == 'it_IT') {
                $this->logger->info("Update data in Italian");
                $productCategory->setTitle($new_title);
                $productCategory->setSlug(TextUtils::slugify($row['title']));
                $productCategory->setEpicKey($new_epicKey);
                $productCategory->setFromEpic(true);
                $productCategory->setLocale($row['locale']);
                $this->em->persist($productCategory);
            } else {
                if (count($productCategoriesTranslationFromDb) > 0) {
                    $this->logger->info("Update translations");

                    $productCategoriesTranslationFromDb_update = $this->setProductTranslation($productCategory, 'title', $new_title, $row['locale']);
                    $this->logger->info('title updated :: ' . $productCategoriesTranslationFromDb_update->getContent());

                    $productCategoriesTranslationFromDb_update = $this->setProductTranslation($productCategory, 'slug', TextUtils::slugify($row['title']), $row['locale']);
                    $this->logger->info('slug updated :: ' . $productCategoriesTranslationFromDb_update->getContent());

                } else {
                    $this->logger->info("Insert translations");
                    $productCategoriesTranslationFromDb_update = $this->setProductTranslation($productCategory, 'title', $new_title, $row['locale']);
                    $this->logger->info('title updated :: ' . $productCategoriesTranslationFromDb_update->getContent());

                    $productCategoriesTranslationFromDb_update = $this->setProductTranslation($productCategory, 'slug', TextUtils::slugify($row['title']), $row['locale']);
                    $this->logger->info('slug updated :: ' . $productCategoriesTranslationFromDb_update->getContent());

                }
            }
            if ($this->epicUtils->getEpicCommit()) {
                $this->em->flush();
            }

            return true;

        }
        
    }

    private function setProductTranslation($productCategory, $field, $value, $locale)
    {
        //Inserisco o aggiorno il campo passato
        $productCategoriesTranslationFromDb = $this->em->getRepository('AppBundle:ProductCategoryTranslation')->findOneBy(
            array('locale' => $locale, 'field' => $field, 'object' => $productCategory->getId()),
            array()
        );
        if (!count($productCategoriesTranslationFromDb) > 0) {
            $productCategoriesTranslationFromDb = new ProductCategoryTranslation();
        }
        $productCategoriesTranslationFromDb->setObject($productCategory);
        $productCategoriesTranslationFromDb->setField($field);
        $productCategoriesTranslationFromDb->setLocale($locale);
        $productCategoriesTranslationFromDb->setContent($value);
        $this->em->persist($productCategoriesTranslationFromDb);

        return $productCategoriesTranslationFromDb;
    }

    public function linkExistingCategoriesWithEpic()
    {
        $this->logger->info("Reading file ");

        $file = new \SplFileObject($this->epicUtils->getEpicElaborationDir() . 'product_categories_link.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $this->logger->info("Row " . json_encode($row));

            $productCategoryFromDb = null;

            if ($row['linked_to_db_id']) {
                $productCategoryFromDb = $this->em->getRepository('AppBundle:ProductCategory')->find($row['linked_to_db_id']);
                $this->logger->info("FOUND " . $productCategoryFromDb->getTitle());

                if ($productCategoryFromDb->getEpicKey() == null) {
                    $productCategoryFromDb->setEpicKey($row['key']);
                    $this->em->persist($productCategoryFromDb);
                    if ($this->epicUtils->getEpicCommit()) {
                        $this->em->flush();
                    }
                } else if ($productCategoryFromDb->getEpicKey() != $row['key']) {
                    throw new Exception("cannot link more than one epicKey to a category");
                }

            } else {
                $productCategoryFromDb = new ProductCategory();

                $productCategoryDuplicatedOf = null;

                if ($row['duplicated_of_db_title']) {
                    $productCategoriesDuplicatedOfFromDb = $this->em->getRepository('AppBundle:ProductCategory')->findNotDuplicationByTitle($row['duplicated_of_db_title']);
                    $productCategoryDuplicatedOf = $productCategoriesDuplicatedOfFromDb[0];
                }

                $productCategoryFromDb->setTitle($row['ita']);
                $productCategoryFromDb->setSlug(TextUtils::slugify($row['ita']));
                $productCategoryFromDb->setEpicKey($row['key']);
                if ($productCategoryDuplicatedOf != null) {
                    $productCategoryFromDb->setDuplicatedOf($productCategoryDuplicatedOf);
                }
                $productCategoryFromDb->setLocale('it_IT');
                $this->em->persist($productCategoryFromDb);
                if ($this->epicUtils->getEpicCommit()) {
                    $this->em->flush();
                }

            }

        }

    }


}