<?php
namespace AppBundle\Services;
use AppBundle\Bean\ClassificationBranch;
use AppBundle\Utils\Constants;

class ProductClassificationUrlManager
{

    private $router;

    private $em;

    public function __construct($router, $em)
    {
        $this->router = $router;
        $this->em = $em;
    }

    // @Deprecated: use ClassificationBranch->getSlug. This is correct only for first levels!!!
    public function getDynamicSlugFromBranch($branch)
    {
        if (! is_array($branch)) {
            return $this->getDynamicSlugFromBranch(array(
                $branch
            ));
        } else {
            $slug = "";
            foreach ($branch as $sprig) {
                $slug = $slug . ($slug ? "/" : "") . $sprig->getSlug();
            }
            return $slug;
        }
    }
    
    public function getClassificationBranchFromDynamicSlug($dynamicSlug,$current_locale) {        
        $rootClassificationBranch = $this->calculateTree($current_locale);
        $classificationBranches = $this->getAllClassificationBranches($rootClassificationBranch);
        if (array_key_exists($dynamicSlug, $classificationBranches)) {
            return $classificationBranches[$dynamicSlug];
        }
        return null;
    }
    
    public function getAllClassificationBranches($classificationBranch) {
        $classificationBranches = array();
        $this->addDoorClassificationBranches($classificationBranch, $classificationBranches);
        //$classificationBranches = array_unique($classificationBranches);
        return $classificationBranches;
    }
    
    private function addDoorClassificationBranches($classificationBranch, &$classificationBranches) {
        if ($classificationBranch->getSlug() && $classificationBranch->getHasDedicatedPage()) {
            $classificationBranches[$classificationBranch->getSlug()] = $classificationBranch;
        }
        foreach($classificationBranch->getDoors() as $door) {
            $this->addDoorClassificationBranches($door, $classificationBranches);
        }
    }
    
    public function calculateTree($current_locale) {
        
        
        
        $rootClassificationBranch = new ClassificationBranch();
        
        $catsLev2 = $this->em
        ->getRepository('AppBundle:ProductClassificationRow')
        ->findDistinctProductCategoriesOfLev2($current_locale);
        
        foreach ($catsLev2 as $catLev2) {
            $catLev2ClassificationBranch = new ClassificationBranch();
            $catLev2ClassificationBranch->addStep($catLev2);
            $catLev2ClassificationBranch->setProductTemplate('AppBundle:frontend/product:product_lev2.html.twig');
            
            $catsLev3 = $this->em
            ->getRepository('AppBundle:ProductClassificationRow')
            ->findDistinctProductCategoriesOfLev3($current_locale,$catLev2);
        
            foreach ($catsLev3 as $catLev3) {
                
                if ($catLev3->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {

                    $catLev3ClassificationBranch = new ClassificationBranch($catLev2ClassificationBranch->getWalk());
                    $catLev3ClassificationBranch->addStep($catLev3);
                    $catLev3ClassificationBranch->setProductTemplate('AppBundle:frontend/product:product_lev3.html.twig');
                    
                    $catsLev4 = $this->em
                    ->getRepository('AppBundle:ProductClassificationRow')
                    ->findDistinctProductCategoriesOfLev4($current_locale,$catLev2, $catLev3);
                    
                    foreach ($catsLev4 as $catLev4) {
                        $catLev4ClassificationBranch = new ClassificationBranch($catLev3ClassificationBranch->getWalk());
                        $catLev4ClassificationBranch->addStep($catLev4);
                        $catLev4ClassificationBranch->setHasDedicatedPage(false);
                    
                        $catsLev5 = $this->em
                        ->getRepository('AppBundle:ProductClassificationRow')
                        ->findDistinctProductCategoriesOfLev5($current_locale,$catLev2, $catLev3, $catLev4);
                    
                        foreach ($catsLev5 as $catLev5) {
                            
                            if ($catLev5->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                                $catLev5ClassificationBranch = new ClassificationBranch($catLev4ClassificationBranch->getWalk());
                                $catLev5ClassificationBranch->addStep($catLev5);
                                $catLev5ClassificationBranch->setProductTemplate('AppBundle:frontend/product:product_lev5.html.twig');

                                $products = $this->em
                                ->getRepository('AppBundle:ProductClassificationRow')
                                ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev4, $catLev5);
                                
                                foreach ($products as $product) {
                                    $productClassificationBranch = new ClassificationBranch($catLev5ClassificationBranch->getWalk());
                                    $productClassificationBranch->addStep($product);
                                    $productClassificationBranch->setHasDedicatedPage(false);
                                    $catLev5ClassificationBranch->addDoor($productClassificationBranch);
                                    
                                }
                                
                                $catLev4ClassificationBranch->addDoor($catLev5ClassificationBranch);
                    
                            } else {
                    
                                $products = $this->em
                                ->getRepository('AppBundle:ProductClassificationRow')
                                ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev4, $catLev5);
                    
                                foreach ($products as $product) {
                                    $productClassificationBranch = new ClassificationBranch($catLev4ClassificationBranch->getWalk());
                                    $productClassificationBranch->addStep($product);
                                    $productClassificationBranch->setProductTemplate('AppBundle:frontend/product:product_lev5.html.twig');
                                    $productClassificationBranch->setIsSingleProduct(true);
                                    
                                    $catLev4ClassificationBranch->addDoor($productClassificationBranch);
                                }
                    
                            }
                    
                        }
                    
                        $catLev3ClassificationBranch->addDoor($catLev4ClassificationBranch);
                    }

                    $catLev2ClassificationBranch->addDoor($catLev3ClassificationBranch);
                    
                } else if (count($catsLev3) == 1) {
                    
                    $products = $this->em
                    ->getRepository('AppBundle:ProductClassificationRow')
                    ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev3, $catLev3);
                    
                    foreach ($products as $product) {
                        $productClassificationBranch = new ClassificationBranch($catLev2ClassificationBranch->getWalk());
                        $productClassificationBranch->addStep($product);
                        $productClassificationBranch->setHasDedicatedPage(false);
                        $catLev2ClassificationBranch->addDoor($productClassificationBranch);
                    }

                    $catLev2ClassificationBranch->setProductTemplate('AppBundle:frontend/product:product_lev5.html.twig');
                    $catLev2ClassificationBranch->setMenuStart(1);
                    $catLev2ClassificationBranch->setMenuDepth(1);
                    
                } else {
                	
                	// do nothing...  
                }

            }
        
            $rootClassificationBranch->addDoor($catLev2ClassificationBranch);
        }    
        
        return $rootClassificationBranch;
    }
    
}
