<?php

namespace AppBundle\Services;

use Gedmo\Translatable\TranslatableListener;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class UrlManager 
{
    /**
     * @var RouterInterface
     */
    protected $router;

    protected $em;

    protected $productClassificationUrlManager;
    
    protected $areaClassificationUrlManager;

    protected $locales;

    /**
     * @var TranslatableListener
     */
    protected $listener;

    public function __construct(RouterInterface $router, $em, $productClassificationUrlManager, $areaClassificationUrlManager, TranslatableListener $listener, $locales, $epicUtils)
    {
        $this->router = $router;
        $this->em = $em;
        $this->productClassificationUrlManager = $productClassificationUrlManager;
        $this->areaClassificationUrlManager = $areaClassificationUrlManager;
        $this->listener = $listener;
        // TODO: uncomment when enable all locales
        $this->locales = $locales;
        //$this->locales = array("it_IT");
        $this->epicUtils = $epicUtils;
    }

    public function calculateUrlFromPage($page, $absolute = false) {
        $url = "#";
        if ($page->getRouteId() == 'page_show') {
            $url = $this->router->generate('page_show', array('slug' => $page->getSlug()), $absolute ? UrlGeneratorInterface::ABSOLUTE_URL : UrlGeneratorInterface::ABSOLUTE_PATH);
        } else {
            if ($page->getRouteParams()) {
                // parameterized uri... not render the url... or get a parameter from menu ...
            } else {
                $url = $this->router->generate($page->getRouteId(), array());
            }
        }

        return $url;
    }


    public function calculateUrlFromRouteIdAndSlug($routeId, $slug) {
        return $this->router->generate($routeId, array('slug' => $slug));
    }

    public function calculateAllSiteUrls($base_url) {
        $urls = array();
        $context = $this->router->getContext();
        $hostOriginal = $context->getHost();
        $context->setHost($base_url);

        foreach($this->locales as $locale) {
            $context->setParameter('_locale', $locale);

            $this->listener->setTranslatableLocale($locale);

            // homepage
            $urls[] = $this->router->generate('homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            
            //posts
            $urls[] = $this->router->generate('post_list', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            
            $posts = $this->em->getRepository('AppBundle:Post')->findAll();
            foreach($posts as $post) {
                if ( (strpos($post->getLanguage(), $locale) !== false) &&  $post->getPublished()) {
                    $urls[] = $this->router->generate('post_detail', array('slug' => $post->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
                }
            }
            
            //contacts
            $urls[] = $this->router->generate('contact_list', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            
            $contacts = $this->em->getRepository('AppBundle:ContactGroup')->findAll();
            foreach($contacts as $contact) {
                if ( (strpos($contact->getLanguage(), $locale) !== false) && $contact->getPublished()) {
                    $urls[] = $this->router->generate('contact_detail', array('slug' => $contact->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
                }
            }

            //products
            $urls[] = $this->router->generate('product_list', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            
            //prende la lingua in uso, se non funziona provare con $locale
            $current_locale =  $locale;//$this->get('request')->getLocale();
            
            $rootClassificationBranch = $this->productClassificationUrlManager->calculateTree($current_locale);
            $classificationBranches = $this->productClassificationUrlManager->getAllClassificationBranches($rootClassificationBranch);
            foreach($classificationBranches as $classificationBranch) {
                $urls[] = $this->router->generate('product_detail', array('slug' => $classificationBranch->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
            }
            
            // areas
            $urls[] = $this->router->generate('area_list', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            
            $rootClassificationBranch = $this->areaClassificationUrlManager->calculateTree($current_locale);
            $classificationBranches = $this->areaClassificationUrlManager->getAllClassificationBranches($rootClassificationBranch);
            foreach($classificationBranches as $classificationBranch) {
                $urls[] = $this->router->generate('area_detail', array('slug' => $classificationBranch->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
            }

            //generic pages
            $pages = $this->em->getRepository('AppBundle:Page')->findBy(
                array('routeId'=> 'page_show')
                );
            foreach ($pages as $page) {
                if ($page->getPublished()) {
                    $urls[] = $this->calculateUrlFromPage($page, true);                    
                }
            }

            //business solutions
            $urls[] = $this->router->generate('businesssolution_list', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            $businesssolutions = $this->em->getRepository('AppBundle:BusinessSolution')->findAll();
            foreach($businesssolutions as $businesssolution) {
                if ( (strpos($businesssolution->getLanguage(), $locale) !== false) && $businesssolution->getPublished()) {
                    $urls[] = $this->router->generate('businesssolution_detail', array('slug' => $businesssolution->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
                }
            }
            
            $this->em->clear();
        }
        
        $context->setHost($hostOriginal);
        
        return $urls;
    }
    
    
    public function getRoutesByRouteId($routeId, $current_locale) {
        
        $link = array();
        $context = $this->router->getContext();
        //$context->setHost($base_url);
        
        foreach($this->locales as $locale) {
            $context->setParameter('_locale', $locale);
            
            $this->listener->setTranslatableLocale($locale);
            $link[$locale] = $this->router->generate($routeId, array(), UrlGeneratorInterface::ABSOLUTE_URL);
            if(!strpos($link[$locale], 'https') !== false){
                $link[$locale] = str_replace("http","https",$link[$locale]);
            }
        }
        
        
        //ripristino la lingua in uso
        $context->setParameter('_locale', $current_locale);        
        $this->listener->setTranslatableLocale($current_locale);
        
        
        return $link;
    }
    
    
    public function getRouteByRouteId($routeId, $current_locale) {
        
        $link = "";
        $link = $this->router->generate($routeId, array(), UrlGeneratorInterface::ABSOLUTE_URL);
        if(!strpos($link, 'https') !== false){
            $link = str_replace("http","https",$link);
        }
        
        return $link;
    }
    
    
    public function getHost() {
        return $this->router->getContext()->getHost();
    }
    
    
    public function sitemapGenerator($path="") {
        
        $host = "https://".$this->epicUtils->getHostStatic();
        $prefixPath = $this->epicUtils->getWebPath()."/";
        
        $siteUrls = $this->calculateAllSiteUrls("");
        
        $slugProducts = $this->em->getRepository('AppBundle:Product')->findAllSlugProducts();
        //$slugProductsCategory = $this->em->getRepository('AppBundle:ProductCategory')->findAllSlugProductCategory();
        //$slugAreaCategory = $this->em->getRepository('AppBundle:AreaCategory')->findAllSlugAreaCategory();
        
        
        foreach($this->locales as $locale) {
            $context = $this->router->getContext();
            $context->setParameter('_locale', $locale);            
            $this->listener->setTranslatableLocale($locale);
            
            $linkAreaCategory = $this->getRouteByRouteId("area_list",$locale);
            $linkAreaCategory = str_replace("https://localhost","",$linkAreaCategory);
            $urlAreaCategoryArray = explode("/",$linkAreaCategory);
            $linkProductCategory = $this->getRouteByRouteId("product_list",$locale);
            $linkProductCategory = str_replace("https://localhost","",$linkProductCategory);
            $urlProductCategoryArray = explode("/",$linkProductCategory);
            
            $content = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            
            $checkDuplicateUrlArray = array();
            
            foreach($siteUrls as $url) {
                
                $linkReference = "";
                $urlOriginal = $url;
                $urlArray = explode("/",$url);
                
                if(count($urlArray) > 2  && (strpos($url, $locale) !== false)){
                    
                    $changefreq = "daily";
                    
                    $priority = 1.0;
                    $priority = $priority-((count($urlArray)-2)/10);
                    
                    //homepage
                    if($urlArray[2] == ""){ $priority = 1.0; $changefreq = "weekly"; }
                                        
                    $lastPartUrl = end($urlArray);
                    if($lastPartUrl != ""){
                        //controllo se prodotto, product/area category 
                        if( ($urlArray[2] == $urlAreaCategoryArray[2])  || ($urlArray[2] ==  $urlProductCategoryArray[2]) ){
                            //controllo se prodotto
                            if(in_array($lastPartUrl, $slugProducts)){
                                $linkReference = $this->em->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByProductAndLanguages($lastPartUrl,$locale);
                                if($linkReference == ""){
                                    $linkReference = $this->em->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByCategoryAndLanguages($lastPartUrl,$locale);
                                    if($linkReference != ""){
                                        $priority = max(0.8,$priority);
                                    }
                                }
                                else{
                                    $priority = 0.7;
                                }
                                if($linkReference != ""){
                                    $url = $linkProductCategory."/".$linkReference;
                                }
                                
                            }
                            else{
                                // product/area category
                                $linkReference = $this->em->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByCategoryAndLanguages($lastPartUrl,$locale);
                                if($linkReference == ""){
                                    $linkReference = $this->em->getRepository('AppBundle:ProductClassificationRow')->findFirstProductClassificationRowByProductAndLanguages($lastPartUrl,$locale);
                                    if($linkReference != ""){
                                        $priority = 0.7;
                                    }
                                }
                                else{
                                    $priority = max(0.8,$priority);
                                }
                                if($linkReference != ""){
                                    $url = $linkProductCategory."/".$linkReference;
                                }
                            }
                            
                            //se linkReference non è product category o product, riprendo url iniziale
                            if($linkReference == ""){
                                $url = $urlOriginal;
                                $priority = max(0.8,$priority);
                            }
                            
                            //controllo se ho già inserito lo stesso link canonico
                            if(!in_array($lastPartUrl,$checkDuplicateUrlArray)){
                                $checkDuplicateUrlArray[] = $lastPartUrl;
                            }
                            else{
                                $url = "";
                            }
                            
                        }
                        else{
                            //pagine di servizio es. news,contatti, chi siamo
                            $priority = 0.5;
                        }
                    }
                    
                    
                    if($url != "" /*&& (strpos(get_headers($host.$url)[0],'404') === false)*/ ){
                        $url = $host.$url;
                        $content .= '
    <url>
        <loc>'.$url.'</loc>
        <lastmod>'.date("Y-m-d").'</lastmod>
        <changefreq>'.$changefreq.'</changefreq>
        <priority>'.$priority.'</priority>
    </url>';
                    }
                }
            
            }
            
            $content .= '
</urlset>';
            
            //creazione file sitemap per ogni lingua
            file_put_contents($prefixPath.$path."sitemap_".$locale.".xml",$content);
            file_put_contents($prefixPath.$path."sitemap_".strtolower($locale).".xml",$content);
            
            //scrittura link sitemap nel file robots
            if( !(strpos(file_get_contents($prefixPath.$path."robots.txt"),"sitemap_".$locale) !== false) ) {
                file_put_contents($prefixPath.$path."robots.txt","\n"."Sitemap:".$host."/sitemap_".$locale.".xml",FILE_APPEND);
            }
            
            $this->em->clear();
        }
    }
 

    
}
