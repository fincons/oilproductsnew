<?php

namespace AppBundle\Services;

use AppBundle\Entity\EpicArchive;
use DOMDocument;
use Exception;
use XMLReader;
use ZipArchive;

class EpicDataSynchronizer
{ 

    private $processResult;

    public function __construct($em, $logger, $epicUtils, $epicProductSynchronizer, $epicProductClassificationSynchronizer, $epicAreaClassificationSynchronizer)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
        $this->epicProductSynchronizer = $epicProductSynchronizer;
        $this->epicProductClassificationSynchronizer = $epicProductClassificationSynchronizer;
        $this->epicAreaClassificationSynchronizer = $epicAreaClassificationSynchronizer;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
        $this->epicProductSynchronizer->setProcessResult($processResult);
        $this->epicProductClassificationSynchronizer->setProcessResult($processResult);
        $this->epicAreaClassificationSynchronizer->setProcessResult($processResult);
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function synch($processResult,$output)
    {

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Synch Epic data");
        
        //TODO toglie il limite di memoria
        ini_set('memory_limit', '-1');
        
        $epicArchivesToElaborate = array();
        if ($this->epicUtils->getEpicElaborateCurrent()) {
            $current = new EpicArchive();
            $current->setArchiveName("current");
            $current->setDownloadedAt(new \DateTime("now"));
            $epicArchivesToElaborate[] = $current;
        } else {
            $epicArchivesToElaborate = $this->em->getRepository('AppBundle:EpicArchive')->findEpicArchivesToElaborate(
                $this->epicUtils->getEpicMaxNumOfArchivesToElaborate(),
                $this->epicUtils->getEpicElaborateProducts(),
                $this->epicUtils->getEpicElaborateProductClassification(),
                $this->epicUtils->getEpicElaborateAreaClassification()
            );

            if (!$epicArchivesToElaborate) {
                $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "No new files to elaborate");
            }
        }

        foreach ($epicArchivesToElaborate as $epicArchiveToElaborate) {
            gc_collect_cycles();
            
            $output->writeln("Elaborate Archive: ".$epicArchiveToElaborate->getArchiveName());
            
            $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Synch archive " . $epicArchiveToElaborate);

            $this->epicProductsCode = [];
            $this->notEpicProductsCode = [];

            $is_to_elaborate_products = false;
            $is_to_elaborate_product_classification = false;
            $is_to_elaborate_area_classification = false;
            

            $this->checkArchiveToElaborate($epicArchiveToElaborate, $is_to_elaborate_products, $is_to_elaborate_product_classification, $is_to_elaborate_area_classification);

            $this->unzipArchive($epicArchiveToElaborate);
            
            $arrayNations = array();
            $arrayKeys = array();
            
            $this->findLanguageInCatalog($epicArchiveToElaborate, $arrayNations, $arrayKeys);
            
            
            foreach ($arrayNations as $Nation) {
                    
                    $output->writeln("Elaborate Catalog: ".$Nation);
                    $output->writeln("Memory usage start: ".round(memory_get_usage()/1048576.0) . " Mb");
                
                    $product_datas = null;
                    $product_category_datas = null;
                    $area_category_datas = null;
                    $product_classification_row_datas = null;
                    $area_classification_row_datas = null;
                    
                    $product_datas = array();
                    $product_category_datas = array();
                    $area_category_datas = array();
                    $product_classification_row_datas = array();
                    $area_classification_row_datas = array();
                    
                    //$arrayVariable = print_r(get_defined_vars(),true);
                    //$output->writeln("arrayVariable: ".$arrayVariable);
                    
                    //leggo ed elaboro l'alberatura da catalog.xml
                    $this->readCatalog($epicArchiveToElaborate, $product_datas, $product_category_datas, $product_classification_row_datas, $area_category_datas, $area_classification_row_datas, $Nation, $arrayNations, $arrayKeys);
                    $output->writeln("Memory usage after read catalog: ".round(memory_get_usage()/1048576.0) . " Mb");
                    
                    //In base ai parametri passati al lancio del comando faccio partire le elaborazioni dedicate
                    if ($is_to_elaborate_products && $this->epicUtils->getEpicElaborateProducts()) {
                        $this->epicProductSynchronizer->elaborateProducts($epicArchiveToElaborate, $product_datas, $Nation);
                        $output->writeln("Memory usage after ProductSynchronizer: ".round(memory_get_usage()/1048576.0) . " Mb");
                    } else {
                        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "No elaborate product to do");
                    }
                    
                    if ($is_to_elaborate_product_classification && $this->epicUtils->getEpicElaborateProductClassification()) {
                        $this->epicProductClassificationSynchronizer->elaborateProductCategoriesAndClassificationRows($epicArchiveToElaborate, $product_category_datas, $product_classification_row_datas, $Nation);
                        $output->writeln("Memory usage after epicProductClassificationSynchronizer: ".round(memory_get_usage()/1048576.0) . " Mb");
                    } else {
                        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "No elaborate product classification to do");
                    }
                    
                    if ($is_to_elaborate_area_classification && $this->epicUtils->getEpicElaborateAreaClassification()) {
                        // TODO: implement in epicAreaClassificationSynchronizer
                        //dump("sto per lanciare AreaClassificationSync");
                        $this->epicAreaClassificationSynchronizer->elaborateAreaCategoriesAndClassificationRows($epicArchiveToElaborate, $area_category_datas, $area_classification_row_datas, $Nation);
                        $output->writeln("Memory usage after epicAreaClassificationSynchronizer: ".round(memory_get_usage()/1048576.0) . " Mb");
                    } else {
                        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "No elaborate area classification to do");
                    }
                    
            }
                     
        }
        
    }

    private function checkArchiveToElaborate($epicArchiveToElaborate, &$is_to_elaborate_products, &$is_to_elaborate_product_classification, &$is_to_elaborate_area_classification)
    {

        if ($epicArchiveToElaborate->getProductElaborationAt() == null) {
            $is_to_elaborate_products = true;
        }
        if ($epicArchiveToElaborate->getProductClassificationElaborationAt() == null) {
            $is_to_elaborate_product_classification = true;
        }
        if ($epicArchiveToElaborate->getAreaClassificationElaborationAt() == null) {
            $is_to_elaborate_area_classification = true;
        }

        if ($is_to_elaborate_products) {
            $this->checkArchiveToElaborateByType($epicArchiveToElaborate, 'product');
        }

        if ($is_to_elaborate_product_classification) {
            $this->checkArchiveToElaborateByType($epicArchiveToElaborate, 'productClassification');
        }

        if ($is_to_elaborate_area_classification) {
            $this->checkArchiveToElaborateByType($epicArchiveToElaborate, 'areaClassification');
        }

    }

    private function checkArchiveToElaborateByType($epicArchiveToElaborate, $type)
    {
        if ($epicArchiveToElaborate->getArchiveName() == "current") {
            return;
        }
        $lastElaborationByType = $this->em->getRepository('AppBundle:EpicArchive')->findLastEpicArchiveElaboratedByType($type);
        if ($lastElaborationByType) {
            $this->logger->info("Last $type elaboration was " . $lastElaborationByType->getArchiveName());
            if ($this->epicUtils->getEpicOlderFileCheck()) {
                if ($this->epicUtils->extractNumberFromArchiveName($lastElaborationByType->getArchiveName()) > $this->epicUtils->extractNumberFromArchiveName($epicArchiveToElaborate->getArchiveName())) {
                    throw new Exception("Trying to $type elaborate a file older (" . $epicArchiveToElaborate->getArchiveName() . ") than last epic $type elaboration (" . $lastElaborationByType->getArchiveName() . ")");
                }
            }
            if ($this->epicUtils->getEpicSequentialFileCheck()) {
                if ($this->epicUtils->extractNumberFromArchiveName($lastElaborationByType->getArchiveName()) + 1 != $this->epicUtils->extractNumberFromArchiveName($epicArchiveToElaborate->getArchiveName())) {
                    throw new Exception("Trying to $type elaborate a not sequential file (" . $epicArchiveToElaborate->getArchiveName() . ") respect the last epic $type elaboration (" . $lastElaborationByType->getArchiveName() . ")");
                }
            }
        }
    }

    private function unzipArchive($epicArchiveToElaborate)
    {

        if ($epicArchiveToElaborate->getArchiveName() == "current") {
            return;
        }

        $archiveZipPath = $this->epicUtils->getEpicDownloadDir() . $epicArchiveToElaborate->getArchiveName();
        $archiveDestinationPath = $this->epicUtils->getEpicElaborationDirOfArchive($epicArchiveToElaborate);
        $zip = new ZipArchive;
        $opened = $zip->open($archiveZipPath);
        if ($opened === TRUE) {
            $zip->extractTo($archiveDestinationPath);
            $zip->close();
        } else {
            throw new Exception("Problems unzipping archive " . $archiveZipPath . " [ERROR] - Possible Error Message: " . $this->epicUtils->getZipError($opened));
        }
    }
    
    private function findLanguageInCatalog($epicArchiveToElaborate, &$arrayNations, &$arrayKeys)
    {
        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "find language in catalog");
        
        $dirPath = $this->epicUtils->getEpicElaborationDirOfArchive($epicArchiveToElaborate);
        
        $catalogFilePath = $dirPath . "catalog.xml";
        $reader = new XMLReader();
       
        
        $reader->open($catalogFilePath);
        while ($reader->read()) {
            switch ($reader->nodeType) {
                case (XMLREADER::ELEMENT):
                    
                    if ($reader->name == 'Root') {
                        
                        $doc = new DOMDocument('1.0', 'UTF-8');
                        $xml = simplexml_import_dom($doc->importNode($reader->expand(), true));
                        $keyNation = (string) $xml->attributes()['key'];
                        
                        if(strpos($keyNation, 'Germany') !== false){
                            array_push($arrayKeys,$keyNation);
                            if(!in_array('Germany', $arrayNations)){
                                array_push($arrayNations,'Germany');
                            }
                        }
                        if(strpos($keyNation, 'Benelux') !== false){
                            array_push($arrayKeys,$keyNation);
                            if(!in_array('Benelux', $arrayNations)){
                                array_push($arrayNations,'Benelux');
                            }
                        }
                        if(strpos($keyNation, 'Apac') !== false){
                            array_push($arrayKeys,$keyNation);
                            if(!in_array('Apac', $arrayNations)){
                                array_push($arrayNations,'Apac');
                            }
                        }
                        if(strpos($keyNation, 'Iberia') !== false){
                            array_push($arrayKeys,$keyNation);
                            if(!in_array('Iberia', $arrayNations)){
                                array_push($arrayNations,'Iberia');
                            }
                        }
                        if($keyNation == 'Product Catalog' || $keyNation == 'Sector Catalog'){
                            array_push($arrayKeys,$keyNation);
                            if(!in_array('Italy', $arrayNations)){
                                array_push($arrayNations,'Italy');
                            }
                        }
                    }
            }
        }
        $reader->close();
        
        
    }
    

    /**
     * Metodo che legge il file Catalog.xml che corrisponde all'alberatura del sito secondo Epic
     *
     * @param $epicArchiveToElaborate
     * @param $product_datas
     * @param $product_category_datas
     * @param $product_classification_row_datas
     * @param $area_category_datas
     * @param $area_classification_row_datas
     */
    private function readCatalog($epicArchiveToElaborate, &$product_datas, &$product_category_datas, &$product_classification_row_datas, &$area_category_datas, &$area_classification_row_datas, &$Nation, &$arrayNations, &$arrayKeys)
    {

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Reading catalog file " . $epicArchiveToElaborate->getArchiveName());

        $dirPath = $this->epicUtils->getEpicElaborationDirOfArchive($epicArchiveToElaborate);

        $catalogFilePath = $dirPath . "catalog.xml";
        $reader = new XMLReader();
        
        $posKey = array_search($Nation, $arrayNations)*2;
        $openScan = false;
        // read product and category datas

        $reader->open($catalogFilePath);
        $i = 0;
        $catType = 'product';
        while ($reader->read()) {
            switch ($reader->nodeType) {
                case (XMLREADER::ELEMENT):
                    
                    if ($reader->name == 'Root') {
                        
                        $doc = new DOMDocument('1.0', 'UTF-8');
                        $xml = simplexml_import_dom($doc->importNode($reader->expand(), true));
                        if(in_array($xml->attributes()['key'], array_slice($arrayKeys,$posKey,2))){
                            $openScan = true;
                        }
                        else{
                            $openScan = false;
                        }
                    }
                if($openScan){
                    
                    if ($reader->name == "Product") {

                        $doc = new DOMDocument('1.0', 'UTF-8');
                        $xml = simplexml_import_dom($doc->importNode($reader->expand(), true));

                        $product_datas[".$xml->Code."] = $xml;

                        $i++;

                    } else if ($reader->name == 'Root') {

                        $doc = new DOMDocument('1.0', 'UTF-8');
                        $xml = simplexml_import_dom($doc->importNode($reader->expand(), true));

                        if (strpos($xml->attributes()['key'], 'Sector') !== false || strpos($xml->attributes()['key'], 'Area') !== false) {
                            $catType = 'area';
                        }

                    } else if (substr($reader->name, 0, strlen('Category')) === 'Category') {

                        $doc = new DOMDocument('1.0', 'UTF-8');
                        $xml = simplexml_import_dom($doc->importNode($reader->expand(), true));

                        $category_data = array();
                        foreach ($xml->attributes() as $attribute_key => $attribute_value) {
                            $attribute_value = preg_replace("/\r |\n /", "", $attribute_value);
                            $category_data[$attribute_key] = strval($attribute_value);
                        }

                        if ($catType == 'product') {
                            $product_category_datas[$category_data['key']] = $category_data;
                        } else if ($catType == 'area') {
                            $area_category_datas[$category_data['key']] = $category_data;
                        }

                    }
                }
            }
        }

        $reader->close();

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Found $i products (with duplicates). Distinct " . count($product_datas));

        // read classification datas (TODO: do all in a single read)

        $xml = simplexml_load_file($catalogFilePath);

        // xml as array
        $parent = array();
        $parent['name'] = 'Start';
        $xmlAsArray = $this->xml2array($xml, $parent);

        // extract rows from tree
        $root = array();
        $root[] = $xmlAsArray;
        $rows = array();
        $this->extractRowsFromTree($root, $rows, $Nation, $arrayNations, $arrayKeys);

        // print rows (TODO: remove)
        //$this->printRows($rows);

        // extract pcrs and acrs from tree
        $pcrs = array();
        $acrs = array();
        $this->extractPcrsAndAcrsFromRows($epicArchiveToElaborate, $rows, $pcrs, $acrs);
        $product_classification_row_datas = $pcrs;
        $area_classification_row_datas = $acrs;
    }


    private function xml2array($xmlObject, $parent)
    {
        $out = array();
        $out['parent'] = $parent;
        $out['name'] = $xmlObject->getName();
        $out['value'] = $xmlObject->__toString();
        $out['@attributes'] = array();
        foreach ($xmlObject->attributes() as $attr => $val) {
            $out['@attributes'][$attr] = (string)$val;
        }
        foreach ($xmlObject as $node) {
            $out['childs'][] = $this->xml2array($node, $out);
        }
        return $out;
    }


    private function extractRowsFromTree($array, &$rows, $Nation, $arrayNations, $arrayKeys, $openScan = false)
    {
        
        $posKey = array_search($Nation, $arrayNations) *2;
        
        if (count($array)) {
            foreach ($array as $vals) {

                if (isset($vals) && array_key_exists('name', $vals)) {
                    
                    if ($vals['name'] == 'Root'  && array_key_exists('key', $vals['@attributes'])) {
                        
                        //print($vals['@attributes']['key']."<br>");
                        if(in_array($vals['@attributes']['key'], array_slice($arrayKeys,$posKey,2))){
                            $openScan = true;
                        }
                        else{
                            $openScan = false;
                        }
                    }
                    
                    if($openScan){
                        
                        if ($vals['name'] == 'Product') {
                            $row = array();
                            if(isset($vals['childs'][0]['value'])){
                                $row['Product'] = $vals['childs'][0]['value'];
                            }
                            else{
                                $row['Product'] = "";
                            }
                            
                            if(isset($vals['childs'][2]['value'])){
                                $row['Product_Title'] = $vals['childs'][2]['value'];
                            }
                            else{
                                $row['Product_Title'] = "";
                            }
                            
                            if(isset($vals['childs'][4]['value'])){
                                $row['Product_Position'] = $vals['childs'][4]['value'];
                            }
                            else{
                                $row['Product_Position'] = "";
                            }
                            
                            $level = 1;
                            $this->registerLevelsAndType($vals, $row, $level);
                            $rows[] = $row;
                        }
                    }
                }

                if (isset($vals) && (array_key_exists('childs', $vals) && count($vals['childs']))) {
                    $this->extractRowsFromTree($vals['childs'], $rows, $Nation, $arrayNations, $arrayKeys, $openScan);
                }

            }
        }
    }

    private function registerLevelsAndType($vals, &$row, $level)
    {

        if (array_key_exists('parent', $vals)) {

            if (array_key_exists('name', $vals['parent']) && $vals['parent']['name'] == 'Root') {

                if (array_key_exists('@attributes', $vals['parent'])) {
                    if (strpos($vals['parent']['@attributes']['key'], 'Product') !== false) {
                        $row['type'] = 'pcr';
                    } else if (strpos($vals['parent']['@attributes']['key'], 'Sector') !== false || strpos($vals['parent']['@attributes']['key'], 'Area') !== false) {
                        $row['type'] = 'acr';
                    } else {
                        $row['type'] = 'none';
                    }
                }


            } else {
                $row[$level] = $this->printCategory($vals);
                $row['level'] = $level;
                $this->registerLevelsAndType($vals['parent'], $row, $level + 1);
            }


        }

    }

    private function printCategory($vals)
    {
        $string = "";
        if (array_key_exists('@attributes', $vals['parent'])) {
            $string = $vals['parent']['@attributes']['key'];
            $string = preg_replace("/\r |\n /", "", $string);
        }
        return $string;
    }


    private function printRows($rows)
    {
        foreach ($rows as $row) {
            $this->logger->info('ROW ' . json_encode($row));
        }
    }

    /**
     * Metodo per suddividere correttamente in array separati le:
     *  - Product Classification Row => pcrs
     *  - Area Classification Row    => acrs
     *
     * @param $epicArchiveToElaborate
     * @param $rows
     * @param $pcrs
     * @param $acrs
     * @throws Exception
     */
    private function extractPcrsAndAcrsFromRows($epicArchiveToElaborate, $rows, &$pcrs, &$acrs)
    {
        foreach ($rows as $row) {

            $cr = array();
            $cr['Product'] = $row['Product'];
            $cr['Product_Title'] = $row['Product_Title'];
            $cr['Product_Position'] = $row['Product_Position'];

            if ($row['type'] == 'pcr') {

                $pointer = $row['level'];
                for ($i = 1; $i <= 4; $i++) {

                    if ($pointer >= 1) {
                        $cr['ProductCategoryLev' . ($i + 1) . "_epicKey"] = $row[strval($pointer)];
                    } else {
                        $cr['ProductCategoryLev' . ($i + 1) . "_epicKey"] = null;
                    }

                    $pointer--;

                }

                $pcrs[] = $cr;


            } else if ($row['type'] == 'acr') {

                $pointer = $row['level'];

                for ($i = 1; $i <= 5; $i++) {

                    if ($pointer >= 1) {
                        $cr['AreaLev' . ($i + 1) . "_epicKey"] = $row[strval($pointer)];
                    } else {
                        $cr['AreaLev' . ($i + 1) . "_epicKey"] = null;
                    }

                    $pointer--;

                }

                $acrs[] = $cr;

            } else {
                throw new Exception ("There is row not acr and not pcr!");
            }
        }


    }


}