<?php
namespace AppBundle\Services;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Utils\Constants;
use AppBundle\Bean\ClassificationBranch;
use AppBundle\Bean\AreaClassificationBranch;

class BreadcrumbManager
{

    private $em;
    private $urlManager;
    
    private $items;
    
    public function __construct($em, $urlManager)
    {
        $this->em = $em;
        $this->urlManager = $urlManager;
        $this->items = new ArrayCollection();
    }
    
    public function addHome() {
       $this->addPageByRouteId('homepage'); 
       return $this;
    }
    
    public function addPage($page) {
        $url = $this->urlManager->calculateUrlFromPage($page);
        $this->addItem(array('title' => $page->getTitle(), 'url' => $url));
        return $this;
    }
    
    public function addPageByRouteId($routeId) {
        $page = $this->em->getRepository('AppBundle:Page')->findOneByRouteId($routeId);
        $url = $this->urlManager->calculateUrlFromPage($page);
        $this->addItem(array('title' => $page->getTitle(), 'url' => $url));
        return $this;
    }
    
    public function addClassificationBranch($classificationBranch) {
        $walk = array();
        foreach ($classificationBranch->getWalk() as $step) {
            $walk[] = $step;
            $classificationBranch = new ClassificationBranch($walk);
            if ($classificationBranch->getHasDedicatedPage()) {
                $title = $step->getTitle();
                $url = $this->urlManager->calculateUrlFromRouteIdAndSlug('product_detail', $classificationBranch->getSlug());
                $this->addItem(array('title' => $title, 'url' => $url));                
            }
        }
        return $this;
    }
    
    public function addAreaClassificationBranch($classificationBranch) {
        $walk = array();
        foreach ($classificationBranch->getWalk() as $step) {
            $walk[] = $step;
            $classificationBranch = new AreaClassificationBranch($walk);
            if ($classificationBranch->getHasDedicatedPage() && $classificationBranch->getLastStep()->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                $title = $step->getTitle();
                $url = $this->urlManager->calculateUrlFromRouteIdAndSlug('area_detail', $classificationBranch->getSlug());
                $this->addItem(array('title' => $title, 'url' => $url));
            }
        }
        return $this;
    }
    
    public function getItems()
    {
    	return $this->items;
    }
    
    public function addItem($item)
    {
    	$this->items->add($item);
    	return $this;
    }
	
}
