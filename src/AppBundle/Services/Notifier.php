<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Notifier
{

    public function __construct($logger, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->container = $container;
    }


    public function notify($to, $subject, $body, $attachments, $contentType)
    {
        $logger = $this->container->get('monolog.logger.enioil_admin');

        $env = $this->container->getParameter('kernel.environment');

        $logger->info("Send " . $subject . " mail to " . json_encode($to));

        $message = \Swift_Message::newInstance()
            ->setSubject('[ENIOIL][' . $env . ']' . $subject)
            ->setFrom($this->container->getParameter('oilproducts_from_email'))
            ->setTo($to)
            ->setBody($body, $contentType);

        foreach ($attachments as $file) {
            $message->attach(\Swift_Attachment::fromPath($file));
        }

        $logger->info("Message:\n" . $message->getBody());
        $logger->info("Attachments: " . json_encode($attachments));
        $this->container->get('mailer')->send($message);
    }


}