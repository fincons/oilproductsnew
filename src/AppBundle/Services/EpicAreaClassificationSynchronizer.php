<?php

namespace AppBundle\Services;

class EpicAreaClassificationSynchronizer
{

    private $processResult;

    public function __construct($em, $logger, $epicUtils, $epicAreaCategorySynchronizer, $epicACRSynchronizer)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
        $this->epicAreaCategorySynchronizer = $epicAreaCategorySynchronizer;
        $this->epicACRSynchronizer = $epicACRSynchronizer;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
        $this->epicAreaCategorySynchronizer->setProcessResult($processResult);
        $this->epicACRSynchronizer->setProcessResult($processResult);
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function elaborateAreaCategoriesAndClassificationRows($epicArchiveToElaborate, $area_categories_datas, $area_classification_row_datas, $Nation)
    {
        //dump("test area classification rows");
        //dump($area_classification_row_datas);
        //exit();
        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Elaborate area categories for archive " . $epicArchiveToElaborate->getArchiveName());

        $this->epicAreaCategorySynchronizer->synchAreaCategories($epicArchiveToElaborate, $area_categories_datas, $Nation);
        $this->epicACRSynchronizer->synchAreaClassificationRows($epicArchiveToElaborate, $area_classification_row_datas, $Nation);
        $this->updateEpicArchiveTable($epicArchiveToElaborate);
    }

    private function updateEpicArchiveTable($epicArchiveToElaborate)
    {
        if ($epicArchiveToElaborate->getArchiveName() == "current") {
            return;
        }
        if (!$this->epicUtils->getEpicCommit()) {
            return;
        }
        $this->logger->info("Update EpicArchive row for file " . $epicArchiveToElaborate->getArchiveName());
        $epicArchiveToElaborate->setAreaClassificationElaborationAt(new \DateTime("now"));
        $this->em->persist($epicArchiveToElaborate);
        $this->em->flush();
        $this->epicUtils->logAbstractAndAppendToProcessResult($this->processResult, $this->logger, "SYNCHRONIZED AREA CLASSIFICATION FOR " . $epicArchiveToElaborate->getArchiveName());
    }

}