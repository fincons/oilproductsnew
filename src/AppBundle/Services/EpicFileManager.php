<?php

namespace AppBundle\Services;
use AppBundle\Entity\EpicArchive;
use Exception;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;


class EpicFileManager 
{

	private $processResult;
	
    public function __construct($em, $logger, $epicUtils)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->output = "";
        $this->epicUtils = $epicUtils;
    }

    public function setProcessResult($processResult) {
    	$this->processResult = $processResult;
    }
    
    public function getProcessResult() {
    	return $this->processResult;
    }    
    
    public function downloadFiles() {
    	if ($this->epicUtils->getEpicDownloadFromRemote()) {
    		$this->hardyDownloadFilesFromEpic();
    	} 
    	$this->scanDownloadedDir();
    }
    
    // NB: with this implementation I can put in downloaded dir also files not directly downloaded from epic... they will be registered and then elaborated...
    public function scanDownloadedDir() {
    	
    	$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Scan Epic downloaded dir");
    	
    	foreach(glob($this->epicUtils->getEpicDownloadDir().'/*.*') as $file) {
    		
    		$filename = basename($file);

    		if (substr($filename, 0, strlen('products_')) === 'products_') {
    			
    			$epicArchive = $this->em->getRepository('AppBundle:EpicArchive')->findBy(
    					array('archiveName'=> $filename),
    					array());
    			 
    			if ($epicArchive == null) {
    				$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Register EpicArchive record for file $filename");
    				$epicArchive = new EpicArchive();
    				$epicArchive->setArchiveName($filename);
    				$downloadedAt = new \DateTime();
    				$downloadedAt->setTimestamp(filemtime($file));
    				$epicArchive->setDownloadedAt($downloadedAt);
    				$this->em->persist($epicArchive);
    				$this->em->flush();
    			}
    			
    		} else {
    			
    			$this->epicUtils->logWarningAndAppendToProcessResult($this->processResult, $this->logger, "Found file $filename that doesn't start with products_");

    		}

    	}

	}

	public function hardyDownloadFilesFromEpic() {
		$secondsSleepBeforeRetry = 30;
		try {
			$this->downloadFilesFromEpic();
		} catch(\Exception $e){
			$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Error during download from Epic: " . $e->getMessage());
			$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Retry... attempt 1 after $secondsSleepBeforeRetry seconds");
			try {
				sleep($secondsSleepBeforeRetry);
				$this->downloadFilesFromEpic();
			} catch(\Exception $e){
				$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Error during download from Epic: " . $e->getMessage());
				$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Retry... attempt 2 after $secondsSleepBeforeRetry seconds");
				try {
					sleep($secondsSleepBeforeRetry);
					$this->downloadFilesFromEpic();
				} catch(\Exception $e){
					$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Error during download from Epic: " . $e->getMessage());
					$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Retry... attempt 3 after $secondsSleepBeforeRetry seconds");
					sleep($secondsSleepBeforeRetry);
					$this->downloadFilesFromEpic();
				}
			}
		}
	}
	
	
    public function downloadFilesFromEpic()
    {
    	
    	$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Download files from Epic");
         
        $sftp = new SFTP($this->epicUtils->getEpicServerName(),$this->epicUtils->getEpicServerPort());
        
        $Key = new RSA();

        //$Key->loadKey(file_get_contents($this->epicUtils->getEpicSecretKeyPath()));
        
        if (!$sftp->login($this->epicUtils->getEpicServerUser(), $this->epicUtils->getEpicSecretPassword())) {
        	throw new Exception('Login to Epic server failed');
        }
        
        $sftp->chdir($this->epicUtils->getEpicServerDir());
        
        $files = $sftp->nlist();
        
        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Files found " . json_encode($files));
        //dump($files);
        foreach ($files as $file) {

        	$file_downloaded_path = $this->epicUtils->getEpicDownloadDir() . $file;
        	if (file_exists($file_downloaded_path)) {
        		$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "File $file already downloaded at " . date('Y-m-d H:i:s', filemtime($file_downloaded_path)));
        	} else {
				$this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Download file $file in " . $this->epicUtils->getEpicDownloadDir());
				$sftp->get($file, $this->epicUtils->getEpicDownloadDir() . $file);
				$this->epicUtils->logAbstractAndAppendToProcessResult($this->processResult, $this->logger, "DOWNLOADED $file FROM EPIC");
        	}
        	
        }
        
    }



}