<?php
namespace AppBundle\Services;
use AppBundle\Bean\AreaClassificationBranch;
use AppBundle\Utils\Constants;

class AreaClassificationUrlManager
{

    private $router;

    private $em;

    public function __construct($router, $em)
    {
        $this->router = $router;
        $this->em = $em;
    }

    // @Deprecated: use ClassificationBranch->getSlug. This is correct only for first levels!!!
    public function getDynamicSlugFromBranch($branch)
    {
        if (! is_array($branch)) {
            return $this->getDynamicSlugFromBranch(array(
                $branch
            ));
        } else {
            $slug = "";
            foreach ($branch as $sprig) {
                $slug = $slug . ($slug ? "/" : "") . $sprig->getSlug();
            }
            return $slug;
        }
    }
    
    public function getClassificationBranchFromDynamicSlug($dynamicSlug,$current_locale) {
        $rootClassificationBranch = $this->calculateTree($current_locale);
        $classificationBranches = $this->getAllClassificationBranches($rootClassificationBranch);
        if (array_key_exists($dynamicSlug, $classificationBranches)) {
            return $classificationBranches[$dynamicSlug];
        }
        return null;        
    }

    public function getAllClassificationBranches($classificationBranch) {
        $classificationBranches = array();
        $this->addDoorClassificationBranches($classificationBranch, $classificationBranches);
        //$classificationBranches = array_unique($classificationBranches);
        return $classificationBranches;
    }
    
    private function addDoorClassificationBranches($classificationBranch, &$classificationBranches) {
        if ($classificationBranch->getSlug() && $classificationBranch->getHasDedicatedPage()) {
            $classificationBranches[$classificationBranch->getSlug()] = $classificationBranch;
        }
        foreach($classificationBranch->getDoors() as $door) {
            $this->addDoorClassificationBranches($door, $classificationBranches);
        }
    }
    
    public function calculateTree($current_locale) {
    
        $rootClassificationBranch = new AreaClassificationBranch();        
        
        $catsLev2 = $this->em
        ->getRepository('AppBundle:AreaClassificationRow')
        ->findDistinctAreaCategoriesOfLev2($current_locale);
        
        foreach ($catsLev2 as $catLev2) {
            
            $catLev2ClassificationBranch = new AreaClassificationBranch();
            $catLev2ClassificationBranch->addStep($catLev2);
            $catLev2ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev2.html.twig');
            
            $catsLev3 = $this->em
            ->getRepository('AppBundle:AreaClassificationRow')
            ->findDistinctAreaCategoriesOfLev3($current_locale,$catLev2);
    
            foreach ($catsLev3 as $catLev3) {
                $catLev3ClassificationBranch = new AreaClassificationBranch($catLev2ClassificationBranch->getWalk());
                $catLev3ClassificationBranch->addStep($catLev3);
                $catLev3ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev2.html.twig');
    
                // if empty lev3 emtpy all...
                if ($catLev3->getTitle() == Constants::EMPTY_CATEGORY_TITLE) {
                    $products = $this->em
                    ->getRepository('AppBundle:AreaClassificationRow')
                    ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev3, $catLev3, $catLev3);

                    $catLev2ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev5.html.twig');
                    $catLev2ClassificationBranch->setDoors(array());
                    
                    foreach ($products as $product) {
                         $productClassificationBranch = new AreaClassificationBranch($catLev2ClassificationBranch->getWalk());
                         $productClassificationBranch->addStep($product);
                         $productClassificationBranch->setHasDedicatedPage(false);
                         $productClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev5.html.twig');

                         $catLev2ClassificationBranch->addDoor($productClassificationBranch);
                    }

                } else {
                
                    $catsLev4 = $this->em
                    ->getRepository('AppBundle:AreaClassificationRow')
                    ->findDistinctAreaCategoriesOfLev4($current_locale,$catLev2, $catLev3);
        
                    foreach ($catsLev4 as $catLev4) {
                        
                        $catLev4ClassificationBranch = new AreaClassificationBranch($catLev3ClassificationBranch->getWalk());
                        $catLev4ClassificationBranch->addStep($catLev4);
                        $catLev4ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev3.html.twig');
                        
                        $catsLev5 = $this->em
                        ->getRepository('AppBundle:AreaClassificationRow')
                        ->findDistinctAreaCategoriesOfLev5($current_locale,$catLev2, $catLev3, $catLev4);
                        
                        foreach ($catsLev5 as $catLev5) {
                            
                            $catLev5ClassificationBranch = new AreaClassificationBranch($catLev4ClassificationBranch->getWalk());
                            $catLev5ClassificationBranch->addStep($catLev5);
                            $catLev5ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev3.html.twig');
                            
                            $catsLev6 = $this->em
                            ->getRepository('AppBundle:AreaClassificationRow')
                            ->findDistinctAreaCategoriesOfLev6($current_locale,$catLev2, $catLev3, $catLev4, $catLev5);
                            
                            foreach ($catsLev6 as $catLev6) {
                            
                                if ($catLev6->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                            
                                    $catLev6ClassificationBranch = new AreaClassificationBranch($catLev5ClassificationBranch->getWalk());
                                    $catLev6ClassificationBranch->addStep($catLev6);
                                    $catLev6ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev5.html.twig');
                            
                                    $products = $this->em
                                    ->getRepository('AppBundle:AreaClassificationRow')
                                    ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev4, $catLev5, $catLev6);
                            
                                    foreach ($products as $product) {
                                        $productClassificationBranch = new AreaClassificationBranch($catLev5ClassificationBranch->getWalk());
                                        $productClassificationBranch->addStep($product);
                                        $productClassificationBranch->setHasDedicatedPage(false);
                                        $catLev6ClassificationBranch->addDoor($productClassificationBranch);
                                    }
                            
                                    $catLev5ClassificationBranch->addDoor($catLev6ClassificationBranch);
                            
                            
                                } else {
                            
                                    $products = $this->em
                                    ->getRepository('AppBundle:AreaClassificationRow')
                                    ->findDistinctProducts($current_locale,$catLev2, $catLev3, $catLev4, $catLev5, $catLev6);
                            
                                    foreach ($products as $product) {
                            
                                        $productClassificationBranch = new AreaClassificationBranch($catLev5ClassificationBranch->getWalk());
                                        $productClassificationBranch->addStep($product);
                                        $productClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev5.html.twig');
                                        $productClassificationBranch->setIsSingleProduct(true);
                            
                                        $catLev5ClassificationBranch->addDoor($productClassificationBranch);

                                    }
                            
                                }
                                
                                
                            }
                            
                            if ($catLev4->getTitle() == Constants::EMPTY_CATEGORY_TITLE) {
                                $catLev3ClassificationBranch->addDoor($catLev5ClassificationBranch);
                                $catLev3ClassificationBranch->setProductTemplate('AppBundle:frontend/area:area_lev3.html.twig');
                            } else {
                                $catLev4ClassificationBranch->addDoor($catLev5ClassificationBranch);
                            }
        
                        }
        
                        if ($catLev4->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                            $catLev3ClassificationBranch->addDoor($catLev4ClassificationBranch);
                        }                     
                    }
                }
    
                if ($catLev3->getTitle() != Constants::EMPTY_CATEGORY_TITLE) {
                    $catLev2ClassificationBranch->addDoor($catLev3ClassificationBranch);                    
                }

            }
    
            $rootClassificationBranch->addDoor($catLev2ClassificationBranch);
        }
    
        return $rootClassificationBranch;
    }
    
    
}
