<?php

namespace AppBundle\Services;

class EpicProductClassificationSynchronizer
{

    private $processResult;

    public function __construct($em, $logger, $epicUtils, $epicProductCategorySynchronizer, $epicPCRSynchronizer)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
        $this->epicProductCategorySynchronizer = $epicProductCategorySynchronizer;
        $this->epicPCRSynchronizer = $epicPCRSynchronizer;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
        $this->epicProductCategorySynchronizer->setProcessResult($processResult);
        $this->epicPCRSynchronizer->setProcessResult($processResult);
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function elaborateProductCategoriesAndClassificationRows($epicArchiveToElaborate, $product_categories_datas, $product_classification_row_datas, $Nation)
    {
        $this->epicProductCategorySynchronizer->synchProductCategories($epicArchiveToElaborate, $product_categories_datas, $Nation);
        $this->epicPCRSynchronizer->synchProductClassificationRows($epicArchiveToElaborate, $product_classification_row_datas, $Nation);
        $this->updateEpicArchiveTable($epicArchiveToElaborate);
    }

    private function updateEpicArchiveTable($epicArchiveToElaborate)
    {
        if ($epicArchiveToElaborate->getArchiveName() == "current") {
            return;
        }
        if (!$this->epicUtils->getEpicCommit()) {
            return;
        }
        $this->logger->info("Update EpicArchive row for file " . $epicArchiveToElaborate->getArchiveName());
        $epicArchiveToElaborate->setProductClassificationElaborationAt(new \DateTime("now"));
        $this->em->persist($epicArchiveToElaborate);
        $this->em->flush();
        $this->epicUtils->logAbstractAndAppendToProcessResult($this->processResult, $this->logger, "SYNCHRONIZED PRODUCT CLASSIFICATION FOR " . $epicArchiveToElaborate->getArchiveName());
    }

}