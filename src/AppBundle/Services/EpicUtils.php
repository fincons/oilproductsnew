<?php

namespace AppBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Exception;

class EpicUtils 
{

	private $epicDownloadFromRemote = null; 
	private $epicElaborateProducts = null; 
	private $epicElaborateProductClassification = null; 
	private $epicElaborateAreaClassification = null; 
	private $epicElaborateCurrent = null;
	private $epicCommit = null; 
	private $epicSequentialFileCheck = null; 
	private $epicOlderFileCheck = null; 
	private $epicProcessNotificationToEmail = null;
	private $epicMaxNumOfArchivesToElaborate = null;
	private $hostStatic = null;
	private $webPath = null;
	
	
    public function __construct($em, $logger, ContainerInterface $container)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->container = $container;
        $this->epicDownloadFromRemote = $this->container->getParameter('epic_download_from_remote');
        $this->epicElaborateProducts = $this->container->getParameter('epic_elaborate_products');
        $this->epicElaborateProductClassification = $this->container->getParameter('epic_elaborate_product_classification');
        $this->epicElaborateAreaClassification = $this->container->getParameter('epic_elaborate_area_classification');
        $this->epicElaborateCurrent = false;
        $this->epicMaxNumOfArchivesToElaborate = -1;
        $this->epicCommit = $this->container->getParameter('epic_commit');
        $this->epicSequentialFileCheck = $this->container->getParameter('epic_sequential_file_check');
        $this->epicOlderFileCheck = $this->container->getParameter('epic_older_file_check');
        $this->epicProcessNotificationToEmail = $this->container->getParameter('epic_process_notification_to_email');
        $this->hostStatic = $this->container->getParameter('fe_base_url');
        $this->webPath = $this->container->getParameter('assetic.write_to');
    }

    public function getEpicElaborationDir() {
    	$epic_elaboration_dir_prefix = $this->container->getParameter('assetic.write_to') . "/uploads/media/";
    	if ($this->container->getParameter('epic_elaboration_dir_prefix')) {
    		$epic_elaboration_dir_prefix = $this->container->getParameter('epic_elaboration_dir_prefix');
    	}
    	return  $epic_elaboration_dir_prefix . "epic-elab/";
    }
    
    public function getEpicDownloadDir() {
    	return $this->getEpicElaborationDir() . "download/";
    }
    
    public function getEpicCurrentDir() {
    	return $this->getEpicElaborationDir() . "current/";
    }
    
    public function getEpicPdfDir() {
    	return $this->container->getParameter('assetic.write_to') . "/uploads/media/epic/";
    }
    
    public function getEpicElaborationDirOfArchive($epicArchiveToElaborate) {
    	return $this->getEpicElaborationDir() . $epicArchiveToElaborate->getArchiveName() . "/";
    }

    public function getEpicReportPathOfArchive($epicArchive, $suffix) {
    	return $this->getEpicElaborationDirOfArchive($epicArchive) . $this->extractNumberFromArchiveName($epicArchive->getArchiveName()) . "_" . $suffix;
    }
    
    public function getEpicServerName() {
    	return $this->container->getParameter('epic_server_name');
    }
    
    public function getEpicServerPort() {
    	return $this->container->getParameter('epic_server_port');
    }
    
    public function getEpicSecretKeyPath() {
    	return $this->container->getParameter('epic_secret_key_path');
    }

    public function getEpicSecretPassword() {
    	return $this->container->getParameter('epic_secret_password');
    }
    
    public function getEpicServerUser() {
    	return $this->container->getParameter('epic_server_user');
    }
    
    public function getEpicServerDir() {
    	return $this->container->getParameter('epic_server_dir');
    }
    
    public function extractNumberFromArchiveName($archiveName) {
    	$archiveName = str_replace("products_", "", $archiveName);
    	$archiveName = str_replace(".zip", "", $archiveName);
    	return $archiveName;
    }
    
    public function logInfoAndAppendToProcessResult($processResult, $logger, $message) {
		$logger->info($message);
    	$processResult->appendMessage($message);
    }
    
    public function logWarningAndAppendToProcessResult($processResult, $logger, $message) {
    	$logger->warning($message);
    	$processResult->appendMessage("WARN! " . $message);
    	$processResult->appendWarning($message);
    }
    
    public function logErrorAndAppendToProcessResult($processResult, $logger, $exception) {
    	$processResult->appendException($exception);
    	$logger->error($processResult->printExceptions());
    }
    
    public function logAbstractAndAppendToProcessResult($processResult, $logger, $message) {
		$logger->info($message);
    	$processResult->appendAbstract($message);
    }
    
    public function notifyNotEpicBecome($epicArchiveToElaborate, $codes) {
    	$emails_to_notify = $this->container->getParameter('epic_process_notification_to_email');
    	$body = $this->container->get('templating')->render('AppBundle:email:become_not_epic.txt.twig', array('products' => $codes));
    	$subject = "Alert! There are products removed from Epic in " . $epicArchiveToElaborate->getArchiveName();
    	$this->container->get('notifier')->notify($emails_to_notify, $subject, $body, array(),'text/html');
    }
    
    public function getEpicDownloadFromRemote() {
    	return $this->epicDownloadFromRemote;
    }
    
    public function updateEpicDownloadFromRemote($parameterInput) {
    	if ($parameterInput != null) {
    		$this->epicDownloadFromRemote = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicElaborateProducts() {
    	return $this->epicElaborateProducts;
    }
    
    public function updateEpicElaborateProducts($parameterInput) {
        if ($parameterInput != null) {
    		$this->epicEpicElaborateProducts = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicElaborateProductClassification() {
    	return $this->epicElaborateProductClassification;
    }
    
    public function updateEpicElaborateProductClassification($parameterInput) {
        if ($parameterInput != null) {
    		$this->epicElaborateProductClassification = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicElaborateAreaClassification() {
    	return $this->epicElaborateAreaClassification;
    }
    
    public function updateEpicElaborateAreaClassification($parameterInput) {
        if ($parameterInput != null) {
    		$this->epicElaborateAreaClassification = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicElaborateCurrent() {
    	return $this->epicElaborateCurrent;
    }
    
    public function updateEpicElaborateCurrent($parameterInput) {
    	if ($parameterInput != null) {
    		$this->epicElaborateCurrent = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicCommit() {
    	return $this->epicCommit;
    }
    
    public function updateEpicCommit($parameterInput) {
       	if ($parameterInput != null) {
    		$this->epicCommit = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicSequentialFileCheck() {
    	return $this->epicSequentialFileCheck;
    }
    
    public function updateEpicSequentialFileCheck($parameterInput) {
       	if ($parameterInput != null) {
    		$this->epicSequentialFileCheck = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicOlderFileCheck() {
    	return $this->epicOlderFileCheck;
    }
    
    public function updateEpicOlderFileCheck($parameterInput) {
    	if ($parameterInput != null) {
    		$this->epicOlderFileCheck = ($parameterInput === 'true');
    	}
    }
    
    public function getEpicProcessNotificationToEmail() {
    	return $this->epicProcessNotificationToEmail;
    }
    
    public function updateEpicProcessNotificationToEmail($parameterInput) {
    	if ($parameterInput != null) {
    		$this->epicProcessNotificationToEmail = $parameterInput;
    	}
    }
    
    public function getEpicMaxNumOfArchivesToElaborate() {
    	return $this->epicMaxNumOfArchivesToElaborate;
    }
    
    public function updateEpicMaxNumOfArchivesToElaborate($parameterInput) {
    	if ($parameterInput != null) {
    		$this->epicMaxNumOfArchivesToElaborate = $parameterInput;
    	}
    }
    
    
    public function printEpicParameters() {
    	$string = "";
    	$string.= "download from remote = " . $this->epicDownloadFromRemote . ", ";
    	$string.= "elaborate products = " . $this->epicElaborateProducts . ", ";
    	$string.= "elaborate product classification = " . $this->epicElaborateProductClassification . ", ";
    	$string.= "elaborate area classification = " . $this->epicElaborateAreaClassification . ", ";
    	if ($this->epicElaborateCurrent) {
    		$string.= "elaborate current = " . $this->epicElaborateCurrent . ", ";    		
    	}
    	$string.= "commit = " . $this->epicCommit . ", ";
    	$string.= "sequential file check = " . $this->epicSequentialFileCheck . ", ";
    	$string.= "older file check = " . $this->epicOlderFileCheck . ", ";
    	$string.= "notification to emails = " . json_encode($this->epicProcessNotificationToEmail);
    	return $string;
    }
    
    public function checkEpicDirs() {
    	if (!file_exists($this->getEpicElaborationDir())) {
    		mkdir($this->getEpicElaborationDir());
    	}
    	if (!file_exists($this->getEpicDownloadDir())) {
    		mkdir($this->getEpicDownloadDir());
    	}
    	if (!file_exists($this->getEpicCurrentDir())) {
    		mkdir($this->getEpicCurrentDir());
    	}
    	if (!file_exists($this->getEpicPdfDir())) {
    		mkdir($this->getEpicPdfDir());
    	}
    }
    
    public function getEpicSynchCategoriesAutoDuplicates() {
    	return $this->container->getParameter('epic_synch_categories_auto_duplicates');
    }

    public function getZipError($code)
    {
        switch ($code)
        {
            case 0:
                return 'No error';

            case 1:
                return 'Multi-disk zip archives not supported';

            case 2:
                return 'Renaming temporary file failed';

            case 3:
                return 'Closing zip archive failed';

            case 4:
                return 'Seek error';

            case 5:
                return 'Read error';

            case 6:
                return 'Write error';

            case 7:
                return 'CRC error';

            case 8:
                return 'Containing zip archive was closed';

            case 9:
                return 'No such file';

            case 10:
                return 'File already exists';

            case 11:
                return 'Can\'t open file';

            case 12:
                return 'Failure to create temporary file';

            case 13:
                return 'Zlib error';

            case 14:
                return 'Malloc failure';

            case 15:
                return 'Entry has been changed';

            case 16:
                return 'Compression method not supported';

            case 17:
                return 'Premature EOF';

            case 18:
                return 'Invalid argument';

            case 19:
                return 'Not a zip archive';

            case 20:
                return 'Internal error';

            case 21:
                return 'Zip archive inconsistent';

            case 22:
                return 'Can\'t remove file';

            case 23:
                return 'Entry has been deleted';

            default:
                return 'An unknown error has occurred('.intval($code).')';
        }
    }
    
    public function getHostStatic()
    {
        return $this->hostStatic;
    }
    
    public function getWebPath()
    {
        return $this->webPath;
    }

    public function setHostStatic($hostStatic)
    {
        $this->hostStatic = $hostStatic;
    }

    public function setWebPath($webPath)
    {
        $this->webPath = $webPath;
    }

}