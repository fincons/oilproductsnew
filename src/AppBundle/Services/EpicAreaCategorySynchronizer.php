<?php

namespace AppBundle\Services;

use AppBundle\Entity\AreaCategory;
use AppBundle\Entity\AreaCategoryTranslation;
use AppBundle\Entity\EpicAreaCategory;
use AppBundle\Utils\TextUtils;
use Ddeboer\DataImport\Reader\CsvReader;

class EpicAreaCategorySynchronizer extends EpicAbstractSynchronizer
{

    private $processResult;

    private $notEpicAreaCategories = [];

    public function __construct($em, $logger, $epicUtils)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->epicUtils = $epicUtils;
    }

    public function setProcessResult($processResult)
    {
        $this->processResult = $processResult;
    }

    public function getProcessResult()
    {
        return $this->processResult;
    }

    public function synchAreaCategories($epicArchiveToElaborate, $area_category_datas, $Nation)
    {

        $this->epicUtils->logInfoAndAppendToProcessResult($this->processResult, $this->logger, "Elaborate area categories for archive " . $epicArchiveToElaborate->getArchiveName());

        $area_category_datas_enriched = array();

        // disable all db categories
        $categoriesFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findAll();
        foreach ($categoriesFromDb as $categoryFromDb) {
            if ($categoryFromDb->getFromEpic()) {
                $this->notEpicAreaCategories[$categoryFromDb->getId()] = self::NOT_EPIC_BECOME;
            } else {
                $this->notEpicAreaCategories[$categoryFromDb->getId()] = self::NOT_EPIC_ALREADY;
            }
            $categoryFromDb->setFromEpic(false);
        }

        foreach ($area_category_datas as $area_category_data) {
            $this->logger->info("Area category: " . json_encode($area_category_data));

            $areaCategoryFromDb = null;
            $area_category_data['linked_to_db_id'] = "";
            $area_category_data['linked_to_db_title'] = "";
            $area_category_data['duplicated_of_db_id'] = "";
            $area_category_data['duplicated_of_db_title'] = "";

            //Aggiungo nuova query che cerca nella tabella di associazioni
            //$areaCategoriesFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findByEpicKey($area_category_data['key']);
            $areaCategoriesFromDb = null;

            //Se trovo la chiave nella tabella di associazione -> prendo l'id della categoria e proseguo
            //Se non trovo la chiave nella tabella di associaz -> salvo la nuova categoria e salvo l'id nelle associazioni
            //TODO forse conviene inserire qui una ricerca tra le categorie per titolo senza epickey
            $add_assoc = false;
            $epicAreaCategoriesFromDb = $this->em->getRepository('AppBundle:EpicAreaCategory')->findByEpicKey($area_category_data['key']);
            if (count($epicAreaCategoriesFromDb) > 0) {
                $this->logger->info("Found Epic Area category on db by epic key with this id => " . $epicAreaCategoriesFromDb[0]->getAreaCategoryId());

                $areaCategoriesFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findById($epicAreaCategoriesFromDb[0]->getAreaCategoryId());
            }

            if (count($areaCategoriesFromDb) == 1) {

                $this->logger->info("Found category on db by epic key");
                $area_category_data['type'] = self::EPIC_UPDATED_BY_KEY;
                $areaCategoryFromDb = $areaCategoriesFromDb[0];
                $area_category_data['linked_to_db_id'] = $areaCategoriesFromDb[0]->getId();
                $area_category_data['linked_to_db_title'] = $areaCategoriesFromDb[0]->getTitle();

            } else if (count($areaCategoriesFromDb) == 0 || $areaCategoriesFromDb == null) {
                //TODO aggiungo anche quando è null così creo la nuova categoria

                $this->logger->info("NOT Found Area category on db by epic key");

                if ($this->epicUtils->getEpicSynchCategoriesAutoDuplicates()) {

                    $areaCategoriesFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findNotDuplicationByTitle($area_category_data['ita']);

                    if (count($areaCategoriesFromDb) == 1) {
                        $this->logger->info("Found category on db by title");
                        if ($areaCategoriesFromDb[0]->getEpicKey() == null) {
                            $area_category_data['type'] = self::EPIC_UPDATED_BY_TITLE;
                            $areaCategoryFromDb = $areaCategoriesFromDb[0];
                            $area_category_data['linked_to_db_id'] = $areaCategoriesFromDb[0]->getId();
                            $area_category_data['linked_to_db_title'] = $areaCategoriesFromDb[0]->getTitle();
                        } else {
                            $this->logger->info("Found category on db with same title but epicKey different -> duplication....");
                            $area_category_data['type'] = self::DUPLICATED;
                            $areaCategoryFromDb = new AreaCategory();
                            // TODO: add a property to disable that setting...
                            $areaCategoryFromDb->setDuplicatedOf($areaCategoriesFromDb[0]);
                            $area_category_data['duplicated_of_db_id'] = $areaCategoriesFromDb[0]->getId();
                            $area_category_data['duplicated_of_db_title'] = $areaCategoriesFromDb[0]->getTitle();
                        }
                    } else if (count($areaCategoriesFromDb) == 0) {
                        $this->logger->info("Missing  category on db");
                        $area_category_data['type'] = self::EPIC_CREATED;
                        $areaCategoryFromDb = new AreaCategory();
                    } else {
                        $this->logger->info("Found more than one category with that title on db!");
                        $area_category_data['type'] = self::ERROR;
                        $areaCategoryFromDb = new AreaCategory();
                        // TODO: create but not set duplicated of anything (it will be done manually)
                    }

                } else {

                    $this->logger->info("Missing  category on db");
                    $area_category_data['type'] = self::EPIC_CREATED;

                    // Se sono qua significa che la categoria non è tra quelle associate
                    // Quindi creo la nuova categoria e l'associazione
                    //$areaCategoryFromDb = new AreaCategory();

                    $add_assoc = true;

                    //Controllo se esiste già una categoria con Titolo identico ma senza EpicKey assegnata
                    $areaCategoriesFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findBy(
                        array('title' => $area_category_data['ita'], 'epicKey' => null),
                        array()
                    );

                    $this->logger->info("Get Area category from db");
                    if (count($areaCategoriesFromDb) > 0) {
                        $this->logger->info("FOUND! Found Area category on db by title # " . sizeof($areaCategoriesFromDb));
                        $areaCategoryFromDb = $areaCategoriesFromDb[0];
                    } else {
                        $areaCategoryFromDb = new AreaCategory();
                    }
                }


            } else {
                $this->logger->info("Found more than one category with that epicKey on db!");
                $area_category_data['type'] = self::ERROR;
            }

            // it data
            $row_it = array();
            $row_it['epicKey'] = $area_category_data['key'];
            $row_it['title'] = str_replace("'","’",$area_category_data['ita']);
            $row_it['locale'] = 'it_IT';

            // en data
            $row_en = array();
            $row_en['epicKey'] = $area_category_data['key'];
            $row_en['title'] = str_replace("'","’",$area_category_data['enu']);
            $row_en['locale'] = 'en_GB';
            
            if($Nation == "Germany"){
                
                // de data
                $row_de = array();
                $row_de['epicKey'] = $area_category_data['key'];
                $row_de['title'] = str_replace("'","’",$area_category_data['deu']);
                $row_de['locale'] = 'de_DE';
                
                // en_de data
                $row_en_de = array();
                $row_en_de = $row_en;
                $row_en_de['locale'] = 'en_DE';
            }
            
            if($Nation == "Benelux"){
                
                // en_bx data
                $row_en_bx = array();
                $row_en_bx = $row_en;
                $row_en_bx['locale'] = 'en_BX';
                
                // fr_bx data
                $row_fr_bx = array();
                $row_fr_bx['epicKey'] = $area_category_data['key'];
                $row_fr_bx['title'] = str_replace("'","’",$area_category_data['fra']);
                $row_fr_bx['locale'] = 'fr_BX';
                
                // nl_bx data
                $row_nl_bx = array();
                $row_nl_bx['epicKey'] = $area_category_data['key'];
                $row_nl_bx['title'] = str_replace("'","’",$area_category_data['nld']);
                $row_nl_bx['locale'] = 'nl_BX';
            }
            
            if($Nation == "Apac"){
                
                // en_apac data
                $row_en_apac = array();
                $row_en_apac = $row_en;
                $row_en_apac['locale'] = 'en_APAC';
            }
            
            if($Nation == "Iberia"){
                
                // es_es data
                $row_es_es = array();
                $row_es_es['epicKey'] = $area_category_data['key'];
                $row_es_es['title'] = str_replace("'","’",$area_category_data['esn']);
                $row_es_es['locale'] = 'es_ES';
                
                // pt_pt data
                $row_pt_pt = array();
                $row_pt_pt['epicKey'] = $area_category_data['key'];
                $row_pt_pt['title'] = str_replace("'","’",$area_category_data['ptb']);
                $row_pt_pt['locale'] = 'pt_PT';
            }
            

            if ($areaCategoryFromDb) {
                if ($this->epicUtils->getEpicCommit()) {
                    
                    //if($Nation == "Italy"){
                        $updatedDataIt = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_it);
                        $updatedDataEn = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_en);
                    //}
                    
                    if($Nation == "Germany"){
                        $updatedDataDe = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_de);
                        $updatedDataEnDe = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_en_de);
                    }
                    
                    if($Nation == "Benelux"){
                        $updatedDataEnBX = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_en_bx);
                        $updatedDataFRBX = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_fr_bx);
                        $updatedDataNLBX = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_nl_bx);
                    }
                    
                    if($Nation == "Apac"){
                        $updatedDataEnApac = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_en_apac);
                    }
                    
                    if($Nation == "Iberia"){
                        $updatedDataEsES = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_es_es);
                        $updatedDataPtPT = $this->updateAreaCategoryFieldsWithEpicData($areaCategoryFromDb, $row_pt_pt);
                    }

                    //crea l'associazione
                    if ($add_assoc) {
                        $this->logger->info("Add assoc category on db");
                        $epicAreaCategoriy = new EpicAreaCategory();
                        $updatedDataEpicAreaCategory = $this->insertEpicAreaCategoryFields($epicAreaCategoriy, $area_category_data['key'], $areaCategoryFromDb->getId());
                    }
                }
                
                if ($Nation == "Italy" && !$updatedDataIt && !$updatedDataEn) {
                    $area_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Germany" && !$updatedDataDe && !$updatedDataEnDe) {
                    $area_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Benelux" && !$updatedDataEnBX && !$updatedDataFRBX && !$updatedDataNLBX) {
                    $area_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Apac" && !$updatedDataEnApac) {
                    $area_category_data['type'] = self::EPIC_UNTOUCHED;
                }
                
                if ($Nation == "Iberia" && !$updatedDataEsES && !$updatedDataPtPT) {
                    $area_category_data['type'] = self::EPIC_UNTOUCHED;
                }

                $area_category_data['id'] = $areaCategoryFromDb->getId();

            }

            $area_category_datas_enriched[] = $area_category_data;

            if (in_array($areaCategoryFromDb->getId(), array_keys($this->notEpicAreaCategories))) {
                $this->notEpicAreaCategories = array_diff_key($this->notEpicAreaCategories, array($areaCategoryFromDb->getId() => 0));
            }

        }

        foreach (array_keys($this->notEpicAreaCategories) as $id) {
            $pc = array();
            $pc['id'] = $id;
            $pc['type'] = $this->notEpicAreaCategories[$id];
            $area_category_datas_enriched[] = $pc;
        }

        $categoriesFile = $this->epicUtils->getEpicReportPathOfArchive($epicArchiveToElaborate, "area_categories.csv");
        $categoriesKeys = ['id', 'key', 'ptb', 'esn', 'nld', 'fra', 'deu', 'enu', 'ita', 'linked_to_db_id', 'linked_to_db_title', 'duplicated_of_db_id', 'duplicated_of_db_title', 'type'];
        $this->exportToCsv($categoriesFile, $area_category_datas_enriched, $categoriesKeys);

    }

    private function removeUntouched($area_category_datas)
    {
        $area_category_datas_to_return = array();
        foreach ($area_category_datas as $area_category_data) {
            if ($area_category_data['type'] != self::EPIC_UNTOUCHED) {
                $area_category_datas_to_return[] = $area_category_data;
            }
        }
        return $area_category_datas_to_return;
    }

    private function insertEpicAreaCategoryFields($epicAreaCategoriy, $Epickey, $categoryId)
    {

        $epicAreaCategoriy->setAreaCategoryId($categoryId);
        $epicAreaCategoriy->setEpicKey($Epickey);
        $epicAreaCategoriy->setPublished(1);
        $this->em->persist($epicAreaCategoriy);
        if ($this->epicUtils->getEpicCommit()) {
            $this->em->flush();
        }
        return true;
    }

    private function updateAreaCategoryFieldsWithEpicData($areaCategory, $row)
    {
        $this->logger->info("Update values for area category");
        $new_title = mb_strimwidth($row['title'], 0, self::LENGTH_255, self::TRIMMARKER);
        $new_epicKey = mb_strimwidth($row['epicKey'], 0, self::LENGTH_255, self::TRIMMARKER);
        $old_title = $areaCategory->getTitle();
        $old_epicKey = $areaCategory->getEpicKey();
        $old_slug = $areaCategory->getSlug();
        $new_slug = TextUtils::slugify($row['title']);
        $areaCategoriesTranslationFromDb = null;

        //if ($row['locale'] == 'en_GB') {
        if ($row['locale'] !== 'it_IT') {
            // TODO: get en field from db
            $this->logger->info("Not italian so i need to find the correct title");

            $areaCategoriesTranslationFromDb = $this->em->getRepository('AppBundle:AreaCategoryTranslation')->findOneBy(
                array('locale' => $row['locale'], 'field' => 'title', 'object' => $areaCategory->getId()),
                array()
            );
            if (isset($areaCategoriesTranslationFromDb)) {
                $old_title = $areaCategoriesTranslationFromDb->getContent();
            } else {
                $old_title = "";
            }
            $this->logger->info("title in " . $row['locale'] . ' -> ' . $old_title);
        }

        if (strcmp($new_title, $old_title) == 0 && strcmp($new_slug, $old_slug) == 0 && strcmp($new_epicKey, $old_epicKey) == 0) {

            $this->logger->info("Update data in Italian " . $row['locale'] . " - NO change ");
            $this->logger->info("new title " . $new_title . " - old title " . $old_title);
            $areaCategory->setFromEpic(true);
            $areaCategory->setLocale($row['locale']);
            $this->em->persist($areaCategory);
            if ($this->epicUtils->getEpicCommit()) {
                $this->em->flush();
            }
            return false;

        } else {

            if ($row['locale'] == 'it_IT') {
                $this->logger->info("Update data in Italian");
                $areaCategory->setTitle($new_title);
                $areaCategory->setSlug(TextUtils::slugify($row['title']));
                $areaCategory->setEpicKey($new_epicKey);
                $areaCategory->setFromEpic(true);
                $areaCategory->setLocale($row['locale']);
                $this->em->persist($areaCategory);
            } else {
                if (count($areaCategoriesTranslationFromDb) > 0) {
                    $this->logger->info("Update translations");

                    $areaCategoriesTranslationFromDb_update = $this->setAreaTranslation($areaCategory, 'title', $new_title, $row['locale']);
                    $this->logger->info('title updated :: ' . $areaCategoriesTranslationFromDb_update->getContent());

                    $areaCategoriesTranslationFromDb_update = $this->setAreaTranslation($areaCategory, 'slug', TextUtils::slugify($row['title']), $row['locale']);
                    $this->logger->info('slug updated :: ' . $areaCategoriesTranslationFromDb_update->getContent());

                } else {
                    $this->logger->info("Insert translations");
                    $areaCategoriesTranslationFromDb_update = $this->setAreaTranslation($areaCategory, 'title', $new_title, $row['locale']);
                    $this->logger->info('title updated :: ' . $areaCategoriesTranslationFromDb_update->getContent());

                    $areaCategoriesTranslationFromDb_update = $this->setAreaTranslation($areaCategory, 'slug', TextUtils::slugify($row['title']), $row['locale']);
                    $this->logger->info('slug updated :: ' . $areaCategoriesTranslationFromDb_update->getContent());

                }
            }

            if ($this->epicUtils->getEpicCommit()) {
                $this->em->flush();
            }

            return true;

        }
    }


    private function setAreaTranslation($areaCategory, $field, $value, $locale)
    {
        //Inserisco o aggiorno il campo passato
        $areaCategoriesTranslationFromDb = $this->em->getRepository('AppBundle:AreaCategoryTranslation')->findOneBy(
            array('locale' => $locale, 'field' => $field, 'object' => $areaCategory->getId()),
            array()
        );
        if (!count($areaCategoriesTranslationFromDb) > 0) {
            $areaCategoriesTranslationFromDb = new AreaCategoryTranslation();
        }
        $areaCategoriesTranslationFromDb->setObject($areaCategory);
        $areaCategoriesTranslationFromDb->setField($field);
        $areaCategoriesTranslationFromDb->setLocale($locale);
        $areaCategoriesTranslationFromDb->setContent($value);
        $this->em->persist($areaCategoriesTranslationFromDb);

        return $areaCategoriesTranslationFromDb;
    }

    public function linkExistingCategoriesWithEpic()
    {
        $this->logger->info("Reading file ");

        $file = new \SplFileObject($this->epicUtils->getEpicElaborationDir() . 'area_categories_link.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $this->logger->info("Row " . json_encode($row));

            $areaCategoryFromDb = null;

            if ($row['linked_to_db_id']) {
                $areaCategoryFromDb = $this->em->getRepository('AppBundle:AreaCategory')->find($row['linked_to_db_id']);
                $this->logger->info("FOUND " . $areaCategoryFromDb->getTitle());

                if ($areaCategoryFromDb->getEpicKey() == null) {
                    $areaCategoryFromDb->setEpicKey($row['key']);
                    $this->em->persist($areaCategoryFromDb);
                    if ($this->epicUtils->getEpicCommit()) {
                        $this->em->flush();
                    }
                } else if ($areaCategoryFromDb->getEpicKey() != $row['key']) {
                    throw new Exception("cannot link more than one epicKey to a category");
                }

            } else {
                $areaCategoryFromDb = new AreaCategory();

                $areaCategoryDuplicatedOf = null;

                if ($row['duplicated_of_db_title']) {
                    $areaCategoriesDuplicatedOfFromDb = $this->em->getRepository('AppBundle:AreaCategory')->findNotDuplicationByTitle($row['duplicated_of_db_title']);
                    $areaCategoryDuplicatedOf = $areaCategoriesDuplicatedOfFromDb[0];
                }

                $areaCategoryFromDb->setTitle($row['ita']);
                $areaCategoryFromDb->setSlug(TextUtils::slugify($row['ita']));
                $areaCategoryFromDb->setEpicKey($row['key']);
                if ($areaCategoryDuplicatedOf != null) {
                    $areaCategoryFromDb->setDuplicatedOf($areaCategoryDuplicatedOf);
                }
                $areaCategoryFromDb->setLocale('it_IT');
                $this->em->persist($areaCategoryFromDb);
                if ($this->epicUtils->getEpicCommit()) {
                    $this->em->flush();
                }

            }

        }

    }


}