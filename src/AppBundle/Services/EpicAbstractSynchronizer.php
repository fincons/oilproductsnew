<?php

namespace AppBundle\Services;

abstract class EpicAbstractSynchronizer {

	const ERROR = "ERROR";
	const EPIC_UPDATED = "EPIC_UPDATED";
	const EPIC_UPDATED_BY_KEY = "EPIC_UPDATED_BY_KEY";
	const EPIC_UPDATED_BY_TITLE = "EPIC_UPDATED_BY_TITLE";
	const EPIC_CREATED = "EPIC_CREATED";
	const EPIC_UNTOUCHED = "EPIC_UNTOUCHED";
	const DUPLICATED = "DUPLICATED";
	const NOT_EPIC_ALREADY = "NOT_EPIC_ALREADY";
	const NOT_EPIC_BECOME = "NOT_EPIC_BECOME";
	
	const TRIMMARKER = '...';
	
	const LENGTH_65535 = 65.535;
	const LENGTH_255 = 255;
	const LENGTH_1000 = 1000;
	const LENGTH_2 = 2;
	
	protected function exportToCsv($file, $rows, $keys) {
		$fp = fopen($file, 'w');
		fputcsv($fp, $keys);
	
		foreach ($rows as $row) {
			$ordered_fields = array();
			foreach ($keys as $key) {
				if (array_key_exists($key, $row)) {
					$ordered_fields[$key] = $row[$key];
				}
			}
	
			fputcsv($fp, $ordered_fields);
		}
	
		fclose($fp);
	}

}