Eni Oilproducts CMS
========================
 
## Info

Simfony2 CMS built using Simfony 2.8 and a lot of useful Sonata bundles. 

## Install

Get composer and then run

    composer install 

## Build the assets

Assets aren't handled dynamically (use_controller: false), so run (also in dev env):

	php app/console assetic:dump -env={YOUR_ENV}

## Loading sample data

Use the bin/build.sh script (see later).


## Run 

Configuring a virtual host (http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html), for example oilproducts.digitalmill.com and go to:

dev url: 
http://oilproducts.dev.digitalmill.com/app_dev.php (fe)
http://oilproducts.dev.digitalmill.com/app_dev.php/admin (be)

official url:
http://oilproducts.dev.digitalmill.com/
http://oilproducts.dev.digitalmill.com/admin

The env in official url derives from this instruction: 

	$env = getenv('APP_ENV') ? getenv('APP_ENV')

To define APP_ENV add this in your apache envvars file

	export APP_ENV=stage

## Commands

Run:

	## called from admin
	php app/console eop:synchWithEpic
	php app/console eop:eop:notifyProducts
	php app/console eop:cleanCachesAndWarmingUp
	
	## useful ones
	php app/console eop:banMemcache
	php app/console eop:callSiteUrls
	php app/console eop:printAllSiteUrls
	php app/console eop:linkExistingProductsWithEpic
	php app/console eop:linkExistingCategoriesWithEpic	


## During development

### Useful script
There is a useful sh script that automates some useful tasks: bin/build.sh

### To reinstall the env with all sample data
bin/build.sh -reinstall (uncomment exit in load_data)

### To release 
bin/build.sh -release 

### To easy integrate style from eni-oilproducts-static
Run bin/build.sh -update-from-static

### To easy translate 

To import AppBundle translation messages in backend admin run:

	./app/console lexik:translations:import AppBundle

To export your changed translations from backend admin to translations dir:

	./app/console lexik:translations:export

### Tests
For tests run

	phpunit -c app
